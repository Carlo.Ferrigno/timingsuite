/*
  KUIPER LIBRARY
  ==============

  Stephane Paltani
 University of Geneva
  Stephane.Paltani@unige.ch

  This library implements calculation of Kuiper statistics (Kuiper 1960)
  to test the periodicity of events measured disjoint time intervals. It
  also calculates the significance of the Kuiper statistics using the
  exact formulae of Stephens (1965) if possible, and Kuiper's original
  asymptotic formula in other cases. 

  The equation numbers below refer to  "Paltani S., 2004, A&A 420, 789". Please
  cite this paper if you make any use of this library or of the methods
  described in this paper in your research.

*/



#include <cstring>
#include <cmath>
#include <iostream>
#include "kuiper.h"

#include <gsl/gsl_sf.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
//#include <gsl/gsl_sort_double.h>

using namespace std;

double get_phase_complete(double time, double t_ref, double f, double df, double df2, double *i_phase)
{
    double dt = time - t_ref;
    //double tmp2=dt*dt;
    //double phase = dt*(f + dt*( (df/2.)  + dt*((df2/6.) + dt*(df3/24.) ) ) ) + 1 - d_ph;
    double phase = dt*(f + dt*( (df/2.) + dt* (df2/6.)  ) );
    *i_phase=floor(phase);
    return phase- (*i_phase);
}


double get_phase_frac(double time, double t_ref, double f, double df, double df2)
{
    double dt = time - t_ref;
    //double tmp2=dt*dt;
    //double phase = dt*(f + dt*( (df/2.)  + dt*((df2/6.) + dt*(df3/24.) ) ) ) + 1 - d_ph;
    double phase = dt * (f + dt*( (df/2.) + dt* (df2/6.)  ) );
    return phase-floor(phase);
}


int Kuiper::doublecompare(double *i, double *j)
{
  if (*i > *j)
    return (1);
  if (*i < *j)
    return (-1);
  return (0);
}

int Kuiper::rand_index(int max_N)
{
	static const double RandFact=(double)RAND_MAX+1;
	
	
	/*--------------------------------------
	 * generates an integer random number uniformly
	 * distributed in range [0 max_N[
	 *--------------------------------------*/		
	return int((rand()+(double)rand()/RandFact)/RandFact*max_N);
}

int Kuiper::rand_unit(void)
{
	static const double RandFact=(double)RAND_MAX+1;
	
	
	/*--------------------------------------
	 * generates an integer random number uniformly
	 * distributed in range [0 max_N[
	 *--------------------------------------*/		
	return ((double)rand()+(double)rand()/RandFact)/RandFact;
}


double Kuiper::Qkp_wrap(double x, void *params)
{
    int N = (int)((double *)params)[0];
    double p0 = ((double *)params)[1];
    return Kuiper::Qkp(x,N)-p0;
    
}

double Kuiper::inv_QkP(double p,int N)
{
    /*
    double V_run=5e2/N;
    double V_step=V_run/1e3;
    double p_test=1.;
    FILE *fp = fopen("test.qdp", "w");
    fprintf(fp, "read\ncpd /XW\nlab X V (%d)\nlab y P\n!Check for p =%19.12e\n", N, p);

    while (p_test>p)
    {
        p_test=Qkp(V_run, N);
        fprintf(fp,"%g %g\n", V_run, p_test);
        V_run+=V_step;
    }
    fclose(fp);
    return p_test;
    */
    //printf("Trying to find the V for probability %g with N=%d\n", p, N);
    	const gsl_root_fsolver_type *T = gsl_root_fsolver_bisection;
	gsl_root_fsolver * s = gsl_root_fsolver_alloc(T);
        gsl_function FF;
	FF.function = &(Kuiper::Qkp_wrap);
        double params[] = {(double)N, p};
        FF.params = (void *)params;
        double V=1;
        //printf("%g %g\n", Qkp(2e-3, N), Qkp(6e-3,N));
        //gsl_root_fsolver_set(s, &FF, 5e2/N,5e4/N);
        gsl_root_fsolver_set(s, &FF, 0,1);
        int iter=0, status=GSL_CONTINUE;
	int max_iter=10000;
	do{
		iter++;
        	status = gsl_root_fsolver_iterate(s);
		//V0=V;
		//V=gsl_root_fsolver_root(s);
                double x_lo=gsl_root_fsolver_x_lower(s);
                double x_hi=gsl_root_fsolver_x_upper(s);
                status = gsl_root_test_interval(x_lo, x_hi, 0 , 0.001);
		//status = gsl_root_test_delta(V, V0, 1e-9, 1e-8);
                V=(x_lo+x_hi)/2;
                //printf("%d %e %e %d\n", iter, x_lo, x_hi, status);
                
	} while(status == GSL_CONTINUE && iter < max_iter);


	gsl_root_fsolver_free(s);
        
        //printf("Found value at %g\n", V);
        
        return V;
}

double Kuiper::Qkp(double x,int N)
{
  int j,j2,M;
  double s,a,b,ds,rac;
  double k,kk,xx,uu,uuu;

  static int oldN=-1;
  static double inv_N_local,inv_N_local_2,sqrt_N_local,inv_sqrt_N_local,lnfact_N_local;
 
  if (oldN!=N)
    {
      /* Calculate some functions of N ahead for speed */
      //printf("redefine N = %d\n",N );
      oldN=N;
      inv_N_local=1.0/N;
      inv_N_local_2=inv_N_local*inv_N_local;
      sqrt_N_local=sqrt(N);
      inv_sqrt_N_local=1.0/sqrt_N_local;
      lnfact_N_local=gsl_sf_lnfact(N);
    }

  /* Decide which equations to use */
  uu=(GSL_IS_ODD(N) && N>5) ?  0.5 *(1.0-2.0*inv_N_local) : 0.5;
  if (x>=uu && N<500) {
    /* Use equations 6 & 7 */
    M=N*(1-x);
    s=0.0;
    uu=3.0-2.0*inv_N_local;
    uuu=inv_N_local_2;
    for (j=0;j<=M;j++) {
      k=x+j*inv_N_local;
      kk=k*k;
      s+=gsl_sf_choose(N,j)*gsl_pow_int(1-k,N-j-1)*
	(gsl_pow_int(k,j-3)*(N*kk*k-kk*j*uu+k*j*(j-1)*uu*inv_N_local-j*(j-1)*(j-2)*uuu));
    }
    
  } else if (x>=inv_N_local && x<=2*inv_N_local) {
    /* Use equation 4 */
    s=1.0-exp(lnfact_N_local+(N-1)*log(x-inv_N_local));
  } else if (x>2*inv_N_local && x<=3*inv_N_local) {
    /* Use equation 5 */
    ds=N*x;
    rac=sqrt(-ds*ds+6*ds-7);
    a=ds-1;
    b=a;
    a=0.5*(a-rac);
    b=0.5*(b+rac);
    /* (N-1)*log(N), because we should have (N-1)!, and not N! */
    s=1.0-(exp(lnfact_N_local-(N-1)*log(N))*
      (gsl_pow_int(b,N-1)*(1-a)-gsl_pow_int(a,N-1)*(1-b))/rac);
  } else {
    /* This is the asymptotic formula of equation 3 */
    x*=sqrt_N_local;
    k=-8*x*0.3333333333333333333333333*inv_sqrt_N_local;
    x=2*x*x;
    s=(2*(2*x-1)+k*(2*x-3))*exp(-x);
    j=1;
    do {
      j++;
      j2=j*j;
      xx=2*x*j2;
      ds=(2*(xx-1)+k*j2*(xx-3))*exp(-j2*x);
      s+=ds;
    } while (fabs(ds)>1e-8*fabs(s));
  }

  if(fabs(s)>1 || !finite(s) )
  {
      cerr << "Warning :: value of probability in Qkp is " << s << " reset to 1\n";
      s=1.0;
  }
  return(s);
}

Kuiper::Kuiper()
{
    A=0,B=0;
    Phase=0;
    times=0;
    t_start=0;
    t_stop =0;

    N_events=0;
    N_segments=0;
    N_GTI=0;
    invN=0;
}

Kuiper::Kuiper(const Kuiper &q)
{
    A=0,B=0;
    Phase=0;
    times=0;
    t_start=0;
    t_stop =0;

    N_events=0;
    N_segments=0;
    N_GTI=0;
    invN=0;
    if(q.N_events>0 && q.N_GTI>0)
        init(q.times,q.N_events, q.t_start, q.t_stop, q.N_GTI);
}

Kuiper::Kuiper( double *t,int N_times,double *tstart,double *tstop,int N_GTI_inp)
{
    A=0,B=0;
    Phase=0;
    times=0;
    t_start=0;
    t_stop =0;
    N_events=0;
    N_segments=0;
    N_GTI=0;
    invN=0;
    init(t,N_times,tstart,tstop,N_GTI_inp);
}

int Kuiper::init( double *t_inp,int N_times,double *tstart,double *tstop,int N_GTI_inp)
{
    
    
    /*
     it can be used also to reinitialize
     */
    
    if (N_events!=N_times)
    {
      invN=1.0/N_times;
      N_events=N_times;

      //fprintf(stderr, "Alloc events !\n");
      if (Phase == 0)
      {
        Phase=(double *)malloc(N_events*sizeof(double));
      }
      else
      {
        Phase=(double *)realloc(Phase,sizeof(double)*N_events);
      }
      if (times == 0)
      {
        times=(double *)malloc(N_events*sizeof(double));
      }
      else
      {
        times=(double *)realloc(times,sizeof(double)*N_events);
      }
      if (Phase==0 || times==0)
	{
	  printf("Error in Kuiper: cannot allocate space for Phases\n");
	  return(-1);
	}
      for (int i=0;i<N_events;i++)
      {
          times[i]=t_inp[i];
      }
    }

  if (N_segments!=10*N_GTI_inp)
    {
      N_segments=10*N_GTI_inp;
      N_GTI=N_GTI_inp;
      //fprintf(stderr, "Alloc segments %d\n", N_segments);
      
      if (A==0)
      {
          A=(double*)malloc(N_segments*sizeof(double));
      }
      else
      {
        A=(double *)realloc(A,sizeof(double)*N_segments);
      }
      
      if (B==0)
      {
          B=(double *)malloc(N_segments*sizeof(double));
      }
      else
      {
        B=(double *)realloc(B,sizeof(double)*N_segments);
      }
      
      if (t_start==0)
      {
          t_start=(double *)malloc(N_GTI*sizeof(double));
      }
      else
      {
        t_start=(double *)realloc(t_start,sizeof(double)*N_GTI);
      }
      
      if (t_stop==0)
      {
          t_stop=(double *)malloc(N_GTI*sizeof(double));
      }
      else
      {
        t_stop=(double *)realloc(t_stop,sizeof(double)*N_GTI);
      }
      
      if (A==0 || B==0 || t_start==0 || t_stop==0)
	{
	  printf("Error in Kuiper: cannot allocate space for Segements\n");
	  return(-1);
	}
      for (int i=0;i<N_GTI;i++)
      {
          t_start[i]=tstart[i];
          t_stop[i]=tstop[i];
      }
      
    }

  return 0;

}

double Kuiper::V_noise(double t_ref, double f, double df, double df2)
{
    double *temp_times = new double[N_events];
    temp_times[0]=times[0];
    for(int i=1;i<N_events;i++)
    {
        temp_times[i]=times[i];
        times[i]=temp_times[i-1]+(temp_times[i]-temp_times[i-1])*rand_unit();
    }
    double V_out=V(t_ref,f,df,df2);
    
    for(int i=0;i<N_events;i++)
    {
        times[i]=temp_times[i];
    }
    
    delete [] temp_times;
    
    return V_out;
    
}

double Kuiper::V(double t_ref, double f, double df, double df2)
{
  int i,j,k;
  double p1,p2,IA, int_p1=0, int_p2=0;

  int Nseg,n_per;

  int ind;
  double Dp,Dm,step1,step2,Aloc,nDp,nDm;

  
      /* Calculate the null hypothesis cumulative distribution */
  //cout << N_segments << endl;
  for(i=0;i<N_segments;i++)
  {
      A[i]=0;
      B[i]=0;
  }

  B[0]=1.0;
  
  Nseg=1;
  for (i=0;i<N_GTI;i++) {
      p1=get_phase_complete(t_start[i], t_ref, f, df, df2, &int_p1);
      p2=get_phase_complete(t_stop[i],  t_ref, f, df, df2, &int_p2);
    n_per=int_p2-int_p1-1;
    if (n_per>0) for (j=0;j<Nseg;j++) A[j]+=n_per;
    
    j=0;
    while (p2>B[j]) j++;
    for(k=Nseg;k>j;k--) {
      B[k]=B[k-1];
      A[k]=A[k-1];
    }
    B[j]=p2;
    Nseg++;
    j=0;
    while (p1>B[j]) j++;
    for(k=Nseg;k>j;k--) {
      B[k]=B[k-1];
      A[k]=A[k-1];
    }
    B[j]=p1;
    Nseg++;
    if (p1<=p2) {
      for (j=0;j<Nseg;j++) if (B[j]>p1 && B[j]<=p2) A[j]++;
      if (n_per>=0) {
	for (j=0;j<Nseg;j++) A[j]++;
      }
    } else {
      for (j=0;j<Nseg;j++) if (B[j]<=p2 || B[j]>p1) A[j]++;
    }
  }
	      
  A[0]=A[0]*B[0];
  for (i=1;i<Nseg;i++) A[i]=A[i-1]+A[i]*(B[i]-B[i-1]);
  IA=1/A[Nseg-1];
  for (i=0;i<Nseg;i++) 
  {
      A[i]*=IA;
      //cout << A[i] << " " << B[i] << "\n";
  }
  /*
   A is the cumulative distribution of the pulse cycles 
   B is the phase of change in the slopes (the phases of GTI intervals)
   */
  
  /*
    Sort the background photons according to the phase
  */
  for (i=0;i<N_events;i++) Phase[i]=get_phase_frac(times[i], t_ref, f, df,df2);
  qsort((char *) Phase, N_events, sizeof(double),(int (*) (const void *, const void *)) doublecompare);
  //gsl_sort(Phase, 1, N_times);
  /*
    Calculate Kuiper's test
  */
  Dp=0.0;
  Dm=0.0;
  ind=0;
  i=0;
  while (Phase[i]==0.0) i++;
  step1=i*invN;
  Dm=step1;
  while (i<N_events) {
    while (B[ind]<=Phase[i]) ind++;
    step2=step1+invN;
    if(ind>0)
        Aloc=(Phase[i]-B[ind-1])*(A[ind]-A[ind-1])/(B[ind]-B[ind-1])+A[ind-1];
    else
        Aloc=(Phase[i]-B[ind])*(A[ind+1]-A[ind])/(B[ind+1]-B[ind])+A[ind];
    if (Dp< (nDp=step1-Aloc)) Dp=nDp;
    if (Dp< (nDp=step2-Aloc)) Dp=nDp;
    if (Dm< (nDm=Aloc-step1)) Dm=nDm;
    if (Dm< (nDm=Aloc-step2)) Dm=nDm;
    i++;
    step1=step2;
  }

  Dp+=Dm;
  
  return(Dp);
}

Kuiper::~Kuiper(void)
{
    myfree();
}

void Kuiper::myfree(void)
{

    if(Phase!=0)
    {
        free(Phase);
        Phase=0;
    }
    if(times!=0)
    {
        free(times);
        times=0;
    }
    if(t_start!=0)
    {
        free(t_start);
        t_start=0;
    }
    if(t_stop!=0)
    {
        free(t_stop);
        t_stop=0;
    }
    N_GTI=0;
    
  N_events=0;
  if(A!=0)
  {
      free(A);
      A=0;
  }
  if(B!=0)
  {
      free(B);
      B=0;
  }
  N_segments=0;
}
