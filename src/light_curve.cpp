/*
 *  light_curve.cpp
 *  Light_Curve_Analyzer
 *
 *  Created by Carlo Ferrigno on 28/10/09.
 *  Copyright 2009 ISDC/IAAT. All rights reserved.
 *
 */

#include <cstdio>
#include <iostream>
#include <fstream>
#include "light_curve.h"
#include <limits>
#include "nr3.h"
#include "ask.h"
#include <string>
#include <iomanip>
#include "fourier.h"
#include "moment.h"
#include "binarycor.h"
#include <cmath>
#include "interp_1d.h"

#include "ran.h"
#include "deviates.h"

#include "stattests.h"

#include "tools.h"
#include "event_files.h"

using namespace std;  

light_curve::light_curve()
{
	_x=0;
	_dx=0;
	_y=0;
	_dy=0;
	_frac_exp=0;
	_size=0;
	GTI_STOP=0;
	GTI_START=0;
	init_values();
	init_keywords();
}

light_curve::light_curve(int n)
{
	init_values();
	init_keywords();
	allocate(n);	
}


light_curve::light_curve(const char *fname)
{
	init_values();
	init_keywords();
	if(read_fits(fname))
	{
            cerr << "Reading FITS file '" << fname << "'failed, try ASCII\n";
            if(read_ascii(fname) <= 2)
            {
                cerr << "Fatal error in reading file, abort\n";
                exit(1);
            }
	}
}

light_curve::light_curve(light_curve *a)
{
	_size=a->_size;
	
    if(_size>0)
	{
		allocate(_size);
		for(int i=0;i<_size;i++)
		{
			
			_x[i]=a->_x[i];
			_dx[i]=a->_dx[i];
			_y[i]=a->_y[i];
			_dy[i]=a->_dy[i];
			_frac_exp[i]=a->_frac_exp[i];
		}
		
	}
	n_gti=a->n_gti;
	if(n_gti>0)
	{
		GTI_START=new double[n_gti];
		GTI_STOP=new double[n_gti];
		for(int i=0;i<n_gti;i++)
		{
			GTI_START[i]=a->GTI_START[i];
			GTI_STOP[i]=a->GTI_STOP[i];
			
		}
		
	}
	else
	{
		GTI_START=0;
		GTI_STOP=0;
	}
	
	_time_del=a->_time_del;
	sprintf(timeref, "%s", a->timeref);
	sprintf(timesys, "%s", a->timesys);
	sprintf(telescop, "%s", a->telescop);
	sprintf(object, "%s",a->object);
	sprintf(instrume, "%s", a->instrume);
	sprintf(timeunit, "%s", a->timeunit);
	sprintf(filename, "%s", a->filename);
	sprintf(scwname, "%s", a->scwname);
        
	_ontime=a->_ontime;
	
	_mjd_ref=a->_mjd_ref;
	
	_t_start=a->_t_start;
	_phase_t_start=a->_phase_t_start;
	_t_stop=a->_t_stop;
	_timepixr=a->_timepixr;
        
	
}


light_curve::light_curve(const light_curve &a)
{
	_size=a._size;
	
    if(_size>0)
	{
		allocate(_size);
		for(int i=0;i<_size;i++)
		{
			
			_x[i]=a._x[i];
			_dx[i]=a._dx[i];
			_y[i]=a._y[i];
			_dy[i]=a._dy[i];
			_frac_exp[i]=a._frac_exp[i];
		}
		
	}
	n_gti=a.n_gti;
	if(n_gti>0)
	{
		GTI_START=new double[n_gti];
		GTI_STOP=new double[n_gti];
		for(int i=0;i<n_gti;i++)
		{
			GTI_START[i]=a.GTI_START[i];
			GTI_STOP[i]=a.GTI_STOP[i];
			
		}
		
	}
	else
	{
		GTI_START=0;
		GTI_STOP=0;
	}
	
	_time_del=a._time_del;
	sprintf(timeref, "%s", a.timeref);
	sprintf(timesys, "%s", a.timesys);
	sprintf(telescop, "%s", a.telescop);
	sprintf(object, "%s",a.object);
	sprintf(instrume, "%s", a.instrume);
	sprintf(timeunit, "%s", a.timeunit);
	sprintf(filename, "%s", a.filename);
	sprintf(scwname, "%s", a.scwname);
        
	_ontime=a._ontime;
	
	_mjd_ref=a._mjd_ref;
	
	_t_start=a._t_start;
	_phase_t_start=a._phase_t_start;
	_t_stop=a._t_stop;
	_timepixr=a._timepixr;
        
	
}

light_curve::light_curve(event_file &a, double time_bin, double e_min, double e_max)
{
	
//	cout <<"Init LC from event file\n";
	_size=0;
	
	n_gti=a.n_gti;
//	cout <<" " << n_gti << endl;

	if(n_gti>0)
	{
		GTI_START=new double[n_gti];
		GTI_STOP=new double[n_gti];
		for(int i=0;i<n_gti;i++)
		{
			GTI_START[i]=a.GTI_START[i];
			GTI_STOP[i]=a.GTI_STOP[i];			
		}
		
	}
	else
	{
		GTI_START=0;
		GTI_STOP=0;
	}
	
	_time_del=a._time_del;
	sprintf(timeref, "%s", a.timeref);
    sprintf(timesys, "%s", a.timesys);
    sprintf(telescop, "%s", a.telescop);
	sprintf(object, "%s", a.object);
	sprintf(instrume, "%s", a.instrume);
	sprintf(timeunit, "%s", a.timeunit);
	sprintf(filename, "%s", a.filename);
	sprintf(scwname, "%s", a.scwname);
	_ontime=a._ontime;
	
	_mjd_ref=a._mjd_ref;
	
	_t_start=a._t_start;
	_t_stop=a._t_stop;
	//_timepixr=a._timepixr;
	_timepixr=0.5;
        
	if(time_bin >0)
	{
			_size=(int)ceil((_t_stop-_t_start)/time_bin);
	}
	else
		_size=0;
		
	//cout << "Allocating a light curve with " << _size << " bins from " << a.filename << endl;
	if(_size >= 1)
	{
		allocate(_size);
		is_rate=0;
		//Initialize LC
		for (int i=0;i<_size;i++)
		{
			_x[i]  = time_bin * ((double)i+0.5) + _t_start;
			_dx[i] = time_bin/2;
			//Assumes events are time ordered as usual
			_y[i] =0;
			_dy[i]=0;
			_frac_exp[i]=0.0; // TBD check the GTI !
			for(int j=0;j<n_gti;j++)
			{
				//Here we assume small bins 
				if (_x[i] >= GTI_START[j] && _x[i] <=GTI_STOP[j])
				{
					_frac_exp[i]=1.0;
					break;
				}

			}
			//cout << i << " " << _frac_exp[i] << endl;
		}
		//Now loop on events
		for (int i=0;i<a.size();i++)
		{
			int ind = (int)floor((a._times[i]-_t_start)/time_bin);
			if (ind >= 0 && ind <_size)
			{
				_y[ind]++;
				_dy[ind]++;
			}
			else
				cout << "Wrong index " << ind << endl;
		}

		for (int i=0;i<_size;i++)
		{
			_dy[i]=sqrt(_dy[i]); 
		}
		_time_del=time_bin;
	}
        
}


light_curve::~light_curve()
{
	deallocate();
	//init_keywords();
	//init_values();
}

void light_curve::init_keywords()
{
	sprintf(timeref, "none");
    sprintf(timesys, "none");
	sprintf(timeunit, "none");
	sprintf(timeunit_table, "none");
	sprintf(telescop, "none");
	sprintf(instrume, "none");
	sprintf(object, "none");
	sprintf(scwname, "none");

}

void light_curve::init_values()
{
	_mjd_ref=0;
	_t_start=0;
	_phase_t_start=0;
	_t_stop=0;
	_ontime=0;
	_time_del=0;
	_time_zero=0;
	_x = 0;
	_dx= 0;
	_y= 0;
	_dy= 0;
	_frac_exp=0;
	_size=0;
	n_gti=0;
	GTI_STOP=0;
	GTI_START=0;
	_timepixr=0.5;
	is_rate=1;

}

void light_curve::allocate(int n)
{
    if (n < 1)
    {
        cerr << "Warning :: attempt to allocate a lightcurve with no bins" << endl;
        return;
    }
	_x = new double[n];
	_dx= new double[n];
	_y= new double[n];
	_dy= new double[n];
	_frac_exp=new double[n];
	_size=n;
	for(int i=0;i<n;i++)
	{
		_x[i]=0;
		_dx[i]=0;
		_y[i]=0;
		_dy[i]=0;
		_frac_exp[i]=0;
	}
}

void light_curve::clean_values()
{
	init_keywords();
	init_values();
}

void light_curve::deallocate_lc()
{
	if(_x!=0)
	{
		delete [] _x;
		_x=0;
	}
	if(_dx!=0)
	{
		delete [] _dx;
		_dx=0;
	}
	if(_y!=0)
	{
		delete [] _y;
		_y=0;
	}
	if(_dy!=0)
	{
		delete [] _dy;
		_dy=0;
	}
	if(_frac_exp!=0)
	{
		delete [] _frac_exp;
		_frac_exp=0;
	}
	
	
	_size=0;

}

void light_curve::deallocate_gti()
{
	if(GTI_STOP!=0)
	{
		delete [] GTI_STOP;
		GTI_STOP=0;
	}
	if(GTI_START!=0)
	{
		delete [] GTI_START;
		GTI_START=0;
	}
	n_gti=0;
}

void light_curve::deallocate()
{

	deallocate_lc();
	deallocate_gti();


}	

int light_curve::read_ascii(const char *fname)
{
	sprintf(filename, "%s", fname);
	int status=0;
	//int noffpix = 10; // number of pixel to offset central pixel (variable...)

	long nrows = 0;
	
        ifstream d_file(fname);
        vector<double> vx, vdx, vy, vdy ;
        double x=0,y=0,dx=0,dy=0;
        
        if(!d_file.is_open())
	{
		cerr << "Error in opening dat file %"<< fname << "%\n";
		return -1;
	}
	
	int max_line=1024;
	char line[max_line];                                                                                                  
	while(d_file.getline(line, max_line, '\n'))
	{ 
		if(line[0] != '\n' && line[0] != '\0')                                                                               
		{                                                                                                                    
                    if (sscanf(line, "%lf %lf %lf %lf", &x, &dx, &y, &dy) == 4)
                    {
                        vx.push_back(x);
                        vdx.push_back(dx);
                        vy.push_back(y);
                        vdy.push_back(dy);
                        
                    }
		}                                                                                                                    
	}
	
	d_file.close();                                                                                                              

        nrows=vx.size();
	
	deallocate();
	allocate(nrows);
        _ontime=0;
        for(int i=0;i<nrows;i++)
        {
            _x[i]  = vx[i]-vdx[i]; //Initial time of the bin !!
            _dx[i] = vdx[i]*2;
            _y[i]  = vy[i]*_dx[i];
            _dy[i] = vdy[i]*_dx[i];
            _ontime+=_dx[i];
            _frac_exp[i]=1.0;
            
        }
        
        _t_start=_x[0];
        _t_stop=_x[nrows-1]+_dx[nrows-1];
        
        _time_del=_ontime/nrows;
        
        is_rate=0;
        vx.clear();
        vy.clear();
        vdx.clear();
        vdy.clear();
        
        return nrows;
}	

int light_curve::read_fits(const char *fname)
{
	fitsfile *fptr;       /* pointer to the FITS file, defined in fitsio.h */
	sprintf(filename, "%s", fname);
	int status=0;
	int hdunum, hdutype;
	//int noffpix = 10; // number of pixel to offset central pixel (variable...)
	char keyword[FLEN_KEYWORD];
	//char keyvalu[FLEN_VALUE];
	char keycomm[FLEN_COMMENT];
	char colname[FLEN_KEYWORD];

	long nrows = 0;
	
	if (fits_open_file(&fptr, fname, READONLY, &status))
	{
        fprintf(stderr, "Could not read %s\n",  fname);
        return status;
	}
	
	if ( fits_get_hdu_num(fptr, &hdunum) == 1 )
        fits_movabs_hdu(fptr, 2, &hdutype, &status);
	else
        fits_get_hdu_type(fptr, &hdutype, &status); /* Get the HDU type */
	
	if (hdutype == IMAGE_HDU)
	{
        fprintf(stderr, "Error: this program only displays tables, not images\n");
        return 1;
	}                                                                                                                                  
	
	if(fits_get_num_rows(fptr, &nrows, &status))
	{
		fits_report_error(stderr, status);
		fits_close_file(fptr,&status);
		return status;
	}
	
	deallocate();
	allocate(nrows);
	
	char read_dx = 0; //check if need to read TIMEDEL, 0 no read dx, 1 read TIMEDEL keyword, 2 read XAX_E (ignoring TIMEDEL keyword
	//char read_fracexp=0;
	int dtime_ind = -1;
	sprintf(colname, "XAX_E");
	if( fits_get_colnum(fptr, 0, colname, &dtime_ind, &status) == 0)
	{
		cerr << "Found col '" << colname <<"' at index " << dtime_ind<< " ignore keyword TIMEDEL\n";
		status=0;
		read_dx=2;
	}
	else
	{
		status=0;
	//	sprintf(colname, "FRACEXP");
//		if( fits_get_colnum(fptr, 0, colname, &dtime_ind, &status) == 0)
//		{
//			cerr << "Found col '" << colname <<"'to be used with TIMEDEL\n";
//			status=0;
//			read_fracexp=1;
//		}
		sprintf(keyword, "TIMEDEL");
		if(fits_read_key( fptr,  TDOUBLE, keyword, &_time_del, keycomm, &status))
		{
			fprintf(stderr, "Could not find keyword '%s'\n", keyword);
			status=0;
			read_dx=1;
		}
	}
	// printf("Flag read_dx = %d\n", read_dx);
	
	sprintf(keyword, "MJDREF");
	if(fits_read_key( fptr,  TDOUBLE, keyword, &_mjd_ref, keycomm, &status))
	{
		fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
		status=0;
		cerr<<"Attempt to use MJDREFI and MJDREFF\n";
		sprintf(keyword, "MJDREFI");
		if(fits_read_key( fptr,  TDOUBLE, keyword, &_mjd_ref, keycomm, &status))
		{
			fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
			status=0;
		}
		else
		{
			double tmp=0;
			sprintf(keyword, "MJDREFF");
			if(fits_read_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status))
			{
				fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
				status=0;
			}
			_mjd_ref+=tmp;
		}
	}
	
	sprintf(keyword, "ONTIME");
	if(fits_read_key( fptr,  TDOUBLE, keyword, &_ontime, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
        sprintf(keyword, "TIMESYS");
	if(fits_read_key( fptr,  TSTRING, keyword, timesys, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}

	sprintf(keyword, "TIMEUNIT");
	if(fits_read_key( fptr,  TSTRING, keyword, timeunit, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}

	sprintf(keyword, "TIMEREF");
	if(fits_read_key( fptr,  TSTRING, keyword, timeref, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	sprintf(keyword, "OBJECT");
	if(fits_read_key( fptr,  TSTRING, keyword, object, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	
	sprintf(keyword, "TELESCOP");
	if(fits_read_key( fptr,  TSTRING, keyword, telescop, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}

	sprintf(keyword, "INSTRUME");
	if(fits_read_key( fptr,  TSTRING, keyword, instrume, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}

	char origin[FLEN_VALUE];
	sprintf(keyword, "ORIGIN");
	if(fits_read_key( fptr,  TSTRING, keyword, origin, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	
	sprintf(keyword, "SCWNAME");
	if(fits_read_key( fptr,  TSTRING, keyword, scwname, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s' try", keyword);
		sprintf(keyword, "SWID");
		fprintf(stderr, "'%s'\n", keyword);
		status=0;
		if(fits_read_key( fptr,  TSTRING, keyword, scwname, keycomm, &status))
		{
			fprintf(stderr, "Could not find keyword '%s'\n", keyword);
			sprintf(scwname, "none");
			status=0;
		}
	}
	
	sprintf(keyword, "TSTART");
	if(fits_read_key( fptr,  TDOUBLE, keyword, &_t_start, keycomm, &status))
	{
		fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
		status=0;
		cerr<<"Attempt to use integer and float parts\n";
		sprintf(keyword, "TSTARTI");
		if(fits_read_key( fptr,  TDOUBLE, keyword, &_t_start, keycomm, &status))
		{
			fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
			status=0;
		}
		else
		{
			double tmp=0;
			sprintf(keyword, "TSTARTF");
			if(fits_read_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status))
			{
				fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
				status=0;
			}
			_t_start+=tmp;
		}
	}
	
	sprintf(keyword, "TSTOP");
	if(fits_read_key( fptr,  TDOUBLE, keyword, &_t_stop, keycomm, &status))
	{
		fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
		status=0;
		cerr<<"Attempt to use integer and float parts\n";
		sprintf(keyword, "TSTOPI");
		if(fits_read_key( fptr,  TDOUBLE, keyword, &_t_stop, keycomm, &status))
		{
			fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
			status=0;
		}
		else
		{
			double tmp=0;
			sprintf(keyword, "TSTOPF");
			if(fits_read_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status))
			{
				fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
				status=0;
			}
			_t_stop+=tmp;
		}
	}
	
	sprintf(keyword, "TIMEZER");
	if(fits_read_key( fptr,  TDOUBLE, keyword, &_time_zero, keycomm, &status))
	{
		fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
		status=0;
		sprintf(keyword, "TIMEZERO");
		if(fits_read_key( fptr,  TDOUBLE, keyword, &_time_zero, keycomm, &status))
		{
			fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
			status=0;
			
		
			cerr<<"Attempt to use integer and float parts\n";
			sprintf(keyword, "TIMEZERI");
			if(fits_read_key( fptr,  TDOUBLE, keyword, &_time_zero, keycomm, &status))
			{
				fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
				status=0;
			}
			else
			{
				double tmp=0;
				sprintf(keyword, "TIMEZERF");
				if(fits_read_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status))
				{
					fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
					status=0;
				}
				_time_zero+=tmp;
			}
		}
	}
	
	sprintf(keyword, "TIMEPIXR");

	if(fits_read_key( fptr,  TDOUBLE, keyword, &_timepixr, keycomm, &status))
	{
		fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
		status=0;
		_timepixr=0.5;
		//f not present the default value is 0.5, the center of the bin.
	}
	
	if(strcmp(timeunit, "d")==0)
	{
		cout << "Time unit of headers is '" << timeunit << "' convert to seconds\n";
		//sprintf(timeunit, "s");\\We need to have it original in case it is not defined in the template
                //waitkey();
                
		_time_zero*=86400;
		_ontime*=86400;
		
		
		_time_zero*=86400;
		
		_t_start*=86400;
		_t_stop*=86400;
                //cout << "***************\n";
                //output();
                //cout << "***************\n";
	}
	   
	//cout << "Timezer = " << _time_zero << endl;
	
	//Find the column indexes
	int time_ind=0;
	int rate_ind=0;
	int drate_ind=0;
	int frac_exp_ind=0;
	
	char frac_exp_flag=1; //assume there is a column and check otherwise
	
	if(strcmp(origin, "ISDC")==0 && strcmp(timeref, "LOCAL") ==0)
	{
		sprintf(colname, "TIME"); //OSA fills barytime with zeros, why ?!
	}
	else
		sprintf(colname, "BARYTIME");

	//cout << "Try to use column '" << colname << "' for the time\n";
	
	if(fits_get_colnum(fptr, 0, colname, &time_ind, &status))
	{
		cerr << "Warning : cannot find col '" << colname <<"', try normal time\n";
		status=0;
		sprintf(colname, "TIME");
		if(fits_get_colnum(fptr, 0, colname, &time_ind, &status))
		{
			cerr << "Error : cannot find col '" << colname <<"', abort\n";
			fits_close_file(fptr, &status);
			return status;
		}
	}
	else
	{
		sprintf(timeref,"SOLARSYSTEM");
	}
	
	cout << "Read time from column: '" << colname << "'\n";
	cout << "Reference system is:   '" << timeref << "'\n";
	
	sprintf(keyword, "TUNIT%d", time_ind);
	if(fits_read_key( fptr,  TSTRING, keyword, &timeunit_table, keycomm, &status))
	{
            
            fprintf(stderr, "Could not find keyword '%s'\n", keyword);
            sprintf(timeunit_table, "%s", timeunit);
            status=0;
            
	}
	
	sprintf(colname, "RATE");
	if(fits_get_colnum(fptr, 0, colname, &rate_ind, &status))
	{
		cerr << "WARNING : cannot find col '" << colname <<"' try '";
		sprintf(colname, "RATE1");
		cerr << colname << "'\n";
		status=0;
		if(fits_get_colnum(fptr, 0, colname, &rate_ind, &status))
		{
			cerr << "WARNING : cannot find col '" << colname <<"' try '";
			sprintf(colname, "NET_COUNTS"); //Chandra
			cerr << colname << "'\n";
			status=0;
			if(fits_get_colnum(fptr, 0, colname, &rate_ind, &status))
			{
				cerr << "WARNING : cannot find col '" << colname <<"' try '";
				sprintf(colname, "COUNTS");
				cerr << colname << "'\n";
				status=0;
				if(fits_get_colnum(fptr, 0, colname, &rate_ind, &status))
				{
					cerr << "ERROR : cannot find col '" << colname <<"'\n";

					fits_close_file(fptr, &status);
					return status;
				}
				else
					is_rate=0;

			}
			else
				is_rate=0;
		}
	}
	
	sprintf(colname, "ERROR");
	if(fits_get_colnum(fptr, 0, colname, &drate_ind, &status))
	{
		cerr << "WARNING : cannot find col '" << colname <<"' try '";
		sprintf(colname, "ERROR1");
		cerr << colname << "'\n";
		status=0;
		if(fits_get_colnum(fptr, 0, colname, &drate_ind, &status))
		{
			status=0;
			cerr << "WARNING : cannot find col '" << colname <<"' try '";
			sprintf(colname, "NET_ERR"); //Chandra LC
			cerr << colname << "'\n";

			if(fits_get_colnum(fptr, 0, colname, &drate_ind, &status))
			{
				cerr << "ERROR : cannot find col '" << colname <<"'\n";
				status=0;
				cerr << "WARNING : cannot find col '" << colname <<"' try '";
				sprintf(colname, "RATE_ERR"); //BAT LC
				cerr << colname << "'\n";

				if(fits_get_colnum(fptr, 0, colname, &drate_ind, &status))
				{
						cerr << "WARNING : cannot find col '" << colname <<"', we assume Poissonian errors\n";
						drate_ind=-1;
						//fits_close_file(fptr, &status);
						//return status;
				}
			}
		}
	}
	
	if(read_dx)
	{
		sprintf(colname, "XAX_E");
		if(fits_get_colnum(fptr, 0, colname, &dtime_ind, &status))
		{
			cerr << "WARNING : cannot find col '" << colname <<"' try '";
			sprintf(colname, "TIMEDEL");
			cerr << colname << "'\n";
			status=0;
			if(fits_get_colnum(fptr, 0, colname, &dtime_ind, &status))
			{
				cerr << "ERROR : cannot find col '" << colname <<"'\n";
				
				fits_close_file(fptr, &status);
				return status;
			}
			else
			{
				cout << "Found " << colname << " at index #" << dtime_ind << endl;
			}
			
		}
		else
		{
			cout << "Found " << colname << " at index #" << dtime_ind << endl;
		}
	}
	
	sprintf(colname, "FRACEXP");
	if(fits_get_colnum(fptr, 0, colname, &frac_exp_ind, &status))
	{
		cerr << "WARNING : cannot find col '" << colname <<"'\n";
		status=0;
		frac_exp_flag=0;
		for(int i=0;i<_size;i++)
			_frac_exp[i]=1;
	}

	char format_rate[FLEN_KEYWORD];
	char format_err[FLEN_KEYWORD];
	sprintf(keyword, "TFORM%d", rate_ind);
	//cout << keyword << endl;
	if(fits_read_key( fptr,  TSTRING, keyword, format_rate, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		sprintf(format_rate, "1E");
		status=0;
	}
	sprintf(keyword, "TFORM%d", drate_ind);
	//cout << keyword << endl;
	if(fits_read_key( fptr,  TSTRING, keyword, format_err, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		sprintf(format_err, "%s", format_rate);
		status=0;
	}

	if(strcmp(format_rate,format_err))
	{
		cerr << "Format of rate and error columns are different, check the input !\n";
		cerr << format_rate << " " << format_err << endl;
		fits_close_file(fptr, &status);
		return 1;
	}

	int n_rate_col=0;
	char tmp[2];
	sscanf(format_rate,"%d %s", &n_rate_col, tmp);
	//cout <<"Format " << format_rate << endl;
	//cout << n_rate_col << " " << tmp << endl;
	//exit(1);
	//Actual reading
	int firstrow  = 1;  // first row in table to write   
	int firstelem = 1;  // first element in row  (ignored in ASCII tables) 

	int anynul=0, anynul_loc=0;
	double nulldbl = NAN;
	//numeric_limits<double>::quiet_NaN();
	
	if(fits_read_col(fptr, TDOUBLE,    time_ind, firstrow, firstelem, nrows,
				  &nulldbl, _x, &anynul, &status))
	{
		cerr << "ERROR :: error reading column in '"<<fname<<"'\n";
		fits_close_file(fptr, &status);
		return status;
	}
	if(anynul)
	{
		anynul_loc+=anynul;
		cout << "TIME ANYNUL " << anynul << endl;
	}

	if(n_rate_col <= 1)
	{
		if(fits_read_col(fptr, TDOUBLE,    rate_ind, firstrow, firstelem, nrows,
											&nulldbl, _y, &anynul, &status))
		{
				cerr << "ERROR :: error reading column in '"<<fname<<"'\n";
				fits_close_file(fptr, &status);
				return status;
		}
	    if(anynul)
		{
			anynul_loc+=anynul;
			cout << "RATE ANYNUL " << anynul << endl;
		}
 

//	for (int ii=0;ii<nrows-1;ii++)
//		cout << _y[ii] << endl;
		
		if(drate_ind >=0)
		{
			if(fits_read_col(fptr, TDOUBLE,    drate_ind, firstrow, firstelem, nrows,
												&nulldbl, _dy, &anynul, &status))
			{
					cerr << "ERROR :: error reading column in '"<<fname<<"'\n";
					fits_close_file(fptr, &status);
					return status;
			}
			if(anynul)
			{
				anynul_loc+=anynul;
				cout << "D_RATE ANYNUL " << anynul << endl;
			}
		}
		else
		{
			for (int i=0;i<nrows;i++)
			{
				if(_y[i]>0)
				{
			
					_dy[i]=sqrt(_y[i]);
				}
				else
					_dy[i]=0;
			}
		}

	}
	else
	{
		cout << "Reads multiple column format for RATE and ERROR\n";

		//PRint comment field to know the colum
		char comment_field[FLEN_KEYWORD];
		sprintf(keyword, "Comment");
		//cout << keyword << endl;
		cout << "Comment field *************************************************\n";
		int old_pos=0, new_pos=0, n_key=0;

		while (status==0)
		{
			//if (fits_find_nextkey (fptr, (char **)&keyword, 1, (char **)&keyword, 0, keycomm, &status))
			if(fits_read_key( fptr,  TSTRING, keyword, comment_field, keycomm, &status))
			{
				cerr << "Cannot find comment field\n";

			}
			fits_get_hdrpos (fptr, &n_key, &new_pos, &status);
			//cout << n_key << " " << new_pos << endl;
			if(new_pos < old_pos)
				break;

			old_pos=new_pos;

			//cout << comment_field << endl;
			cout << keycomm << endl;
		}
		status=0;
		cout << "Comment field *************************************************\n";
		
		char query[1024];
		sprintf(query,"Choose the first column you want to sum");
		int col1=1;
		ask_integer(query, &col1);
		sprintf(query,"Choose the last column you want to sum");
		int col2=n_rate_col-2;
		ask_integer(query, &col2);
		if (col1 <1 || col2 > n_rate_col)
		{
			cerr <<"Wong column interval "<< col1 << " " << col2 << endl;
			return 1;
		}

		double *temp_rate =new double[n_rate_col*nrows];
		double *temp_err =new double[n_rate_col*nrows];

		if(fits_read_col(fptr, TDOUBLE,    rate_ind, firstrow, firstelem, nrows*n_rate_col,
											&nulldbl, temp_rate, &anynul, &status))
		{
				cerr << "ERROR :: error reading column in '"<<fname<<"'\n";
				fits_close_file(fptr, &status);
				return status;
		}
		if(anynul)
		{
			anynul_loc+=anynul;
			cout << "RATE ANYNUL " << anynul << endl;
		}


//	for (int ii=0;ii<nrows-1;ii++)
//		cout << _y[ii] << endl;

		if(drate_ind>=0)
		{
			if(fits_read_col(fptr, TDOUBLE,    drate_ind, firstrow, firstelem, nrows*n_rate_col,
												&nulldbl, temp_err, &anynul, &status))
			{
					cerr << "ERROR :: error reading column in '"<<fname<<"'\n";
					fits_close_file(fptr, &status);
					return status;
			}
			if(anynul)
			{
				anynul_loc+=anynul;
				cout << "D_RATE ANYNUL " << anynul << endl;
			}
		}
		else
		{
			for(int i=0;i<nrows*n_rate_col;i++)
			{
				if(temp_rate[i]>0)
					temp_err[i]=sqrt(temp_rate[i]);
				else
					temp_err[i]=0;
			}
		}


		for (int j=0;j<nrows;j++)
		{
			_y[j]=0;
			_dy[j]=0;
		}

		for (int j=0;j<nrows;j++)
		{
			for (int k=col1-1;k<= col2-1;k++)
			{
				_y[j]+=temp_rate[j*n_rate_col+k];
				_dy[j]+=SQR(temp_err[j*n_rate_col+k]);
			}
//                cout << _y[j] << " " << sqrt(_dy[j]) << endl;
//                waitkey();
		}

		for (int j=0;j<nrows;j++)
		{
			_dy[j]=sqrt(_dy[j]);
		}

		delete [] temp_rate;
		delete [] temp_err;
		//exit(1);
	}//end of multiple column reading

    if(read_dx)
	{
		if(fits_read_col(fptr, TDOUBLE,    dtime_ind, firstrow, firstelem, nrows,
						 &nulldbl, _dx, &anynul, &status))
		{
			cerr << "ERROR :: error reading column in '"<<fname<<"'\n";
			fits_close_file(fptr, &status);
			return status;
		}
		
		cout << "Read colummn #" << dtime_ind << endl;
		
		VecDoub_I dt(_size, _dx);
		Doub a=0,v=0;
		avevar(dt,a,v);
		v=sqrt(v);
		cout << "Average delta time  = " << a << endl;
		cout << "STD delta time = " << v << endl;
		if(v/a > 1e-4)
		{
			cout << "WARNING High variance for delta_time\n";
			//waitkey();
		}
		_time_del = a;
		if(read_dx == 2)
		{
			_time_del*=2;
			for(int kk=0;kk<_size;kk++)
				_dx[kk]*=2;
			
			//_time_zero+=_time_del; //why was it present?
		}
	}
	else
	{
		for(int i=0;i<_size;i++)
			_dx[i]=_time_del;
	}

	for(int kk=0;kk<_size;kk++)
		if (_dx[kk] <0)
		{
			cerr << "Negative DX " << kk << " " << _dx[kk] << endl;
			waitkey();
		}

	if(anynul)
	{
		anynul_loc+=anynul;
		cout << "DX ANYNUL " << anynul << endl;
	}

	double time_offset=0;
	if(_x[0] == 0)
    	time_offset =_t_start;
	//Do it here for XAX_x
	cout << "Info Shifting x by " << 0.5 -  _timepixr << " delta_x plus " << time_offset << "\n";
	for(int kk=0;kk<_size;kk++)
	{
		// TODO ATTTENTION, this manipulation is not ROBUST !!
        _x[kk] += _dx[kk]*(0.5 - _timepixr) + time_offset;
        //_x[kk]+=_time_zero-_dx[kk]*_timepixr; // I do not rememebr why I was using _time_zero, it looks a mistake
		//This make absolute time at the beginning of the bin.
	}
	
	
	if(frac_exp_flag)
	{
            if(fits_read_col(fptr, TDOUBLE,    frac_exp_ind, firstrow, firstelem, nrows,
                                             &nulldbl, _frac_exp, &anynul, &status))
            {
                    cerr << "ERROR :: error reading column in '"<<fname<<"'\n";
                    fits_close_file(fptr, &status);
                    return status;
            }
	}
	else
	{
            for(int j=0;j<_size;j++)
            {
                    _frac_exp[j]=1;
            }
	}
	
	//Always counts
	if(is_rate == 1)
	{
		double convert=1;
		
		if(strcmp(timeunit_table, "d") == 0)
			convert=86400.0;
                cout << "Converting rate to counts with unit of time '" << convert << "'\n";
                //waitkey();
		for(int j=0;j<_size;j++)
		{
			_y[j]*=_dx[j]*_frac_exp[j]*convert;
			_dy[j]*=_dx[j]*_frac_exp[j]*convert;
			//cout << "Multiply frac exp\n";
			if(_frac_exp[j]==0)
				_dx[j]*=_frac_exp[j];
				//this conversion is done later
		}
		is_rate=0;
	}
	
	if(anynul_loc  > 0)
	{
		
		vector <double> xx, yy, dxx, dyy, fracexp;
		cout << "ANYNUL " << anynul_loc << endl;
		for (int ii=0;ii<nrows;ii++)
		{
			//cout << _y[ii] << endl;
			if(finite(_x[ii]) && finite(_dx[ii]) && finite(_y[ii])&& finite(_dy[ii]))
			{
				xx.push_back(_x[ii]);
				dxx.push_back(_dx[ii]);
				yy.push_back(_y[ii]);
				dyy.push_back(_dy[ii]);
				fracexp.push_back(_frac_exp[ii]);
			}
		}
		
		delete [] _x;
		delete [] _dx;
        delete [] _y;
		delete [] _dy;
		delete [] _frac_exp;
		_size=xx.size();
		
		cout << "Size before NULL removal "<< nrows<< endl;
		
		cout << "Size after  NULL removal " << _size << endl;
		
		_x= new double[_size];
		_dx= new double[_size];
		_y= new double[_size];
		_dy= new double[_size];
		_frac_exp = new double[_size];
		
		for (int ii=0;ii<_size;ii++)
		{
			_x[ii]=xx[ii];
			_dx[ii]=dxx[ii];
			_y[ii]=yy[ii];
			_dy[ii]=dyy[ii];
			_frac_exp[ii]=fracexp[ii];
			//cout << _x[ii] << " " <<_y[ii] << endl;
			
		}
		cerr << "WARNING :: read " << nrows - _size << " NULL entries\n";
	}
	
	//End of null value removal
        
	
	//Mistake, this header time unit
	if(strcmp(timeunit_table, "d") == 0)
	{
		cout << "Time unit of column is in " << timeunit_table << " , convert to second\n";
		_time_del *=86400;
		for(int j=0;j<_size;j++)
		{
			_x[j]*=86400;
			_dx[j]*=86400;
		}
//                if(n_gti==0)
//                {
//                    _t_start*=86400;
//                    _t_stop*=86400;
//                    _ontime=_t_stop-_t_start;
//                }
		sprintf(timeunit_table, "s");
		sprintf(timeunit, "s");
                //Not done before, for ill-built LCs
	}
	   
	if((status = get_gti(fptr, status)))
	{
		cerr << "Error reading GTI, skipping, reset status to zero\n";
		status=0;
	}
	if(n_gti==0)
	{
		_ontime =_t_stop-_t_start;
	}
	else
		filter_gti();
                    
	if ( fits_close_file(fptr, &status) )       // close the FITS file
        fits_report_error(stderr, status );  
//	for (int ii=0;ii<_size;ii++)
//        {
//               cout << _x[ii] << " " <<_y[ii] << endl;
//
//        }
	return status;
	
}

int light_curve::filter_gti()
{
    int status = 0;
    if (n_gti==0)
    {
        cerr << "WARNING  light_curve::filter_gti number GTI is " << n_gti << "  quit silently\n";
        return 1;
    }
    
    vector <double> xx, yy, dxx, dyy, fracexp;

    int nrows=_size;
    
    
    
    for (int ii=0;ii<nrows;ii++)
    {
        for (int j=0;j<n_gti;j++)
        {//cout << _y[ii] << endl;
            //cout << _x[ii] << " " << GTI_START[j] << " " << GTI_STOP[j] <<endl;
            
            if( _x[ii] >= GTI_START[j]  && _x[ii] < GTI_STOP[j] )
            {
                
                xx.push_back(_x[ii]);
                dxx.push_back(_dx[ii]);
                yy.push_back(_y[ii]);
                dyy.push_back(_dy[ii]);
                fracexp.push_back(_frac_exp[ii]);
                break;
            }
        }
    }

    
    if (_size == xx.size())
    {
        cout << "light_curve::filter_gti no elements to remove, skipping\n";
        return status;
    }
    
    delete [] _x;
    delete [] _dx;
    delete [] _y;
    delete [] _dy;
    delete [] _frac_exp;
    _size=xx.size();

    cout << "Size before GTI filter "<< nrows<< endl;

    cout << "Size after  GTI filter " << _size << endl;

    _x= new double[_size];
    _dx= new double[_size];
    _y= new double[_size];
    _dy= new double[_size];
    _frac_exp = new double[_size];

    for (int ii=0;ii<_size;ii++)
    {
            _x[ii]=xx[ii];
            _dx[ii]=dxx[ii];
            _y[ii]=yy[ii];
            _dy[ii]=dyy[ii];
            _frac_exp[ii]=fracexp[ii];
            //cout << _x[ii] << " " <<_y[ii] << endl;

    }
    cerr << "WARNING :: removed " << nrows - _size << " out of GTI bins\n";
    return status;
}


void light_curve::output()
{
	cout << "Tstart, Tstop are " << scientific << setw(19) << setprecision(12) <<  _t_start << "\t" << _t_stop << endl; 
	cout << "Time del is " << time_del() << " " << time_unit() << endl;
	cout << "Ontime is " << ontime() << " " << time_unit() << endl;
	cout << "There are " << size() << " bins\n";
}

void light_curve::clean_frac_exp(double min_frac)
{
	for(int j=0;j<_size;j++)
	{
		if(_frac_exp[j]<min_frac )
		{
			_y[j]=0;
			_dy[j]=0;
		}
	}
}

void light_curve::get_times(vector<double> &times, double min_sn)
{
	static int seed=time(NULL);		

	for(int i=0;i<_size;i++)
	{
		int how_many=0;
		if(_dy[i]>0  && _y[i]>0)
		{
			double ratio=_y[i]/_dy[i];
			if( ratio > min_sn)
				how_many=(int)ceil(ratio);
		}

		Ranq1 myran(seed++);
		for(int k=0;k<how_many;k++)
			times.push_back(_x[i] + (myran.doub()*2-1)*_dx[i] );
	}
	
}

double light_curve::get_Rk2(int k, double omega, double t_ref, char flag_err)
{
    // This is the implementation of binned extended Rayleigh statistics 
    // omega : angular frequency
    // k : harmonic order
    // t_ref : reference time for phasing
    // flag_err : if true, weight by the error
    // based on Belanger (2016)
    
    double sum_c2=0, sum_c=0, sum_s2=0, sum_s=0;
    
    for (int i = 0;i<_size;i++)
    {
        double w=_y[i];
        if (flag_err == 'y' && _dy[i]>0)
            w/=_dy[i];
        
        double dt=_x[i]-t_ref;
        double c1=w * cos(k*omega*dt);
        double s1=w * sin(k*omega*dt);
        
        sum_s  += s1;
        sum_s2 += s1*s1;
        sum_c  += c1;
        sum_c2 += c1*c1;
        //cout << s1 << " " << c1 << " " << sum_s2 << " " << sum_c2 << endl;
    }
    
    return sum_s*sum_s/sum_s2+sum_c*sum_c/sum_c2;
}


int light_curve::normalize(double level, char use_wave='y')
{
	int status=0;
	
	
	VecDoub_I y(_size,_y);
	VecDoub_I dy(_size,_dy);
	Doub av=0;
	Doub var=0;
	
	if(use_wave == 'y')
	{
		wavevar(y,dy,av,var);
		if(level >0 && level < 1000)
		{

			wavevar_excl(y,dy,av,var, level);
		}
		cout << "Average  = " << av <<endl;
		cout << "STD = " << sqrt(var) << endl;

	}
	else
	{
		avevar(y,av,var);
		if(level >0 && level < 1000)
		{
			avevar_excl(y,av,var, level);
		}
		cout << "Average  = " << av <<endl;
		cout << "STD = " << sqrt(var) << endl;
	}

	var=sqrt(var);

	for(int j=0;j<_size;j++)
	{
		if(_dy[j] == 0)
			continue;
		
		
		_y[j]-=av;
		_y[j]/=var;
		_dy[j]/=var;
	}
		
	return status;
	
}
	


int light_curve::find_extremes(double min_SN, double min_deviation, double burst_duration)
{

	int n_b=0;
	if(burst_duration <0)
	{
		cerr <<"Negative burst duration, take positive\n";
		burst_duration=-burst_duration;
	}
	
	for(int j=0;j<_size;j++)
	{
		if(_dy[j] == 0)
			continue;
		
		if(_y[j]/_dy[j]<min_SN)
			continue;
		
		if(_y[j] >= min_deviation)
		{
			int min_j = (int)(j - floor(0.2*burst_duration/_time_del));
			int max_j = (int)(j + ceil(burst_duration/_time_del));
			
			if(min_j<0)
				min_j=0;
			if(max_j>=_size)
				max_j=_size-1;
			
//			cout << min_j << endl;
//			cout << max_j << endl;
//			cout << _time_del << endl;
			
			string out_name=filename;
			size_t pos=0;
//			cout << out_name <<endl;
			if( (pos = out_name.find_last_of(".")) != string::npos)
				out_name.erase(pos, out_name.size()-1);
//			cout << out_name <<endl;
			if( (pos = out_name.find_last_of("/") ) != string::npos)
				out_name.erase(0, pos+1);
//			cout << out_name <<endl;
			
			char tmp[100];
			sprintf(tmp, "_%012.2lf", _x[j]);
			if(strcmp(scwname, "none"))
			{
				out_name+="_";
				out_name+=scwname;
				cout << "Found SCWNAME '" << scwname << "'\n";
			}
			
			out_name+=tmp;
			out_name+=".qdp";

			cout << "Found possible burst at t=" << _x[j] << " output on '" << out_name << "'\n"; 
			n_b++;
			ofstream ff(out_name.c_str());
			
			ff<<"read serr 1 2\n";
			ff<<"cpd /xw\n";
			ff <<"ma 4 on 2\n";
			ff <<"lw 2\n";
			ff <<"r y\n";
			ff <<"lab y Rate cts/s\n";
			ff <<"cs 1.1\n";
			ff <<"font roman\n";
			ff <<"time off\n";
			ff <<"lab title\n";
			ff <<"lab x Time [t -" << scientific << setw(19) << setprecision(12) << _x[0] << " s]\n";
			ff <<"lab ox see MJDREF in your LC\n";
			
			
			for(int k=min_j;k<=max_j;k++)
			{
				ff << scientific << setw(19) << setprecision(12)<< _x[k] - _x[0] << "\t";
				ff << scientific << setw(19) << _dx[k] / 2<< "\t";
				ff << scientific << setw(19) << _y[k] << "\t";
				ff << scientific << setw(19) << _dy[k] << "\n";
				
			}
			//waitkey();
			ff.close();
			
			j=max_j;
		}//end of burst found
			
			
	}
	
	return n_b;
}

void light_curve::rebin(int factor)
{

	if(factor<=1)
	{
		cerr << "Skip rebinning factor" << factor << endl;
		return;
	}
	
	int new_size=_size/factor;
	
	double *new_x= new double[new_size];
	double *new_dx= new double[new_size];
	double *new_y= new double[new_size];
	double *new_dy= new double[new_size];
	double *new_exp_frac= new double[new_size];

	for(int i=0;i<new_size;i++)
	{
		new_x[i]=0;
		new_dx[i]=0;
		new_y[i]=0;
		new_dy[i]=0;
		new_exp_frac[i]=0;
		
	}
	
	for(int i=0;i<new_size;i++)
	{
		double count=0;
		for(int j=0;j<factor;j++)
		{
			if(i*factor+j>=_size-1)
				break;
			
			count++;
			new_x[i]+=_x[i*factor+j]+_dx[i*factor+j]/2;
			new_dx[i]+=_dx[i*factor+j];
			new_y[i]+=_y[i*factor+j];
			new_dy[i]+=SQR(_dy[i*factor+j]);
			new_exp_frac[i]+=_frac_exp[i*factor+j]*_dx[i*factor+j];
		}
		
		new_dy[i]=sqrt(new_dy[i]);
		if(count>0)
		{
			new_x[i]/=count;
			new_x[i]-=new_dx[i]/2;
			//new_y[i]/=count;
			//new_dy[i]/=count;
			new_exp_frac[i]/=count;
			
		}
	}

	deallocate_lc();
	allocate(new_size);
	
	for(int i=0;i<new_size;i++)
	{
		_x[i]=new_x[i];
		_dx[i]=new_dx[i];
		_y[i]=new_y[i];
		_dy[i]=new_dy[i];
		_frac_exp[i]=new_exp_frac[i]/new_dx[i];
		
	}
	
	delete [] new_x;
	delete [] new_dx;
	delete [] new_y;
	delete [] new_dy;
	delete [] new_exp_frac;
	
	_time_del*=factor;
	
}

void light_curve::binary_corr(char *file_name)
{
	
	
	char binary_file[FLEN_FILENAME];
	char query[256];
	sprintf(binary_file, "%s", file_name);
	char setup_file_exist=1;
	char interactive_flag=0;
	if (strcmp(binary_file, "none")==0)
	{
		sprintf(query, "Enter the name of the file to store the binary parameters");
		askInfo(query, binary_file);
		//sprintf(binary_file,"%s_orbit.dat",active_src_name);
		interactive_flag=1;
		setup_file_exist=0;
	}
	
	binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist,_mjd_ref);
	
	//double off = ( _mjd_ref - MJDREF_INTEGRAL ) * DAY2SEC;
	
	double corr_time =  binarycor( _t_start  );
	_t_start=corr_time;                                                                                                            
	corr_time =   binarycor( _t_stop );                                                                    
	_t_stop=corr_time;                                                                                                             
	
	for(long i=0;i<_size;i++)                                                                                                    
	{                                                                                                                            
		//cout << _x[i] << endl;
		corr_time =  binarycor( _x[i]);

		/*
                printf( "%19.12e\t%19.12e\n", _x[i],  corr_time - _x[i]  );
		if(fabs(corr_time - _x[i]) > 1e2)
		{
                	printf( "%19.12e\t%19.12e\n", _x[i],  corr_time - _x[i]  );
			waitkey();
		}
		*/
		_x[i] = corr_time;                                                                                             
		
	} 
	
	if(n_gti)
	{
		for(int i=0;i<n_gti;i++)
		{
			corr_time =  binarycor( GTI_START[i]  );                                                      
			GTI_START[i] = corr_time;
			corr_time =  binarycor( GTI_STOP[i]  );                                                      
			GTI_STOP[i] = corr_time;
		}
	}
	
	sprintf(timeref, "BINARY");
	
}

int light_curve::get_gti(fitsfile *fptr, int status)
{
	int hdutype=0;
	char keyword[FLEN_KEYWORD];
	char extname[FLEN_VALUE];
	int num_hdus=1;
	int hdu_num=1;
	
	if(fits_get_num_hdus(fptr, &num_hdus, &status))
		fits_report_error(stderr, status );
	
	if(num_hdus < 3)
	{
		cerr << "LC file does not contain GTI or EVENTS, skipping\n";
		n_gti=0;

/*
 * GTI_START=new double[n_gti];
 * GTI_STOP=new double[n_gti];
 * GTI_START[0]=_x[0];
 * GTI_STOP[0]=_x[_size-1];
 */
		_t_stop=_x[_size-1];
		_t_start=_x[0];
		
		return 1;
	}
	
	fits_get_hdu_num(fptr, &hdu_num);
	
	fits_movrel_hdu(fptr, 1, &hdutype, &status);
	if(status)
	{
		fits_report_error(stderr, status);
		cerr << "Error: cannot move to GTI extension place\n";
		return status;
	}
	char gti_timeunit[FLEN_VALUE];
        sprintf(keyword, "TIMEUNIT");
	if(fits_read_key( fptr,  TSTRING, keyword, gti_timeunit, NULL, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	sprintf(keyword,"EXTNAME");
	if(fits_read_key(fptr, TSTRING, keyword, extname, NULL, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	char hduclas1[FLEN_VALUE];
	sprintf(keyword, "HDUCLAS1");
	if(fits_read_key( fptr,  TSTRING, keyword, hduclas1, NULL, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	else
	{
		if(strcmp(hduclas1, "GTI")!=0)
		{
			fits_movabs_hdu(fptr, hdu_num, &hdutype, &status);
			cerr << "Extension " << extname << " does not contain GTI \n";
			return 1;	
		}
	}
	
	
	if(strcmp(extname, "GTI")!=0)
	{
		cerr << "WARNING :: Extension for GTI is " << extname << endl;
	}
	
	
	
	
	if (hdutype == IMAGE_HDU)
	{
		cerr<< "Error: GTI is an image\n";
		return 1;
	}
	
	long nrows;
	fits_get_num_rows(fptr, &nrows, &status);
	
	//printf("num_rows = %ld\n", nrows);
	
	GTI_START=new double[nrows];
	GTI_STOP=new double[nrows];
	
	n_gti=nrows;
	
	int anynul=0;
	double null_value = -1;
	
	if (fits_read_col(fptr, TDOUBLE, 1, 1, 1, nrows, &null_value, GTI_START, &anynul, &status) )
		fits_report_error(stderr, status);
	
	if (fits_read_col(fptr, TDOUBLE, 2, 1, 1, nrows, &null_value, GTI_STOP, &anynul, &status) )
		fits_report_error(stderr, status);
	
	_ontime=0;
	for (int i=0;i<nrows;i++)
	{
		if(strcmp(gti_timeunit, "d")==0)
		{
			GTI_START[i]*=86400;
		
			GTI_STOP[i]*=86400;
		}
		
		_ontime+=GTI_STOP[i]-GTI_START[i];
	}
	
	fits_movabs_hdu(fptr, hdu_num, &hdutype, &status);
	return status;
}

int light_curve::scan_gti()
{
	
	if(n_gti==0)
		return 1;
	
	double *gti_start=GTI_START;
	double *gti_stop=GTI_STOP;
	double min_time=1e12;
	double max_time=-1e12;
	double min_gap=1e12;
	double max_gap=-1e12;
	
	
	for(int i=0;i<n_gti;i++)
	{
		double tmp=gti_stop[i]-gti_start[i];
		if(tmp<min_time)
			min_time=tmp;
		if(tmp>max_time)
			max_time=tmp;
	}
	
	for(int i=1;i<n_gti;i++)
	{
		double tmp=-gti_stop[i-1]+gti_start[i];
		if(tmp<min_gap)
			min_gap=tmp;
		if(tmp>max_gap)
			max_gap=tmp;
	}
	
	cout <<"There are " << n_gti << " GTI intervals\n";
	cout <<"Minimum and maximum lengths are [s] " << min_time << " " << max_time << endl;
	if(min_gap <= max_gap)
		cout <<"Minimum and maximum gaps are [s] " << min_gap << " " << max_gap << endl;
	
	return 0;
}

int *light_curve::make_rebin_table(double min_sn, double max_time, double gti_threshold, double min_bin_size, double max_point_sn, char single_point_check)
{

	int *table = new int[_size];
	
	double tt=0;
	double rr=0;
	double ee=0;
	int curr_gti_ind=0;
	int gti_flag=0;
	int end_flag=0;
        //OUTPUT
	//-2 discard for GTI  
        //-1 continue accumulation
        //0 discard
        //1 stop accumulation
	double tot_disregard_time=0;
	for(int i=0;i<_size;i++)
	{
		if (i == _size-1)
			end_flag=1;

		table[i]=0;
		if(isnan(_y[i]) || isnan(_dy[i]))
		   continue;
		   
		if (_y[i] != 0)
		{
			if ( fabs(_dy[i]/_y[i]) > max_point_sn)
			{
				cout << "Point " << i << " exceeds max_point_sn, discarded\n";
				continue;
			}
		}
				


		if(n_gti>0)
		{
			
                      //cout <<endl<< i << " " << _x[i] << " " << GTI_START[curr_gti_ind] << " " << GTI_STOP[curr_gti_ind]  << endl;
                      //waitkey();
                      if(_x[i] < GTI_START[0] || _x[i] >  GTI_STOP[n_gti-1])
			{
				table[i]=-2;
//                                cerr <<"Urka!\n";
				continue;
			}
			
 
                        //If you are NOT inside the current GTI
			if(! (_x[i] >= GTI_START[curr_gti_ind] &&  _x[i] <= GTI_STOP[curr_gti_ind] ) )
			{
                            cout << "Event # << " << i << " time " << _x[i] << " not in gti bin " << curr_gti_ind << " " << GTI_START[curr_gti_ind]  << " == " <<  GTI_STOP[curr_gti_ind] << endl;
                            cout << _x[i] - GTI_START[curr_gti_ind] << " || " <<  _x[i] - GTI_STOP[curr_gti_ind] << endl;
                                //Trivial checks to see if events are out of GTI (it should not happen))
				if(curr_gti_ind>0)
				{
					if(_x[i] > GTI_STOP[curr_gti_ind-1] && _x[i] < GTI_START[curr_gti_ind] )
					{				
						table[i]=-2;
						//cout << i << endl;
						continue;
					}
				}
				

				
				if(curr_gti_ind<n_gti-1)
				{
					if(_x[i] < GTI_START[curr_gti_ind+1] && _x[i] > GTI_STOP[curr_gti_ind])
					{	
						table[i]=-2;
						//cout << i << endl;
						continue;
					}
					
				}	
				
                                //Check if we can continue accumulation despite the GTI break
//				char neglect_gti=0;
                                //gti_flag=1 means stopping accumulation, gti_flag=0 means do not be bothered by the GTI
                                   //to be checked
                                    //removed < GTI_START[curr_gti_ind]
                                    //Rememeber that we are outside the current GTI, so we have gone to the next one
                                if( _x[i] - GTI_STOP[curr_gti_ind]  <= gti_threshold)
                                {
                                    cout << "Event does not exceed the GTI gap\n";
                                    gti_flag=0;
                                }
                                else
                                {
                                    cout << "GTI Threshold exceeded Stop accumulation\n";
                                    gti_flag=1;
                                }
				
                                //Increases GTI index
				curr_gti_ind++;
				if(curr_gti_ind >= n_gti)
				{
					end_flag=1;
					curr_gti_ind=n_gti-1;
				}
			}
		}
                else
                {
                    double delta_t=0;
                     if(i>0)
                        delta_t=_x[i]-_x[i-1];
                    
                     //cout <<delta_t<<endl;
                     if(delta_t >= gti_threshold)
                     {
                         //cout <<"Single GTI case, GTI threshold exceeded\n";
                         gti_flag=1;
                     }
                     else
                         gti_flag=0;
                }

		
                if(gti_flag)
                {
                    //cout <<"Entered check for break in GTI exceeded!!\n";
                    //cout <<"If not enough S/R, disregard all previous bins!!\n";
                    if (i==0)
                        continue;
                    
                    
                    if( rr <= min_sn*sqrt(ee) ) //includes the rr=ee=0 case
                    {
                        double local_t=0;

                        //remove points if not enough S/N is reached
                        for(int j=i-1;j>=0;j--)
                        {
                            //cout << j << " " << table[j] << endl;
                            if(table[j]>0)// || _x[j] < GTI_START[curr_gti_ind-1] )
                                    break;
                            if(table[j] != -2)
                                    local_t+=_dx[j];

                            //cerr << j  << " = " << table[j] << " " << local_t << endl;

                            table[j]=0;

                        }
                        if(!end_flag)
                        {
                            if (n_gti >0 )
                                cout << "\nDisregard points in GTI bin interval #" << curr_gti_ind -1 << " = [s]  " << 100*local_t/(GTI_STOP[curr_gti_ind-1] - GTI_START[curr_gti_ind-1]) << " " << endl;
                            else if (local_t>0)
                                cout <<"\nDisregard " << local_t << " s for long gap at " << _x[i] <<"\n";

                            tot_disregard_time+=local_t;
                            if (local_t <0)
                            {
                                cerr <<" Negative local_t, stop\n";
                                waitkey();
                            }
                        }

                    }
                    else
                    {
                            table[i-1]=1;
                            //cout << i << " " << rr/sqrt(ee) << " " << tt << " " << table[i] << endl;
                            //waitkey();
                    }
                    rr=0;
                    ee=0;
                    tt=0; 
                    gti_flag=0;		

                }
               
		
		//If a point alone is better than S/W threshold, we should take it by its own and 
		//discard all the previous ones
		
        if ( single_point_check=='y' && ( _y[i] / _dy[i] ) >= min_sn )
        {
            //cout << "Point " << i << " has high S/N using it\n";

            table[i]=1;
                        //remove points if not enough S/N is reached
            //cout << "Removing points\n";
            double local_t=0;
                
            for(int j=i-1;j>=0;j--)
            {
                    //cout << j << " " << table[j] << endl;
                    if(table[j]>0)// || _x[j] < GTI_START[curr_gti_ind-1] )
                            break;
                                
                    if(table[j] != -2)
                            local_t+=_dx[j];

                                //cerr << j  << " = " << table[j] << " " << local_t << endl;

                    table[j]=0;

            }


            tot_disregard_time+=local_t;
            if (local_t>0)
                cout <<"Disregard " << local_t << " time units becuase found a point with high S/N at index " << i << endl;
            
            if (local_t <0)
            {
                cerr <<" Negative local_t, stop\n";
                waitkey();
            }

            rr=0;
            ee=0;
            tt=0;
            gti_flag=0;
            continue;

        }


		rr+=_y[i];
		ee+=SQR(_dy[i]);

		//For data with large gaps. We use the time span not the bin size
		//In principle, this should be cought by the GTI, but INTEGRAL light curves in ASCII format
		//have not GTIs, when produced manually from science window lists.
		//We should remove this patch and have proper GTIs for all light curves
		//The first condition tried to catch this

		if (n_gti >1)
			tt+=_dx[i];
		else
		{
			if (i==0)
				tt+=_dx[i];
			else
				tt+=(_x[i]+_dx[i] - _x[i-1] - _dx[i-1]);
		}


                //default value continue accumulation
		table[i]=-1;
		
		if(ee>0) //to avoid div by zero
		{
			if( (rr/sqrt(ee)>=min_sn  && tt > min_bin_size ) || tt >=max_time )
			{
				//cout << "Entered loop check " << i << endl;
				if(rr/sqrt(ee)<min_sn)
				{
					//cout << "Removing points\n";
					double local_t=0;
					
                                        //remove points if not enough S/N is reached
					for(int j=i;j>=0;j--)
					{
						//cout << j << " " << table[j] << endl;
						if(table[j]>0)// || _x[j] < GTI_START[curr_gti_ind-1] )
							break;
						if(table[j] != -2)
							local_t+=_dx[j];
						
						//cerr << j  << " = " << table[j] << " " << local_t << endl;

						table[j]=0;
						
					}
					if(!end_flag)
					{
                        //cout <<"Not end flag\n";
                        if (local_t <0)
                        {
                            cerr <<" Negative local_t, stop\n";
                            waitkey();
                        }
						tot_disregard_time+=local_t;

						
						cout << "\nDisregard points at index " << i << " for " << local_t << " time units due to maximum bin length or maximum time gap\n";
						
					}

				}
				else
				{
					//cout << "Setting bin=1 at " << i << endl;
					table[i]=1;
					//cout << i << " " << rr/sqrt(ee) << " " << tt << " " << table[i] << endl;
					//waitkey();
				}
				rr=0;
				ee=0;
				tt=0;
				gti_flag=0;		
				
			}
		}
		else if (gti_flag == 1)
		{
			//cout << "GTI FLAG\n";
			double local_t=0;
			
			for(int j=i;j>=0;j--)
			{
				if(table[j]==1 )//|| _x[j] < GTI_START[curr_gti_ind-1] )
					break;
				if(table[j]!=-2)
					local_t+=_dx[j];
				table[j]=0;
				//cout << j  << " GF= " << _dx[j] << " " << local_t << endl;
				
			}
			if(!end_flag)
			{
				cout << "Disregard points in interval " << curr_gti_ind -1 << " = [s]  " << 100*local_t/(GTI_STOP[curr_gti_ind-1] - GTI_START[curr_gti_ind-1]) << " % (GTI FLAG)" << endl;
                
                if (local_t <0)
                {
                    cerr <<" Negative local_t, stop\n";
                    waitkey();
                }
                
				tot_disregard_time+=local_t;
			}
			
			gti_flag=0;			
		}

			
		//cerr << i << " " << rr << " " << sqrt(ee) << " "  << " " << rr / sqrt(ee)<< tt << " " << table[i] << endl;
		
	}//end of loop
	
	cout << "Total disregarded time is [s] " << tot_disregard_time << endl;
	
	return table;

}

int light_curve::rebin(int *table)
{

	vector<double> new_x;
	vector<double> new_dx;
	vector<double> new_y;
	vector<double> new_dy;
	vector<double> new_frac_exp;
	
	
	double nx=0, ndx=0, ny=0, ndy=0, nn=0, nf=0, tmin=1e100, tmax=-1e100;//, nn_nonnull=0;
	
	//ofstream ff("rebin_table.txt");
	//for(int i=0;i<_size;i++)
	//     ff << i << " " << table[i] << endl;
	//ff.close();
 	
	
	//-2 discard for GTI  
        //-1 continue accumulation
        //0 discard
        //1 stop accumulation

	//ofstream out_tmp("try.txt");
	for(int i=0;i<_size;i++)
	{
		
		//out_tmp << i << " " << _x[i] << " " << table[i] << endl;
		if(isnan(_y[i]) || isnan(_dy[i]))
		   continue;
			  
		if(table[i] != 0 && table[i] != -2 )
		{
			nx+=_x[i]+_dx[i]/2; //we have assumed _x as initial time of the bin
			ndx+=_dx[i];
			ny+=_y[i];//*_dx[i];
			ndy+=SQR(_dy[i]);//*_dx[i]);
			nf+=_frac_exp[i]*_dx[i];
//			if(_frac_exp[i])
//				nn_nonnull++;
			if (tmin > _x[i])
				tmin=_x[i];

			if (tmax < _x[i]+_dx[i])
				tmax=_x[i]+_dx[i];
			
			nn++;
			if(isnan(ny) || isnan(ndy))
			{
				cout << i << " " << _x[i] << " " << _dx[i] << " " << _y[i] << " " << _dy[i] << endl;
				cout << i << " " << nx << " " << ndx << " " << ny << " " << ndy << endl;
				waitkey();
				//break;
				nx=0;
				ndx=0;
				ny=0;
				ndy=0;
				nn=0;
				nf=0;
				tmin=1e100;
				tmax=-1e100;
			}
		}
		
		if(table[i]==1 || i ==  _size-1)
		{
			if(i==_size-1)
				cout << "Arrived to the end\n";
			if(nn>0 && ndx>0)
			{
				new_x.push_back(tmin); // inital time of the bin
				new_dx.push_back(tmax-tmin);
                
                //test
                //new_x.push_back(nx/nn-ndx/2);
				//new_dx.push_back(ndx);
                
				new_y.push_back(ny);
				new_dy.push_back(sqrt(ndy));
				new_frac_exp.push_back(nf / (tmax-tmin) );
				//cout << i << " " << nx/nn << " " << ndx << " " << ny/ndx << " " << sqrt(ndy)/ndx << " " << nn << endl;
				if (ny ==0)
                                {
                                    if(i == _size-1)
                                    {
                                        cerr << "WARNING :: The last bin has zero counts, you should remove it by hand\n";
                                        waitkey();
                                    }
                                    else
                                    {
                                        cerr << "WARNING :: bin with zero counts in the middle of the curve: it should not happen\n";
                                        waitkey();
                                    }
                                }
                                nx=0;
				ndx=0;
				ny=0;
				ndy=0;
				nn=0;
				nf=0;
				tmin=1e100;
				tmax=-1e100;
				
			}
			else
				cout << "No signal\n";
		}
	}
	
	if(new_x.size() == 0)
	{
		cerr << "No suitable rebinning table\n";
//		for(int i=0;i<_size;i++)
//		{
//			cout << i << " " << table[i] << endl;
//		}
		return 1;
	}
	
	deallocate_lc();
	_size=new_x.size();
	cout << "Rebinning, new LC will have " << _size << " bins\n";
	allocate(_size);
	
	for(int i=0;i<_size;i++)
	{
		_x[i]=new_x[i];
		_dx[i]=new_dx[i];
		_y[i]=new_y[i];
		_dy[i]=new_dy[i];
		_frac_exp[i]=new_frac_exp[i];
		//cout << i << " " << _x[i] << " "  << _dx[i] << " " << _y[i] << " " << _dy[i] << "\n";
	}
	
	new_x.clear();
	new_dx.clear();
	new_y.clear();
	new_dy.clear();
	new_frac_exp.clear();
	

	//out_tmp.close();
	return 0;
	
}


double light_curve::get_phase_complete(double time, double t_ref, double f, double df, double ddf, double *i_phase)
{
	// 	if(binary_corr_flag)
	// 		time = binarycor(time)
    double dt = time - t_ref;
    //double tmp2=dt*dt;
    //double phase = dt*(f + dt*( (df/2.)  + dt*((df2/6.) + dt*(df3/24.) ) ) ) + 1 - d_ph;
    double phase = dt*(f + dt*( (df/2.) + dt* (ddf/6.)  ) );
    *i_phase=floor(phase);
    return phase- (*i_phase);
}

int light_curve::compute_phase_exposure(double timeref, double f, double df, double ddf)
{

	if(n_gti==0)
	{
		cerr << "compute phase exposure GTI is not present\n";
		allocate_gti(1);
		GTI_START[0] = _t_start;
		GTI_STOP[0] = _t_stop;
	}
	
	double *en_ph_phase_exposure = _frac_exp;
		
	//cout <<"Phase exposure of '" << filename << "'\n";
	int i;
	int n_GTIs=n_gti;
	int n_phase_bins=_size;
	double if1,if2;
	double ff1,ff2 ;
	double GTI_exp=0;
	double n;
	double Norm;
	int a1,a2;
	
	double *gti_stop=GTI_STOP;
	double *gti_start=GTI_START;
	
	//for(i=0;i<n_phase_bins;i++)
	//     en_ph_phase_exposure[i]=0;                    
	
	//printf("\n add_pulse_exposure: entry");
	
	//for(i=0;i<n_GTIs;i++)
	//   printf("\n %2d) %lf %lf %lf",i+1,gti_start[i]-gti_start[0],
	//         gti_stop[i]-gti_start[0],
	//          gti_stop[i]-gti_start[i]);                                                      
	
	//GTI_exp=0;
	
	for(i=0;i<n_GTIs;i++)
	{
		
		// printf("\n %2d) %lf %lf %lf",i+1,gti_start[i]-gti_start[0],
		//       gti_stop[i]-gti_start[0],
		//        gti_stop[i]-gti_start[i]);		

		GTI_exp += gti_stop[i]-gti_start[i];
		
		ff1 = get_phase_complete(gti_start[i], timeref, f, df, ddf, &if1);
		ff2 = get_phase_complete(gti_stop[i], timeref, f, df, ddf, &if2);
		
		n = if2-if1;
		
		Norm = (gti_stop[i]-gti_start[i])/(if2+ff2-if1-ff1)/(double)n_phase_bins;
				
		a1 = (int)floor(ff1 * n_phase_bins);
		a2 = (int)floor(ff2 * n_phase_bins);
		
		// printf("\n ff1=%lf; ff2=%lf; diff=%g",ff1,ff2,ff2-ff1);
		// printf("\n a1=%d; a2=%d",a1,a2);
		
		
		if(ff1 < ff2)
		{			
			//	if (1==0)
			//		if(n==0 && a1==a2)
			//		{
			//			
			//            //printf("\n a1=%d %lf",a1,(ff2-ff1)*Norm);
			//            
			//           en_ph_phase_exposure[a1] += (ff2-ff1)*n_phase_bins*Norm;
			//			continue;
			//			
			//		}
			//		else if(a1 == a2)
			//			cerr << "Warn GTI bounds are the same\n";
			
			double n1 = (n+1.);
			double alf1 = (double)(a1+1) - ff1*n_phase_bins;
			double alf2 = ff2*n_phase_bins - (double)(a2);	
			
			n*=Norm;
			alf1*=Norm;
			alf2*=Norm;
			n1*=Norm;	
			
			int j;
			for(j=0;j<a1;j++)
			    en_ph_phase_exposure[j] += n;
			
			en_ph_phase_exposure[a1] += n + alf1;
			
			for(j=a1+1;j<a2;j++)
			    en_ph_phase_exposure[j] += n1;
			
			if (a2>a1)	
				en_ph_phase_exposure[a2] += n + alf2;
			else
				en_ph_phase_exposure[a2] += alf2-Norm;
			
			
			for(j=a2+1;j<n_phase_bins;j++)
				en_ph_phase_exposure[j] += n;
			
		}
		else // ff1> ff2 
		{
			
			double n1 = (n-1.);///factor;
			
			if(n1<0)
				cerr << "WARNING:: probable error in GTI folding, value less 0 encountered\n";
			
			double alf1 = ff2*n_phase_bins - (double)(a2);
			double alf2 = (double)(a1+1) - ff1*n_phase_bins;
			
			n*=Norm;
			alf1*=Norm;
			alf2*=Norm;
			n1*=Norm;
			
			int j;
			for(j=0;j<a2;j++)
			    en_ph_phase_exposure[j] += n;
			
			en_ph_phase_exposure[a2] += n1 + alf1;
			
            for(j=a2+1;j<a1;j++)
				en_ph_phase_exposure[j] += n1;
			
			if (a1>a2)
				en_ph_phase_exposure[a1] += n1 + alf2;
			else
				en_ph_phase_exposure[a1] +=  alf2;
			
			for(j=a1+1;j<n_phase_bins;j++)
				en_ph_phase_exposure[j] += n;
		}
	}  /* next GTI */
    //printf("\n");
	double tot_pulse_exp=0;
	
    for(int k=0;k<n_phase_bins;k++)
		tot_pulse_exp+=en_ph_phase_exposure[k];
	if(fabs(tot_pulse_exp / GTI_exp - 1) >1e-9)
	{
		cout << "\nTotal pulse exposure (sec): " << tot_pulse_exp <<endl;
		cout << "GTI exposure (sec): " << GTI_exp << endl;  // sum of GTI
		//double factor = exposure / tot ;
		cout << "Ratio is " << tot_pulse_exp / GTI_exp << endl;
		cout << "Diff (sec): " << tot_pulse_exp - GTI_exp << endl;
	}	
    //double av_ph_exp=tot_pulse_exp/n_phase_bins;
    //for(int k=0;k<n_phase_bins;k++)
	//	en_ph_phase_exposure[k]/=n_phase_bins;
	return 0;	
}

void light_curve::allocate_gti(int n)
{
	n_gti=n;
	GTI_START=new double[n_gti];
	GTI_STOP=new double[n_gti];
	
	for (int i=0;i<n_gti;i++)
	{
		GTI_START[i]=0;
		GTI_STOP[i]=0;
	}	
}


light_curve &light_curve::cut(double t1, double t2)
{
	light_curve *c=new light_curve(*this);

	if(t1>=t2)
	{
		cerr <<"Limits io cut light cure are wrong, do nothing: " << t1 << ">=" << t1 << endl;
		waitkey();
		return *c;
	}
	
	if(t1 <= GTI_START[0] && t2 >= GTI_STOP[n_gti-1]) //nothing to do
	{
		cerr << "WARNING:: limits to cut light curves are too wide, do nothing\n";
		return *c;
	}
	
	if(t2 <=  GTI_START[0] || t1 >= GTI_STOP[n_gti-1])
	{
		cerr << "Limits to cut light curves would delete everything, do nothing\n";
		waitkey();
		return *c;
	}
	
	//Start crop lightcurve
	//c->allocate(n_bins);
	
	vector<double> new_tstart;
	vector<double> new_tstop;
	
	if(n_gti >0)
	{
		for(int i=0;i<n_gti;i++)
		{
			if(t1 >= GTI_STOP[i] || t2 <= GTI_START[i]) //nothing to do
				continue;
			
			if(t1 >= GTI_START[i] && t1 < GTI_STOP[i])
			{
				new_tstart.push_back(t1);
				if(t2 >= GTI_START[i] && t2 < GTI_STOP[i])
				{
					new_tstop.push_back(t2);
					break;
				}
				else
				{
					new_tstop.push_back(GTI_STOP[i]);
				}
			}
			
			if(t1<=GTI_START[i] && t2>=GTI_STOP[i])
			{
				new_tstart.push_back(GTI_START[i]);
				new_tstop.push_back(GTI_STOP[i]);
			}
			
			if(t1<= GTI_START[i] && t2<=GTI_STOP[i])
			{
				new_tstart.push_back(GTI_START[i]);
				new_tstop.push_back(t2);
				break;
			}
		}
	
		
		if(new_tstart.size() != new_tstop.size())
		{
			cerr << "Error, GTI of cropped curve are not consistent, do nothing\n";
			cerr << new_tstart.size() << " " << new_tstop.size() << endl;
			waitkey();
			return *c;
		}
		
		if(new_tstart.size() < 1)
		{
			cerr << "WARNING, GTI of cropped curve areempty, do nothing\n";
			//cerr << new_tstart.size() << " " << new_tstop.size() << endl;
			//waitkey();
			return *c;
		}
		c->deallocate_gti();

		c->allocate_gti(new_tstart.size() );
		for (int i=0;i<c->n_gti;i++)
		{
			c->GTI_START[i]=new_tstart[i];
			c->GTI_STOP[i]=new_tstop[i];
		}
	}
	else
	{
		c->deallocate_gti();

		c->allocate_gti(1);
		c->GTI_START[0] = t1;
		c->GTI_STOP[0]  = t2;
	}
	
	cout << "Cropped GTI\n";
	
	new_tstart.clear();
	new_tstop.clear();
	
	vector<double> new_x;
	vector<double> new_dx;
	vector<double> new_y;
	vector<double> new_dy;
	vector<double> new_fe;
	
//	double nx=0, ndx=0, ny=0, ndy=0, nn=0;
	
	for(int i=0;i<_size;i++)
	{
		
		//cout << i << " " << table[i] << endl;
		double x1=_x[i];
		double x2=x1+_dx[i];
		
		if(t1 >= x2 || t2 <= x1)
			continue;
		
		if (x1 >= t1 && x2 <= t2)
		{
			new_x.push_back(_x[i]);
			new_dx.push_back(_dx[i]);
			new_y.push_back(_y[i]);
			new_dy.push_back(_dy[i]);
			new_fe.push_back(_frac_exp[i]);
		}
		/*
		else if( x1 >= t1 && x2 >= t2)
		{
			new_x.push_back(_x[i]);
			new_dx.push_back(_dx[i]);
			new_y.push_back(_y[i]);
			new_dy.push_back(_dy[i]);
			new_fe.push_back((t2-x1)/_dx[i]);
		}
		else if( x1 <= t1 && x2 <= t2)
		{
			new_x.push_back(_x[i]);
			new_dx.push_back(_dx[i]);
			new_y.push_back(_y[i]);
			new_dy.push_back(_dy[i]);
			new_fe.push_back((x2-t1)/_dx[i]);
		}
		 */
		
	}
	
	if(new_x.size() == 0)
	{
		cerr << "No bins in cropped light curve, do nothing\n";
		new_x.clear();
		new_dx.clear();
		new_y.clear();
		new_dy.clear();
		new_fe.clear();
		return *c;
	}
	
	c->deallocate_lc();
	cout << "Rebinning, new LC will have " << _size << " bins\n";
	c->allocate(new_x.size());
	
	for(int i=0;i<c->size();i++)
	{
		c->_x[i]=new_x[i];
		c->_dx[i]=new_dx[i];
		c->_y[i]=new_y[i];
		c->_dy[i]=new_dy[i];
		c->_frac_exp[i]=new_fe[i];
		//cout << i << " " << _x[i] << " "  << _dx[i] << " " << _y[i] << " " << _dy[i] << "\n";
	}
	
	new_x.clear();
	new_dx.clear();
	new_y.clear();
	new_dy.clear();
	new_fe.clear();

	
	return *c;
}

void light_curve::init_folded()
{
	double n_bins=_size;
	for(int i=0;i<n_bins;i++)
	{
		_x[i]=((double)i+0.5)/n_bins;
		_dx[i]=1./(double)n_bins;
	}
}

light_curve *light_curve::fold(int n_bins, double timeref, double f, double df, double ddf, int weight_method, double w_f)
{
	light_curve *c=new light_curve(*this);
	c->deallocate_lc();
	c->allocate(n_bins);
	
	double *exposure=c->_frac_exp;
	double *pulse=c->_y;
	double *err_pulse=c->_dy;
	
	for(int i=0;i<n_bins;i++)
	{
		exposure[i]=0;
		pulse[i]=0;
		err_pulse[i]=0;
		c->_x[i]=((double)i+0.5)/n_bins;
		c->_dx[i]=1./(double)n_bins;
	}
	
	
	int flag_exp = c->compute_phase_exposure(timeref, f, df, ddf); //0 if done, 1 if not
        //cout <<endl<< "Flag_exp = " << flag_exp << endl << endl;
	
	get_phase_complete(_t_start, timeref, f, df, ddf, &_phase_t_start);
	
	//weighting
	double vs=0;
	if(weight_method==3)
	{
		vs=get_vs(w_f);
		//cout << "Vs is " << vs << endl;
	}

	for(int i=0;i<_size;i++)
	{
		if(_frac_exp[i] ==0 || (_y[i]==0 && _dy[i]==0) ) //efficiency 
			continue;
		
		double if1=0, if2=0;
		double f1 = get_phase_complete(_x[i], timeref, f, df, ddf, &if1);
		double f2 = get_phase_complete(_x[i]+_dx[i], timeref, f, df, ddf, &if2);
		
		
		int i1 = (int)floor(f1 * n_bins);
		int i2 = (int)floor(f2 * n_bins);
		int n = i2-i1;

		double w=1;//./_dx[i];
		double ee=SQR(_dy[i]);

		if(weight_method==2)
		{
			if(_dy[i]>0)
			{
				w*=1/SQR(_dy[i]);
				ee=w;
			}
		}
		
		if(weight_method==3)
		{
			w*=1/(SQR(w_f*_dy[i])+vs);
			ee=w;
		}


		if(i1==i2)
		{
			c->_y[i1]+=_y[i]*w;//*_dx[i];
			c->_dy[i1]+=ee;//*_dx[i]);
                        if(flag_exp)
                        {
                            c->_frac_exp[i1]+=_dx[i]*_frac_exp[i]*w;
                        }
			continue;
		}
		
		if(n==1)
		{
//			cerr << f1 << " " << f2 << endl;
//			cerr << if1 << " " << if2 << endl;
//			cerr << i1 << " " << i2 << endl;
			f1*=n_bins;
			f2*=n_bins;
//			cerr << f1 << " " << f2 << endl;
			double tot=f2-f1;
			f1=(ceil(f1)-(f1))/tot;
			f2=(f2-floor(f2))/tot;
			
			if( fabs(f1+f2-1)>1e-6)
			{
				cerr << "Wrong in folding\n";
				cerr << f1 << " " << f2 << " " << f1+f2 << endl;
				waitkey();
				continue;
			}
			
			c->_y[i1]+=_y[i]*f1*w;//*_dx[i];
			c->_dy[i1]+=ee*SQR(f1);//*_dx[i]);
			
			
			c->_y[i2]+=_y[i]*f2*w;//*_dx[i];
			c->_dy[i2]+=ee*SQR(f2);//*_dx[i]);

			if(flag_exp)
			{
				c->_frac_exp[i1]+=f1*_dx[i]*_frac_exp[i]*w;
				c->_frac_exp[i2]+=f2*_dx[i]*_frac_exp[i]*w;
			}

			continue;
		}

		if( -n==n_bins-1)
		{
			//i2+=n_bins;
			//temporary to share the bin
	//		cerr << f1 << " " << f2 << endl;
//			cerr << if1 << " " << if2 << endl;
//			cerr << i1 << " " << i2 << endl;
			f1*=n_bins;
			f2=(1+f2)*n_bins;
//			cerr << f1 << " " << f2 << endl;
			double tot=f2-f1;
			f1=(ceil(f1)-(f1))/tot;
			f2=(f2-floor(f2))/tot;
			
			if( fabs(f1+f2-1)>1e-6)
			{
				cerr << "Wrong in folding wrapping\n";
				cerr << f1 << " " << f2 << " " << f1+f2 << endl;
				waitkey();
				continue;
			}
			
			c->_y[i1]+=_y[i]*f1*w;//*_dx[i];
			c->_dy[i1]+=SQR(f1)*ee;//*_dx[i]);
			//i2-=n_bins;
			
			c->_y[i2]+=_y[i]*f2*w;//*_dx[i];
			c->_dy[i2]+=SQR(f2)*ee;//*_dx[i]);
                        
                        if(flag_exp)
                        {
                            c->_frac_exp[i1]+=f1*_dx[i]*_frac_exp[i]*w;
                            c->_frac_exp[i2]+=f2*_dx[i]*_frac_exp[i]*w;
                        }
			continue;
		}
		
		
		if( (n>1)|| (n <0))
		{
			cerr << "Wrong in folding, use finer time bins in your lc or less phase bins\n";
			cerr <<  scientific << setw(19) << setprecision(12) << _x[i] << " " << _dx[i] << " " << i << endl;
                        cerr << "Delta t " << _x[i] -timeref << endl;
                        cerr << f1 << " " << f2 << endl;
			cerr << if1 << " " << if2 << endl;
			cerr << i1 << " " << i2 << endl;
			waitkey();
			continue;
		}
		
		   
		
	}
	
	//if(flag_exp==0)
	c->divide_frac_exp_sqrt_err(); //now computed in any case (23/05/2012)!!
//	else if (c->ontime() >0)
//	{
//            c->sqrt_err();
//
//            if(weight_method>1)
//                c->invert_err();
//		// *c /= (c->ontime()*n_bins);
//	}
	return c;
}

void light_curve::divide_frac_exp_sqrt_err()
{
//	cout << "Divide for factional exposure\n";
	
	for(int i=0;i<_size;i++)
	{
		
			// if (_frac_exp[i] < 0.8)
			// 	cout << i << " " << _frac_exp[i] << " " << _y[i] << endl;
			if(_frac_exp[i]>0)
			{
				//cout << "Divide 1 " << _y[i] << " " << _frac_exp[i] << endl;
				_y[i]/=_frac_exp[i];///(_ontime*_size);
				_dy[i]=sqrt(_dy[i])/_frac_exp[i];//*(_ontime*_size);
			}
			else
			{
				//cout << "Set to Zero " << _y[i] << endl;

				_y[i]=0;
				_dy[i]=0;
			}
	}
	
}

void light_curve::output_lc(char *fname, bool is_phase, double min_frac_exp, double t_0)
{
		
	ofstream ff(fname);
	if(!ff.is_open())
	{
		cerr << "Error opening " << fname << endl;
		return;
	}
	
	ff << "read serr 1 2\n";
	ff<<"cpd /xw\n";
	ff << "scr white\n";
//	ff <<"ma 1 on 2\n";
	ff <<"lw 2\n";
	ff <<"cs 1.1\n";
	ff <<"font roman\n";
	ff <<"time off\n";
	ff <<"lab title " << object << " observed by " << instrume << endl;
	ff <<"lab file From " << _t_start << " to " << _t_stop << " at resolution " << _time_del << " " << timeunit << endl;
	if (is_phase)
		ff <<"lab x Phase\n";
	else
		ff <<"lab x Time [" << time_unit() << "]\n";
        ff <<"col off 3\n";
	ff << "lab rate\n";

        ff << "! X DX Y DY FRAC_EXP (FRAC_EXP plotting is suppressed)\n";

	for(int i=0;i<_size;i++)
	{
		if (_frac_exp[i] >= min_frac_exp)
		{
			ff << scientific << setw(19) << setprecision(12) << _x[i]-t_0 << "\t";
			ff << scientific << setw(19) << setprecision(12) << _dx[i]/2 << "\t";
			ff << scientific << setw(19) << setprecision(12) << _y[i]  << "\t";
			ff << scientific << setw(19) << setprecision(12) << _dy[i] << "\t";
			ff << scientific << setw(19) << setprecision(12) << _frac_exp[i] << "\n";
		}
	}
	//cerr <<"Is Phase " << is_phase << endl;
	if (is_phase)
	{
		for(int i=0;i<_size;i++)
		{
			if (_frac_exp[i] >= min_frac_exp)
			{
				ff << scientific << setw(19) << setprecision(12) << _x[i]- t_0 + 1 << "\t";
				ff << scientific << setw(19) << setprecision(12) << _dx[i]/2 << "\t";
				ff << scientific << setw(19) << setprecision(12) << _y[i]  << "\t";
				ff << scientific << setw(19) << setprecision(12) << _dy[i] << "\t";
				ff << scientific << setw(19) << setprecision(12) << _frac_exp[i] << "\n";
			}
		}

	}

	
	ff.close();
	
}


int light_curve::get_z2_period(char flag_binarycor, char *binary_file, char log_spacing, double nu_start, double nu_stop, int n_bins, int n_harm, int flag_out, const char *filename,  double t_ref, double &best_freq, double &err_best_freq, double min_sn)
{
	

	int status=0;
	
	if(flag_binarycor == 'y' && strcmp(timeref, "BINARY") )
	{
		//this should be already ok
		binary_corr(binary_file);
	}
	
	VecDoub frequency(n_bins), z2(n_bins), lz2(n_bins);
	double step=(nu_stop-nu_start)/(n_bins);
	
	if(log_spacing == 'n')
	{
		for(int i=0;i<n_bins;i++)
		{
			frequency[i]=nu_start+(i+0.5)*step;
		}
	}
	else
	{
		double tmp=log10(nu_start);
		step=(log10(nu_stop)-tmp)/n_bins;
		for(int i=0;i<n_bins;i++)
		{
			frequency[i]=pow(10,tmp+(i+0.5)*step);
		}
	}
	
	
	for(int i_nu=0;i_nu<n_bins;i_nu++)
	{
		z2[i_nu]=0;
		lz2[i_nu]=0;
	}
	
	vector<double> times;
	double n_events=0;
	get_times(times, min_sn);
	
	int n=times.size();
	
	for(int i_nu=0;i_nu<n_bins;i_nu++)
	{
		
		for(int j=1;j<=n_harm;j++)
		{
			double tmp1=0, tmp2=0;
			for(int i=0;i<n;i++)
			{
				
				double phase=(times[i]-t_ref)*frequency[i_nu];
				phase=phase-floor(phase);
				
				tmp1+=cos(2*M_PI*j*phase);
				tmp2+=sin(2*M_PI*j*phase);
				//cout << phase  << " " << tmp1 << " " << tmp2 << " " << lz2[i_nu] << endl;
			}
			lz2[i_nu]+=tmp1*tmp1+tmp2*tmp2;
		}
		
	}
	n_events+=times.size();
	
	times.clear();
	

	//return 0;
	
	cout << "Accumulated " << n_events << " equivalent events\n";
	if(n_events == 0)
	{
		cerr << "No events, returning\n";
		return 1;
	}
	int max_ind=0;
	double max_value=-10;
	
	for(int i_nu=0;i_nu<n_bins;i_nu++)
	{
		//lz2[i_nu]/=n;
		//cout << lz2[i_nu] << " " << lz2[i_nu]/n_events << endl;
		z2[i_nu]=lz2[i_nu]/n_events*2.0;
		if(z2[i_nu]>max_value)
		{
			max_value=z2[i_nu];
			max_ind=i_nu;
		}
	}
	
	cout << "Maximum of Z2 statistics is at " << frequency[max_ind] << " Hz\n";
	
	if(flag_out)
	{
		ofstream ff(filename);
		if(!ff.is_open())
		{
			cerr << "Error in opening " << filename << endl;
			return 1;
		}
		
		ff << "read\n";
		ff<<"cpd /xw\n";
		if(log_spacing =='y')
			ff <<"log x on\n";
		else
			ff <<"log x off\n";
		
		//ff <<"ma 1 on 2\n";
		ff <<"lw 2\n";
		ff <<"r y 0. " << 8*n_harm << "\n";
		ff <<"cs 1.1\n";
		ff <<"plot vert\n";
		ff <<"font roman\n";
		ff <<"time off\n";
		ff <<"lab title\n";
		ff <<"lab x Frequency [Hz]\n";
		ff <<"LAB  2 COL 4 LIN 0 100 JUS Lef\n";
		ff <<"LAB  2 POS 1e-2 "<< 2*n_harm << "\" \" \n";	
		ff <<"LAB  3 COL 5 LIN 0 100 JUS Lef\n";
		ff <<"LAB  3 POS 1e-2 "<< 2*n_harm + sqrt(2*n_harm) << "\" \" \n";	
		
		ff << "lab y2 Z\\u2\\d\n";
		ff << "lab y3 P (%)\n";	
		ff << "lab y4 \\gs\n";
		
		ff << "lab file " << 2 * n_harm << " dof\n";
		Chisqdist chi2(2*n_harm);
		Normaldist gaus;
		
		
		for(int i_nu=0;i_nu<n_bins;i_nu++)
		{
			//z2[i_nu]/=n_files;
			double prob = chi2.cdf(z2[i_nu]);
			ff <<  scientific << setw(19) << setprecision(12) << frequency[i_nu] << "\t" << 
			setw(19) << setprecision(12) << z2[i_nu] << "\t" << setw(19) << setprecision(12) << prob * 100.0 << "\t";
			
			double sig=0;
			if(prob>0 && prob<0.5)
				prob=1-prob;
			
			if(prob>=0.5 && prob<1)
				sig=(gaus.invcdf(prob));
			
			if(prob==1)
				sig=100;
			
			ff << setw(19) << setprecision(12) << sig << endl;
		}
		
		ff.close();
	}
	
	
	
	string fn = "fit_";
	fn+=filename;
	double err_best_freq_u=0, err_best_freq_d=0;
	status+=get_best_frequency(frequency, z2, best_freq, err_best_freq_u, err_best_freq_d, max_value, (char *)fn.c_str(), flag_out);
	err_best_freq=(err_best_freq_u+err_best_freq_d)/2;
	return status;

	
}

int light_curve::build_gti(double max_allowed_gap)
{
	int status = 0;

	vector<double> bti_start, bti_stop;
	double delta = 0.5 -  _timepixr ;
	cout << "build_GTI: Delta = " << delta << endl;
	for(int kk = 0; kk<_size-1;kk++)
	{
		if ( ( ( _x[kk+1] - (1-delta)*_dx[kk+1] ) - (_x[kk] + _dx[kk]*(1+delta) ) ) > max_allowed_gap )
		{
			// cout << "Pushing BTI\n";
			bti_start.push_back(_x[kk] + _dx[kk]*(1+delta));
			bti_stop.push_back(_x[kk+1] - _dx[kk+1]*(1-delta));
		}
	}

	vector<double> gti_start, gti_stop;
	int kk=0;
	gti_start.push_back(_x[kk] - (1-delta)*_dx[kk]);
	for(kk=0;kk<bti_start.size();kk++)
	{
		gti_stop.push_back(bti_start[kk]);
		gti_start.push_back(bti_stop[kk]);
	}
	
	kk=_size-1;
	gti_stop.push_back(_x[kk] + _dx[kk]*(1+delta));

	if (GTI_START)
		delete [] GTI_START;
	if (GTI_STOP)
		delete [] GTI_STOP;
	n_gti =  gti_start.size();
	GTI_START = new double[n_gti];
	GTI_STOP = new double[n_gti];

	for(kk=0;kk<n_gti;kk++)
	{
		// cout << "GTI #" << kk << endl;
		GTI_START[kk] = gti_start[kk];
		GTI_STOP[kk] = gti_stop[kk];
	}

	return status;
}

int light_curve::detrend(double timescale, char flag_out, char *fname)
{
    //use sliding window to detrend with moving average
    int i=0;
    double t1=_x[0];
    double t2=t1+timescale/2;
    //double new_dx=0;
    double new_x=0;
    double new_y=0;
    //double new_dy=0;

    vector<double> new_x_v, new_y_v;

    //first run
    while(_x[i]<t2)
    {
        //new_x+=_x[i];
        new_y+=_y[i];
    //    new_dx+=_dx[i];
      //  new_dy+=_dy[i];

        i++;
    
    }
    if(i<=0)
    {
        cerr <<"Error in detrend, time window is too small\n";
        return 1;
    }
    int start_bin=0;
    int n_bins=i;

    //new_x/=n_bins;
    new_y/=n_bins;

    n_bins*=2; //we used just half interval for our first point
    new_x_v.push_back(_x[0]);
    new_y_v.push_back(new_y);

    start_bin=0;
    while(start_bin < _size)
    {
        int stop_bin=MIN(start_bin+n_bins,_size);
        for(i=start_bin;i<stop_bin;i++)
        {
            if( (_x[i]-_x[start_bin]) > timescale*3. )
            {

                //cout << "Interrupt accumulation for detrend\n";
                break;
            }
            new_x+=_x[i];
            new_y+=_y[i];
        }

        int n_bins_actual=i-start_bin;

        if(n_bins_actual>0)
        {
            new_x/=n_bins_actual;
            new_y/=n_bins_actual;
            if(new_x - new_x_v[new_x_v.size()-1] > timescale/3.)
            {
                new_x_v.push_back(new_x);
                new_y_v.push_back(new_y);
            }

        }
        else
            cerr <<"WARNING :: not usable bin in detrend\n";
        new_x=0;
        new_y=0;
        int increment=(n_bins_actual)/2;
        if(increment < 1)
            increment=1;
        start_bin+=increment;
        //cout << start_bin << endl;
    }

    for(i=_size-n_bins/2;i<_size;i++)
    {
            //new_x+=_x[i];
            new_y+=_y[i];

    }
    int n_bins_actual=n_bins/2;

    if(n_bins_actual>0)
    {
        //new_x/=n_bins_actual;
        new_y/=n_bins_actual;
        new_x_v.push_back(_x[_size-1]);
        new_y_v.push_back(new_y);


    }
    else
        cerr <<"WARNING :: not usable bin in detrend\n";

    int nn=new_x_v.size()-1;

    if(new_x_v[nn] -new_x_v[nn-1]<timescale )
    {
        new_x_v.pop_back();
        new_y=new_y_v[nn];
        new_y_v.pop_back();
        new_y_v.pop_back();
        new_y_v.push_back(new_y);
        new_x_v[nn-1]=_x[_size-1];
    }
    
    VecDoub_I xx(new_x_v);
    VecDoub_I yy(new_y_v);

    
    ofstream ff;
    if(flag_out)
    {
        ff.open(fname);
        ff << "read\ncpd /xw\nmark 17 on 3\nline on 2\nmark 1 on 1\nskip single\n";
    }

    Spline_interp sp(xx,yy);
    for(i=0;i<_size;i++)
    {
        double zz=sp.interp(_x[i]);
        if(flag_out)
            ff << scientific << setw(19) << setprecision(12) << _x[i]-_x[0] << " " << _y[i] << " " << zz << endl;
        _y[i]-=zz;
    
    }
    
    if(flag_out)
    {
        ff << "NO NO NO\n";
        for(i=0;i<new_x_v.size();i++)
        {
            ff << xx[i]-_x[0] << " " << yy[i] << " NO\n";
        }

        ff.close();
    }

    new_x_v.clear();
    new_y_v.clear();

    return 0;
}

int light_curve::crop_gti(double t1, double t2)
{
	
	if(t2 <= t1)
	{
		cerr <<"No crop GTI possible t2>= t1\n";
		cerr <<"t1 = " << t1 <<endl;
		cerr <<"t2 = " << t1 <<endl;
		return 1;
	}

	int status=0;
	
	if (n_gti == 0)
	{
		allocate_gti(1);
		if (t1 > _t_start)
			GTI_START[0]=t1;
		else
			GTI_START[0]=_t_start;

		if (t2 < _t_stop)
			GTI_START[0]=t2;
		else
			GTI_START[0]=_t_stop;
		
		return status;
	}

	vector<double> new_gti_start;
	vector<double> new_gti_stop;
	
	for(int i=0;i<n_gti;i++)
	{
		
		//cout <<  scientific << setw(19) << setprecision(12) << t1 << " " << t2 << " " << GTI_START[i] << " " <<  GTI_STOP[i] << endl;
		if(t1 > GTI_STOP[i] || t2 < GTI_START[i])
			continue;
		
		//cout << "Pippo\n";
		
		if(t1>= GTI_START[i] && t1 < GTI_STOP[i])
		{
			new_gti_start.push_back(t1);
//			cout << "Pippo 1\n";
		}
		
		if(t2>= GTI_START[i] && t2 < GTI_STOP[i])
		{
			new_gti_stop.push_back(t2);
//			cout << "Pippo 2\n";
		}
		
		if(t1<GTI_START[i])
		{
			new_gti_start.push_back(GTI_START[i]);
			//cout << "Pippo\n";
		}
		
		if(t2>=GTI_STOP[i])
		{
			new_gti_stop.push_back(GTI_STOP[i]);
			//cout << "Pippo\n";
		}
		
	}
	
	if(new_gti_start.size() != new_gti_stop.size())
	{
		cerr << "GTI crop, sizes of cropped gti differes, error, skip !!\n";
		cerr << new_gti_start.size() << " " << new_gti_stop.size() << endl;
		return 1;
	}
	
	deallocate_gti();	
	allocate_gti(new_gti_stop.size());
	
//	cout << "Crop allocate "  << n_gti << endl;
	_ontime=0;
	for(int i=0;i<n_gti;i++)
	{
		//cout << i << " " << new_gti_start[i] << " " << new_gti_stop[i] << endl;
		GTI_STOP[i]=new_gti_stop[i];
		GTI_START[i]=new_gti_start[i];
		_ontime+=GTI_STOP[i]-GTI_START[i];

	}
	
	new_gti_stop.clear();
	new_gti_start.clear();
	
	
	return status;
}

int light_curve::get_phase_shift()
{
	
	return 0;
}


#define ERROR_CODE_INPUT 1

int light_curve::update_tphase(char *fname, double back_level)
{
	
	if(fabs(average() + back_level) < 1e-5)
	{
		cerr <<" WARN, empty pulse, skip\n";
		return 0;
	}
	
	fitsfile *fptr;
	int status=0;
	if(fits_open_file(&fptr, fname, READWRITE, &status))
	{
		cerr << "Error opening '" << fname <<"'\n";
		return ERROR_CODE_INPUT;
	}
	
	long nrows=0;
	int hdutype=0;
	fits_movabs_hdu(fptr, 2, &hdutype, &status);
	fits_get_num_rows(fptr, &nrows, &status);
	if(status)
	{
		cerr <<"Error in finding nrows in '"<<fname<<"'\n";
		return status;
	}
	
	long firstrow= nrows+1, firstelem=1;
	double tmp= GTI_START[0];
	fits_write_col(fptr, TDOUBLE, 1, firstrow, firstelem, 1, &tmp , &status);
	tmp= GTI_STOP[n_gti-1];
	fits_write_col(fptr, TDOUBLE, 2, firstrow, firstelem, 1, &tmp , &status);
	fits_write_col(fptr, TDOUBLE, 3, firstrow, firstelem, _size, _y , &status);
	fits_write_col(fptr, TDOUBLE, 4, firstrow, firstelem, _size, _dy , &status);
	fits_write_col(fptr, TDOUBLE, 5, firstrow, firstelem, _size, _frac_exp , &status);
	//for(int i=0;i<_size;i++)
	//	cout << _y[i] << " " << _dy[i] << endl;
	if(status)
	{
		fits_report_error(stderr, status);
		cerr <<"Error writing elements in '"<<fname<<"'\n";
		return status;
	}

	fits_close_file(fptr, &status);
	
	return status;
	
}//end of update tphase

void light_curve::make_rate()
{
//Always counts
	if(is_rate == 0)
	{
		for(int j=0;j<_size;j++)
		{
			double norm=_dx[j]*_frac_exp[j];
			if(norm)
			{
				_y[j]/=norm;
				_dy[j]/=norm;
			}
		}
		is_rate=1;
	}
}

int light_curve::init_tphase(char *fname, int n_bins, double t_ref, double period, double df, double df2)
{
	
	int status=0;
	//char fname[FLEN_FILENAME];
	//sprintf(fname, "!%s_TPHASE.fits", object);
	fitsfile *fptr;
	fits_create_file(&fptr, fname, &status);
	if(status)
	{
		cerr <<"Error creating Time Phase matrix '" << fname <<"'\n";
		exit(ERROR_CODE_INPUT);
	}
	char extname[] = "TPHMATR";       /* extension name */
	//char spefilen[80];    /* name for the spectral file */
	char tmp[6];
	sprintf(tmp, "%dE", n_bins);
	char *ttype[] = {"T_START", "T_STOP",   "MATRIX", "Error", "Exposure"};
	char *tform[] = {"1D",      "1D",       tmp    , tmp, tmp};
	char *tunit[] = {"s",       "s",   "counts/s",  "counts/s", "s"};
	int tfields=5;
	
	
	if ( fits_create_tbl(fptr, BINARY_TBL, 0, tfields, ttype, tform,
						 tunit, extname, &status) )
	{
		fits_report_error(stderr, status );
		exit(status);
	}
	char keyword[FLEN_KEYWORD];
	int itmp;
	if(add_key_matrix(fptr, t_ref, period, df, df2))
	{
		cerr << "Error in writing keyword in the ENERGY-PHASE matrix\n";
	}
	
	{
		itmp=_size;
		sprintf(keyword,"NPHASES");
		//sprintf(keyvalu,  "%f", spec->exposure);
		char keycomm[] = "number of phase channels in the pulses";
		fits_update_key( fptr,  TINT, keyword, &itmp, keycomm, &status);
	}
	// 	{
	// 		sprintf(keyword,"BINCORR");
	// 		char 
	// 	//sprintf(keyvalu,  "%f", spec->exposure);
	// 		char keycomm[] = "1 if times are at systm barycenter, 0 if at solar system ";
	// 		fits_update_key( fptr,  TBYTE, keyword, &binary_corr_flag, keycomm, &status);
	// 	}
	
	fits_close_file(fptr, &status);
	if(status)
	{
		cerr <<"Error closing Time Phase matrix '" << fname <<"'\n";
		return status;
	}
	
	return status;
}

int light_curve::add_key_matrix(fitsfile *fptr, double t_ref, double period, double df, double df2)
{
	
	char keyword[FLEN_KEYWORD];
	char keycomm[FLEN_COMMENT];
	int status=0;
	
	fits_update_key( fptr,  TSTRING, "OBJECT", object, "name of observed object", &status);
	sprintf(keyword, "TREF");
	double tmp = t_ref;
	sprintf(keycomm,  "[INTEGRAL seconds] ref. time for the folding");
	fits_update_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status);
	fits_report_error(stderr, status);
	
	sprintf(keyword, "FREQ");
	tmp = period;
	sprintf(keycomm,  "[s] spin period");
	fits_update_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status);
	fits_report_error(stderr, status);
	
	sprintf(keyword, "dFREQ");
	tmp = df;
	sprintf(keycomm,  "[s/s] spin period first derivative");
	fits_update_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status);
	fits_report_error(stderr, status);
	
	sprintf(keyword, "ddFREQ");
	tmp = df2;
	sprintf(keycomm,  "[s/s^2] spin period second derivative");
	fits_update_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status);
	fits_report_error(stderr, status);
	
	return status;
	//ORIGIN            
	//IMAGETYP
		
}

/*
int phase::compute_phi()
{
	
	double I_s=0, I_c=0, I_s2=0, I_c2=0, alpha_0=0, A=0, phi0=0, phi0_2=0;
	double I_s_err=0, I_c_err=0, I_s2_err=0, I_c2_err=0, alpha_0_err=0, A_err=0, phi0_err=0, phi0_2_err=0;
	string stmp = "dphi_"+src_name+".qdp";
	ofstream norma(stmp.c_str());
	stmp = "times_"+src_name+".qdp";
	ofstream time_file(stmp.c_str());
	
	
	norma << "read ser 1 2 3 4 5\n";
	norma << "cpd /XW\n";
	norma << "plot vertical\n";
	norma << "lab x (t- t_ref) [s] t_ref=" << t_ref << "\n";
	norma << "lab y2 \\ga\\d0\\u\n";
	norma << "lab y3 A\n";
	norma << "lab y4 \\gf\n";
	norma << "lab y5 \\gf\\d2\\u\n";
	
	time_file << "read ser 1 2 3\n";
	time_file << "cpd /XW\n";
	time_file << "plot vertical\n";
	time_file << "lab x time [MJD]\n";
	time_file << "lab y2 \\gDt\n";
	time_file << "lab y3 \\gn\\gf\n";
	
	
	
	for(int j=0;j<n_pulses;j++)
	{
		double *pulse = pulses+j*n_phases_mat;
		double *pulse_err = err_pulses+j*n_phases_mat;
		
		for(int i=0;i<n_phases_mat;i++)
		{
			float phase = 2.* M_PI * ( ((float)i + 0.5)/((float)n_phases_mat) ) ;
			alpha_0+=pulse[i];
			I_c += cos(phase) * pulse[i];
			I_s += sin(phase) * pulse[i];
			
			double tmp = pulse_err[i] * pulse_err[i];
			alpha_0_err+=tmp;
			I_c_err += cos(phase)*cos(phase) * tmp;
			I_s_err += sin(phase)*sin(phase) * tmp;
			
			I_c2 += cos(2*phase) * pulse[i];
			I_s2 += sin(2*phase) * pulse[i];
			
			I_c2_err += cos(2*phase)*cos(2*phase) * tmp;
			I_s2_err += sin(2*phase)*sin(2*phase) * tmp;
		}
		
		alpha_0/=2.0*M_PI*n_phases_mat;
		alpha_0_err = sqrt(alpha_0_err)/2./M_PI/n_phases_mat;
		
		I_c /= M_PI;
		I_s /= M_PI;
		
		I_c_err /= M_PI * M_PI;
		I_s_err /= M_PI * M_PI;
		
		
		I_c2 /= M_PI;
		I_s2 /= M_PI;
		
		I_c2_err /= M_PI * M_PI;
		I_s2_err /= M_PI * M_PI;
		
		if(I_c!=0)
		{
			phi0 = atan2(I_s, I_c);
			double x=I_s/I_c;
			double dtgx = 1/(1+x*x);
			phi0_err = dtgx /fabs(I_c) * sqrt( I_s_err + x*x * I_c_err);
			
			//second harmonic
			phi0_2 = atan2(I_s2, I_c2);
			x=I_s2/I_c2;
			dtgx = 1/(1+x*x);
			phi0_2_err = dtgx /fabs(I_c2) * sqrt( I_s2_err + x*x * I_c2_err);
			
			A = sqrt(I_c*I_c+I_s*I_s+I_c2*I_c2+I_s2*I_s2);
			A_err =  sqrt(I_s *I_s* I_s_err + I_c * I_c * I_c_err + I_s2 *I_s2* I_s2_err + I_c2 * I_c2 * I_c2_err) / fabs(A) ;
			A/=n_phases_mat;
			A_err/=n_phases_mat;
			I_s_err = sqrt(I_s_err);
			I_c_err = sqrt(I_c_err);
			phi0 /= 2*M_PI;
			//phi0 += 0.5;
			phi0_err /= 2*M_PI;
			phi0_2 /= 2*M_PI;
			//phi0 += 0.5;
			phi0_2_err /= 2*M_PI;
			I_s2_err = sqrt(I_s2_err);
			I_c2_err = sqrt(I_c2_err);
			
		}
		
		//Cleaning 
		// 		if(phi0_err > 0.2 || alpha_0/alpha_0_err < 3  )
		// 			continue;
		
		double dt = t_start[j] - t_ref;
		double phase = (dt*(f + dt*( (df/2.) + dt* (df2/6.)  ) ) );
		
		phase = phi0 + floor(phase);
		double new_time = get_dt(phase);
		double new_time_error = get_dt(phase+phi0_err);
		new_time_error -= new_time;
		
		norma.setf(ios_base::scientific,ios_base::floatfield);
		double time = (t_start[j]+t_stop[j])/2.;
		// 		if(f && t_ref)
		// 		{
		// 			double iph=0;
		// 			get_phase_complete(time, &iph);
		// 			time = get_dt(iph+phi0);
		// 		}
		norma << setw(19)<< setprecision(12)
		<< dt << "\t" << (-t_start[j]+t_stop[j])/2. << "\t"
		<< setw(11)<< setprecision(4)
		<< alpha_0 << "\t" << alpha_0_err << "\t"
		<< A << "\t" << A_err << "\t"
		<< phi0 << "\t" << phi0_err << "\t"
		<< phi0_2 << "\t" << phi0_2_err << "\n";
		dt=new_time;
		phase = (dt*(f + dt*( (df/2.) + dt* (df2/6.)  ) ) );
		// 		if(phase -floor(phase) > 0.82)
		// 			phase = floor(phase)+1;
		// 		else
		phase = floor(phase)+0.5;
		
		time_file<< setw(19)<< setprecision(12)
		<< (new_time+t_ref)/86400.+51544. << "\t" << new_time_error/86400. << "\t"
		<< (new_time-get_dt(phase))/86400. << "\t" << new_time_error/86400. << "\t"
		<< phi0/f/86400. << "\t" << phi0_err/f/86400.
		<< "\n";
		
		// 	cout << time << "\t" << delta_t << "\t" << alpha_0 << "\t" 
		// 		<< alpha_0_err << "\t" << I_c << "\t" << I_c_err << "\t" << I_s << "\t" << I_s_err << "\n";
		
		
	}
	norma.close();
	time_file.close();
	
	return CODE_SUCCESS;
}
*/


#define ERROR_CODE_INPUT 1

int light_curve::update_ephase(char *fname, double back_level,double e_min, double e_max)
{
	
	if(fabs(average() + back_level) < 1e-5)
	{
		cerr <<" WARN, empty pulse, skip\n";
		return 0;
	}
	
	fitsfile *fptr;
	int status=0;
	if(fits_open_file(&fptr, fname, READWRITE, &status))
	{
		cerr << "Error opening '" << fname <<"'\n";
		return ERROR_CODE_INPUT;
	}
	
	long nrows=0;
	int hdutype=0;
	fits_movabs_hdu(fptr, 2, &hdutype, &status);
	fits_get_num_rows(fptr, &nrows, &status);
	if(status)
	{
		cerr <<"Error in finding nrows in '"<<fname<<"'\n";
		return status;
	}
	
	long firstrow= nrows+1, firstelem=1;
	double tmp= e_min;
	fits_write_col(fptr, TDOUBLE, 1, firstrow, firstelem, 1, &tmp , &status);
	tmp= e_max;
	fits_write_col(fptr, TDOUBLE, 2, firstrow, firstelem, 1, &tmp , &status);
	fits_write_col(fptr, TDOUBLE, 3, firstrow, firstelem, _size, _y , &status);
	fits_write_col(fptr, TDOUBLE, 4, firstrow, firstelem, _size, _dy , &status);
	fits_write_col(fptr, TDOUBLE, 5, firstrow, firstelem, _size, _frac_exp , &status);
	//for(int i=0;i<_size;i++)
	//	cout << _y[i] << " " << _dy[i] << endl;
	if(status)
	{
		fits_report_error(stderr, status);
		cerr <<"Error writing elements in '"<<fname<<"'\n";
		return status;
	}

	fits_close_file(fptr, &status);
	
	return status;
	
}//end of update tphase


void light_curve::concatenate(light_curve *ee)
{
    //Concatenates only times energy and GTI
    //keeps all specifics of current file
    
    if(ee->_t_start <= _t_start)
    {
        cerr << "Impossible to concatenate event files which are not time ordered\n";
        return;
    }

    int n_size=ee->size() + _size;
    double *n_x = new double[n_size];
    double *n_dx = new double[n_size];
    double *n_y = new double[n_size];
    double *n_dy = new double[n_size];
    double *n_frac_exp = new double[n_size];
    int n_n_gti = n_gti + ee->n_gti;
    double *n_GTI_START = new double[n_n_gti];
    double *n_GTI_STOP = new double[n_n_gti];

    for(int i=0;i<_size;i++)
    {
        n_x[i]=_x[i];
        n_dx[i]=_dx[i];
        n_y[i]=_y[i];
        n_dy[i]=_dy[i];
        n_frac_exp[i]=_frac_exp[i];
    }
    for(int i=0;i<ee->_size;i++)
    {
        n_x[_size+i]=ee->_x[i];
        n_dx[_size+i]=ee->_dx[i];
        n_y[_size+i]=ee->_y[i];
        n_dy[_size+i]=ee->_dy[i];
        n_frac_exp[_size+i]=ee->_frac_exp[i];
    }

    for(int i=0;i<n_gti;i++)
    {
        n_GTI_START[i]=GTI_START[i];
        n_GTI_STOP[i]=GTI_STOP[i];
    }

    for(int i=0;i<ee->n_gti;i++)
    {
        n_GTI_START[n_gti+i]=ee->GTI_START[i];
        n_GTI_STOP[n_gti+i]=ee->GTI_STOP[i];
    }

    _ontime+=ee->_ontime;
    _t_stop=ee->_t_stop;

    delete [] _x;
    delete [] _dx;
    delete [] _y;
    delete [] _dy;
    delete [] _frac_exp;
    delete [] GTI_START;
    delete [] GTI_STOP;

    _x = new double[n_size];
    _dx = new double[n_size];
    _y = new double[n_size];
    _dy = new double[n_size];
    _frac_exp = new double[n_size];
    
    GTI_START = new double[n_n_gti];
    GTI_STOP = new double[n_n_gti];

    _size=n_size;
    n_gti=n_n_gti;

    for(int i=0;i<_size;i++)
    {
        _x[i]=n_x[i];
        _dx[i]=n_dx[i];
        _y[i]=n_y[i];
        _dy[i]=n_dy[i];
        _frac_exp[i]=n_frac_exp[i];
    }

    for(int i=0;i<n_gti;i++)
    {
        GTI_START[i]=n_GTI_START[i];
        GTI_STOP[i]=n_GTI_STOP[i];
    }

    delete [] n_x;
    delete [] n_dx;
    delete [] n_y;
    delete [] n_dy;
    delete [] n_frac_exp;
    delete [] n_GTI_START;
    delete [] n_GTI_STOP;
    
} //end of concatenae

int light_curve::write(char *fname)
{
	
	int status=0;
	char loc_fname[FLEN_FILENAME];
	sprintf(loc_fname, "!%s", fname);
	fitsfile *fptr_loc;
	fits_create_file(&fptr_loc, loc_fname, &status);
	if(status)
	{
		cerr <<"Error creating Time file'" << fname <<"'\n";
		exit(1);
	}
	char extname[] = "RATE";       /* extension name */
	//char spefilen[80];    /* name for the spectral file */
        char keyvalu[FLEN_VALUE];
        if(is_rate == 0)
            sprintf(keyvalu, "COUNTS");
        else
            sprintf(keyvalu, "RATE");
        
        char keyvalu1[FLEN_VALUE];
        if(is_rate == 0)
            sprintf(keyvalu1, "cts");
        else
            sprintf(keyvalu1, "cts/s");
        
	char *ttype[] = {"TIME", "XAX_E", keyvalu, "ERROR", "FRACEXP"};
	char *tform[] = {"1D",      "1D",  "1E",     "1E",    "1E"};
	char *tunit[] = {timeunit,  timeunit, keyvalu1, keyvalu1 , ""};
	int tfields=5;
	long firstelem=1;
	
	if ( fits_create_tbl(fptr_loc, BINARY_TBL, 0, tfields, ttype, tform,
						 tunit, extname, &status) )
	{
		fits_report_error(stderr, status );
		exit(status);
	}
       	
	fits_write_col(fptr_loc, TDOUBLE, 1, 1, firstelem, _size,
                            _x, &status);

        fits_write_col(fptr_loc, TDOUBLE, 2, 1, firstelem, _size,
                            _dx, &status);
        
        fits_write_col(fptr_loc, TDOUBLE, 3, 1, firstelem, _size,
                            _y, &status);
        
        fits_write_col(fptr_loc, TDOUBLE, 4, 1, firstelem, _size,
                            _dy, &status);
        
        fits_write_col(fptr_loc, TDOUBLE, 5, 1, firstelem, _size,
                            _frac_exp, &status);
        
//        sprintf(keyword,"E_MIN");
//        sprintf(keycomm, "Minimum energy of pulses");
//        fits_update_key( fptr_loc,  TDOUBLE, keyword, &e_min, keycomm, &status);
//
//        sprintf(keyword,"E_MAX");
//        sprintf(keycomm, "Maximum energy of pulses");
//        fits_update_key( fptr_loc,  TDOUBLE, keyword, &e_max, keycomm, &status);
//        
        status=write_timing_keywords(fptr_loc);
        
        status=write_gtis(fptr_loc);
	
	fits_close_file(fptr_loc, &status);
	if(status)
	{
		cerr <<"Error closing file '" << fname <<"'\n";
		return status;
	}
	return status;
}

int light_curve::write_gtis(fitsfile *evt_on)
{
     //copies GTI
    int status=0;
  int hdutype;
  int nrows = 0;
  long firstelem=1;
  char keyword[FLEN_KEYWORD];
  char keyvalu[FLEN_VALUE];
  char keycomm[FLEN_COMMENT];
  sprintf(keyword, "GTI");
  int tfields = 2;  
  char *ttype[] = {"START", "STOP"};
  char *tform[] = {"1D",   "1D"};
  char *tunit[] = {timeunit,   timeunit};
  
  if ( fits_create_tbl(evt_on, BINARY_TBL, nrows, tfields, ttype, tform,
					   tunit, keyword, &status) )
	fits_report_error(stderr, status );
  
  write_timing_keywords(evt_on);
  
  sprintf(keyword,"EXTNAME");
  sprintf(keyvalu,"GTI");
  sprintf(keycomm,  "extension standard name");
  fits_update_key( evt_on,  TSTRING, keyword, keyvalu, keycomm, &status);
  
  	
	
	
    fits_write_col(evt_on, TDOUBLE, 1, 1, firstelem, n_gti,
                            GTI_START, &status);

    fits_write_col(evt_on, TDOUBLE, 2, 1, firstelem, n_gti,
                            GTI_STOP, &status);

    return status;
        
}

int light_curve::write_timing_keywords(fitsfile *evt_on) 
{
    int status = 0;
    char keyword[FLEN_KEYWORD];
    char keycomm[FLEN_COMMENT];
    char keyvalu[FLEN_VALUE];
    
    sprintf(keyword, "TELESCOP");
    sprintf(keyvalu, "%s", telescop);
    sprintf(keycomm, "Telescope");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);

    sprintf(keyword, "INSTRUME");
    sprintf(keyvalu, "%s", instrume);
    sprintf(keycomm,  "Instrument");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);

//    sprintf(keyword, "DETNAM");
//    sprintf(keyvalu, "ISGRI");
//    //sprintf(keycomm,  "");
//    fits_update_key(evt_on, TSTRING, keyword, keyvalu, NULL, &status);

 
    sprintf(keyword, "TUNIT");
    sprintf(keyvalu, "%s", timeunit);
    sprintf(keycomm, "Time unit");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);

    sprintf(keyword, "MJDREF");
    double tmp = _mjd_ref;
    sprintf(keycomm, "Modified Julian Date of time origin");
    fits_update_key(evt_on, TDOUBLE, keyword, &tmp, keycomm, &status);

    sprintf(keyword, "TIMESYS");
    sprintf(keyvalu, "%s", timesys);
    sprintf(keycomm, "Time frame system");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);

    sprintf(keyword, "TIMEREF");
    sprintf(keyvalu, "%s", timeref);
    sprintf(keycomm,  "Reference location of photon arrival times");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);
    
    sprintf(keyword, "OBJECT");
    sprintf(keyvalu, "%s", object);
    sprintf(keycomm,  "Name of observed object");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);
 
//    sprintf(keyword, "ORIGIN");
//    sprintf(keyvalu, "%s", origin);
//    sprintf(keycomm,  "Origin of data");
//    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);
// 
    sprintf(keyword, "TIMEDEL");
    tmp = _time_del;
    sprintf(keycomm, "Time resolution");
    fits_update_key(evt_on, TDOUBLE, keyword, &tmp, keycomm, &status);
    
    sprintf(keyword, "ONTIME");
    tmp = _ontime;
    sprintf(keycomm, "Sum of GTIs");
    fits_update_key(evt_on, TDOUBLE, keyword, &tmp, keycomm, &status);
    
    sprintf(keyword, "TSTART");
    tmp = _t_start;
    sprintf(keycomm, "Start time of first frame");
    fits_update_key(evt_on, TDOUBLE, keyword, &tmp, keycomm, &status);
    
    sprintf(keyword, "TSTOP");
    tmp = _t_stop;
    sprintf(keycomm, "Stop time of last frame");
    fits_update_key(evt_on, TDOUBLE, keyword, &tmp, keycomm, &status);
    
    sprintf(keyword, "TIMEPIXR");
    tmp = _timepixr;
    sprintf(keycomm, "Time stamp");
    fits_update_key(evt_on, TDOUBLE, keyword, &tmp, keycomm, &status);
           
    return status;
    
}


light_curve operator-(const light_curve &a, const light_curve &b)
{
    
    light_curve c = light_curve(a);
    
    if(a.size() != b.size())
    {
        cerr << "Error in operator, different sizes\n";
        return a;
    }
    
    for(int i=0;i<a.size();i++)
    {
        if(a._x[i] != b._x[i])
        {
            cerr << "WARNING LC operation, different times" << a._x[i] << " " << b._x[i] << " we take times of the first operand" << endl;
        }
        c._x[i]=a._x[i];
        c._dx[i]=a._dx[i];
        
        c._y[i] =a._y[i]-b._y[i];
        c._dy[i] = sqrt(SQR(a._dy[i]) + SQR(b._dy[i]));
    }
    return c;
}

light_curve operator+(const light_curve &a, const light_curve &b)
{
    
    light_curve c = light_curve(a);
    
    if(a.size() != b.size())
    {
        cerr << "Error in operator, different sizes\n";
        return a;
    }
    
    for(int i=0;i<a.size();i++)
    {
        if(a._x[i] != b._x[i])
        {
            cerr << "WARNING LC operation, different times" << a._x[i] << " " << b._x[i] << " we take times of the first operand" << endl;
        }
        c._x[i]=a._x[i];
        c._dx[i]=a._dx[i];
        
        c._y[i] =a._y[i] + b._y[i];
        c._dy[i] = sqrt(SQR(a._dy[i]) + SQR(b._dy[i]));
    }
    
    return c;
}

light_curve operator/(const light_curve &a, const light_curve &b)
{
    
    light_curve c = light_curve(a);
    //cout << "Allocated /c\n";
    if(a.size() != b.size())
    {
        cerr << "Error in operator, different sizes\n";
        return a;
    }
    //cout << " Cheking " << _size  << " elements\n";
    
    for(int i=0;i<a.size();i++)
    {
        if(a._x[i] != b._x[i])
        {
            cerr << "WARNING LC operation, different times" << a._x[i] << " " << b._x[i] << " taking the first operand time"<< endl;
        }
        

        
        c._x[i]=a._x[i];
        c._dx[i]=a._dx[i];
        
        c._y[i] =a._y[i]/b._y[i];
        c._dy[i] = c._y[i]*sqrt(SQR(b._dy[i]/b._y[i]) + SQR(a._dy[i]/a._y[i]));
        
        if (a._y[i] == 0  || b._y[i] == 0)
        {
            cerr << "WARNING LC division, one operand is zero at index " << i << " with values ";
            cerr << c._y[i] << " +/- " << c._dy[i] << endl;
        }
        
        //cout << i <<endl;
    }
    
    return c;
}


light_curve operator*(const light_curve &a, const light_curve &b)
{
    
    light_curve c = light_curve(a);
    //cout << "Allocated /c\n";
    if(a.size() != b.size())
    {
        cerr << "Error in operator, different sizes\n";
        return a;
    }
    //cout << " Cheking " << _size  << " elements\n";
    
    for(int i=0;i<a.size();i++)
    {
        if(a._x[i] != b._x[i])
        {
            cerr << "WARNING LC operation, different times" << a._x[i] << " " << b._x[i] << " taking the first operand time"<< endl;
        }
        
        if (a._y[i] == 0  || b._y[i] == 0)
        {
            cerr << "WARNING LC multiplication/division, one operand is zero at index " << i << " , undefined uncertainty"<< endl;
        }
        
        c._x[i]=a._x[i];
        c._dx[i]=a._dx[i];
        
        c._y[i] =a._y[i] * b._y[i];
        c._dy[i] = c._y[i]*sqrt(SQR(b._dy[i]/b._y[i]) + SQR(a._dy[i]/a._y[i]));
        //cout << i <<endl;
    }
    
    return c;
}


light_curve hratio(const light_curve a, const light_curve &b)
{
    
    light_curve c = light_curve(a);
    
    if(a.size() != b.size())
    {
        cerr << "Error in operator, different sizes\n";
        return a;
    }
    
    for(int i=0;i<a.size();i++)
    {
        if(a._x[i] != b._x[i])
        {
            cerr << "WARNING LC operation, different times" << a._x[i] << " " << b._x[i] << endl;
        }
        
        c._y[i] =(b._y[i] - a._y[i] )/(b._y[i] + a._y[i] );
        c._dy[i] = 2./SQR(a._y[i] + b._y[i])*sqrt(SQR(b._dy[i]*b._y[i]) + SQR(a._dy[i]*a._y[i]));
    }
    return c;
}


	light_curve simul_gaus(const light_curve &a) //Simulaiton with Gaussian deviates
	{
		
		light_curve *c = new light_curve(a);
		static int seed=time(NULL);		
		for(int i=0;i<a._size;i++)
		{
			Normaldev ndev(a._y[i], a._dy[i], seed++);
			c->_y[i] =ndev.dev();
			
		}
		return *c;
	}
	
	light_curve simul_poisson(const light_curve &a) //Simulation with Ppoisson deviate
	{
		
		light_curve *c = new light_curve(a);
		static int seed=time(NULL);	
		for(int i=0;i<a._size;i++)
		{
			if(a._y[i] > 0)
			{
				Poissondev ndev(a._y[i], seed++);
				c->_y[i] =ndev.dev();
				c->_dy[i]=sqrt(c->_y[i]);
			}
			else
				cerr <<"Poisson simulaiton, value of counts is <0, " << i << " " << a._y[i] << " skipping\n";
		}
		return *c;
	}



