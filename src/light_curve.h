/*
 *  light_curve.h
 *  Light_Curve_Analyzer
 *
 *  Created by Carlo Ferrigno on 28/10/09.
 *  Copyright 2009 ISDC/IAAT. All rights reserved.
 *
 */

#ifndef LIGHT_CURVE_H
#define LIGHT_CURVE_H



#include "fitsio.h"
#include "event_files.h"
#include "nr3.h"
#include "deviates.h"
#include <vector>


class light_curve{
public:
	int _size;
	double *_x;
	double *_dx;
	double *_y;
	double *_dy;
	double *_frac_exp;
	double _time_del;
	double _timepixr;
	char timeref[FLEN_VALUE];
    char timesys[FLEN_VALUE];
	char telescop[FLEN_VALUE];
	char object[FLEN_VALUE];
	char instrume[FLEN_VALUE];
	char timeunit[FLEN_VALUE]; //this is for header
	char timeunit_table[FLEN_VALUE]; //this is for table
	
	char filename[FLEN_FILENAME];
	char scwname[FLEN_VALUE];
	double _ontime;
	char is_rate;
	
	double _mjd_ref;
	double _time_zero;
	
	double _t_start;
	double _t_stop;
	double _phase_t_start;
	
	
	double *GTI_START;
	double *GTI_STOP;
	
	int n_gti;
	
	
	
	//constr destr
	light_curve();
	light_curve(int n);
	light_curve(const char *fname);
	light_curve(const light_curve &a);
    light_curve(light_curve *a);
	light_curve(class event_file &a, double time_bin, double e_min, double e_max);
	
	~light_curve();
	
	//methods
	void clean_values();
	int read_fits(const char *fname);
        int read_ascii(const char *fname);
	void allocate(int n);
	void allocate_gti(int n);
	void deallocate();
	void deallocate_lc();
	void deallocate_gti();
	void init_keywords();
	void init_values();
        int filter_gti();
	void output();
	void clean_frac_exp(double min_frac);
	int normalize(double level, char use_wave);
	int find_extremes(double min_SN, double min_deviation, double burst_duration);
	void get_times(std::vector<double> &times, double min_sn);
	void rebin(int factor);
	void binary_corr(char *file_name);
	int get_gti(fitsfile *fptr, int status);
	int scan_gti();
	int *make_rebin_table(double min_sn, double max_time, double gti_threshold, double min_bin_size, double max_point_sn, char single_point_check='n');
	light_curve *fold(int n_bins, double timeref, double f, double df, double ddf, int weight_method=1, double weight_f=1.3);
	light_curve &cut(double t1, double t2);
	int rebin(int *table);
	int compute_phase_exposure(double timeref, double f, double df, double ddf);
	void divide_frac_exp_sqrt_err();
	void output_lc(char *fname, bool is_phase=0, double min_frac_exp=0, double t_0=0);
	int get_z2_period(char flag_binarycor, char *binary_file, char log_spacing, double nu_start, double nu_stop, int n_bins, int n_harm, int flag_out, const char *filename,  double t_ref, double &best_freq, double &err_best_freq, double min_sn=1);	
	void init_folded();
	int crop_gti(double t1, double t2);
	int get_phase_shift();
	int add_key_matrix(fitsfile *fptr, double t_ref, double period, double df, double df2);
	int init_tphase(char *fname, int n_bins, double t_ref, double period, double df, double df2);
	int update_tphase(char *fname, double back_level);
	int update_ephase(char *fname, double back_level,double e_min, double e_max);
	int detrend(double timescale, char flag_out, char *fname);
	void concatenate(light_curve *ee);
	int write(char *fname);
	int write_timing_keywords(fitsfile *evt_on);
	int write_gtis(fitsfile *evt_on);
	void make_rate();
	double get_Rk2(int k, double omega, double t_ref, char flag_err);

	int build_gti(double max_allowed_gap);

	//no inline because of "const" attribute
// 	double time_del const ();
// 	
// 	double mjd_ref const ();
// 	
// 	double ontime const ();
// 	
// 	char *time_unit const ();
// 	
// 	int size const ();
// 	
// 	double t_start const ();
// 	
//     double t_stop const ();
	
    
    double time_del() const
    {
        return _time_del;
    }
    double mjd_ref() const
    {
        return _mjd_ref;
    }
    double ontime() const
    {
        return _ontime;
    }

    char *time_unit()
    {
        return timeunit;
    }

    int size() const
    {
        return _size;
    }

    double t_start () const
    {
        return _t_start;
    }
    
    double t_stop() const
    {
        return _t_stop;
    }
    
	//Inline
	inline void sqrt_err()
	{
		for(int i=0;i<_size;i++)
		{
			_dy[i]=sqrt(_dy[i]);
		}
	}
			
	inline void invert_err()
	{
		for(int i=0;i<_size;i++)
		{
                    if(_dy[i]!=0)
			_dy[i]=1/(_dy[i]);
		}
	}

	inline double total(double t1=-1e10, double t2=1e10)
	{
		double tot=0;
		for(int i=0;i<_size;i++)
		{
			if(_x[i] < t1 || _x[i] > t2)
				continue;
			tot+=(_y[i]);
		}
		return tot;
	}

	inline void sub_with_err(double v, double e)
	{
		double e2=e*e;
		for(int i=0;i<_size;i++)
		{
			_y[i]-=v;
			_dy[i]=sqrt( SQR(_dy[i]) + e2);
		}
	}
	
	inline double average(double t1=-1e10, double t2=1e10)
	{
		double tot=0;
		double n_ele=0;
		for(int i=0;i<_size;i++)
		{
			if(_x[i] < t1 || _x[i] > t2)
				continue;
			tot+=(_y[i]);
			n_ele++;
		}
		if(n_ele)
			tot/=n_ele;
		return tot;
	}
        
        inline double average_error(double t1=-1e10, double t2=1e10)
	{
		double tot=0;
		double n_ele=0;
		for(int i=0;i<_size;i++)
		{
			if(_x[i] < t1 || _x[i] > t2)
				continue;
			tot+=(_dy[i]);
			n_ele++;
		}
		if(n_ele)
			tot/=n_ele;
		return tot;
	}
	
        inline double var(double t1, double t2, double average)
	{
		double tot=0;
		double n_ele=0;
		for(int i=0;i<_size;i++)
		{
			if(_x[i] < t1 || _x[i] > t2)
				continue;
			tot+=SQR(_y[i]-average);
			n_ele++;
		}

                
		if(n_ele>1)
			tot/=n_ele-1;
		return tot;
	}

        inline double chi(double average)
	{
		double tot=0;
		//double n_ele=0;
		for(int i=0;i<_size;i++)
		{
                    if(_dy[i]>0)
			tot+=SQR((_y[i]-average)/_dy[i]);
			//n_ele++;
		}

                return tot;
	}
        
        
	inline double get_phase(double dt, void *params)
	{
		double f = ((double *)params)[0];
		double df=  ((double *)params)[1];
		double df2 = ((double *)params)[2];
		//      printf("%e\t%e\t%e\n", f,df,df2);
		return dt*(f + dt*(df/2. + dt*df2/6.));
	}

      	inline void simple_weight()
	{
            if(_size<=0)
                    return;

            for(int i=0;i<_size;i++)
            {
                if(_dy[i]>0)
                   _y[i]/=SQR(_dy[i]);
                else
                    _y[i]=0;
            }

	}

        inline double get_vs(double f)
        {
            //weightng factor from Corbet et al. 1997
            double av=average(-1e100,1e100);
            double vv=var(-1e100,1e100,av);
            double sf=0;

            for(int i=0;i<_size;i++)
            {
                sf+=SQR(f*_dy[i]);
            }
            sf/=_size;
            double vs=vv-sf;
            if (vs <0)
                vs=0;
            return vs;
        }


   	inline void complex_weight(double f)
	{
            //weightng factor from Corbet et al. 1997
            if(_size<=0)
                    return;

            double vs=get_vs(f);
            for(int i=0;i<_size;i++)
            {
                if(_dy[i]>0)
                   _y[i]/=SQR(f*_dy[i])+vs;
                else
                    _y[i]=0;
            }

	}

	inline void set_times(double tt_ref, double time_resolution)
	{
		_time_del=time_resolution;
		if(_size<=0)
			return;
		
		for(int i=0;i<_size;i++)
		{
			_x[i]=tt_ref+i*time_resolution;
			_dx[i]=time_resolution;
			_frac_exp[i]=1;
		}
		
	}
	
	
	inline light_curve & operator-=(light_curve &a) //lc subtraction
	{
		
		if(a.size() != _size)
			return *this;
		for(int i=0;i<_size;i++)
		{
			if(_x[i] != a._x[i])
			{
				cerr << "WARNING LC operation, different times" << _x[i] << " " << a._x[i] << endl;
			}
			_y[i]-=a._y[i];
			_dy[i] = sqrt(SQR(_dy[i]) + SQR(a._dy[i]));
		}
		return *this;
	}

	inline light_curve & operator-=(double a) //lc subtraction
	{
		
		for(int i=0;i<_size;i++)
		{
			_y[i]-=a;
		}
		return *this;
	}
	
	inline light_curve & operator+=(light_curve &a) //lc addition
	{
		
		if(a.size() != _size)
			return *this;
		for(int i=0;i<_size;i++)
		{
			if(_x[i] != a._x[i])
			{
				cerr << "WARNING LC operation, different times" << _x[i] << " " << a._x[i] << endl;
			}
			_y[i]+=a._y[i];
			_dy[i] = sqrt(SQR(_dy[i]) + SQR(a._dy[i]));
			_frac_exp[i] += a._frac_exp[i];
		}
		return *this;
	}
	
	//Arithmetic operators
	friend light_curve operator-(const light_curve &a, const light_curve &b); 
	friend light_curve operator+(const light_curve &a, const light_curve &b); 
	friend light_curve operator/(const light_curve &a, const light_curve &b); 
	friend light_curve operator*(const light_curve &a, const light_curve &b); 
    
    friend light_curve simul_gaus(const light_curve &a);
    friend light_curve simul_poisson(const light_curve &a);
	
	
	friend light_curve &hratio(const light_curve &a, const light_curve &b);
    
	
	inline light_curve & operator/=(light_curve a) //lc subtraction
	{
		
		if(a.size() != _size)
			return *this;
		for(int i=0;i<_size;i++)
		{
			if(_x[i] != a._x[i])
			{
				cerr << "WARNING LC operation, different times" << _x[i] << " " << a._x[i] << endl;
			}
			double ratio = _y[i]/a._y[i];
			_dy[i] = ratio*sqrt(SQR(_dy[i]/_y[i]) + SQR(a._dy[i]/a._y[i]));
			_y[i]=ratio;
		}
		return *this;
	}
	
	inline light_curve & operator/=(double a) //lc subtraction
	{
		for(int i=0;i<_size;i++)
		{
			_dy[i] /= a;
			_y[i]  /= a;
		}
		return *this;
	}
	

    inline void blank()
    {
        for(int i=0;i<_size;i++)
        {
            _y[i]=0;
            _dy[i]=0;
        }

    }

	static double get_phase_complete(double time, double t_ref, double f, double df, double ddf, double *i_phase);
};




#endif
