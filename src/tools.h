/*
 *  tools.h
 *  Light_Curve_Analyzer
 *
 *  Created by Carlo Ferrigno on 04/12/09.
 *  Copyright 2009 ISDC/IAAT. All rights reserved.
 *
 */

#include "nr3.h"
#include <vector>
#include "ran.h"
#include "deviates.h"

using namespace std;

struct Cumulative_Sin_Deviate {

	Doub  &omega, &time_uniform;
	VecDoub rms, phi;
        //int n_pulse_bins;
	
	Cumulative_Sin_Deviate(VecDoub &irms, Doub   &iomega, Doub &itime_uniform , VecDoub &iphase ) :
	rms(irms), omega(iomega), time_uniform(itime_uniform), phi(iphase) {}

//	inline void set_phi(Doub &iphi)
//	{
//		phi=iphi;
//	}
	
	inline Doub operator()(const Doub t) {
		double res=t;
		for (int j=1;j<=rms.size();j++)
			res+=2*rms[j-1]*(sin(j*omega*t-phi[j-1]))/(j*omega);
		return  time_uniform - res ;
                //+sin((n_pulse_bins-j)*omega*t+phi[j-1])
	}
	
	
};

struct simul_obj {
	
	double t_ref;
	double fr[2];
	double rates[2];
	double lag;
	double frac_noise;
	
	Normaldev *pp;
	
	simul_obj(double tr, double f1, double r1, double f2, double r2, double ll, double fr_n) : t_ref(tr), lag(ll), frac_noise(fr_n) {
		fr[0]=(f1);
		fr[1]=(f2);
		rates[0]=r1;
		rates[1]=r2;
		//pp = new Poissondev(1, time(NULL));
	} 
	
	inline double get(double t, char fc) { 
		if(frac_noise >0)
		{
			pp = new Normaldev(1, 1, time(NULL));
			double tmp=rates[fc]*(1+sin(2*M_PI*fr[fc]*(t-t_ref+fc*lag)))*(1+frac_noise*pp->dev());
			tmp += 0.5*rates[fc]*(1+sin(2*2*M_PI*fr[fc]*(t-t_ref+fc*lag)))*(1+frac_noise*pp->dev());
                        return tmp+pp->dev();
		}
		else
		{
			return rates[fc]*(sin(2*M_PI*fr[fc]*(t-t_ref+fc*lag)) + .5*sin(2*M_PI*2*fr[fc]*(t-t_ref+fc*lag)) );
		}
	}
	inline double get(double t, double err ,char fc) { 
		if(frac_noise >0)
		{
			double tmp=rates[fc]*(1+sin(2*M_PI*fr[fc]*(t-t_ref+(double)fc*lag)));
			tmp += 0.5 + rates[fc]*(1+sin(2*M_PI*2*fr[fc]*(t-t_ref+(double)fc*lag)));
			pp = new Normaldev(tmp, err*tmp, time(NULL));
			return tmp+pp->dev();
		}
		else
		{
			return rates[fc]*(1+sin(2*M_PI*fr[fc]*(t-t_ref+(double)fc*lag))+ .5*(1+sin(2*M_PI*2*fr[fc]*(t-t_ref+fc*lag))) );
		}
	}
	
};


int read_list(vector<string> &list, char *fname);

void fgauss(const Doub x, VecDoub_I &a, Doub &y, VecDoub_O &dyda);
void fsync2(const Doub x, VecDoub_I &a, Doub &y, VecDoub_O &dyda);

int get_best_frequency(vector <double> frequency, vector <double> z2, double &best_freq, double &err_best_freq, double &max_peak, char *filename, char flag_out, bool fit_flag=1);

int get_best_frequency( VecDoub frequency, VecDoub z2, double &best_freq, double &err_best_freq_u, double &err_best_freq_d, double &max_peak, char *filename, char flag_out, bool fit_flag=1);

int make_z2_stat(vector<string> list, char evt_flag, double e_min, double e_max, char flag_binarycor,
        char *binary_file, char log_spacing, double nu_start, double nu_stop,
        int n_bins, int n_harm, int flag_out, const char *filename, double mjdref,
        double t_ref, double &best_freq, double &err_best_freq, double &max_z2, long int n_events_per_chunk ,
        double min_sn, char int_bary, double ra_obj, double dec_obj, char *og_name,
        double max_time_separation, double min_significance, char do_simul, int nruns ,
        double max_time_separation_simul, char flag_out_simul, double tolerance);

int make_z2_stat_gb(vector<string> list, char evt_flag, double e_min, double e_max, char flag_err, char flag_binarycor,
        char *binary_file, char log_spacing, double nu_start, double nu_stop,
        int n_bins, int n_harm, int flag_out, const char *filename, double mjdref,
        double t_ref, double &best_freq, double &err_best_freq, double &max_z2, long int n_events_per_chunk ,
        double min_sn, char int_bary, double ra_obj, double dec_obj, char *og_name,
        double max_time_separation, double min_significance, char do_simul, int nruns ,
        double max_time_separation_simul, char flag_out_simul, double tolerance);


int make_z2_stat_slide_time_window(vector<string> list, char evt_flag, double e_min, double e_max,
        char flag_binarycor, char *binary_file, char log_spacing, double nu_start, double nu_stop,
        int n_bins, int n_harm, int flag_out, const char *filename, double mjdref,
        double t_ref, double &best_freq, double &err_best_freq, double &max_z2, double interval,
        double step_interval, double min_sn, double min_significance);

int actual_make_z2_stat(vector <double> times, char log_spacing, double nu_start, double nu_stop, int n_bins, int n_harm, int flag_out, const char *filename, double t_ref, double &best_freq, double &err_best_freq, double &max_z2, int &counter, double min_significance);
int output_z2_stat(VecDoub frequency, VecDoub z2, VecDoub lz2, VecDoub sig, char log_spacing, double nu_start,
        double nu_stop, int n_bins, int n_harm, int flag_out,
        const char *filename, double t_ref, double &best_freq, double &err_best_freq, double &max_z2, int &counter,
        double min_significance, int n_events , double t_stop, double t_start);
int make_simul_Z2(vector <double> times, char log_spacing, double nu_start, double nu_stop, int n_bins,
        int n_harm, int flag_out, const char *filename, double t_ref, double &best_freq, double &err_best_freq,int &counter,
        double min_significance, int nruns, double max_time_separation, double tolerance);
int elaborate_lc_fold();

void readline_nospace(char *out_str, char *query);
void readline_withspace(char *out_str, char *query);
void strip_spaces(char *out_str, char*in_str);
void strip_final_spaces(char *out_str, char *in_str);
double *compute_phase_exposure(double timeref, double f, double df, double ddf,int n_GTIs, int n_phase_bins,double *gti_start,double *gti_stop );
double wrapper_mp_z2_circ(VecDoub_I & x, void *vars);
double get_z2_single_freq(vector <double> *times, int n_harm, double frequency, double t_ref, 
        double asini_i, double ecc_i, double omega_deg_i, double t0_i, double porb_i, double pporb_i);
int get_orbital_solution(vector<string> list, char evt_flag, double e_min, double e_max, 
        double &nu_start, double &nu_start_e, 
        int n_harm, int flag_out, const char *filename, double mjdref, double t_ref,
        double &best_freq, double &err_best_freq, double &max_z2, 
        double ecc, double &asin_i, double &porb, double &t0, double &omega_deg, double &pporb, 
        double ecc_e, double &asin_i_e, double &porb_e, double &t0_e, double &omega_deg_e, double &pporb_e, 
        long int n_events_per_chunk,
        double min_sn, 
        double max_time_separation, double min_significance, char do_simul, int nruns,
        double max_time_separation_simul, char flag_out_simul, double tolerance);

int get_energy_bounds_rmf(char *rmf_name, vector <double> &emin, vector <double> &emax);
int mergeGTI(int numIntervals1, double * gtiStart1, double * gtiEnd1, 
	                int numIntervals2, double * gtiStart2, double * gtiEnd2, 
	                int * mumMergedIntervals, 
			double * mergedGtiStart, double * mergedGtiEnd,
 	                int status);
void loc_piksr2(VecDoub &arr, VecDoub &brr);
