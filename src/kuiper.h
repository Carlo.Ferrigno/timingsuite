/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   kuiper.h
 * Author: Stephane Paltani
 *
 * Created on November 2, 2016, 11:34 AM

  KUIPER LIBRARY
  ==============

  Stéphane Paltani
 Universtiyt of Geneva
  Stephane.Paltani@unige.ch

  This library implements calculation of Kuiper statistics (Kuiper 1960)
  to test the periodicity of events measured disjoint time intervals. It
  also calculates the significance of the Kuiper statistics using the
  exact formulae of Stephens (1965) if possible, and Kuiper's original
  asymptotic formula in other cases. 

  The equation numbers below refer to  "Paltani S., 2004, A&A 420, 789". Please
  cite this paper if you make any use of this library or of the methods
  described in this paper in your research.

*/


#ifndef KUIPER_H
#define KUIPER_H



double get_phase_complete(double time, double t_ref, double f, double df, double df2, double *i_phase);
double get_phase_frac(double time, double t_ref, double f, double df, double df2);

class Kuiper{
public:
    double *A;
    double *B;
    double *Phase;
    double *times;
    double *t_start;
    double *t_stop;

    int     N_events;
    int     N_segments;
    int     N_GTI;
    double invN;
    
    Kuiper();
    ~Kuiper();
    void myfree(void);
    Kuiper(double *t_inp, int N_times,double *tstart,double *tstop,int N_GTI);
    Kuiper(const Kuiper &q);
    int init(double *t_inp, int N_times,double *tstart,double *tstop,int N_GTI);
    double V(double t_ref, double f, double df, double df2);
    double V_noise(double t_ref, double f, double df, double df2);
    static  int doublecompare(double *i, double *j);
    int rand_index(int max_N);
    int rand_unit(void);

    /*
  This function returns the value of Kuiper statistics. "F" is the
  frequency. "t" is an array of "N_times" event times. "tstart" and
  "tstop" are arrays of respectively start and end times.
    */

    static double Qkp(double x,int N);
//Wrpper for inversion
    static double Qkp_wrap(double x, void *params);
//Numerical inversion
    double inv_QkP(double p,int N);
    /*
  This function calculates the probability of the Kuiper statistics for
  a sample of "N" elements.
*/
    
};







#endif /* KUIPER_H */

