
#ifndef BINARYCOR_H
#define BINARYCOR_H 1
//
// last update: January 2002: modified the askInfo function to avoid memory leakage when
//                            input string was too long.
//

#define MJDREF_INTEGRAL  51544.0
#define DAY2SEC  86400.0

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

__BEGIN_DECLS



	/*---------------------------------------------------------------------------------------
     * binarycor_setup(): setup the orbital parameters used by binarycor()
     *
	 *          FileName: name of an ASCII file where the orbit parameter are stored
	 *
	 *          interactive_flag: if <>0 an user-friendly interface is executed in order to
	 *                            enter/modify the orbital parameters
	 *          actual_setup: if <>0 the setup is performed 
	 *                            otherwise just the parameters are asked and the file is filled
	 *
	 *
	 *	return:	0 if no problems in file I/O
	 *
	 *----------------------------------------------------------------------------------------*/



int  binarycor_setup(char *FileName,int interactive_flag, int actual_setup, int load_file_flag, double mjdref);


	/*---------------------------------------------------------------------------------------
     * binarycor(): return the time corrected for the orbital motion using the parameters
	 *              defined by binarycor_setup()
     *
	 *     ev_time: time (sec) to be corrected
	 *
	 *
	 *---------------------------------------------------------------------------------------*/


int set_binarrycor_par(double asini_i, double ecc_i, double omega_deg_i, double t0_i, double porb_i, double pporb_i);


double  binarycor(double ev_time);  /* return the time corrected for binary orbit */
int binarycor_set_mjdref(double mjdref);

__END_DECLS

#endif

