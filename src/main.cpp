#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <chrono>
#include "nr3.h"
#include "fourier.h"
#include "light_curve.h"
#include <cstdio>
#include <vector>
#include <string>
#include <ctime>
#include "ask.h"
#include "event_files.h"

#include "tools.h"

#include "kuiper.h"

#include "spectrum.h"

#include "ran.h"
#include "deviates.h"

#include "correl.h"
#include "binarycor.h"

#include "moment.h"

#include "stattests.h"

#include "fitexy.h"
#include "fitab.h"
#include "fasper.h"

#include "mpfit.h"

using namespace std;


int simple_simul()
{
	double time_interval=512;
	double time_resolution=pow(2.0,-9.0);
	char query[2048];
	char filename1[FLEN_FILENAME];
	char filename2[FLEN_FILENAME];
	sprintf(filename1,"Prova.qdp");
	sprintf(query, "Output file");
	askInfo(query, filename1);
	
	sprintf(query, "Length of time interval [s]");
	ask_double(query, &time_interval);

	sprintf(query, "Time resolution [s]");
	ask_double(query, &time_resolution);

	int n_t = (int)(time_interval/time_resolution);
	int n=4;
	while(n<=n_t/2)
		n*=2;
	cout << "Original n is "<< n_t <<endl;
	cout << "Final    n is "<< n << "= " << n*time_resolution << " s" <<endl;
	
	double f1=0.2776;
	double r1=1;
	double f2=0.2776;
	double r2=1;
	double tl=0.3;
	double frac_noise=0;
	
	sprintf(query, "First frequency");
	ask_double(query, &f1);
	sprintf(query, "First rate");
	ask_double(query, &r1);
	sprintf(query, "Second frequency");
	ask_double(query, &f1);
	sprintf(query, "Second rate");
	ask_double(query, &r1);
	sprintf(query, "Time lag [s]");
	ask_double(query, &tl);
	sprintf(query, "Use Gaussian noise (0=no)");		
	ask_double(query, &frac_noise);
	
	double t_ref=0;
	simul_obj *simul=new simul_obj(t_ref, f1,r1,f2,r2,tl,frac_noise);
	
	light_curve *l1= new light_curve;
	l1->allocate(n);
	light_curve *l2= new light_curve(n);
	l2->allocate(n);
	l1->set_times(t_ref,time_resolution);
	l2->set_times(t_ref,time_resolution);
	
	l1->output();
	
	for(int i=0;i<l1->size();i++)
	{
		double er=1;
		//cout << i << endl;
		//er=l1->_dy[i]/l1->_y[i];
			//cout << l1->_y[i] << "\t";
		l1->_y[i] = simul->get(l1->_x[i], er, 0);
			//l1->_dy[i] = l1->_y[i] * er;
			//cout << l1->_y[i] << "\t";
			
			//cout << l2->_y[i] << "\t";
			//er=l2->_dy[i]/l2->_y[i];
		l2->_y[i] = simul->get(l2->_x[i], er, 1);
			//l2->_dy[i] = l2->_y[i] * er;
			//cout << l2->_y[i] << "\t";
		//cout << endl;
	}
	
	
	VecDoub a_s1(n), a_s2(n), a_cor(2*n);
	for(int i=0;i<n;i++)
	{
		a_s1[i]=0;
		a_s2[i]=0;
		a_cor[2*i]=0;
		a_cor[2*i+1]=0;
	}
	
	
	VecDoub sf1(2*n), sf2(2*n), corf(2*n);
	for(int i=0;i<2*n;i++)
	{
		sf1[i]=0;
		sf2[i]=0;
		corf[i]=0;
	}
	
	for(int i=0;i<n/2;i++)
	{
		sf1[i]=l1->_y[i]*welch(i,n); //avoid fluctuations
		sf2[i]=l2->_y[i]*welch(i,n);
	}
	ofstream a_ff;
/*	
	correl(sf1,sf2,corf);
	
	sprintf(filename2, "all_%s", filename1);
	a_ff.open(filename2);
	
	a_ff << "read serr 1\ncpd /xw\nr x 1e-3 500\nr y\n";
	a_ff << "log y on\nerror x off\nls 1 on 2\n";

	for(int i=0;i<(n>>1);i++)
	{
		a_ff << scientific << setw(19) << setprecision(12)<< i * time_resolution << setw(19) <<"\t" << time_resolution<< "\t";
		a_ff << setw(13) << corf[i] << endl;
		//a_ff << sf1[i] << "\t" << sf2[i] << endl;
	}

	
	for(int i=1;i<=(n>>1);i++)
	{
		a_ff << scientific << setw(19) << setprecision(12)<< -i * time_resolution << setw(19) <<"\t" << time_resolution<< "\t";
		a_ff << setw(13) << corf[n-i] << endl;
	}
	
	a_ff.close();
	
	return 0;
//*/	
	realft(sf1,1);
	realft(sf2,1);
	//int no2=n>>1;
	//cout << n << " " << no2 << endl;
	for (int i=2;i<n;i+=2) {
		//tmp=sf1[i];
		corf[i]=(sf1[i]*sf2[i]+sf1[i+1]*sf2[i+1]);
		corf[i+1]=(sf1[i+1]*sf2[i]-sf1[i]*sf2[i+1]);
	}
	//Real F_0
	corf[0]=sf1[0]*sf2[0];
	//Real F_n/2
	corf[1]=sf1[1]*sf2[1];
	
//	four1(sf1,1);
//	four1(sf2,1);
//	for(int i=0;i<2*n;i++)
//	{
//		corf[2*i]   = sf1[2*i]*sf2[2*i]+sf1[2*i+1]*sf2[2*i+1];
//		corf[2*i+1] = -sf1[2*i]*sf2[2*i+1]+sf1[2*i+1]*sf2[2*i];
//	}
//	
	for(int i=1;i<n;i++)
	{
		a_s1[i]+=sf1[2*i]*sf1[2*i] + sf1[2*i+1]*sf1[2*i+1];
		a_s2[i]+=sf2[2*i]*sf2[2*i] + sf2[2*i+1]*sf2[2*i+1];
		a_cor[2*i]+=corf[2*i];
		a_cor[2*i+1]+=corf[2*i+1];
		//cout << i << " " << a_cor[2*i] << " " << a_cor[2*i+1] << endl;
	}
	
	
	delete l1;
	delete l2;
	
	VecDoub gam2(n);
	VecDoub lag(n);
//	VecDoub dgam2(n);
//	VecDoub dlag(n);
	double fr = 0.5/(n*time_resolution); //The 0.5 factor is due to the 2*n padding !

	for(int i=1;i<n;i++)
	{
		//need to compute also errors
		gam2[i]=(a_cor[2*i] * a_cor[2*i] + a_cor[2*i+1]*a_cor[2*i+1])/(a_s1[i]*a_s2[i]);
		lag[i] = atan2(a_cor[2*i+1], a_cor[2*i])/M_PI/2./((i+0.5)*fr);
	}
	
	int nbins=60;
	double fmin=log10(fr);
	double fmax=log10(fr*n);
	double step=(fmax-fmin)/(nbins+1);
	ofstream ff(filename1);
	sprintf(filename2, "all_%s", filename1);
	a_ff.open(filename2);
	
	a_ff << "read serr 1\ncpd /xw\nr x 1e-3 500\nr y 1e-6 1.1\n";
	a_ff << "log x on\nlog y on\nerror x off\nls 1 on 2\nls 3 on 3\n";
	
	
	ff<<"read serr 1 2 3\n";
	ff<<"cpd /xw\n";
	ff <<"log x on\nlog y off\n";
	ff <<"r y -1 1\n";
	ff <<"ma 3 on 3\nma 4 on 2\n";
	
	for(int j=1;j<nbins;j++)
	{
		double f1,f2;
		f1=pow(10.0,fmin+j*step);
		f2=pow(10.0,fmin+(j+1)*step);
		int ind1=(int)floor(f1/fr);
		int ind2=(int)(floor(f2/fr));
		double av_gam=0, s_gam=0;
		double av_lag=0, s_lag=0;
		double n_done=0;
		
		for(int i=ind1;i<ind2;i++)
		{
			av_gam+=gam2[i];
			av_lag+=lag[i];
			n_done++;
		}
		
		if(n_done)
		{
			av_gam/=n_done;
			av_lag/=n_done;
		}
		
		for(int i=ind1;i<ind2;i++)
		{
			s_gam+=(gam2[i]-av_gam)*(gam2[i]-av_gam);
			s_lag+=(lag[i]-av_lag)*(lag[i]-av_lag);
			a_ff << scientific << setw(19) << setprecision(12)<< i * fr << setw(19) <<"\t" << fr << "\t";
			a_ff << setw(13) << gam2[i] << "\t" << lag[i] << endl;
		}
		
		if(n_done > 1)
		{
			s_gam=sqrt(s_gam/(n_done*(n_done-1)));
			s_lag=sqrt(s_lag/(n_done*(n_done-1)));
		}
		
		ff<<setw(15) << (f2+f1)/2 << "\t" << (f2-f1)/2<<"\t";
		ff<<setw(13) << av_gam << "\t" << s_gam << "\t";
		ff<<setw(13) << av_lag << "\t" << s_lag << endl;
		
	}
	
	ff.close();
	a_ff.close();
	
	return 0;
}



int elaborate_lc()
{
	double time_interval=1000;
	
	char filename1[FLEN_FILENAME];
	char filename2[FLEN_FILENAME];
	char query[2048];
	
	sprintf(filename1, "soft_lc.txt");
	sprintf(filename2, "hard_lc.txt");
	
	sprintf(query, "List of light curves 1");
	askInfo(query, filename1);
	
	sprintf(query, "List of light curves 2");
	askInfo(query, filename2);
	
	vector<string> list1;
	vector<string> list2;
	
	read_list(list1,filename1);
	read_list(list2,filename2);
	int nf=0;
	if((nf=list1.size()) != list2.size())
	{
		cerr << "The two lists should have the same number of files\n";
		return 1;
	}
	
	//investigate first lc
	
	light_curve *l1, *l2; 
	l1 = new light_curve(list1[0].c_str());
	cout << "Light curve " << list1[0].c_str() << endl;
	l1->output();

	int rebin_factor=1;
	sprintf(query, "Rebin factor (integer >= 1)");	
	ask_integer(query, &rebin_factor);
	//rebin_factor=floor(rebin_factor);
	
	cout << "Rebin factor is " << rebin_factor <<endl;
	cout << "New time bin is " << l1->time_del()*rebin_factor << " s\n";
	l1->rebin(rebin_factor);
	
	double t_ref = l1->t_start();
//	int size_simu=l1->size(); //used for simulations to avoid long i/o
	
	sprintf(query, "Length of time interval");
	ask_double(query, &time_interval);
	//get the closest power of two length of interval
	double time_resolution=l1->time_del();
	int n_t = (int)(time_interval/time_resolution);
	int n=4;
	while(n<n_t/2)
		n*=2;
	cout << "Original n is "<< n_t <<endl;
	cout << "Final    n is "<< n << "= " << n*time_resolution << " s" <<endl;
		
	
//	l1->deallocate();
//	l1->clean_values();
	
	delete l1;

	sprintf(filename1,"Prova.qdp");
	sprintf(query, "Output file");
	askInfo(query, filename1);
	
	sprintf(query, "Perform simulation?");
	char flag_simul='n';
	flag_simul=ask_yes_no(query, flag_simul);
	
	simul_obj *simul;
	char use_shot_noise='n';
	if(flag_simul == 'y')
	{
		double f1=0.2776;
		double r1=1;
		double f2=0.2776;
		double r2=1;
		double tl=0.02;
		double frac_noise=0;
		
		sprintf(query, "First frequency");
		ask_double(query, &f1);
		sprintf(query, "First rate");
		ask_double(query, &r1);
		sprintf(query, "Second frequency");
		ask_double(query, &f1);
		sprintf(query, "Second rate");
		ask_double(query, &r1);
		sprintf(query, "Time lag [s]");
		ask_double(query, &tl);
		sprintf(query, "Use Gaussian noise (0=no)");		
		ask_double(query, &frac_noise);
		
		sprintf(query,"Use shot noise from data?");
		use_shot_noise=ask_yes_no(query, use_shot_noise);
		
		simul=new simul_obj(t_ref, f1,r1,f2,r2,tl,frac_noise);
		
	}
	
	VecDoub a_s1(n), a_s2(n), a_cor(2*n);
	int tot_loops=0;
	double tot_ontime=0;
	for(int k=0;k<nf;k++) //loop on files
	{
		cout << list1[k] << " Vs " << list2[k] << endl;

		l1 = new light_curve(list1[k].c_str());
		l2 = new light_curve(list2[k].c_str());

		l1->rebin(rebin_factor);
		l2->rebin(rebin_factor);
		
		
		//cout << "PP\n";
		if(flag_simul == 'y')
		{
			cout << "SizeSB " << l1->size() << endl;
			
			for(int i=0;i<l1->size();i++)
			{
				double er=1;
				//cout << i << endl;
				if(use_shot_noise=='y' && l1->_y[i]>0)
				{
					//er=l1->_dy[i]/l1->_y[i];
					//cout << l1->_y[i] << "\t";
					l1->_y[i] = simul->get(l1->_x[i], er, 0);
					//l1->_dy[i] = l1->_y[i] * er;
					//cout << l1->_y[i] << "\t";
					
				}
				//cout << i << endl;			
				if(use_shot_noise=='y' && l2->_y[i]>0)
				{
					//cout << l2->_y[i] << "\t";
					//er=l2->_dy[i]/l2->_y[i];
					l2->_y[i] = simul->get(l2->_x[i], er, 1);
					//l2->_dy[i] = l2->_y[i] * er;
					//cout << l2->_y[i] << "\t";
				}
				//cout << endl;
			}
			
		}
		tot_ontime += l1->ontime();
		if(l1->size() != l2->size())
		{
			cerr << "Light curves in the two bands have different sizes !\n";
			waitkey();
		}
		if(l1->time_del() != l2->time_del())
		{
			cerr << "Light curves in the two bands have different time resolution, abort !\n";
			exit(1);
		}
		l1->output();
		l2->output();
		
		int j=0;
		while((j+1)*n < l1->size())
		{
			VecDoub sf1(4*n), sf2(4*n), corf(4*n);
			for(int i=0;i<4*n;i++)
			{
				sf1[i]=0;
				sf2[i]=0;
				corf[i]=0;
			}
			for(int i=0;i<n;i++)
			{
				sf1[2*i]=l1->_y[j*n+i];
				sf2[2*i]=l2->_y[j*n+i];
			}
			four1(sf1,1);
			four1(sf2,1);
			for(int i=0;i<2*n;i++)
			{
				corf[2*i]   = sf1[2*i]*sf2[2*i]+sf1[2*i+1]*sf2[2*i+1];
				corf[2*i+1] = -sf1[2*i]*sf2[2*i+1]+sf1[2*i+1]*sf2[2*i];
			}
			
//			all_s1.push_back(sf1);
//			all_s2.push_back(sf2);
//			all_cor.push_back(corf);
			for(int i=0;i<n;i++)
			{
				a_s1[i]+=sf1[2*i]*sf1[2*i] + sf1[2*i+1]*sf1[2*i+1];
				a_s2[i]+=sf2[2*i]*sf2[2*i] + sf2[2*i+1]*sf2[2*i+1];
				a_cor[2*i]+=corf[2*i];
				a_cor[2*i+1]+=corf[2*i+1];
			}
			
			tot_loops++;
			cout << "Check " << (j+1)*n << " "<< l1->size() << endl;
			j++;	
			
		}//end of loop in light curve
		
		
		l1->deallocate();
		l2->deallocate();
		l1->clean_values();
		l2->clean_values();
		
		
	} //end of loop on files
	
	//computes average and stddev
/*	Vec_DP s_s1(n), s_s2(n), s_cor(2*n);

	
	for(int k=0;k<tot_loops;k++)
	{
		Vec_DP sf1 = (all_s1[k]);
		Vec_DP sf2 = (all_s2[k]);
		Vec_DP corf = (all_cor[k]);
		
		for(int i=0;i<n;i++)
		{
			a_s1[i]+=sf1[2*i]*sf1[2*i] + sf1[2*i+1]*sf1[2*i+1];
			a_s2[i]+=sf2[2*i]*sf2[2*i] + sf2[2*i+1]*sf2[2*i+1];
			a_cor[2*i]+=corf[2*i];
			a_cor[2*i+1]+=corf[2*i+1];
		}
	}
	
	for(int i=0;i<n;i++)
	{
		a_s1[i]/=tot_loops;
		a_s2[i]/=tot_loops;
		a_cor[2*i]/=tot_loops;
		a_cor[2*i+1]/=tot_loops;
	}
	
	//stddev
	
	for(int k=0;k<tot_loops;k++)
	{
		Vec_DP sf1 = (all_s1[k]);
		Vec_DP sf2 = (all_s2[k]);
		Vec_DP corf = (all_cor[k]);
		
		for(int i=0;i<n;i++)
		{
			double tmp1=sf1[2*i]*sf1[2*i] + sf1[2*i+1]*sf1[2*i+1] - a_s1[i];
			s_s1[i]+=tmp1*tmp1;
			tmp1=sf2[2*i]*sf2[2*i] + sf2[2*i+1]*sf2[2*i+1]-a_s2[i];
			s_s2[i]+=tmp1*tmp1;
			tmp1=corf[2*i]-a_cor[2*i];
			s_cor[2*i]+=tmp1*tmp1;
			tmp1=corf[2*i+1]-a_cor[2*i+1];
			s_cor[2*i+1]+=tmp1*tmp1;
		}
	}
	
	for(int i=0;i<n;i++)
	{
		s_s1[i]=sqrt(s_s1[i]/(tot_loops*(tot_loops-1)));
		s_s2[i]=sqrt(s_s2[i]/(tot_loops*(tot_loops-1)));
		s_cor[2*i]=sqrt(s_cor[2*i]/(tot_loops*(tot_loops-1)));
		s_cor[2*i+1]=sqrt(s_cor[2*i+1]/(tot_loops*(tot_loops-1)));
	}
*/	
	cout << "Processed " << list1.size() << " files\n";
	cout << "Done " << tot_loops << " loops\n";
	cout << "Total ontime is " << tot_ontime << " s\n";
	
	
	double fr = 0.5/(n*time_resolution); //The 0.5 factor is due to the 2*n padding !
	
	VecDoub gam2(n);
	VecDoub lag(n);
	VecDoub dgam2(n);
	VecDoub dlag(n);
	
	for(int i=0;i<n;i++)
	{
		//need to compute also errors
		gam2[i]=(a_cor[2*i] * a_cor[2*i] + a_cor[2*i+1]*a_cor[2*i+1])/(a_s1[i]*a_s2[i]);
		lag[i] = atan2(a_cor[2*i+1], a_cor[2*i])/M_PI/2./((i+0.5)*fr);
	}
	
	int nbins=60;
	double fmin=log10(fr);
	double fmax=log10(fr*n);
	double step=(fmax-fmin)/(nbins+1);
	ofstream ff(filename1);
	sprintf(filename2, "all_%s", filename1);
	ofstream a_ff(filename2);
	
	a_ff << "read serr 1\ncpd /xw\nr x 1e-3 500\nr y 1e-6 1.1\n";
	a_ff << "log x on\nlog y on\nerror x off\nls 1 on 2\nls 3 on 3\n";
	
	
	ff<<"read serr 1 2 3\n";
	ff<<"cpd /xw\n";
	ff <<"log x on\nlog y off\n";
	ff <<"r y -1 1\n";
	ff <<"ma 3 on 3\nma 4 on 2\n";
	
	for(int j=0;j<nbins;j++)
	{
		double f1,f2;
		f1=pow(10.0,fmin+j*step);
		f2=pow(10.0,fmin+(j+1)*step);
		int ind1=(int)floor(f1/fr);
		int ind2=(int)(floor(f2/fr));
		double av_gam=0, s_gam=0;
		double av_lag=0, s_lag=0;
		double n_done=0;
		
		for(int i=ind1;i<ind2;i++)
		{
			av_gam+=gam2[i];
			av_lag+=lag[i];
			n_done++;
		}

		if(n_done)
		{
			av_gam/=n_done;
			av_lag/=n_done;
		}
		
		for(int i=ind1;i<ind2;i++)
		{
			s_gam+=(gam2[i]-av_gam)*(gam2[i]-av_gam);
			s_lag+=(lag[i]-av_lag)*(lag[i]-av_lag);
			a_ff << scientific << setw(19) << setprecision(12)<< i * fr << setw(19) <<"\t" << fr << "\t";
			a_ff << setw(13) << gam2[i] << "\t" << lag[i] << endl;
		}
		
		if(n_done > 1)
		{
			s_gam=sqrt(s_gam/(n_done*(n_done-1)));
			s_lag=sqrt(s_lag/(n_done*(n_done-1)));
		}
		
		ff<<setw(15) << (f2+f1)/2 << "\t" << (f2-f1)/2<<"\t";
		ff<<setw(13) << av_gam << "\t" << s_gam << "\t";
		ff<<setw(13) << av_lag << "\t" << s_lag << endl;
		
	}
	
	ff.close();
	a_ff.close();
	return 0;
}


int average_lags_coherence()
{
	char filename[FLEN_FILENAME];
	char query[2048];
	int status=0;
	
	sprintf(query, "Enter the generated file");
	sprintf(filename, "all_Prova.qdp");
	
	askInfo(query, filename);
	
	fstream ff(filename, fstream::in);
	
	if(!ff.is_open())
	{
		cerr << "Error in opening " << filename << endl;
		return 1;
	}
	
	vector<double> fv,dfv,g2v,lagv;
	
	int max_line=1024;                                                                                                           
	char line[max_line];                                                                                                         
	double t1=0,t2=0,t3=0,t4=0;
	while(ff.getline(line, max_line, '\n'))                                                                                  
	{   
		//sscanf(line, "%s\t%s", scw_tmp, pif_tmp);                                                                          
		if(line[0] != '\n' && line[0] != '\0' && line[0] != '#')                                                                               
		{                                                                                                                    
			if(sscanf(line, "%lf %lf %lf %lf", &t1, &t2, &t3, &t4)==4)
			{
				fv.push_back(t1);
				dfv.push_back(t2);
				g2v.push_back(t3);
				lagv.push_back(t4);
			}
//			cout << line << endl;
//			cout << t1 << " " << t2 << " " << t3 << " " << t4 <<endl;
		}                                                                                                             
	}
	ff.close();
		
	int n = fv.size();
	cout << "There are " << n << " elements\n";
	cout << "Frequency size is " << dfv[0] << "\n";
	cout << "minimum frequency is " << fv[0] <<"\n";
	cout << "Maximum frequency is " << fv[n-1] <<"\n";
	double fr=dfv[1];
	double log_step=0.15;
	double f1=fv[0] + dfv[0]/2.;
	double f2=f1*(1+log_step);
	
	sprintf(query, "Logarithmic step");
	ask_double(query,&log_step);
	
	sprintf(query, "Enter the output file");
	sprintf(filename, "Rebinned.qdp");
	askInfo(query, filename);
	
	ff.open(filename, fstream::out);
	if(!ff.is_open())
	{
		cerr << "Error in opening " << filename << endl;
		return 1;
	}
	ff<<"read serr 1 2 3\n";
	ff<<"cpd /xw\n";
	ff <<"log x on\nlog y off\n";
	ff <<"ma 6 on 3\nma 1 on 2\n";
	ff <<"lw 2\n";
	ff <<"plot v\n";
	ff <<"r y2 0.003 1\n";
	ff <<"lab y2 Coherence \\gg\\u2\\d\n";
	ff <<"lab y3 Time lag [s]\n";
	ff <<"cs 1.1\n";
	ff <<"font roman\n";
	ff <<"time off\n";
	ff <<"r y3 -0.25 0.25\n";
	ff <<"r x 1e-2 1e2\n";
	ff <<"lab title\n";
//	ff <<"lab file\n";
	ff <<"lab x Frequency [Hz]\n";
	ff <<"win 3\n";
	ff <<"LAB  2 COL 4 LIN 0 100 JUS Lef\n";
	ff <<"LAB  2 POS 1e-2 0 \" \" \n";
	ff <<"win 2\n";
	ff <<"log y on\n";
	ff <<"win all\n";
	
	while(f2 < fv[n-1])
	{
		int ind1=(int)floor(f1/fr);
		int ind2=(int)(floor(f2/fr));
		double av_gam=0, s_gam=0;
		double av_lag=0, s_lag=0;
		double n_done=0;
		
		for(int i=ind1;i<ind2;i++)
		{
			av_gam+=g2v[i];
			av_lag+=lagv[i];
			n_done++;
		}
		
		if(n_done)
		{
			av_gam/=n_done;
			av_lag/=n_done;
		}
		
		for(int i=ind1;i<ind2;i++)
		{
			s_gam+=(g2v[i]-av_gam)*(g2v[i]-av_gam);
			s_lag+=(lagv[i]-av_lag)*(lagv[i]-av_lag);
		}
		
		if(n_done > 1)
		{
			s_gam=sqrt(s_gam/(n_done*(n_done-1)));
			s_lag=sqrt(s_lag/(n_done*(n_done-1)));
		}
		
		ff<<setw(15) << (f2+f1)/2 << "\t" << (f2-f1)/2<<"\t";
		ff<<setw(13) << av_gam << "\t" << s_gam << "\t";
		ff<<setw(13) << av_lag << "\t" << s_lag << endl;
		
		f1=f2;
		f2=f1*(1+log_step);
		
	}//end of loop
	
	ff.close();
	return status;
}

int find_flares()
{
	int status=0;
	
	char filename[FLEN_FILENAME];
	char query[2048];
	
	sprintf(filename, "list_lc.txt");
	
	sprintf(query, "List of light curves");
	askInfo(query, filename);
	
	vector<string> list;
	
	read_list(list,filename);
	
	int n=list.size();
	
	cout << "You have given " << n << " light curves\n";
	
	
	char use_wavevar='y';
	sprintf(query, "Use weighted average?");
	use_wavevar = ask_yes_no(query, use_wavevar);
	
	double min_sn=4;
	double min_significance=4;
	double burst_duration=50;
	
	sprintf(query,"Enter minimum S/N");
	ask_double(query, &min_sn);
	
	sprintf(query,"Enter minimum significance");
	ask_double(query, &min_significance);
	
	sprintf(query,"Enter burt possible duration in s");
	ask_double(query, &burst_duration);

	light_curve *ll;
	
	double tot_ontime=0;
	int tot_bursts=0;
	for(int i=0;i<n;i++)
	{
		
		ll = new light_curve(list[i].c_str());
		
		ll -> normalize(min_sn, use_wavevar);
		ll -> output();
		tot_ontime+=ll->ontime();
		tot_bursts += ll -> find_extremes(min_sn, min_significance, burst_duration);
		
		delete ll;
	}
	
	cout << "Found " << tot_bursts << " possible bursts in " << tot_ontime << " s of observation\n";
	cout << "using min SN " << min_sn << " for the signal in the single bin\n";
	cout << "using minimim significance " << min_significance << " with respect to the normalized single LC\n";
	return status;
	
}

int power_spectrum()
{
	int status=0;

	char filename[FLEN_FILENAME];
	char query[2048];
	
	sprintf(query, "Choose the data format");
	int n_opt=2;
	char *menu[]={"Events", "Light Curves"};
	int evt_flag=2;
	
	evt_flag=ask_menu(n_opt, menu , evt_flag, query);
	
	if(evt_flag ==1)
	{
		sprintf(filename, "list_evt.txt");
		sprintf(query, "List of event files");
		
	}
	else
	{
		sprintf(filename, "list_lc.txt");
		sprintf(query, "List of light curves");
		
	}
	
	askInfo(query, filename);
	
	vector<string> list;
	
	read_list(list,filename);
	
	int n_files=list.size();
	
	cout << "You have given " << n_files << " light curves\n";
	if(n_files ==0)
	{
		return 1;
	}
	
	double time_resolution=0.001, t_ref=0, t_stop=0, mjdref=0;
	double ra_obj=267.021650,dec_obj=-24.779833;
	char int_bary='n',og_name[100];
	sprintf(og_name, "TERZAN5/og_ibis.fits");
	if(evt_flag ==1)
	{
		get_time_resolution(list[0].c_str(), &time_resolution, &t_ref, &t_stop, &mjdref,&int_bary,&ra_obj,&dec_obj,og_name);
		event_file ef(list[0].c_str());
	}
	else
	{
		light_curve ll(list[0].c_str());
		ll.output();
		time_resolution=ll.time_del();
		t_ref=ll.t_start();
		t_stop=ll.t_stop();
		mjdref=ll._mjd_ref;
	}
	
	cout << "Time resolution is [s] " << time_resolution << endl;
	cout << "Nymquist frequency is [Hz] " << 0.5/time_resolution << endl;
	cout << "Minimum frequncy is [Hz] " << 1./(t_stop-t_ref) << endl; 
	
	int rebin_factor=1;
	sprintf(query, "Rebin factor (integer >= 1)");	
	ask_integer(query, &rebin_factor);
	
	cout << "Rebin factor is " << rebin_factor <<endl;
	cout << "New time bin is " << time_resolution*rebin_factor << " s\n";
	
	double e_min=0.5;
	double e_max=10;
	
	if(evt_flag ==1)
	{
		sprintf(query, "Minimum energy [keV for XRT]");
		ask_double(query, &e_min);
		
		sprintf(query, "Maximum energy [keV]");
		ask_double(query, &e_max);
	}

	sprintf(query, "Choose the normalization");
	n_opt=2;
	char *menu1[]={"Leahy", "RMS"};
	int norm_flag=1;	
	norm_flag=ask_menu(n_opt, menu1, norm_flag, query);
	
	sprintf(query, "Choose the window");
	n_opt=4;
	char *menu2[]={"Square", "Barlett", "Welch", "Hamm"};
	int wind_flag=3;	
	wind_flag=ask_menu(n_opt, menu2, wind_flag, query);
	
	double time_interval=1000;
	sprintf(query, "Length of time interval in s");
	ask_double(query, &time_interval);
	//get the closest power of two length of interval
	int n_t = (int)ceil(time_interval/(rebin_factor*time_resolution));
	int n=4;
	while(n<n_t/2)
		n*=2;
	cout << "Original n is "<< n_t <<endl;
	cout << "Final    n is "<< n << "= " << n*rebin_factor*time_resolution << " s" <<endl;
	cout << "Spectrum will have "<< n <<" bins" <<endl;
		
	
	sprintf(filename,"Prova.qdp");
	sprintf(query, "Output file");
	askInfo(query, filename);
	
	double log_step=0.15;
	sprintf(query, "Logarithmic step (>0)\nNo rebinning (=0)\nLinear step(<0)\n");
	ask_double(query,&log_step);

	double tot_ontime=0;
	
	Spectolap spec(n);
	Hann h(n);
	
	if(evt_flag == 1)
	{
		cerr << "Sorry not implemented yet\n";
		return 1;
	}
	else
	{
		for(int k=0;k<n_files;k++) //loop on files
		{
			cout << list[k] << endl;
			light_curve *l1 = new light_curve(list[k].c_str());
			tot_ontime += l1->ontime();
			l1->output();
			l1->rebin(rebin_factor);
			VecDoub sf1( l1->size(), l1->_y);
			switch (wind_flag)
			{
				case 1:
					spec.addlongdata(sf1, square);
					break;
				case 2:
					spec.addlongdata(sf1, bartlett);
					break;
				case 3:
					spec.addlongdata(sf1, welch);
					break;
				case 4:
					spec.addlongdata(sf1, h);
					break;
				
				default:
					cerr <<"Invalid window\n";
					return 1;
					break;

			}
			
			l1->deallocate();
			l1->clean_values();
		} //end of loop on files
	}
	
	VecDoub ss=spec.spectrum(); //divided by the number of summed points, Lehay
	VecDoub fr=spec.frequencies();
	
//	for(int i=0;i<n;i++)
//		cout << fr[i] << "\t" << ss[i] << endl;

	
	double fr_step=1./(rebin_factor*time_resolution);
	fr*=fr_step;
	
	fr_step/=(2*n);
	cout << "Frequency step is " << fr_step << " Hz\n";
	
	if(norm_flag == 2)
	{
		ss *= (n*rebin_factor*time_resolution)/spec.nsum;
	}
	
	
	ofstream ff(filename);
	
	if(!ff.is_open())
	{
		cerr << "Error in opening " << filename << endl;
		return 1;
	}
	
	
	double f1=fr[0]+fr_step; //Otherwise infinite loop due to zero
	double f2=f1*(1+log_step);
	if(log_step<0)
		f2=f1-log_step;
	
	
	ff << "read serr 1 2\n";
	ff<<"cpd /xw\n";
	ff <<"ma 1 on 2\n";
	ff <<"lw 2\n";
	ff <<"cs 1.1\n";
	ff <<"font roman\n";
	ff <<"time off\n";
	ff <<"lab title\n";
	ff <<"lab x Frequency [Hz]\n";
	if(norm_flag==1)
		ff <<"lab f Leahy normalization\n";
	else
		ff <<"lab f RMS normalization\n";
	
	ff << "lab y PSD\n";
	ff << "log y on\nlog x on\n";
	
//	cout << "F1-2 " << f1 <<" " << f2<<endl;
	if(log_step!=0)
	{
		while(f2 < fr[n-1])
		{
			int ind1=(int)floor(f1/fr_step);
			int ind2=(int)(floor(f2/fr_step));
			double av_psd=0, s_psd=0;
			double n_done=0;
			
			for(int i=ind1;i<ind2;i++)
			{
				av_psd+=ss[i];
				n_done++;
			}
			
			if(n_done)
			{
				av_psd/=n_done;
			}
			
			for(int i=ind1;i<ind2;i++)
			{
				s_psd+=(ss[i]-av_psd)*(ss[i]-av_psd);
			}
			
			if(n_done > 1)
			{
				s_psd=sqrt(s_psd/(n_done*(n_done-1)));
			}
			
			if(av_psd>0 && s_psd>0)
			{
				ff<<setw(15) << (f2+f1)/2. << "\t" << (f2-f1)/2<<"\t";
				ff<<setw(13) << av_psd << "\t" << s_psd<< "\n";
			}
			f1=f2;
			if(log_step>0)
				f2=f1*(1+log_step);
			else
				f2=f1-log_step;
			
		}//end of loop
	}
	else
	{
		for(int i=0;i<n;i++)
		{
			ff<<setw(15) << fr[i]+fr_step/2.<< "\t" << 0<<"\t";
			ff<<setw(13) << ss[i] << "\t" << 0<< "\n";
		}
		ff<<"error x off\nerror y off\nmar 2 off\nls 1\n"; //make a line
	}
			
	
	ff.close();
	
	return status;
}


int z2_stat()
{
	int status=0;
	char filename[FLEN_FILENAME];
	char query[2048];
	char out_filename[FLEN_FILENAME];
	sprintf(filename, "list_evt.txt");
	sprintf(out_filename, "Z2");
	
	sprintf(query, "Choose the data format (Attention, events and light curves files are processed separately)");
	int n_opt=2;
	char *menu[]={"Events", "Light Curves"};
	int evt_flag=1;
	double min_sn=4;
	char do_simul='n';
	int nruns=100;
	double max_time_separation_simul=1e12;

	double e_min=0.5;
	double e_max=10;
	char flag_binarycor='n';
	char binary_file[FLEN_FILENAME];
	sprintf(binary_file, "%s", "none");
	double nu_start=0.01;
	double nu_stop=100;
	int n_bins=100;
	int n_harm=2;
	char log_spacing='n';
	int n_events_per_chunk=100000;
	double max_time_separation=1e12;
	double min_significance=2.5;
	char flag_out_simul=0;
	char flag_out_char='n';
        double tolerance=1;
        char flag_sw='n';
        char flag_err='n';
        double interval_sw=5;
        double step_sw=3;
        char gb_method='n'; //New method based on GB's paper if this flag =='y' 


	
	{
            ifstream conf_file(".z2_conf.txt");
            if(conf_file.is_open())
            {
                conf_file >> evt_flag;
                conf_file >> filename ;
                conf_file >> e_min ;
                conf_file >> e_max ;
                conf_file >> min_sn;
                conf_file >> flag_binarycor;
                conf_file >> binary_file;
                conf_file >> nu_start;
                conf_file >> nu_stop;
                conf_file >> n_bins;
                conf_file >> n_harm;
                conf_file >> log_spacing;
                conf_file >> n_events_per_chunk;
                conf_file >> out_filename;
                conf_file >> max_time_separation;
                conf_file >> min_significance;
                conf_file >> do_simul;
                conf_file >> nruns;
                conf_file >> max_time_separation_simul;
                conf_file >> flag_out_char;
                conf_file >> tolerance;
                conf_file >> flag_sw;
                conf_file >> interval_sw;
                conf_file >> step_sw;
                conf_file >> gb_method;
                conf_file >> flag_err;
                conf_file.close();
            }
	}
	evt_flag=ask_menu(n_opt, menu , evt_flag, query);
	
	if(evt_flag ==1)
	{
		sprintf(query, "List of event files");
		
	}
	else
	{
//		sprintf(filename, "list_lc.txt");
		sprintf(query, "List of light curves");
	}
	
	//askInfo(query, filename);
	readline_withspace(filename,query);
	vector<string> list;
	
	read_list(list,filename);
	
	int n_files=list.size();
	
	cout << "You have given " << n_files << " files\n";
	if(n_files ==0)
	{
		return 1;
	}
	
	double time_resolution=0.001, t_ref=0, t_stop=0;
	light_curve *ll;
	double mjdref=0;
	double ra_obj=267.021650,dec_obj=-24.779833;
	char int_bary='n',og_name[100];
	sprintf(og_name, "TERZAN5/og_ibis.fits");
	
	if(evt_flag ==1)
	{
		get_time_resolution(list[0].c_str(), &time_resolution, &t_ref,
                        &t_stop, &mjdref, &int_bary,&ra_obj,&dec_obj,og_name);
		event_file ef(list[0].c_str());
	}
	else
	{
		ll = new light_curve(list[0].c_str());
		time_resolution=ll->time_del();
		t_ref=ll->t_start();
		t_stop=ll->t_stop();
		ll->scan_gti(); //to be used in bnary correction setup.
		mjdref=ll->_mjd_ref;
	}
	
	cout << "Time resolution is [s] " << time_resolution << endl;
	cout << "Nymquist frequency is [Hz] " << 0.5/time_resolution << endl;
	cout << "Minimum frequency step is [Hz] " << 1/(t_stop-t_ref) << endl;
	

	
	if(evt_flag ==1)
	{
		sprintf(query, "Minimum energy [keV for Swift/XRT and XMM, PI channel for MECS, not influent for PCA]");
		ask_double(query, &e_min);
		
		sprintf(query, "Maximum energy [keV]");
		ask_double(query, &e_max);
	}

	

	sprintf(query, "Do you want to apply binary correction?");
	flag_binarycor=ask_yes_no(query	, flag_binarycor);

		
	if(flag_binarycor == 'y')
	{		
		sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
		askInfo(query, binary_file);
		char setup_file_exist=1;
		char interactive_flag=0;
		if (strcmp(binary_file, "none")==0)
		{
			sprintf(query, "Enter the actual name of the file to store the binary parameters");
			askInfo(query, binary_file);

			//sprintf(binary_file,"%s_orbit.dat",active_src_name);
			interactive_flag=1;
			setup_file_exist=0;
		}
		if(interactive_flag)
			binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, mjdref );
	}
	
	if(evt_flag==0)
		delete ll;
        sprintf(query,"Do you want to use a sliding window for the burst oscillations (no GB method)?");
        flag_sw=ask_yes_no(query, flag_sw);

        if(flag_sw == 'y')
        {
            sprintf(query, "Integration time in s");
            ask_double(query, &interval_sw);

            sprintf(query, "Step in s");
            ask_double(query, &step_sw);

        }
	
	sprintf(query, "Start frequency [Hz]");
	ask_double(query, &nu_start);

	sprintf(query, "Stop frequency [Hz]");
	ask_double(query, &nu_stop);

	sprintf(query, "number of bins");
	ask_integer(query, &n_bins);
	
	sprintf(query, "How many harmonics?");
	ask_integer(query, &n_harm);	
	
	if(n_bins<10)
	{
		cerr << "Set n_bins to 10\n";
		n_bins=10;
	}
	
	sprintf(query, "Use logarithmic spacing?");
	log_spacing=ask_yes_no(query, log_spacing);
	
	if(evt_flag ==1)
	{
		sprintf(query, "How many events per chunk of data (not supported for LC)?");
		ask_integer(query, &n_events_per_chunk);
		sprintf(query, "Maximum time separation between subsequent events");
		ask_double(query, &max_time_separation);
	}
	

	
	sprintf(query, "Output qdp root file name");
	askInfo(query, out_filename);

	
	sprintf(query, "Minimum significance for Z2 fit of the peak (computed after trials)");
	ask_double(query, &min_significance);

        sprintf(query, "Do you want to use G. Belanger's method?");
	gb_method=ask_yes_no(query	, gb_method);
        if (gb_method == 'y' && evt_flag != 1)
        {
            sprintf(query, "Do you want to weight for errors of the light curve?");
            flag_err=ask_yes_no(query	, flag_err);
        }
        else if (evt_flag != 1)
	{
		sprintf(query, "Minimum S/N for event conversion");
		ask_double(query, &min_sn);
	}

        
        
        if(flag_sw =='n')
        {
            sprintf(query, "Do you want to perform simulation of errors? (LC only single file, no GB method, "
                    "otherwise behavior is undetermined !!)");

            do_simul =ask_yes_no(query, do_simul);
            if(do_simul == 'y')
            {
                    sprintf(query, "How many simulaiton runs?");
                    ask_integer(query, &nruns);

                    sprintf(query, "Maximum time separation between subsequent events for GTI determination");
                    ask_double(query, &max_time_separation_simul);

                    sprintf(query, "Significance in sigmas of the detection (use a suitable number of trials, e.g. >=1000 for 3 sigmas- 99 percent c.l. is 2.575 sigmas)");
                    ask_double(query, &tolerance);
    //		if(flag_out_simul)
    //			flag_out_char='y';

                    sprintf(query, "Output intermmediate products?");
                    flag_out_char=ask_yes_no(query, flag_out_char);

                    if(flag_out_char == 'y')
                            flag_out_simul=1;
                    else
                            flag_out_simul=0;


            }
        }
        else
            do_simul='n';
	
	double best_freq=0, err_best_freq=0, max_z2=0;
	
	ofstream conf_file(".z2_conf.txt");
	conf_file << evt_flag << endl;
	conf_file << filename << endl;
	conf_file << e_min << endl;
	conf_file << e_max << endl;
	conf_file << min_sn << endl;
	conf_file << flag_binarycor << endl;
	conf_file << binary_file << endl;
	conf_file << nu_start << endl;
	conf_file << nu_stop << endl;
	conf_file << n_bins << endl;
	conf_file << n_harm << endl;
	conf_file << log_spacing << endl;
	conf_file << n_events_per_chunk << endl;
	conf_file << out_filename << endl;
	conf_file << max_time_separation<< endl;
	conf_file << min_significance << endl;
	conf_file << do_simul << endl;
	conf_file << nruns << endl;
	conf_file << max_time_separation_simul<< endl;
	conf_file << flag_out_char<< endl;
	conf_file << tolerance << endl;
        conf_file << flag_sw << endl;
        conf_file << interval_sw << endl;
        conf_file << step_sw << endl;
	conf_file << gb_method << endl;
        conf_file << flag_err << endl;
	
	conf_file.close();

        if(gb_method=='n')
        {
            if(flag_sw =='n')
            {
                status+=make_z2_stat(list, evt_flag, e_min, e_max, flag_binarycor, binary_file,
                    log_spacing, nu_start, nu_stop, n_bins, n_harm, 1, out_filename, mjdref,
                    t_ref, best_freq, err_best_freq, max_z2, n_events_per_chunk, min_sn, int_bary,ra_obj,dec_obj,
                    og_name,max_time_separation, min_significance, do_simul, nruns,
                        max_time_separation_simul, flag_out_simul, tolerance);
            }
            else
            {
                status+=make_z2_stat_slide_time_window(list, evt_flag, e_min, e_max, flag_binarycor, binary_file,
                    log_spacing, nu_start, nu_stop, n_bins, n_harm, 1, out_filename, mjdref,
                    t_ref, best_freq, err_best_freq, max_z2, interval_sw, step_sw, min_sn, min_significance);
            }
        }
        else
        {
            status+=make_z2_stat_gb(list, evt_flag, e_min, e_max, flag_err, flag_binarycor, binary_file,
                    log_spacing, nu_start, nu_stop, n_bins, n_harm, 1, out_filename, mjdref,
                    t_ref, best_freq, err_best_freq, max_z2, n_events_per_chunk, min_sn, int_bary,ra_obj,dec_obj,
                    og_name,max_time_separation, min_significance, do_simul, nruns,
                        max_time_separation_simul, flag_out_simul, tolerance);
        }
        
	return status;
}



int fit_z2_peak()
{
	int status=0;
	char filename[FLEN_FILENAME];
	char query[2048];
		
	sprintf(query, "QDP file");
	sprintf(filename, "Z2.qdp");
	
	askInfo(query, filename);
	
	ifstream fi(filename);
	if (!fi.is_open())
	{
		cerr << "Connot open " << filename	<< endl;
		return 1;
	}
	
	int max_line=1024;                                                                                                           
	char line[max_line];
	vector<double> frequency, z2;
	double t1=0,t2=0,t3=0,t4=0;

	while(fi.getline(line, max_line, '\n'))                                                                                  
	{                                                                                                                            
		if(line[0] != '\n' && line[0] != '\0')                                                                               
		{                    
			if(sscanf(line, "%lf %lf %lf %lf", &t1, &t2, &t3, &t4) == 4)
			{
				frequency.push_back(t1);
				z2.push_back(t2);
			}
		}                                                                                                                    
	}
	
	fi.close();
	
	double best_freq=0;
	double err_best_freq=0;
	double max_peak=0;

	get_best_frequency(frequency, z2, best_freq, err_best_freq, max_peak, filename, 1);
	
	return 	status;
	
}

int lomb_scargle()
{
	int status=0;
	char lc_name[FLEN_FILENAME], conf_file[FLEN_FILENAME], out_root[FLEN_FILENAME], out_filename[FLEN_FILENAME];
	char query[10*FLEN_FILENAME];
	double oafac=4, hifac=0;
	char do_simul='n';
	char do_simul_output='n';
        char do_simul_output_flag=0;
	int nruns=100;
	sprintf(conf_file, ".lomb_conf.txt");
	sprintf(lc_name, "lc.fits");
	sprintf(out_root, "lc_ls");
	int weight_flag=1;
	double ff=1.2;
	char detrend_flag='y';
	double detrend_scale=1;
	double minfac=1;
	double gti_gap=0;
	char flag_binarycor='n';
	char binary_file[FLEN_FILENAME];
	sprintf(binary_file, "%s", "Porbit.dat");
	
	{
		ifstream fp(conf_file);
		if(fp.is_open())
		{
			fp >> lc_name;
			fp >> oafac;
			fp >> hifac;
			fp >> do_simul;
			fp >> nruns;	
			fp >> out_root;
			fp >> do_simul_output;
			fp >> weight_flag;
			fp >> ff;
			fp >> detrend_flag;
			fp >> detrend_scale;
			fp >> minfac;
			fp >> gti_gap;
			fp >> flag_binarycor;
			fp >> binary_file;
			fp.close();
		}
	}
	
	cout << "Lomb Scargle periodogram following the algorithm of NR after Press & Ribicky (1989)\n";
	sprintf(query, "Light curve file");
	askInfo(query, lc_name );
	
	light_curve lc(lc_name);
	lc.output();

	sprintf(query, "Do you want to apply binary correction ?");
	flag_binarycor=ask_yes_no(query	, flag_binarycor);
	double mjdref = lc.mjd_ref();
	if(flag_binarycor == 'y')
	{
		sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
		askInfo(query, binary_file);
		char setup_file_exist=1;
		char interactive_flag=0;
		if (strcmp(binary_file, "none")==0)
		{
			sprintf(query, "Enter the actual name of the file to store the binary parameters");
			askInfo(query, binary_file);
			
			//sprintf(binary_file,"%s_orbit.dat",active_src_name);
			interactive_flag=1;
			setup_file_exist=0;
		}
//		soft.binary_corr(binary_file);
		if(interactive_flag)
                        binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, mjdref );

		lc.binary_corr(binary_file);
	}
		
	if	(lc.scan_gti())
	{
		sprintf(query, "Enter the maximum allowed gap between point not to create a GTI, if <=0 not creating GTIs");
		ask_double(query, &gti_gap);
		cout << "GTI GAP " << gti_gap << endl;
		if (gti_gap>0)
		{
			lc.build_gti(gti_gap);
			lc.scan_gti();
		}
	}
	sprintf(query, "Choose the weighting method");
	int n_opt=3;
	char *menu_2[]={"None", "1/sigma (D'Ai et al, 2010)", "1/(sigma+Vs) (Corbet et al., 2007)"};
	weight_flag=ask_menu(n_opt,menu_2, weight_flag, query);
	if(weight_flag==3)
	{
		sprintf(query, "Factor f of weighting");
		ask_double(query, &ff);
	}

	sprintf(query, "Do you want to detrend the light curve");
	detrend_flag=ask_yes_no(query, detrend_flag);

	if(detrend_flag=='y')
	{
		cout << "The 'detrend scale' is the time window to be used in the sliding average to detrend the light curve\n";
		sprintf(query, "Detrend scale");
		ask_double(query, &detrend_scale);
	}
        

	sprintf(query, "Oversampling factor (typical sampling is every %g s (=%g d))  ",
                (lc.t_stop()-lc.t_start())/lc.size(), (lc.t_stop()-lc.t_start())/lc.size()/86400.);
	ask_double(query, &oafac);
	
	cout << lc.size() << endl;
	cout << lc.t_start() << endl;
	cout << lc.t_stop() << endl;
	
	sprintf(query, "Maximum frequency as function of the Nymquist average %g Hz (=%g d)",
                0.5*lc.size()/(lc.t_stop()-lc.t_start()) , 2*(lc.t_stop()-lc.t_start())/lc.size() / 86400);
	ask_double(query, &hifac);

	sprintf(query, "Minimum frequency as multiple of the length  %g Hz (=%g d)",
                2./(lc.t_stop()-lc.t_start()) , 0.5*(lc.t_stop()-lc.t_start())/86400. );
	ask_double(query, &minfac);



	sprintf(query, "Output root file");
	askInfo(query, out_root);
	
	sprintf(query, "Perform simulation");
	do_simul=ask_yes_no(query, do_simul);
	
	if(do_simul == 'y')
	{
		sprintf(query, "How many runs?");
		ask_integer(query, &nruns);
		
		sprintf(query, "Produce simulation output");
		do_simul_output=ask_yes_no(query, do_simul_output);
		if(do_simul_output=='y')
			do_simul_output_flag=1;
		else
			do_simul_output_flag=0;
		
	}

	ofstream fp(conf_file);
	if(fp.is_open())
	{
		fp << lc_name << endl;
		fp << oafac << endl;
		fp << hifac << endl;
		fp << do_simul << endl;
		fp << nruns << endl;	
		fp << out_root << endl;
		fp << do_simul_output << endl;
		fp << weight_flag << endl;
		fp << ff <<endl;
		fp << detrend_flag << endl;
		fp << detrend_scale << endl;
		fp << minfac << endl; 
		fp << gti_gap << endl;
		fp << flag_binarycor << endl;
		fp << binary_file << endl;
		fp.close();
	}
	
	VecDoub tt(lc.size(), lc._x);
        light_curve ls(&lc);

        if(detrend_flag=='y')
        {
            char fname_detrend[FLEN_FILENAME];
            sprintf(fname_detrend, "%s_detrend.qdp", out_root);
            ls.detrend(detrend_scale, 1, fname_detrend);
        }

        if(weight_flag ==2 )
            ls.simple_weight();
        if(weight_flag ==3 )
            ls.complex_weight(ff);


	VecDoub rr(ls.size(), ls._y);
	
	VecDoub_O fr(1);
	VecDoub_O lp(1);
	
	int nout=0, jmax=0;
	Doub prob=0;
	
	cout << "Start performing LS periodogram (fast)\n";
	fasper(tt, rr, oafac,  hifac, fr, lp, nout, jmax, prob);
	
	cout << "Maximum of LS has probability " << prob << " to be a false periodicity\n"; 
	
	cout << "Write output file\n";
	
	sprintf(out_filename, "%s.qdp", out_root);
	fp.open(out_filename);
	if (fp.is_open())
	{
		fp << "read\n";
		fp << "cpd /xw\nma 17 on\nline on 2\n";
		fp << "lab x Freq [Hz]\n";
		fp << "lab y PSD\n";
		for(int i=0;i<fr.size();i++)
		{
                    if(fr[i] > 3./(lc.t_stop()-lc.t_start()) )
			fp <<  scientific << setw(19) << setprecision(12) << fr[i] << " " << lp[i] << endl;
		}
                fp.close();
	}
	else
	{
		cerr << "Could not open '" << out_filename << "'\n";
	}
	sprintf(out_filename, "%s_d.qdp", out_root);
	fp.open(out_filename);
	if (fp.is_open())
	{
		fp << "read\n";
		fp << "cpd /xw\nma 17 on\nline on 2\n";
		fp << "lab x P [d]\n";
		fp << "lab y PSD\n";
		for(int i=fr.size()-1;i>=0;i--)
		{
                    if(fr[i] > 2./(lc.t_stop()-lc.t_start()) )
			fp <<  scientific << setw(19) << setprecision(12) << 1/fr[i]/86400.0 << " " << lp[i] << endl;
		}
                fp.close();
	}
	else
	{
		cerr << "Could not open '" << out_filename << "'\n";
	}
	double best_freq, err_best_freq_u, err_best_freq_d, max_peak;
	
	sprintf(out_filename, "fit_%s.qdp", out_root);
	char flag_out=1;
        int min_fr_ind=0;
        for (int i=0;i<fr.size();i++)
        {
              if(fr[i] > minfac * 2./(lc.t_stop()-lc.t_start()) )
              {
                  min_fr_ind=i;
                  break;
              }
        }

        cout << "Spectra will be cut at the minimum frequency index " << min_fr_ind << " = " << fr[min_fr_ind] << "Hz, P = " << 1/fr[min_fr_ind]/86400.0 << "d\n";
        fr.section(min_fr_ind, fr.size()-1);
	lp.section(min_fr_ind, fr.size()-1);


	get_best_frequency(fr, lp, best_freq, err_best_freq_u, err_best_freq_d, max_peak, out_filename, flag_out);
	
	if( do_simul == 'y')
	{
//		VecDoub freqs(nruns,0.0);
//		VecDoub probs(nruns,0.0);
//		VecDoub maxs(nruns,0.0);
                vector <double> freqs;
		vector <double> probs;
		vector <double> maxs;

		for (int i=1;i<=nruns;i++)
		{
			light_curve ls = simul_gaus(lc);
                        if(weight_flag ==2 )
                            ls.simple_weight();
                        if(weight_flag ==3 )
                            ls.complex_weight(ff);

			for (int j=0;j<ls.size();j++)
			{
				tt[j] = ls._x[j];
				rr[j] = ls._y[j];
			}
						
			cout << "Start performing LS periodogram (fast)\n";
			fasper(tt, rr, oafac,  hifac, fr, lp, nout, jmax, prob);
			
			cout << "Maximum of LS has probability " << prob << " to be a false periodicity\n"; 
			
			if(do_simul_output_flag && prob<0.5)
			{
				cout << "Write output file\n";
				
				sprintf(out_filename, "%s_ls_simul_%04d.qdp", out_root,i);
				fp.open(out_filename);
				if (fp.is_open())
				{
					fp << "read\n";
					fp << "cpd /xw\nma 17 on\nline on 2\n";
					fp << "lab x Freq [Hz]\n";
					fp << "lab y PSD\n";
					for(int i=0;i<fr.size();i++)
					{
						fp <<  scientific << setw(19) << setprecision(12) << fr[i] << " " << lp[i] << endl;
					}
					fp.close();
				}
				else
				{
					cerr << "Could not open '" << out_filename << "'\n";
				}
			}
			
			sprintf(out_filename, "fit_%s_ls_simul_%04d.qdp", out_root,i);
		        fr.section(min_fr_ind, fr.size()-1);
                	lp.section(min_fr_ind, fr.size()-1);

			if(get_best_frequency(fr, lp, best_freq, err_best_freq_u, err_best_freq_d, max_peak, out_filename, do_simul_output_flag, 0) == 0)
                        {
    //
    //			freqs[i]=best_freq;
    //			probs[i]=prob;
    //			maxs[i]=max_peak;
                            freqs.push_back(best_freq);
                            probs.push_back(prob);
                            maxs.push_back(max_peak);
                        }
		}
		
		sprintf(out_filename, "simul_%s_ls.qdp", out_root);
		fp.open(out_filename);
		if (fp.is_open())
		{
			fp << "read\ncpd /xw\n";
			fp << "lab x Realization\n";
			fp << "lab y2 freq [Hz]\n";
                        fp << "lab y3 Period [d]\n";

			fp << "lab y4 P\\drej\\u\n";
			fp << "lab y5 Peak\n";
			fp << "plot vert\n";
			for (int i=0;i<freqs.size();i++)
			{
				fp <<  scientific << setw(19) << setprecision(12) << i <<
                                   " " << freqs[i] << " " << 1./freqs[i]/86400. << " " << probs[i] << " " << maxs[i] << endl;
			}
		}
		else
			cerr << "Could not open '" << out_filename << "'\n";
		
	}		
		
	return status;
}

int fit_orbital_model()
{
	int status=0;
	char filename[FLEN_FILENAME];
	char query[2048];
	char out_filename[FLEN_FILENAME];
	sprintf(filename, "list_evt.txt");
	sprintf(out_filename, "binary_model");
	
	sprintf(query, "Choose the data format (Attention, events and light curves files are processed separately)");
	int n_opt=2;
	char *menu[]={"Events", "Light Curves"};
	int evt_flag=1;
	double min_sn=4;
	char do_simul='n';
	int nruns=0;
	double max_time_separation_simul=1e12;

	double e_min=0.5;
	double e_max=10;
	double nu_start=100, nu_start_e=1 ;

	int n_harm=2;
	long int n_events_per_chunk=100000;
	double max_time_separation=1e12;
	double min_significance=2.5;
	char flag_out_simul=0;
	char flag_out_char='n';
        double tolerance=1;
        char flag_sw='n';
        double interval_sw=5;
        double step_sw=3;
        double asini_i=0, ecc_i=0, omega_deg_i=0, t0_i=0, porb_i=0, pporb_i=0;
        double asini_e=0, ecc_e=0, omega_deg_e=0, t0_e=0, porb_e=0, pporb_e=0;


	
	{
		ifstream conf_file(".binary_model_conf.txt");
		if(conf_file.is_open())
		{
			conf_file >> evt_flag;
			conf_file >> filename ;
			conf_file >> e_min ;
			conf_file >> e_max ;
			conf_file >> min_sn;
			conf_file >> asini_i;
                        conf_file >> ecc_i;
                        conf_file >> omega_deg_i;
                        conf_file >> t0_i; 
                        conf_file >> porb_i;
                        conf_file >> pporb_i;
			conf_file >> nu_start;
                        conf_file >> asini_e;
                        conf_file >> ecc_e;
                        conf_file >> omega_deg_e;
                        conf_file >> t0_e; 
                        conf_file >> porb_e;
                        conf_file >> pporb_e;
			conf_file >> nu_start_e;
			conf_file >> n_harm;
			conf_file >> n_events_per_chunk;
			conf_file >> out_filename;
			conf_file >> max_time_separation;
			conf_file >> min_significance;
			conf_file >> do_simul;
			conf_file >> nruns;
			conf_file >> max_time_separation_simul;
			conf_file >> flag_out_char;
                        conf_file >> tolerance;
                        conf_file >> flag_sw;
                        conf_file >> interval_sw;
                        conf_file >> step_sw;
                        
			conf_file.close();
		}
	}
	evt_flag=ask_menu(n_opt, menu , evt_flag, query);
	
	if(evt_flag ==1)
	{
		sprintf(query, "List of event files");
		
	}
	else
	{
//		sprintf(filename, "list_lc.txt");
		sprintf(query, "List of light curves");
	}
	
	//askInfo(query, filename);
	readline_withspace(filename,query);
	vector<string> list;
	
	read_list(list,filename);
	
	int n_files=list.size();
	
	cout << "You have given " << n_files << " files\n";
	if(n_files ==0)
	{
		return 1;
	}
	
	double time_resolution=0.001, t_ref=0, t_stop=0;
	light_curve *ll;
	double mjdref=0;
        //just for routine compatibility
	double ra_obj=267.021650,dec_obj=-24.779833;
	char int_bary='n',og_name[100];
	sprintf(og_name, "TERZAN5/og_ibis.fits");
	
	if(evt_flag ==1)
	{
		get_time_resolution(list[0].c_str(), &time_resolution, &t_ref,
                        &t_stop, &mjdref, &int_bary,&ra_obj,&dec_obj,og_name);
		event_file ef(list[0].c_str());
                n_events_per_chunk=ef._size+1;
	}
	else
	{
		ll = new light_curve(list[0].c_str());
		time_resolution=ll->time_del();
		t_ref=ll->t_start();
		t_stop=ll->t_stop();
		ll->scan_gti(); //to be used in bnary correction setup.
		mjdref=ll->_mjd_ref;
	}
	
	cout << "Time resolution is [s] " << time_resolution << endl;
	cout << "Nymquist frequency is [Hz] " << 0.5/time_resolution << endl;
	cout << "Minimum frequency step is [Hz] " << 1/(t_stop-t_ref) << endl;
	

	
	if(evt_flag ==1)
	{
		sprintf(query, "Minimum energy [keV for Swift/XRT and XMM, PI channel for MECS, not influent for PCA]");
		ask_double(query, &e_min);
		
		sprintf(query, "Maximum energy [keV]");
		ask_double(query, &e_max);
	}
	else
	{
		sprintf(query, "Minimum S/N for event conversion");
		ask_double(query, &min_sn);
	}
	


	
	if(evt_flag==0)
		delete ll;

	
	sprintf(query, "Start frequency [Hz]");
	ask_double(query, &nu_start);
	sprintf(query, "Error on Start frequency [Hz]");
	ask_double(query, &nu_start_e);
	
	sprintf(query, "How many harmonics?");
	ask_integer(query, &n_harm);	

        sprintf(query, "a sini [lt-sec]");
	ask_double(query,  &asini_i);
        sprintf(query, "Error on a sini [lt-sec]");
	ask_double(query,  &asini_e);
        sprintf(query, "Eccentricity");
	ask_double(query,  &ecc_i);
        sprintf(query, "Error on eccentricity");
	ask_double(query,  &ecc_e);
        sprintf(query, "Omega [deg]");
	ask_double(query,  &omega_deg_i);
        sprintf(query, "Error on Omega [deg]");
	ask_double(query,  &omega_deg_e);
        sprintf(query, "t_0 [MJD]");
	ask_double(query,  &t0_i); 
        sprintf(query, "Error on t_0 [MJD]");
	ask_double(query,  &t0_e); 
        sprintf(query, "P orb [d]");
	ask_double(query,  &porb_i);
        sprintf(query, "Error on P orb [d]");
	ask_double(query,  &porb_e);
        sprintf(query, "P dot orb [D/s]");
	ask_double(query,  &pporb_i);        
        sprintf(query, "Error on P dot orb [D/s]");
	ask_double(query,  &pporb_e);
        
	if(evt_flag ==1)
	{
		sprintf(query, "How many events per chunk of data (not supported for LC)?");
		ask_long_integer(query, &n_events_per_chunk);
		sprintf(query, "Maximum time separation between subsequent events");
		ask_double(query, &max_time_separation);
	}
	

	
	sprintf(query, "Output qdp root file name");
	askInfo(query, out_filename);


        if(flag_sw =='n')
        {
            sprintf(query, "Do you want to perform simulation of errors? (LC only single file, otherwise behaviour is undetermined !!)");

            do_simul =ask_yes_no(query, do_simul);
            if(do_simul == 'y')
            {
                    sprintf(query, "How many simulaiton runs?");
                    ask_integer(query, &nruns);

                    sprintf(query, "Maximum time separation between subsequent events for GTI determination");
                    ask_double(query, &max_time_separation_simul);

                    sprintf(query, "Significance in sigmas of the detection (use a suitable number of trials, e.g. >=1000 for 3 sigmas- 99 per cent c.l. is 2.575 sigmas)");
                    ask_double(query, &tolerance);
    //		if(flag_out_simul)
    //			flag_out_char='y';

                    sprintf(query, "Output intermediate products?");
                    flag_out_char=ask_yes_no(query, flag_out_char);

                    if(flag_out_char == 'y')
                            flag_out_simul=1;
                    else
                            flag_out_simul=0;


            }
        }
        else
            do_simul='n';
	
	
	ofstream conf_file(".binary_model_conf.txt");
	conf_file << scientific << setw(19) << setprecision(12) << evt_flag << endl;
	conf_file << filename << endl;
	conf_file << e_min << endl;
	conf_file << e_max << endl;
	conf_file << min_sn << endl;
	conf_file << asini_i << endl;
	conf_file << ecc_i << endl;
	conf_file << omega_deg_i << endl;
	conf_file << t0_i << endl; 
	conf_file << porb_i << endl;
	conf_file << pporb_i << endl;
	conf_file << nu_start << endl;
	conf_file << asini_e << endl;
	conf_file << ecc_e << endl;
	conf_file << omega_deg_e << endl;
	conf_file << t0_e << endl; 
	conf_file << porb_e << endl;
	conf_file << pporb_e << endl;
	conf_file << nu_start_e << endl;
	conf_file << n_harm << endl;
	conf_file << n_events_per_chunk << endl;
	conf_file << out_filename << endl;
	conf_file << max_time_separation<< endl;
	conf_file << min_significance << endl;
	conf_file << do_simul << endl;
	conf_file << nruns << endl;
	conf_file << max_time_separation_simul<< endl;
	conf_file << flag_out_char<< endl;
	conf_file << tolerance << endl;
	conf_file << flag_sw << endl;
	conf_file << interval_sw << endl;
	conf_file << step_sw << endl;
	
        	
        
	conf_file.close();
	
        
	int flag_out=0;
	
	double best_freq=0, err_best_freq=0, max_z2=0;
	
	status=  get_orbital_solution(list, evt_flag, e_min, e_max, 
		nu_start, nu_start_e,
		n_harm, flag_out, out_filename, mjdref, t_ref,
		best_freq, err_best_freq, max_z2, 
		ecc_i, asini_i, porb_i, t0_i, omega_deg_i, pporb_i,
		ecc_e, asini_e, porb_e, t0_e, omega_deg_e, pporb_e,
		n_events_per_chunk,
		min_sn, 
		max_time_separation, min_significance, do_simul, nruns,
		max_time_separation_simul, flag_out_simul, tolerance);
        
	return status;
}	

int rebin_lc()
{

	int status=0;
	
	cout << "Perform adaptive rebinning\n";
	char filename1[FLEN_FILENAME];
	char filename3[FLEN_FILENAME];
	char query[2048];
	
	sprintf(query, "Light Curve");
	sprintf(filename1, "src.lc");
	
	askInfo(query, filename1);
	
	sprintf(query, "Out Light Curve (QDP)");
	sprintf(filename3, "src.qdp");
	
	askInfo(query, filename3);
	
	
	light_curve soft(filename1);
	soft.output();
	soft.scan_gti();

	double min_sn=3;
	double max_time=10000;
        double min_bin_size=0;
//	int flag_rebin=1;
	
	sprintf(query, "Minimum S/N for rebinning");
	ask_double(query, &min_sn);
	
	sprintf(query, "Maximum time interval. It does not accumulate longer bins, regardless of S/N and it removes all previous points.");
	ask_double(query, &max_time);
	
	sprintf(query, "Minimum time bin size. No shorter bins are allowed.");
	ask_double(query, &min_bin_size);
        
	double gti_threshold=100;
	sprintf(query, "Threshold to use a GTI GAP. It discards all previous data if a gti gap is larger than this value and S/N threshold is not reached.");
	ask_double(query, &gti_threshold);

	double max_point_sn=100;
	sprintf(query, "The maximum noise over signal ratio allowed (to avoid very noisy points)");
	ask_double(query, &max_point_sn);


	double off_set=0;
	sprintf(query, "Off-set in s for display");
	ask_double(query, &off_set);
	
	int *table;
	table=soft.make_rebin_table(min_sn, max_time,gti_threshold, min_bin_size, max_point_sn);
	
	soft.rebin(table);	
	soft.make_rate();
	ofstream ff(filename3);
	
	ff << "read serr 1 2\n";
	ff<<"cpd /xw\n";
	ff <<"ma 1 on 2\n";
	ff <<"lw 2\n";
	ff <<"cs 1.1\n";
	ff <<"font roman\n";
	ff <<"time off\n";
	ff <<"lab title\n";
	ff <<"lab x Time [s - "  << scientific << setw(19) << setprecision(11) << soft._x[0]+off_set << "]\n";
	ff <<"log x off\n";
	ff <<"lab f\n";
	ff<<"plot vert\n";
	ff << "lab y Rate [cts/s]\n";
	ff.flush();
	for(int i=0;i<soft.size();i++)
	{
		ff << scientific << setw(19) << setprecision(11) << soft._x[i]-soft._x[0]-off_set << "\t";
		ff << scientific << setw(19) << setprecision(11) << soft._dx[i] /2 << "\t";
		ff << scientific << setw(19) << setprecision(11) << soft._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << soft._dy[i] << "\n";
	}
	
	ff.close();
	
	
	return status;
}	



int hardness_ratio()
{
	int status=0;
	char filename1[FLEN_FILENAME];
	char filename2[FLEN_FILENAME];
	char filename3[FLEN_FILENAME];
	char conf_file[FLEN_FILENAME];
	double min_sn=3;
	double max_time=10000;
    double min_bin_size=0, ext_t0=0;
	int flag_rebin=1;
	double gti_threshold=100;

	double max_point_sn=100;
    char single_point_check='n';


	int display_unit=1;
	double scale_unit=1;
	char query[2048];
	sprintf(conf_file, ".conf_hr.txt");
	
	sprintf(filename1, "soft.lc");
	sprintf(filename2, "hard.lc");
	sprintf(filename3, "hratio.qdp");

	{
		ifstream fp(conf_file);
		if(fp.is_open())
		{
			fp >> filename1;
			fp >> filename2;
			fp >> filename3;
                        fp >> min_sn;
                        fp >> max_time;
                        fp >> gti_threshold;
                        fp >> flag_rebin;
                        fp >> min_bin_size;
			fp >> ext_t0;
			fp >> display_unit;
			fp >> max_point_sn;
            fp >> single_point_check;
			fp.close();
		}
                else
                {
                    cerr << "Warning :: could not open " << conf_file << endl;
                }
	}

	sprintf(query, "Soft Curve");
	askInfo(query, filename1);
	//readline_withspace(filename1, query);

	sprintf(query, "Hard Curve");
	askInfo(query, filename2);
	//readline_withspace(filename2, query);
	
	sprintf(query, "Name of output QDP file");
	askInfo(query, filename3);
	//readline_withspace(filename3, query);

	light_curve soft(filename1);
	soft.output();
	soft.scan_gti();
	light_curve hard(filename2);
	hard.output();
	hard.scan_gti();
	
	sprintf(query, "Minimum S/N for rebinning");
	ask_double(query, &min_sn);

	
	sprintf(query, "Maximum time interval. It does not accumulate longer bins, regardless of S/N and it removes all previous points.");
	ask_double(query, &max_time);
	
	sprintf(query, "Minimum time bin size. No shorter bins are allowed.");
	ask_double(query, &min_bin_size);
        
	sprintf(query, "Threshold to use a GTI GAP. It discards all previous data if a gti gap is larger than this value and S/N threshold is not reached.");
	ask_double(query, &gti_threshold);
    
    

	sprintf(query, "The maximum noise over signal ratio allowed (to avoid very noisy points)");
	ask_double(query, &max_point_sn);


	sprintf(query, "Input T0 in seconds (if <0 use first bin of data)");
	ask_double(query, &ext_t0);


    sprintf(query, "Do you want to enable breaking of accumulation if a single point with S/N exceeding threshold is enountered?");
    single_point_check=ask_yes_no(query, single_point_check);

	
	sprintf(query, "Use soft (1) hard (2), sum (3), or HR (4) for rebinning");
	
	do{
		ask_integer(query, &flag_rebin);
	}while(flag_rebin <1 || flag_rebin >4);


	sprintf(query, "Choose display units (for ascii, input is assumed in s)");
    char *menu[]={"s", "h", "d"};
	int n_opt=3;

    display_unit=ask_menu(n_opt, menu , display_unit, query);
	if(display_unit == 2)
		scale_unit=3600;
	else if (display_unit==3)
		scale_unit=86400;
	else
		scale_unit=1;

	
	
	{
		ofstream fp(conf_file);
		if(fp.is_open())
		{
			fp << filename1 << endl;
			fp << filename2 << endl;;
			fp << filename3 << endl;;
			fp << min_sn << endl;;
			fp << max_time << endl;;
			fp << gti_threshold << endl;;
			fp << flag_rebin << endl;
			fp << min_bin_size << endl;
			fp << scientific << setw(19) << setprecision(11) << ext_t0 << endl;
			fp << display_unit << endl;
			fp << max_point_sn << endl ;
			fp << single_point_check <<endl;
			fp.close();
		}
		else
		{
			cerr << "Warning :: could not open " << conf_file << endl;
		}
	}



	int *table=0;
	light_curve sum_lc = soft + hard;
    //light_curve initial_hr;
    
    
	switch(flag_rebin)
	{
		case 1 :
			table=soft.make_rebin_table(min_sn, max_time,gti_threshold,min_bin_size, max_point_sn, single_point_check);
			break;
		case 2:
			table=hard.make_rebin_table(min_sn, max_time,gti_threshold, min_bin_size, max_point_sn, single_point_check);
			break;
		case 3:
			table=sum_lc.make_rebin_table(min_sn, max_time,gti_threshold, min_bin_size, max_point_sn, single_point_check);
			break;
        case 4:
        {
            light_curve initial_hr=hard/soft;
			table=initial_hr.make_rebin_table(min_sn, max_time,gti_threshold, min_bin_size, max_point_sn, single_point_check);
        }   
			break;
		default :
			cerr << "Unknown flag for rebin\n";
			exit(1);
			break;
	}
	
	
	
	if(soft.rebin(table))
        {
                cerr << "Failed to rebin soft curve\n";
                exit(1);
        }
        
	if(hard.rebin(table))
	{
                cerr << "Failed to rebin hard curve\n";
                exit(1);
        }
        
	soft.make_rate();
	hard.make_rate();
	
	ofstream ff(filename3);
	
	ff << "read serr 1 2\n";
	ff<<"cpd /xw\n";
	ff <<"ma 1 on 2\n";
	ff <<"lw 2\n";
	ff <<"cs 1.1\n";
	ff <<"font roman\n";
	ff <<"time off\n";
	ff <<"lab title\n";
	ff <<"lab x Intensity [cts/s]\n";
	ff <<"log x on\n";
	//ff <<"r x 0.05 1.5\n";
	//ff <<"r y 0.0 3.0\n";

	ff << "lab y Hardness ratio\n";
	ff.flush();
	light_curve ratio = hard/soft;// soft.hratio(hard);//(hard-soft);
	light_curve sum   = (hard+soft);
	//ratio/=sum;
	//char flag1=0;
	//char flag2=0;
	
	
	for(int i=0;i<soft.size();i++)
	{
		ff << scientific << setw(19) << setprecision(11) << sum._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << sum._dy[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << ratio._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << ratio._dy[i] << "\t";
		//ff << scientific << setw(19) << setprecision(11) << ratio._dx[i] << "\n";
		ff << "\n";
//		if(flag1==0 && sum._x[i] > 2.96835e8)
//		{
//			ff.close();
//			char tmp[FLEN_FILENAME];
//			sprintf(tmp, "f2_%s", filename3);
//			ff.open(tmp);
//			ff << "read serr 1 2\n";
//			ff<<"cpd /xw\n";
//			ff <<"ma 1 on 2\n";
//			ff <<"lw 2\n";
//			ff <<"cs 1.1\n";
//			ff <<"font roman\n";
//			ff <<"time off\n";
//			ff <<"lab title\n";
//			ff <<"lab x Intensity [cts/s]\n";
//			ff <<"log x on\n";
//			ff <<"r x 0.05 1.5\n";
//			ff <<"r y 0.0 3.0\n";
//	
//			ff << "lab y Hardness ratio\n";
//			ff.flush();
//			//ff << "NO\tNO\tNO\tNO\tNO\n";
//			flag1=1;
//		}
//		if(flag2==0 && sum._x[i] > 2.96845e8)
//		{
//			ff.close();
//			char tmp[FLEN_FILENAME];
//			sprintf(tmp, "f3_%s", filename3);
//			ff.open(tmp);
//			ff << "read serr 1 2\n";
//			ff<<"cpd /xw\n";
//			ff <<"ma 1 on 2\n";
//			ff <<"lw 2\n";
//			ff <<"cs 1.1\n";
//			ff <<"font roman\n";
//			ff <<"time off\n";
//			ff <<"lab title\n";
//			ff <<"lab x Intensity [cts/s]\n";
//			ff <<"log x on\n";
//			ff <<"r x 0.05 1.5\n";
//			ff <<"r y 0.0 3.0\n";
//
//			ff << "lab y Hardness ratio\n";
//			ff.flush();
//			//ff << "NO\tNO\tNO\tNO\tNO\n";
//			flag2=2;
//		}
		
	}
//		ff<<"plot vert\n";
	
	ff.close();
	double off_time=0;
	string fn=filename3;
	fn="lc_"+fn;
	ff.open(fn.c_str());
	ff << "read serr 1 2 3 4\n";
	ff<<"cpd /xw\n";
	//ff <<"ma 1 on 2\n";
	ff <<"lw 2\n";
	ff <<"cs 1.1\n";
	ff <<"font roman\n";
	ff <<"time off\n";
	ff <<"lab title\n";
	if(soft._x[0] > 1e8 && ext_t0 <0)
	{
		off_time=floor(soft._x[0]);

		ff <<"lab x Time since " << ((double)off_time/scale_unit) << "\n";
		ff <<"!lab x Time since " << ((double)off_time) << "\n";
		
	}
	else if (ext_t0 >= 0)
	{
		off_time=floor(ext_t0);
                ff <<"lab x Time since " << (double)off_time/scale_unit << "\n";
                ff <<"!lab x Time since " << (double)off_time << "\n";
	}
	else
		ff <<"lab x Time [s]\n";


	ff << "lab y2 Soft [cts/s]\n";
	ff << "lab y3 Hard [cts/s]\n";	
	ff << "lab y4 Hardness ratio\n";
	ff << "r x " << setw(19) << setprecision(11) << (soft._x[0] - off_time)/scale_unit << " " << (soft._x[soft.size()-1]+2*soft._dx[soft.size()-1] - off_time)/scale_unit << endl;
    
	ff.flush();
        ff<<"plot vert\n";
	

	for(int i=0;i<soft.size();i++)
	{
		ff << scientific << setw(19) << setprecision(11) << (soft._x[i] - off_time + soft._dx[i]/2.0)/scale_unit << "\t";
		ff << scientific << setw(19) << setprecision(11) << soft._dx[i]/2.0/scale_unit << "\t";
		ff << scientific << setw(19) << setprecision(11) << soft._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << soft._dy[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << hard._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << hard._dy[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << ratio._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << ratio._dy[i] << "\n";
	}
	
	ff.close();
	

	
	VecDoub x(sum.size(),sum._y);
	VecDoub dx(sum.size(),sum._dy);
	VecDoub y( sum.size(), ratio._y);
	VecDoub dy( sum.size(), ratio._dy);
	
	
	Doub r=0, prob=0, z=0;
	pearsn(x, y, r, prob,  z);
	cout << "CORRELATION ANLAYSIS\n";
	cout << "Correlation " << r << endl;
	cout << "Null hyp. probability " << prob << endl;
	cout << "Z probability " << z << endl;
	cout << "*****************************************\n";

	
	Doub chi=0, sigma=0;
	test_const(y, dy, chi, prob, sigma);
	cout << "CHI2 simple test\n";
	cout << "Chi2 is " << chi << endl;
	cout << "Null hyp. probability is " << prob << endl;
	cout << "Corresponsind to  " << sigma << " sigma" << endl;
	cout << "*****************************************\n";

	Fitexy fi(x,y,dx,dy);
	
	cout << "Linear correlation test\n";
	cout << "The Linear fit gives H = ( " << fi.a << " +/- " << fi.siga << " ) + Sum * ( " << fi.b << " +/- " << fi.sigb << " )\n";
	cout << "With a chi2_red(dof) = " << fi.chi2/(x.size()-2) << " ( " << x.size()-2 << " )"<< endl;
	cout << "and Null hyp. prob. " << fi.q << endl;
	cout << "*****************************************\n";
	
	delete [] table;
	
	return status;
}

int correlation_analysis()
{
	int status=0;
	char fname[FLEN_FILENAME], ofname[FLEN_FILENAME];
	char query[2048];
	sprintf(query, "Data file (format x y dx dy)");
	readline_withspace(fname, query);
	vector<double> tx, ty, tdx, tdy;
	double ix=0,iy=0,idx=0,idy=0;
        sprintf(ofname, "out_%s", fname);
        
	ifstream d_file(fname);
	if(!d_file.is_open())
	{
		cerr << "Error in opening dat file %"<< fname << "%\n";
		return 1;
	}
	
        
	int max_line=1024;
	char line[max_line];
	while(d_file.getline(line, max_line, '\n'))
	{
		char scw_tmp[max_line/2];
		if(line[0] != '\n' && line[0] != '\0')
		{ 
			if(sscanf(line, "%lf %lf %lf %lf", &ix, &iy, &idx, &idy)==4)
			{
				tx.push_back(ix);
				ty.push_back(iy);
				tdx.push_back(idx);
				tdy.push_back(idy);
			}
		}
	}
	
	d_file.close(); 

	VecDoub x(tx.size());
	VecDoub dx(ty.size());
	VecDoub y( tdx.size());
	VecDoub dy( tdy.size());
	
	for(int i=0;i<tx.size();i++)
	{
		x[i]=tx[i];
		y[i]=ty[i];
		dx[i]=tdx[i];
		dy[i]=tdy[i];
	}
	
	Doub r=0, prob=0, z=0;
	pearsn(x, y, r, prob,  z);
	cout << "CORRELATION ANLAYSIS\n";
	cout << "Correlation " << r << endl;
	cout << "Null hyp. probability " << prob << endl;
	cout << "Z statistics " << z << endl;
	cout << "*****************************************\n";

	
	Doub chi=0, sigma=0;
	test_const(y, dy, chi, prob, sigma);
	cout << "CHI2 simple test\n";
	cout << "Chi2 is " << chi << endl;
	cout << "Null hyp. probability is " << prob << endl;
	cout << "Corresponding to  " << sigma << " sigma" << endl;
	cout << "*****************************************\n";

        Fitab fitab(x,y,dy);
	
	cout << "Linear correlation test (errors on y variables)\n";
	cout << "The Linear fit gives H = ( " << fitab.a << " +/- " << fitab.siga << " ) + Sum * ( " << fitab.b << " +/- " << fitab.sigb << " )\n";
	cout << "With a chi2_red(dof) = " << fitab.chi2/(x.size()-2) << " ( " << x.size()-2 << " )"<< endl;
	cout << "and Null hyp. prob. " << fitab.q << endl;
	cout << "*****************************************\n";

        
	Fitexy fi(x,y,dx,dy);
	
	cout << "Linear correlation test (errors on both variables)\n";
	cout << "The Linear fit gives H = ( " << fi.a << " +/- " << fi.siga << " ) + Sum * ( " << fi.b << " +/- " << fi.sigb << " )\n";
	cout << "With a chi2_red(dof) = " << fi.chi2/(x.size()-2) << " ( " << x.size()-2 << " )"<< endl;
	cout << "and Null hyp. prob. " << fi.q << endl;
	cout << "*****************************************\n";
        
        ofstream ff(ofname);
        ff << "read serr 1 2\n";
        ff << "cpd /xw\n";
        ff << "ma 17 on 2\n";
        ff << "line on 3\n";
        
        for(int i=0;i<tx.size();i++)
	{
		ff << std::scientific << x[i] << " " << dx[i] << " " <<  y[i] << " " << dy[i] << " " << fi.a + x[i]*fi.b <<   endl;
	} 
        ff.close();
	
	return status;
}


int fold_lc()
{

	
	int status=0;
	char filename1[FLEN_FILENAME];
	char conf_file[FLEN_FILENAME];
	char filename3[FLEN_FILENAME];
	char query[2048];
	char flag_binarycor='n';
    sprintf(conf_file,".fold_lc.txt");
	sprintf(filename1, "list_lc.txt");
	sprintf(query, "List of light curves");
	char binary_file[FLEN_FILENAME];
	sprintf(binary_file, "%s", "orbit.dat");
    double t_ref=0;
	double f=1;
	double df=0;
	double ddf=0;
	int n_bins=128;
	int evt_flag=1;
	double e_min=0;
	double e_max=10.0;
	double gti_gap=0;

	{
		ifstream fp(conf_file);
		if(fp.is_open())
		{
			fp >> evt_flag;
			
			fp >> filename1;
			fp >> filename3;
			fp >> flag_binarycor;
			fp >> binary_file;
			fp >> t_ref;
			fp >> f;
			fp >> df;
			fp >> ddf;
			fp >> n_bins;
			fp >> e_min;
			fp >> e_max;
			fp >> gti_gap;
			fp.close();
		}
		else
		{
			cerr << "Warning :: could not open " << conf_file << endl;
		}
	}

	sprintf(query, "Choose the data format");
	int n_opt=2;
	char *menu[]={"Events", "Light Curves"};

	evt_flag=ask_menu(n_opt, menu , evt_flag, query);

	if(evt_flag ==1)
	{
		sprintf(query, "List of event files");
          	askInfo(query, filename1);
                
		sprintf(query, "Minimum energy [keV]");
		ask_double(query, &e_min);

		sprintf(query, "Maximum energy [keV]");
		ask_double(query, &e_max);
	}
	else
	{
		sprintf(query, "List of light curves");
          	askInfo(query, filename1);
	}


	vector<string> list;

	read_list(list,filename1);

	int n_files=list.size();

	cout << "You have given " << n_files << " input files\n";
	if(n_files ==0)
	{
		return 1;
	}
	sprintf(query, "Name of folded light curve");	
	askInfo(query, filename3);
	double t_start=0;
	double mjdref=0;
	
	if(evt_flag ==1)
	{
		event_file soft(list[0].c_str());
		soft.output();
		soft.scan_gti();
		t_start=soft.t_start();
	    mjdref=soft.mjd_ref();
//            char temp_str[FLEN_FILENAME];
//            sprintf(temp_str, "try.qdp");
//            soft.output_lc(temp_str);

	}
	else
	{
		light_curve soft(list[0].c_str());
		soft.output();
		if	(soft.scan_gti())
		{
			sprintf(query, "Enter the maximum allowed gap between point not to create a GTI, if <=0 not creating GTIs");
			ask_double(query, &gti_gap);
			if (gti_gap>0)
			{
				soft.build_gti(gti_gap);
				soft.scan_gti();
			}
		}
		char temp_str[FLEN_FILENAME];
		sprintf(temp_str, "try.qdp");
		soft.output_lc(temp_str);
		t_start=soft.t_start();
		mjdref=soft.mjd_ref();

	}


	sprintf(query, "Do you want to apply binary correction ?");
	flag_binarycor=ask_yes_no(query	, flag_binarycor);
	
	if(flag_binarycor == 'y')
	{
		sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
		askInfo(query, binary_file);
		char setup_file_exist=1;
		char interactive_flag=0;
		if (strcmp(binary_file, "none")==0)
		{
			sprintf(query, "Enter the actual name of the file to store the binary parameters");
			askInfo(query, binary_file);
			
			//sprintf(binary_file,"%s_orbit.dat",active_src_name);
			interactive_flag=1;
			setup_file_exist=0;
		}
//		soft.binary_corr(binary_file);
		if(interactive_flag)
                        binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, mjdref );

	}
	
	
	sprintf(query, "number of phase bins");
	ask_integer(query, &n_bins);
		
	sprintf(query, "Start_time (Start time of light curve is %19.12e )", t_start);
	ask_double(query, &t_ref);
	
	sprintf(query, "frequency");
	ask_double(query, &f);
	
	sprintf(query, "frequency derivative");
	ask_double(query, &df);
	
	sprintf(query, "frequency second derivative");
	ask_double(query, &ddf);


	ofstream fp(conf_file);
	if(fp.is_open())
	{
		fp << evt_flag << endl;
		fp << filename1 << endl;
		fp << filename3 << endl;
		fp << flag_binarycor << endl;
		fp << binary_file << endl;
		fp << scientific << setw(19) << setprecision(12) << t_ref << endl;
		fp << scientific << setw(19) << setprecision(12) << f << endl;
		fp << scientific << setw(19) << setprecision(12) << df << endl;
		fp << scientific << setw(19) << setprecision(12) << ddf << endl;
		fp << n_bins << endl;
		fp << scientific << setw(19) << setprecision(12) << e_min << endl;
		fp << scientific << setw(19) << setprecision(12) << e_max << endl;
		fp << scientific << setw(19) << setprecision(12) << gti_gap << endl;
		
		fp.close();
	}


	light_curve sum(n_bins);
	sum.init_folded();

	for(int i=0;i<n_files;i++)
	{
		light_curve *folded=0;
		if(evt_flag ==1)
		{
			event_file run_evt(list[i].c_str());
			//run_evt.read_times(e_min, e_max);
			cerr << "Warn :: Need to implement energy selection in reading events\n";
			run_evt.read_times();
			run_evt.output();
			run_evt.scan_gti();
			if(flag_binarycor == 'y')
				run_evt.binary_corr(binary_file);

      		folded= run_evt.fold(n_bins, t_ref, f, df, ddf,
                        run_evt.t_start(), run_evt.t_stop(), e_min, e_max);
			sum+=(*folded);
			sprintf(filename1, "%03d_%s", i, filename3);
			folded->output_lc(filename1, 1);

		}
		else
		{
			light_curve run(list[i].c_str());
			run.output();
			if (gti_gap > 0)
				run.build_gti(gti_gap);
			run.scan_gti();
			if(flag_binarycor == 'y')
				run.binary_corr(binary_file);

			folded= run.fold(n_bins, t_ref, f, df, ddf);
			sum+=(*folded);
			sprintf(filename1, "%03d_%s", i, filename3);
			folded->output_lc(filename1, 1);
		}

            if (folded)
                delete folded;
            
	}
	
	sum.output_lc(filename3, 1);
	
	return status;
}

int search_fold()
{


    int status=0;
    char filename1[FLEN_FILENAME];
    char conf_file[FLEN_FILENAME];
    char filename3[FLEN_FILENAME];
    char query[2048];
    char flag_binarycor='n';
    sprintf(conf_file,".search_fold.txt");
    sprintf(filename1, "list_lc.txt");
    sprintf(filename3, "ef");
    sprintf(query, "List of light curves");
    char binary_file[FLEN_FILENAME];
    sprintf(binary_file, "%s", "orbit.dat");
    double t_ref=0;
    double f1=0;
    double f2=1e10;
    double res_sampling=1;
    int n_bins=16;
    int evt_flag=1;
    double e_min=0;
    double e_max=10.0;
    int weight_flag=1;
    double ff=1.2;
    char simul_flag=0;
    int simul_run;
    char flag_out_simul;
    char flag_out_char;
    char detrend_flag='y';
    double detrend_scale=1;
    double df=0, ddf=0, f=f1;
	double gti_gap=0;

    {
		ifstream fp(conf_file);
		if(fp.is_open())
		{
			fp >> evt_flag;
			fp >> filename1;
			fp >> filename3;
			fp >> flag_binarycor;
			fp >> binary_file;
			fp >> t_ref;
			fp >> f1;
			fp >> f2;
			fp >> res_sampling;
			fp >> n_bins;
			fp >> e_min;
			fp >> e_max;
			fp >> weight_flag;
			fp >> ff;
			fp >> simul_flag;
			fp >> simul_run;
			fp >> flag_out_simul;
			fp >> detrend_flag;
			fp >> detrend_scale;
			fp >> df;
			fp >> gti_gap;
			fp.close();
		}
		else
		{
			cerr << "Warning :: could not open " << conf_file << endl;
		}
    }

    if(flag_out_simul)
        flag_out_char='y';
    else
        flag_out_char='n';

    sprintf(query, "Choose the data format");
    int n_opt=2;
    char *menu[]={"Events", "Light Curves"};

    evt_flag=ask_menu(n_opt, menu , evt_flag, query);

    if(evt_flag ==1)
    {
            sprintf(query, "List of event files");
            askInfo(query, filename1);

            sprintf(query, "Minimum energy [keV]");
            ask_double(query, &e_min);

            sprintf(query, "Maximum energy [keV]");
            ask_double(query, &e_max);
    }
    else
    {
            sprintf(query, "List of light curves");
            askInfo(query, filename1);
            sprintf(query, "Choose the weighting scheme (only for LC)");
            n_opt=3;
            char *menu_2[]={"None", "1/sigma (D'Ai et al, 2010)", "1/(sigma+Vs) (Corbet et al., 2007)"};
            weight_flag=ask_menu(n_opt,menu_2, weight_flag, query);
            if(weight_flag==3)
            {
                sprintf(query, "Factor f of weighting");
                ask_double(query, &ff);
            }

    }


    vector<string> list;

    read_list(list,filename1);

    int n_files=list.size();

    cout << "You have given " << n_files << " input files\n";
    if(n_files ==0)
    {
            return 1;
    }
    sprintf(query, "Root name of output files");
    askInfo(query, filename3);
    double t_start=0, t_stop=1e6, timedel=1e-1,mjdref=51544.0;
    if(evt_flag ==1)
    {
        event_file soft(list[0].c_str());
        soft.output();
        soft.scan_gti();
        t_start=soft.t_start();
        event_file soft_1(list[n_files-1].c_str());
        soft_1.output();
        soft_1.scan_gti();
        t_stop=soft_1.t_stop();
        timedel=soft_1.time_del();
        mjdref=soft_1.mjd_ref();
//            char temp_str[FLEN_FILENAME];
//            sprintf(temp_str, "try.qdp");
//            soft.output_lc(temp_str);


    }
    else
    {
        light_curve soft(list[0].c_str());
        soft.output();
		if	(soft.scan_gti())
		{
			sprintf(query, "Enter the maximum allowed gap between point not to create a GTI, if <=0 not creating GTIs");
			ask_double(query, &gti_gap);
			if (gti_gap>0)
			{
				soft.build_gti(gti_gap);
				soft.scan_gti();
			}
		}

        char temp_str[FLEN_FILENAME];
        //sprintf(temp_str, "try.qdp");
        //soft.output_lc(temp_str);
        t_start=soft.t_start();
        t_start=soft._x[0]-10.0; //This is for curves with t_start not changed in barycentric correction
        light_curve soft_1(list[n_files-1].c_str());
        soft_1.output();
        soft_1.scan_gti();
        t_stop=soft_1.t_stop();
        timedel=soft_1.time_del();
        mjdref=soft_1.mjd_ref();
    }


    sprintf(query, "Do you want to apply binary correction ?");
    flag_binarycor=ask_yes_no(query	, flag_binarycor);

    if(flag_binarycor == 'y')
    {
            sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
            askInfo(query, binary_file);
            char setup_file_exist=1;
            char interactive_flag=0;
            if (strcmp(binary_file, "none")==0)
            {
                    sprintf(query, "Enter the actual name of the file to store the binary parameters");
                    askInfo(query, binary_file);

                    //sprintf(binary_file,"%s_orbit.dat",active_src_name);
                    interactive_flag=1;
                    setup_file_exist=0;
            }
            binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist,mjdref);

    }


    sprintf(query, "number of phase bins");
    ask_integer(query, &n_bins);

    sprintf(query, "Start_time (Start time of events is %19.12e )", t_start);
    ask_double(query, &t_ref);

    double min_freq=1/(t_stop - t_start);
    sprintf(query, "Start frequency (minimum is %19.12e) ", min_freq);
    if (f1==0)
        f1=min_freq;
    ask_double(query, &f1);

    double max_freq=0.5/timedel;

    if(f2==1e10)
        f2=max_freq;
    sprintf(query, "Stop frequency (maximum is %19.12e) ", max_freq);
    ask_double(query, &f2);

	if (f2 <= f1)
	{
		cerr << "Please insert a start frequency lower than stop frequency\n";
		sprintf(query, "Start frequency (minimum is %19.12e) ", min_freq);
		if (f1==0)
			f1=min_freq;
		ask_double(query, &f1);
			if(f2==1e10)
			f2=max_freq;
		sprintf(query, "Stop frequency (maximum is %19.12e) ", max_freq);
		ask_double(query, &f2);
		if (f2 <= f1)
		{
			cerr << "Please insert a start frequency lower than stop frequency\n";
			exit(1);
		}
	}

    sprintf(query, "Resolution (over)sampling (0.1-10)");
    ask_double(query, &res_sampling);

    sprintf(query, "Frequency derivative");
    ask_double(query, &df);


    if(evt_flag !=1)
    {
        sprintf(query, "Do you want to detrend the light curve");
        detrend_flag=ask_yes_no(query, detrend_flag);

        if(detrend_flag=='y')
        {
            cout << "The 'detrend scale' is the multiple of averaged searched frequency to smooth on\n";
            sprintf(query, "Detrend scale");
            ask_double(query, &detrend_scale);
        }

        sprintf(query, "Do you want to perform simulation of errors? (LC only single file, otherwise behaviour is undetermined !!)");

        simul_flag =ask_yes_no(query, simul_flag);
        if(simul_flag == 'y')
        {
                sprintf(query, "How many simulaiton runs?");
                ask_integer(query, &simul_run);


                sprintf(query, "Output intermmediate products?");
                flag_out_char=ask_yes_no(query, flag_out_char);

                if(flag_out_char == 'y')
                        flag_out_simul=1;
                else
                        flag_out_simul=0;


        }
    }

    ofstream fp(conf_file);
    if(fp.is_open())
    {
        fp << evt_flag << endl;
            fp << filename1 << endl;
            fp << filename3 << endl;
            fp << flag_binarycor << endl;
            fp << binary_file << endl;
            fp << scientific << setw(19) << setprecision(12) << t_ref << endl;
            fp << scientific << setw(19) << setprecision(12) << f1 << endl;
            fp << scientific << setw(19) << setprecision(12) << f2 << endl;
            fp << scientific << setw(19) << setprecision(12) << res_sampling << endl;
            fp << n_bins << endl;
            fp << scientific << setw(19) << setprecision(12) << e_min << endl;
            fp << scientific << setw(19) << setprecision(12) << e_max<< endl;
            fp << weight_flag << endl;
            fp << ff <<endl;
            fp << simul_flag << endl;
            fp << simul_run << endl;
            fp << flag_out_simul << endl;
            fp << detrend_flag << endl;
            fp << detrend_scale << endl;
            fp << df << endl;
			fp << gti_gap << endl;
            fp.close();
    }

    f=f1;
    cout << "allocate lc with " << n_bins << " bins\n";
    light_curve sum(n_bins);
    sum.init_folded();

    cout <<"Open output files\n";

    char out_fname[FLEN_FILENAME];
    sprintf(out_fname, "%s_periodogram_f.qdp", filename3);
    ofstream out_f(out_fname);
    sprintf(out_fname, "%s_periodogram_p.qdp", filename3);
    ofstream out_p(out_fname);
    out_f << "read serr 1 \n";
    out_f << "cpd /xw 1\n";
    out_f << "lab x \\gn [Hz]\nlab y2 \\gx\\u2\\d\nr x " << f1 << " " << f2 <<"\n";
//    out_f << "plot vert\n";
//    out_f << "lab y3 Average\n";

    out_p << "read serr 1 \n";
    out_p << "cpd /xw 1\n";
    out_p << "lab x P [d]\nlab y2 \\gx\\u2\\d\nr x " << 1./f2/86400. << " " << 1./f1/86400. <<"\n";
//    out_p << "lab y3 Average\n";
//    out_p << "plot vert\n";

    cout <<"Init elaboration\n";
    vector<double> freqs,periods,chis,s_freqs,s_periods;
    
    double freq_max=0, freq_max_err=0;

//        if(n_files >1)
//        {
//            int j=0;
//            cout << endl;
//            while (f<f2)
//            {
//                sum.blank();
//                //cout << "Fold freq " << f << endl;
//                for(int i=0;i<n_files;i++)
//                {
//
//                    light_curve *folded=0;
//                    if(evt_flag ==1)
//                    {
//                        event_file run_evt(list[i].c_str());
//                        run_evt.read_times(e_min, e_max);
//                        //run_evt.output();
//                        //run_evt.scan_gti();
//                        if(flag_binarycor == 'y')
//                            run_evt.binary_corr(binary_file);
//
//                        folded= run_evt.fold(n_bins, t_ref, f, df, ddf,
//                                run_evt.t_start(), run_evt.t_stop(), e_min, e_max);
//                        sum+=(*folded);
//                        //sprintf(filename1, "%03d_%s", i, filename3);
//                        //folded.output_lc(filename1);
//
//                    }
//                    else
//                    {
//                        light_curve run(list[i].c_str());
//                        //run.output();
//                        //run.scan_gti();
//                        if(flag_binarycor == 'y')
//                            run.binary_corr(binary_file);
//
//                        if(detrend_flag=='y')
//                        {
//                            char fname_detrend[FLEN_FILENAME];
//                            sprintf(fname_detrend, "%s_%04d_detrend.qdp", filename3, i);
//                            run.detrend(detrend_scale*2./(f1+f2), 1, fname_detrend);
//                        }
//                        folded= run.fold(n_bins, t_ref, f, df, ddf, weight_flag, ff);
//                        sum+=(*folded);
//
//                        //sprintf(filename1, "%05d_%s.qdp", j++, filename3);
//    //                    folded.output_lc(filename1);
//                    }
//                    if(folded)
//                        delete folded;
//
//                }
//
//                double av=sum.average(-1e100, 1e100);
//                double std_dev=sqrt(sum.var(-1e100, 1e100,av));
//                double chi=sum.chi(av);
//                double s_p=1/(SQR(f)*n_bins*(t_stop-t_start));
//                double s_f=SQR(f)*s_p;
//
//                out_f  << scientific << setw(19) << setprecision(12) << f << " " << s_f/2 << " " << chi << " " << av << " " << std_dev <<  endl;
//                out_p  << scientific << setw(19) << setprecision(12) << 1/f/86400 << " " << s_p/2/86400 << " " << chi << " " << av <<" " << std_dev << endl;
//
//                freqs.push_back(f);
//                //periods.push_back(1/f/86400);
//                s_freqs.push_back(s_f);
//                //s_periods.push_back(s_p/86400);
//                chis.push_back(chi);
//
//                f+=s_f/res_sampling;
//                cout << "Freq " << f << " Hz\r";
//                cout.flush();
//            }
//            cout << endl;
//        }
//        else //only one file
        //{
        int j=0;
        //Placeholders
        event_file *run_evt=0;
        light_curve *run=0;

        //reads first file
        if(evt_flag ==1)
        {
            run_evt = new event_file(list[0].c_str());
            //run_evt->read_times(e_min, e_max); //Need to implement energy selection
            cerr << "WARN No energy selection in event reading, implemented later in folding\n";
            run_evt->read_times();
            run_evt->scan_gti();
            run_evt->output(0,1e10,e_min,e_max);
            if(flag_binarycor == 'y')
                run_evt->binary_corr(binary_file);


        }
        else
        {
            run = new light_curve(list[0].c_str());
            run->output();
			if (gti_gap >0)
				run->build_gti(gti_gap);
            run->scan_gti();
            if(flag_binarycor == 'y')
            {
                cout << "Binary correction \n";
                run->binary_corr(binary_file);
                cout << "Binary correction done \n";
            }
            if(detrend_flag=='y')
            {
                char fname_detrend[FLEN_FILENAME];
                sprintf(fname_detrend, "%s_detrend.qdp", filename3);
                run->detrend(detrend_scale*2./(f1+f2), 1, fname_detrend);
            }

        }


//            cout << "INTERMEDIO " << endl;

    if(n_files >1)
    {
        for(int i=1;i<n_files;i++)
            {

                if(evt_flag ==1)
                {
                    event_file n_run_evt(list[i].c_str());
                    //n_run_evt.read_times(e_min, e_max); //Need to implement energy selection
                    cerr << "WARN No energy selection in event reading, implemented in folding\n";
                    n_run_evt.read_times();
                    //n_run_evt.output();
                    //n_run_evt.scan_gti();
                    if(flag_binarycor == 'y')
                        n_run_evt.binary_corr(binary_file);

                    run_evt->concatenate(&n_run_evt);
                    run_evt->output(0,1e10,e_min,e_max);
                    //sprintf(filename1, "%03d_%s", i, filename3);
                    //folded.output_lc(filename1);

                }
                else
                {

                    //cerr << "Multiple light curve not implemented yet, use the first only\n";
                    light_curve n_run(list[i].c_str());
                    //n_run.output();
                    //n_run.scan_gti();
                    if(flag_binarycor == 'y')
                        n_run.binary_corr(binary_file);

                    if(detrend_flag=='y')
                    {
                        char fname_detrend[FLEN_FILENAME];
                        sprintf(fname_detrend, "%s_%04d_detrend.qdp", filename3, i);
                        n_run.detrend(detrend_scale*2./(f1+f2), 1, fname_detrend);
                    }
                    run->concatenate(&n_run);
//                        folded= run.fold(n_bins, t_ref, f, df, ddf, weight_flag, ff);
//                        sum+=(*folded);
//
//                        //sprintf(filename1, "%05d_%s.qdp", j++, filename3);
//    //                    folded.output_lc(filename1);
                }
            }
    }

    //elaorates
    cout <<endl <<endl;
    //run->output_lc("Pippo.txt");
    double n_events_total =0;
    if(evt_flag == 1)
    {
        n_events_total=run_evt->output(0, 1e10, e_min, e_max);
        t_start=run_evt->t_start();
        t_stop=run_evt->t_stop();
    }
    
    chrono::steady_clock::time_point begin = chrono::steady_clock::now();

    int n_trials=0;
    while (f<f2)
    {
        n_trials++;
        sum.blank();
        //  cout << "Fold freq " << f << endl;
        light_curve *folded=0;
        if(evt_flag ==1)
        {
            //cout << "Start folding\n";
            folded= run_evt->fold(n_bins, t_ref, f, df, ddf,
                 run_evt->t_start(), run_evt->t_stop(), e_min, e_max);
            sum+=(*folded);
            //sprintf(filename1, "%03d_%s", i, filename3);
            //folded.output_lc(filename1);

        }
        else
        {

            //run->output();
            //run->scan_gti();
           //cout << "Freq " << f  <<endl;
           folded= run->fold(n_bins, t_ref, f, df, ddf,
                    weight_flag, ff);
            sum+=(*folded);
            //cout << sum.average(-1e100, 1e100) << " " << run->average(-1e100, 1e100) << endl;
            //sprintf(filename1, "%05d_%s.qdp", j++, filename3);
//                    folded.output_lc(filename1);
        }
        if(folded)
            delete folded;
        double av=sum.average(-1e100, 1e100);
        double std_dev=sqrt(sum.var(-1e100, 1e100,av));
        double chi=sum.chi(av);
        double s_p=1/(SQR(f)*n_bins*(t_stop-t_start));
        double s_f=SQR(f)*s_p;

        out_f  << scientific << setw(19) << setprecision(12) << f << " " << s_f/2 << " " << chi << endl;//" " << av << " " << std_dev <<  endl;
        out_p  << scientific << setw(19) << setprecision(12) << 1/f/86400 << " " << s_p/2/86400 << " " << chi << endl;//" " << av <<" " << std_dev << endl;

        freqs.push_back(f);
        //periods.push_back(1/f/86400);
        s_freqs.push_back(s_f);
        //s_periods.push_back(s_p/86400);
        chis.push_back(chi);

        f+=s_f/res_sampling;
        //if (! j++%10)
        {
            cout << "Freq " << f << " Hz\r";
            cout.flush();
        }
    }

	cout << "We inspected " << n_trials << " frequencies\n";
	if (n_trials < 10)
	{
		cerr << "WARNING :: too few frequencies, results could be inaccurate !";
	}

    chrono::steady_clock::time_point end= chrono::steady_clock::now();
    
    
    cout << endl;

    cout << "Time needed for computation = " << chrono::duration_cast<chrono::seconds>(end - begin).count() << " s\n";

    double max_chi=-1e10;
    for (int k=0;k<chis.size();k++)
    {
        if(chis[k] > max_chi)
        {
            max_chi=chis[k];
            freq_max=freqs[k];
            freq_max_err=s_freqs[k];
        }
    }
    
    //Determine significance and then sets upper limits
    Chisqdist chi2(n_bins-1);
    Normaldist gaus;


    double prob = pow(chi2.cdf(max_chi),n_trials); //This is with the number of trials

    double sig = 0;
    if(prob>=0.5 && prob<1)
            sig=(gaus.invcdf(prob));

    if(prob==1)
            sig=11;
    //cout << z2[i_nu] << " " << sig[i_nu] << endl;

    
    
    char fname_detrend[FLEN_FILENAME];
    

    light_curve *folded=0;
    sprintf(fname_detrend, "%s_folded.qdp", filename3);
    if(evt_flag ==1)
    {
        folded= run_evt->fold(n_bins, t_ref, freq_max, df, ddf,
             run_evt->t_start(), run_evt->t_stop(), e_min, e_max);
        folded->output_lc(fname_detrend, 1);
        //sprintf(filename1, "%03d_%s", i, filename3);
        //folded.output_lc(filename1);

    }
    else
    {

        folded= run->fold(n_bins, t_ref, freq_max, df, ddf,
                weight_flag, ff);
        folded->output_lc(fname_detrend, 1);
        //cout << sum.average(-1e100, 1e100) << " " << run->average(-1e100, 1e100) << endl;
        //sprintf(filename1, "%05d_%s.qdp", j++, filename3);
//                    folded.output_lc(filename1);
    }


    
    
    
    

    
    sprintf(fname_detrend, "%s_res.dat", filename3);
    ofstream f_res(fname_detrend);
    cout << "**************************************\n";
    cout << "Found maximum chi^2 at \n";
    
    cout << "fr = " << scientific << setw(19) << setprecision(12)
            << freq_max << " +/- "<< freq_max_err << " Hz" << endl;
	cout << "p = " << scientific << setw(19) << setprecision(12)
            << 1./freq_max/86400 << " +/- "<< freq_max_err/freq_max/freq_max/86400 << " d" << endl;
	
    cout << "chi^2 = " << scientific << setw(19) << setprecision(12)
            << max_chi << endl;
    cout << "The probability that the fluctuation is due to chance is " 
            << 1.-prob << " corresponding to a detection of " << sig << " sigmas in Gaussian statistics\n";
    double A0=folded->average(0,10);
    double A_max=sqrt(folded->var(0,10,A0))/A0;
    if(sig<=5)
    {
        cout << "The signal is not significant at 5 sigma c.l., we determine upper limits on pulsed fraction for a sinusoidal signal\n";
        //Get the three sigma chi2
        double three_s_chi=chi2.invcdf(gaus.cdf(3));
        //We assume it sums quadratically to the maximum detected noise
        three_s_chi+=max_chi;
        cout << "The chi^2 to compute the u.l. is " << three_s_chi << endl;
        //Now, we get the equivalent pulsed fraction, we approximate equal error as Poissonian on the mean
        A_max=sqrt(2*three_s_chi*SQR((folded->average_error())/A0)); //Definition of chi^2
        cout << "Three sigma upper limit is " << A_max << endl;
        
        if(evt_flag)
        {
            A_max=sqrt(n_bins*2*three_s_chi/n_events_total);
            cout << "Alternative check for event input: 3sigma u.l. is " << A_max << endl;
        }      
    }
    else
    {
        cout << "rms is " << A_max << endl;
    }
    
    cout << "**************************************\n";
    f_res << scientific << setw(19) << setprecision(12)
          << (t_stop +t_start)/2-t_ref << " " << (t_stop -t_start)/2 << " "
            << freq_max << "  "<< freq_max_err <<  " "
            << 1./  freq_max << "  "<< freq_max_err/SQR(freq_max) <<  " " 
            << prob << " " << sig << " " << A_max << endl;
    f_res.close();

    
    
    
    if(evt_flag ==1)
    {
        delete run_evt;
    }
    else
    {
        delete run;
    }
//        }//end only one file

    //Plot the 3 and 5 sigma probability levels with number of trials
    prob=gaus.cdf(3.0);
    prob = pow(prob,1.0/double(n_trials));
    double chi_level= chi2.invcdf(prob);
    A_max=sqrt(2*chi_level*SQR((folded->average_error())/folded->average()));
    out_f << "lab 1 \"3\\gs\" pos "<< f1 << " " <<  chi_level<< " line 0 0.95\n";
    out_p << "lab 1 \"3\\gs\" pos "<< f1 << " " <<  chi_level<< " line 0 0.95\n";
    out_f << "! 3sigma u.l. on pulsed fraction after " << n_trials << " trials is "<< A_max << endl;
    out_p << "! 3sigma u.l. on pulsed fraction after " << n_trials << " trials is "<< A_max << endl;
    prob=gaus.cdf(5.0);
    prob = pow(prob,1.0/double(n_trials));
    chi_level= chi2.invcdf(prob);
    A_max=sqrt(2*chi_level*SQR((folded->average_error())/folded->average()));
    out_f << "lab 2 \"5\\gs\" pos "<< f1 << " " <<  chi_level<< " line 0 0.95\n";
    out_p << "lab 2 \"5\\gs\" pos "<< f1 << " " <<  chi_level<< " line 0 0.95\n";
    out_f << "! 5sigma u.l. on pulsed fraction after " << n_trials << " trials is "<< A_max << endl;
    out_p << "! 5sigma u.l. on pulsed fraction after " << n_trials << " trials is "<< A_max << endl;
    out_f.close();
    out_p.close();
    
    if(folded)
        delete folded;

    if(simul_flag=='y')
    {
        cout << " START " << simul_run << " SIMULATION RUNs\n";
        int n_freq=chis.size();
        VecDoub v_freq(n_freq);
        VecDoub v_chi(n_freq);
        VecDoub best_freq(simul_run);
        VecDoub best_chi(simul_run);

        for(int j=0;j<n_freq;j++)
        {
            v_freq[j]=freqs[j];
        }

        if(n_files>1 || evt_flag == 1)
        {
            cerr << "Not implemented for multiple or event files yet\n";
            return 1;

        }

        char simul_file_name[FLEN_FILENAME];
        char simul_single_file_name[FLEN_FILENAME];

        sprintf(simul_file_name, "simul_%s.qdp", filename3);

        ofstream simul_file(simul_file_name), simul_single_file;
        if(!simul_file.is_open())
        {
            cerr << "Could not open file " << simul_file_name << endl;
            return 1;

        }
        simul_file << "read\n";
        simul_file << "cpd /xw\n";
        simul_file << "lab x Simul X\n";
        simul_file << "lab y3 \\gx\\u2\\d\n";
        simul_file << "lab y2 \\gn [Hz]\n";
        simul_file << "plot vert\n";
        simul_file << "win 2\n";
        simul_file << "lab 2 col 3 line 0 100\n";
        simul_file << "lab 2 pos 0 " << scientific << setw(19) << setprecision(12)
                << freq_max << endl;
        simul_file << "lab 3 col 4 line 0 100\n";
        simul_file << "lab 3 pos 0 " << scientific << setw(19) << setprecision(12)
                << freq_max + freq_max_err<< endl;
        simul_file << "lab 4 col 4 line 0 100\n";
        simul_file << "lab 4 pos 0 " << scientific << setw(19) << setprecision(12)
                << freq_max - freq_max_err<< endl;


        if(flag_out_simul)
        {
            simul_single_file.open(simul_single_file_name);
            if(!simul_single_file.is_open())
            {
                cerr << "Could not open file " << simul_single_file_name << endl;
                return 1;

            }
            simul_single_file << "read\n";
            simul_single_file << "cpd /xw\n";
            simul_single_file << "lab y \\gx\\u2\\d\n";
            simul_single_file << "lab x \\gn [Hz]\n";

        }
        //Placeholders \reads again the LC/EVT files
        //as variables were local in the cycle, possible to improve on this
        event_file *run_evt;
        light_curve *run;

        //reads
        if(evt_flag ==1)
        {
            run_evt = new event_file(list[0].c_str());
            //run_evt->read_times(e_min, e_max); //Need to implement energy selection
            run_evt->read_times();
            run_evt->output();
            run_evt->scan_gti();
            if(flag_binarycor == 'y')
                run_evt->binary_corr(binary_file);

        }
        else
        {
            run = new light_curve(list[0].c_str());
            run->output();
            run->scan_gti();
            if(flag_binarycor == 'y')
                run->binary_corr(binary_file);
            if(detrend_flag=='y')
            {
                run->detrend(detrend_scale*2./(f1+f2), 0, NULL);
            }
        }

        cout << "Perform simul run\n";
        for (int k=0;k<simul_run;k++)
        {
            //cout << "\r";
            //cout << "\r" << (int)floor((float)k/(float)simul_run*100) << " %";
            cout << "\rRun " << k << " of "  << simul_run;
            cout.flush();
            if(flag_out_simul)
            {
                sprintf(simul_single_file_name, "simul_%04d_%s.qdp", k, filename3);

                simul_single_file.open(simul_single_file_name);

                if(!simul_single_file.is_open())
                {
                    cerr << "Could not open file " << simul_single_file_name << endl;
                    return 1;

                }
                simul_single_file << "read\n";
                simul_single_file << "cpd /xw\n";
                simul_single_file << "lab y \\gx\\u2\\d\n";
                simul_single_file << "lab x \\gn [Hz]\n";

            }


            light_curve simul_lc = simul_gaus(*run);
            if(detrend_flag=='y')
            {
                simul_lc.detrend(detrend_scale*2./(f1+f2), 0, NULL);
            }

            light_curve *folded;
        //elaorates
            for(int j=0;j<freqs.size();j++)
            {
                f=freqs[j];
//                    cout << "Fold freq " << f << endl;

                if(evt_flag ==1)
                {
                    ;//Implement me
//                    light_curve folded= run_evt->fold(n_bins, t_ref, f, df, ddf,
//                         run_evt->t_start(), run_evt->t_stop(), e_min, e_max);
//                    sum+=folded;
//                                    //sprintf(filename1, "%03d_%s", i, filename3);
                //folded.output_lc(filename1);

                }
                else
                {

                    folded = (simul_lc.fold(n_bins, t_ref, f, df, ddf, weight_flag, ff));

                    //sprintf(filename1, "%05d_%s.qdp", j++, filename3);
    //                    folded.output_lc(filename1);
                }

                double av=folded->average(-1e100, 1e100);
                //double std_dev=sqrt(folded->var(-1e100, 1e100,av));
                v_chi[j]=folded->chi(av);
                //double s_p=1/(SQR(f)*n_bins*(t_stop-t_start));
                //double s_f=SQR(f)*s_p;
                if(flag_out_simul)
                {

                   simul_single_file  << scientific << setw(19) << setprecision(12) << f << " " <<
                           v_chi[j] << endl;
                }

            }//end of loop on frequencies, ind is j
            if(flag_out_simul)
            {
                simul_single_file.close();
            }
            double max_chi=-1e100;
            int ind=v_chi.max_loc_val(max_chi);
            best_chi[k]=max_chi;
            best_freq[k]=v_freq[ind];
            simul_file << k << " " <<  scientific << setw(19) << setprecision(12) << best_freq[k] << " " <<
                           best_chi[k] << endl;
            simul_file.flush();
        }//end of loop on simulation, ind is k

        cout << "\n";
        simul_file.close();



    }//end of simulation

    return status;
}


int make_tphase()
{
	
	
	int status=0;
	char filename1[FLEN_FILENAME];
	sprintf(filename1, "list_evt.txt");
	char filename2[FLEN_FILENAME];
	sprintf(filename2, "list_bck.txt");
	char filename3[FLEN_FILENAME];
	sprintf(filename3, "none");
	double t_ref=0;
	double f=1035.84002785/2.0;//1./1.930800112558E-03;
	double df=0;
	double ddf=0;
	int n_bins=32;
	double ext_back=21.73;
	double ext_back_err=0.73;
	char binary_file[FLEN_FILENAME];
	sprintf(binary_file, "%s", "none");
	char flag_binarycor='n';
        char delta_t_flag='n';
        char delta_t_file[FLEN_FILENAME];
        double delta_t=0;
        double delta_t_offset=0;
        sprintf(delta_t_file, "none");
        double e_min=0,e_max=100;
	
	char query[2048];
	{
		ifstream conf_file(".tphase_matrix_conf.txt");
		if(conf_file.is_open())
		{
			conf_file >> filename1;
			conf_file >> filename2;
			conf_file >> filename3;
			conf_file >> t_ref;
			conf_file >> f;
			conf_file >> df;
			conf_file >> ddf;
			conf_file >> n_bins;
			conf_file >> ext_back;
			conf_file >> ext_back_err;
			conf_file >> flag_binarycor;
			conf_file >> binary_file;
			conf_file >> delta_t;
			conf_file >> delta_t_flag;
			conf_file >> delta_t_file;
			conf_file >> delta_t_offset;
			conf_file >> e_min;
			conf_file >> e_max;
			conf_file.close();
		}
	}
	sprintf(query, "List of event files");
	//readline_withspace(filename1, query);
	askInfo(query, filename1);
	
	sprintf(query, "List of back curves (none to skip)");
	//readline_withspace(filename2, query);
	askInfo(query, filename2);
	
	char bck_flag=1;
	if(strcmp(filename2, "none") == 0)
	{
		cout << "Skip BCK subtraction\n";
		bck_flag=0;
	}
	
	vector<string> list;
	
	read_list(list,filename1);
	vector<string> list_bck;
	if(bck_flag)
	{
		read_list(list_bck,filename2);
		if(list.size() != list_bck.size())
		{
			cerr <<"Lists have different sizes, abort\n";
			return 1;
		}
	}
		   
	int n_files=list.size();
	
	cout << "You have given " << n_files << " event files\n";
	if(n_files ==0)
	{
		return 1;
	}
	
	event_file ef(list[0].c_str());
	light_curve bl;
	if(bck_flag)
		bl.read_fits(list_bck[0].c_str());
	
	cout << "First file details\n";
	
	
	ef.output();
	ef.scan_gti();
	double mjdref=ef.mjd_ref();
	
	if(strcmp(filename3, "none") == 0)
		sprintf(filename3, "%s_TPHASE.fits", ef.object);
	
	sprintf(query, "Name of TPHASE matrix");
	
	askInfo(query, filename3);
	//readline_withspace(filename3, query);
	
	char temp_str[FLEN_FILENAME];
	sprintf(temp_str, "try.qdp");
	//soft.output_lc(temp_str);
	
	sprintf(query, "Do you want to apply binary correction?");
	flag_binarycor=ask_yes_no(query	, flag_binarycor);
	
	if(flag_binarycor == 'y')
	{
	
		sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
		askInfo(query, binary_file);
		char setup_file_exist=1;
		char interactive_flag=0;
		if (strcmp(binary_file, "none")==0)
		{
			sprintf(query, "Enter the actual name of the file to store the binary parameters");
			askInfo(query, binary_file);
			
			//sprintf(binary_file,"%s_orbit.dat",active_src_name);
			interactive_flag=1;
			setup_file_exist=0;
		}
		//soft.binary_corr(binary_file);
		if(interactive_flag)
                        binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, mjdref );

	}

	sprintf(query, "number of phase bins");
	ask_integer(query, &n_bins);
	
	if(t_ref<=0)
		t_ref=ef._t_start+t_ref;
	
	sprintf(query, "Start_time (if <=0, it adds to t_first)");
	ask_double(query, &t_ref);

	cerr << "Input t_ref " << t_ref << endl;

	if(t_ref<=0)
		t_ref=ef._t_start+t_ref;
	
	cerr << "Used t_ref " << scientific << setw(19) << setprecision(12) << t_ref << endl;
	
	sprintf(query, "Frequency");
	ask_double(query, &f);
	
	sprintf(query, "frequency derivative");
	ask_double(query, &df);
	
	sprintf(query, "frequency second derivative");
	ask_double(query, &ddf);

	
	if(delta_t==0)
		delta_t=1./f*10.0;//(ef._t_stop - ef._t_start)/20.;

	sprintf(query, "Do you want to enter time intervals from a file?");
	delta_t_flag=ask_yes_no(query, delta_t_flag);
	vector<double> tstart_vector, tstop_vector;
        
	if(delta_t_flag == 'y')
	{
		sprintf(query, "Enter the name of the file with the time interval (ascii format with first two columns central_time & delta_t)");
		//readline_withspace(delta_t_file, query);
		//Need to use this if I want batch input
		askInfo(query, delta_t_file);

		sprintf(query, "Time offset value");
		ask_double(query, &delta_t_offset);
		
		ifstream ff(delta_t_file);
		
		int max_line=1024;
		char line[max_line];
		int n_local_entries=0;
		while(ff.getline(line, max_line, '\n'))
		{ 
			double t1=0,t2=0;
				//sscanf(line, "%s\t%s", scw_tmp, pif_tmp);                                                                          
				if(line[0] != '\n' && line[0] != '\0')                                                                               
				{                                                                                                                    
					if(sscanf(line, "%lf %lf", &t1, &t2)==2)
					{
						//Assumes times are t_central delta_t
							tstart_vector.push_back(t1-t2+delta_t_offset);
							tstop_vector.push_back(t1+t2+delta_t_offset);
							n_local_entries++;
					}               
				}
		}
		cout << "We have read " << n_local_entries << " intervals from file '" << delta_t_file << "'\n";
		ff.close();
		
		cout << "Read " << tstart_vector.size() << " time intervals\n";
		if(flag_binarycor == 'y')
		{
			cout << "Perform binary correction on input interval file\n";
			binarycor_setup(binary_file, 0, 1, 1, mjdref );
			for(int i=0;i<tstart_vector.size();i++)
			{
				//std::cout << tstart_vector[i] << " " << tstop_vector[i] << endl;
				tstart_vector[i]=binarycor(tstart_vector[i]);
				tstop_vector[i]=binarycor(tstop_vector[i]);
				//std::cout << tstart_vector[i] << " " << tstop_vector[i] << endl;
			}
		}
		
//                for(int i=0;i<tstart_vector.size();i++)
//                    std::cout << tstart_vector[i] << " " << tstop_vector[i] << endl;
	
	}
	else
	{
			sprintf(query, "Time step for each period");
			ask_double(query, &delta_t);
			tstart_vector.push_back(delta_t);
	}

        
	sprintf(query, "External background level");
	ask_double(query, &ext_back);
        
	sprintf(query, "External background level error");
	ask_double(query, &ext_back_err);

	sprintf(query,"Minimum energy of accumulation");
	ask_double(query,&e_min);
	
	sprintf(query,"Maximum energy of accumulation");
	ask_double(query,&e_max);
        
	ofstream conf_file(".tphase_matrix_conf.txt");
	if(conf_file.is_open())
	{
		conf_file << filename1 << endl;
		conf_file << filename2 << endl;
		conf_file << filename3 << endl;

		conf_file <<  scientific << setw(19) << setprecision(12) << t_ref << endl;
		conf_file <<  scientific << setw(19) << setprecision(12) << f << endl;
		conf_file << scientific << setw(19) << setprecision(12) << df << endl;
		conf_file << scientific << setw(19) << setprecision(12) << ddf << endl;
		conf_file << n_bins << endl;
		conf_file << ext_back << endl;
		conf_file << ext_back_err << endl;
		conf_file << flag_binarycor << endl;
		conf_file << binary_file << endl;
                conf_file << delta_t << endl;
                conf_file << delta_t_flag << endl;
                conf_file << delta_t_file << endl;;
                conf_file << delta_t_offset << endl;
                conf_file << e_min << endl;
                conf_file << e_max << endl;
                
		conf_file.close();
	}
	
	
	ef.init_tphase(filename3, n_bins, t_ref, f, df, ddf, e_min,e_max);
	ef.read_times();
	if(flag_binarycor == 'y')
		ef.binary_corr(binary_file);
        //cout << "About to update TPAHSE matrix\n";
	ef.update_tphase_matrix(filename3, tstart_vector, tstop_vector, n_bins, t_ref, f, df, ddf, bl, bck_flag, 
                ext_back, ext_back_err, e_min,e_max);
	ef.close_fits();
	ef.deallocate_gti();
	ef.deallocate_events();
	bl.deallocate();
	for(int i=1;i<n_files;i++)
	{
		ef.open_fits(list[i].c_str());
		ef.read_times();
		if(flag_binarycor == 'y')
			ef.binary_corr(binary_file);
		if(bck_flag)
			bl.read_fits(list_bck[i].c_str());
		ef.update_tphase_matrix(filename3, tstart_vector, tstop_vector, n_bins, t_ref, f, df, ddf,bl, bck_flag, ext_back, ext_back_err, e_min,e_max);
		ef.close_fits();
		ef.deallocate_gti();
		ef.deallocate_events();
		bl.deallocate();
	}
	
	return status;
}



int make_ephase()
{	
	int status=0;
	char filename1[FLEN_FILENAME];
	sprintf(filename1, "list_evt.txt");
	char filename2[FLEN_FILENAME];
	sprintf(filename2, "list_bck.txt");
	char filename3[FLEN_FILENAME];
	sprintf(filename3, "none");
	double t_ref=0;
	double f=1035.84002785/2.0;//1./1.930800112558E-03;
	double df=0;
	double ddf=0;
	int n_bins=32;
	double ext_back=21.73;
	double ext_back_err=0.73;
	char binary_file[FLEN_FILENAME];
	sprintf(binary_file, "%s", "none");
	char flag_binarycor='n';
	char delta_e_flag='n';
	char delta_e_file[FLEN_FILENAME];
	double delta_e=0;
	double delta_e_offset=0;
	sprintf(delta_e_file, "none");
	double t_min=0,t_max=1e10;
	double e_min=0,e_max=1e10;
	
	char query[2048];
	{
		ifstream conf_file(".ephase_matrix_conf.txt");
		if(conf_file.is_open())
		{
			conf_file >> filename1;
			conf_file >> filename2;
			conf_file >> filename3;
			conf_file >> t_ref;
			conf_file >> f;
			conf_file >> df;
			conf_file >> ddf;
			conf_file >> n_bins;
			conf_file >> ext_back;
			conf_file >> ext_back_err;
			conf_file >> flag_binarycor;
			conf_file >> binary_file;
			conf_file >> delta_e;
			conf_file >> delta_e_flag;
			conf_file >> delta_e_file;
			conf_file >> delta_e_offset;
			conf_file >> t_min;
			conf_file >> t_max;
			conf_file >> e_min;
			conf_file >> e_max;
			conf_file.close();
		}
	}
	sprintf(query, "List of event files");
	//readline_withspace(filename1, query);
	askInfo(query, filename1);
	
	sprintf(query, "List of back curves (none to skip)");
	//readline_withspace(filename2, query);
	askInfo(query, filename2);
	
	char bck_flag=1;
	if(strcmp(filename2, "none") == 0)
	{
		cout << "Skip BCK subtraction\n";
		bck_flag=0;
	}
	
	vector<string> list;
	
	read_list(list,filename1);
	vector<string> list_bck;
	if(bck_flag)
	{
		read_list(list_bck,filename2);
		if(list.size() != list_bck.size())
		{
			cerr <<"Lists have different sizes, abort\n";
			return 1;
		}
	}
		   
	int n_files=list.size();
	
	cout << "You have given " << n_files << " event files\n";
	if(n_files ==0)
	{
		return 1;
	}
	
	event_file ef(list[0].c_str());
	light_curve bl;
	if(bck_flag)
		bl.read_fits(list_bck[0].c_str());
	
	cout << "First file details\n";
	
	
	ef.output();
	ef.scan_gti();
	double mjdref=ef.mjd_ref();
	
	if(strcmp(filename3, "none") == 0)
		sprintf(filename3, "%s_EPHASE.fits", ef.object);
	
	sprintf(query, "Name of EN-PHASE matrix");
	
	askInfo(query, filename3);
	//readline_withspace(filename3, query);
	
	char temp_str[FLEN_FILENAME];
	sprintf(temp_str, "try.qdp");
	//soft.output_lc(temp_str);
	
	sprintf(query, "Do you want to apply binary correction?");
	flag_binarycor=ask_yes_no(query	, flag_binarycor);
	
	if(flag_binarycor == 'y')
	{
	
		sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
		askInfo(query, binary_file);
		char setup_file_exist=1;
		char interactive_flag=0;
		if (strcmp(binary_file, "none")==0)
		{
			sprintf(query, "Enter the actual name of the file to store the binary parameters");
			askInfo(query, binary_file);
			
			//sprintf(binary_file,"%s_orbit.dat",active_src_name);
			interactive_flag=1;
			setup_file_exist=0;
		}
		//soft.binary_corr(binary_file);
		if(interactive_flag)
                        binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, mjdref );
	}
	sprintf(query, "number of phase bins");
	ask_integer(query, &n_bins);
	
	if(t_ref<=0)
		t_ref=ef._t_start+t_ref;

	sprintf(query, "Reference time for folding (if <=0, it adds to t_start)");
	ask_double(query, &t_ref);
	
	if(t_ref<=0)
		t_ref=ef._t_start+t_ref;

	sprintf(query, "Frequency");
	ask_double(query, &f);
	
	sprintf(query, "frequency derivative");
	ask_double(query, &df);
	
	sprintf(query, "frequency second derivative");
	ask_double(query, &ddf);

	
	if(delta_e==0)
		delta_e=0.1;//(ef._t_stop - ef._t_start)/20.;

	sprintf(query, "Do you want to enter energy bins from a file (e.g., rmf)?");
	delta_e_flag=ask_yes_no(query, delta_e_flag);
	vector<double> estart_vector, estop_vector;
	
	if(delta_e_flag == 'y')
	{
		sprintf(query, "Enter the name of the file with the energy bins (third extension by default)");
		//readline_withspace(delta_e_file, query);
		askInfo(query, delta_e_file);
		
		if(get_energy_bounds_rmf(delta_e_file,estart_vector,estop_vector))
		{
			
			cerr << "WARNING :: cannnot read " << delta_e_file << " as fits file, try as ascii\n";
			sprintf(query, "Energy offset value");
			ask_double(query, &delta_e_offset);
		
			ifstream ff(delta_e_file);
		
			int max_line=1024;
			char line[max_line];                                                                                                  
			while(ff.getline(line, max_line, '\n'))
			{ 
				double t1=0,t2=0;
				//sscanf(line, "%s\t%s", scw_tmp, pif_tmp);                                                                          
				if(line[0] != '\n' && line[0] != '\0')                                                                               
				{                                                                                                                    
					if(sscanf(line, "%lf %lf", &t1, &t2)==2)
					{
						estart_vector.push_back(t1+delta_e_offset);
						estop_vector.push_back(t2+delta_e_offset);
					}               
				}
			}
			
			ff.close();
		}
		cout << "Read " << estart_vector.size() << " energy intervals\n";
//                for(int i=0;i<tstart_vector.size();i++)
//                    std::cout << tstart_vector[i] << " " << tstop_vector[i] << endl;
		
	}
	else
	{
		sprintf(query, "Energy step");
		ask_double(query, &delta_e);
		sprintf(query, "Min energy");
		ask_double(query, &e_min);
		sprintf(query, "Max energy");
		ask_double(query, &e_max);
		
		double ee=e_min;
		while (ee<=e_max)
		{
			estart_vector.push_back(ee);
			cout << ee << " ";
			ee+=delta_e;
			estop_vector.push_back(ee);
			cout << ee << "\n";
		}		
	}
    
	sprintf(query, "External background level");
	ask_double(query, &ext_back);
        
	sprintf(query, "External background level error");
	ask_double(query, &ext_back_err);

	sprintf(query,"Minimum time of accumulation");
	ask_double(query,&t_min);
	
	sprintf(query,"Maximum time of accumulation");
	ask_double(query,&t_max);

	if(flag_binarycor == 'y')
	{
		cout << "Perform binary correction on input file\n";
		binarycor_setup(binary_file, 0, 1, 1, mjdref );
		t_min=binarycor(t_min);
		t_max=binarycor(t_max);
		//std::cout << tstart_vector[i] << " " << tstop_vector[i] << endl;

	}
	ofstream conf_file(".ephase_matrix_conf.txt");
	if(conf_file.is_open())
	{
		conf_file << filename1 << endl;
		conf_file << filename2 << endl;
		conf_file << filename3 << endl;

		conf_file <<  scientific << setw(19) << setprecision(12) << t_ref << endl;
		conf_file <<  scientific << setw(19) << setprecision(12) << f << endl;
		conf_file << scientific << setw(19) << setprecision(12) << df << endl;
		conf_file << scientific << setw(19) << setprecision(12) << ddf << endl;
		conf_file << n_bins << endl;
		conf_file << ext_back << endl;
		conf_file << ext_back_err << endl;
		conf_file << flag_binarycor << endl;
		conf_file << binary_file << endl;
		conf_file << delta_e << endl;
		conf_file << delta_e_flag << endl;
		conf_file << delta_e_file << endl;;
		conf_file << delta_e_offset << endl;
		conf_file << t_min << endl;
		conf_file << t_max << endl;
		conf_file << e_min << endl;
		conf_file << e_max << endl;
                
		conf_file.close();
	}
	
	
	ef.init_ephase(filename3, n_bins, t_ref, f, df, ddf, t_min,t_max);
	ef.read_times();
	if(flag_binarycor == 'y')
		ef.binary_corr(binary_file);
	ef.update_ephase_matrix(filename3, estart_vector, estop_vector, n_bins, t_ref, f, df, ddf, bl, bck_flag, ext_back, ext_back_err, t_min,t_max);
	ef.close_fits();
	ef.deallocate_gti();
	ef.deallocate_events();
	bl.deallocate();
	for(int i=1;i<n_files;i++)
	{
		ef.open_fits(list[i].c_str());
		ef.read_times();
		if(flag_binarycor == 'y')
			ef.binary_corr(binary_file);
		if(bck_flag)
			bl.read_fits(list_bck[i].c_str());
		ef.update_ephase_matrix(filename3, estart_vector, estop_vector, n_bins, t_ref, f, df, ddf,bl, bck_flag, ext_back, ext_back_err, t_min,t_max);
		ef.close_fits();
		ef.deallocate_gti();
		ef.deallocate_events();
		bl.deallocate();
	}
	
	return status;
}

int color_color()
{
	int status=0;
	char filename1[FLEN_FILENAME];
	char filename2[FLEN_FILENAME];
	char filename3[FLEN_FILENAME];
        char filename4[FLEN_FILENAME];
	char filename5[FLEN_FILENAME];
        
	char conf_file[FLEN_FILENAME];
	double min_sn=3;
	double max_time=10000;
        double min_bin_size=0;
	double max_point_sn=100;
	int flag_rebin=1;
	double gti_threshold=100;
	char query[2048];
	sprintf(conf_file, ".conf_colorcolor.txt");
	
	sprintf(filename1, "col1.lc");
	sprintf(filename2, "col2.lc");
        sprintf(filename3, "col3.lc");
        sprintf(filename4, "col4.lc");
        
	sprintf(filename5, "color-color.qdp");

	{
		ifstream fp(conf_file);
		if(fp.is_open())
		{
			fp >> filename1;
			fp >> filename2;
			fp >> filename3;
                        fp >> filename4;
                        fp >> filename5;
                        fp >> min_sn;
                        fp >> max_time;
                        fp >> gti_threshold;
                        fp >> flag_rebin;
                        fp >> min_bin_size;
                        fp >> max_point_sn;
			fp.close();
		}
                else
                {
                    cerr << "Warning :: could not open " << conf_file << endl;
                }
	}

	sprintf(query, "Curve 1");
	//askInfo(query, filename1);
	readline_withspace(filename1, query);

	sprintf(query, "Curve 2");
	//askInfo(query, filename2);
	readline_withspace(filename2, query);

	sprintf(query, "Curve 3");
	//askInfo(query, filename2);
	readline_withspace(filename3, query);

	sprintf(query, "Curve 4");
	//askInfo(query, filename2);
	readline_withspace(filename4, query);

	sprintf(query, "Name of output QDP file");
	//askInfo(query, filename3);
	readline_withspace(filename5, query);

	light_curve curve1(filename1);
	curve1.output();
	curve1.scan_gti();
        
	light_curve curve2(filename2);
	curve2.output();
	curve2.scan_gti();
        
        light_curve curve3(filename3);
	curve3.output();
	curve3.scan_gti();
        
        light_curve curve4(filename4);
	curve4.output();
	curve4.scan_gti();
        
	sprintf(query, "Minimum S/N for rebinning");
	ask_double(query, &min_sn);
	
	
	sprintf(query, "Maximum time interval. It does not accumulate longer bins, regardless of S/N and it removes all previous points.");
	ask_double(query, &max_time);
	
	sprintf(query, "Minimum time bin size. No shorter bins are allowed.");
	ask_double(query, &min_bin_size);
        
	sprintf(query, "Threshold to use a GTI GAP. It discards all previous data if a gti gap is larger than this value and S/N threshold is not reached.");
	ask_double(query, &gti_threshold);

	sprintf(query, "Which curve (1,2,3,4) do you use for rebinning");
	
	do{
		ask_integer(query, &flag_rebin);
	}while(flag_rebin <1 || flag_rebin >4);



	sprintf(query, "The max noise over signal ratio allowed (to discard very noisy points)");
	ask_double(query, &max_point_sn);


	
	{
		ofstream fp(conf_file);
		if(fp.is_open())
		{
			fp << filename1 << endl;
			fp << filename2 << endl;;
			fp << filename3 << endl;;
                        fp << filename4 << endl;;
                        fp << filename5 << endl;;
                        fp << min_sn << endl;;
                        fp << max_time << endl;;
                        fp << gti_threshold << endl;;
                        fp << flag_rebin << endl;
                        fp << min_bin_size << endl;
                        fp << max_point_sn << endl;
			fp.close();
		}
                else
                {
                    cerr << "Warning :: could not open " << conf_file << endl;
                }
	}



	int *table=0;
        switch (flag_rebin)
        {
            case (1):
                table=curve1.make_rebin_table(min_sn, max_time,gti_threshold,min_bin_size, max_point_sn);
                break;
            case (2):
                table=curve2.make_rebin_table(min_sn, max_time,gti_threshold,min_bin_size, max_point_sn);
                break;
            case (3):
                table=curve3.make_rebin_table(min_sn, max_time,gti_threshold,min_bin_size, max_point_sn);
                break;
            case (4):
                table=curve4.make_rebin_table(min_sn, max_time,gti_threshold,min_bin_size, max_point_sn);
                break;
            default:
                cerr << "Invalid flag_rebin\n";
                exit(1);
                
        }
	
	
	
	if(curve1.rebin(table))
        {
                cerr << "Failed to rebin curve 1\n";
                exit(1);
        }
        
	if(curve2.rebin(table))
	{
                cerr << "Failed to rebin curve 2\n";
                exit(1);
        }

	if(curve3.rebin(table))
	{
                cerr << "Failed to rebin curve 3\n";
                exit(1);
        }
	if(curve4.rebin(table))
	{
                cerr << "Failed to rebin curve 4\n";
                exit(1);
        }

        
	curve1.make_rate();
	curve2.make_rate();
	curve3.make_rate();
	curve4.make_rate();


        light_curve ratio21 = curve2/curve1;// soft.hratio(hard);//(hard-soft);
	light_curve sum21   = (curve2+curve1);

        light_curve ratio43 = curve4/curve3;// soft.hratio(hard);//(hard-soft);
	light_curve sum43   = (curve4+curve3);

        string fn=filename5;
        fn="h21_"+fn;

	ofstream ff(fn.c_str());
	
	ff << "read serr 1 2\n";
	ff<<"cpd /xw\n";
	ff <<"ma 17 on 2\n";
	ff <<"lw 2\n";
	ff <<"cs 1.1\n";
	ff <<"font roman\n";
	ff <<"time off\n";
	ff <<"lab title\n";
	ff <<"lab x Intensity 2+1 [cts/s]\n";
	ff <<"log x on\n";

	ff << "lab y Hardness ratio 2/1\n";
	ff.flush();
	
	
	for(int i=0;i<sum21.size();i++)
	{
		ff << scientific << setw(19) << setprecision(11) << sum21._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << sum21._dy[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << ratio21._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << ratio21._dy[i] << "\t";
		ff << "\n";
	}
	
	ff.close();

        fn=filename5;
        fn="h43_"+fn;

	ff.open(fn.c_str());
	
	ff << "read serr 1 2\n";
	ff<<"cpd /xw\n";
	ff <<"ma 17 on 2\n";
	ff <<"lw 2\n";
	ff <<"cs 1.1\n";
	ff <<"font roman\n";
	ff <<"time off\n";
	ff <<"lab title\n";
	ff <<"lab x Intensity 4+3 [cts/s]\n";
	ff <<"log x on\n";

	ff << "lab y Hardness ratio 4/3\n";
	ff.flush();
	
	
	for(int i=0;i<sum21.size();i++)
	{
		ff << scientific << setw(19) << setprecision(11) << sum43._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << sum43._dy[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << ratio43._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << ratio43._dy[i] << "\t";
		ff << "\n";
	}
	
	ff.close();


        fn=filename5;
        //fn="h43_"+fn;

	ff.open(fn.c_str());
	
	ff << "read serr 1 2\n";
	ff<<"cpd /xw\n";
	ff <<"ma 17 on 2\n";
	ff <<"lw 2\n";
	ff <<"cs 1.1\n";
	ff <<"font roman\n";
	ff <<"time off\n";
	ff <<"lab title\n";
	ff <<"lab x hardness ratio 2/1\n";
	ff << "lab y Hardness ratio 4/3\n";
	ff.flush();
	
	
	for(int i=0;i<sum21.size();i++)
	{
		ff << scientific << setw(19) << setprecision(11) << ratio21._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << ratio21._dy[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << ratio43._y[i] << "\t";
		ff << scientific << setw(19) << setprecision(11) << ratio43._dy[i] << "\t";
		ff << "\n";
	}
	
	ff.close();

        
	delete [] table;
	
	return status;
}

int merge_event_files()
{
        int status=0;
        char filename1[FLEN_FILENAME];
	char conf_file[FLEN_FILENAME];
	char filename3[FLEN_FILENAME];
        char filename_lc[FLEN_FILENAME];
	char query[2048];
	char flag_binarycor='n';
        char flag_lc='n';
        sprintf(conf_file,".merge_evt.txt");
	sprintf(filename1, "list_evt.txt");
        sprintf(filename3, "merged_evt.fits");
        sprintf(filename_lc, "merged_lc.fits");
	sprintf(query, "List event files");
	char binary_file[FLEN_FILENAME];
	sprintf(binary_file, "%s", "orbit.dat");
        double t_bin=-1;
        double e_min=0;
        double e_max=1e10;
        
        {
		ifstream fp(conf_file);
		if(fp.is_open())
		{
			fp >> filename1;
			fp >> filename3;
                        fp >> flag_binarycor;
                        fp >> binary_file;
                        fp >> e_min;
                        fp >> e_max;
                        fp >> flag_lc;
                        fp >> t_bin;
                        fp >> filename_lc;
			fp.close();
		}
                else
                {
                    cerr << "Warning :: could not open " << conf_file << endl;
                }
	}

        


        sprintf(query, "List of event files");
        askInfo(query, filename1);
	vector<string> list;

	read_list(list,filename1);

	int n_files=list.size();
	cout << "You have given " << n_files << " input files\n";
	if(n_files < 1)
	{
            cout << "Silenly quit, no events\n";
		return 1;
	}
        
        sprintf(query, "Minimum energy [keV]");
        ask_double(query, &e_min);

        sprintf(query, "Maximum energy [keV]");
        ask_double(query, &e_max);
        
        cout << "\n\nAbout to read first file" << list[0] << endl;
        event_file evt(list[0].c_str());
        //evt.read_times(e_min, e_max); //Need to implemnt it !
        evt.read_times();
        cerr << "WARN No energy selection in event reading\n";
        
	sprintf(query, "Output event file");
	askInfo(query, filename3);

        sprintf(query, "Do you want to apply binary correction?");
	flag_binarycor=ask_yes_no(query	, flag_binarycor);
	
	if(flag_binarycor == 'y')
	{
	
		sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
		askInfo(query, binary_file);
		char setup_file_exist=1;
		char interactive_flag=0;
		if (strcmp(binary_file, "none")==0)
		{
			sprintf(query, "Enter the actual name of the file to store the binary parameters");
			askInfo(query, binary_file);
			
			//sprintf(binary_file,"%s_orbit.dat",active_src_name);
			interactive_flag=1;
			setup_file_exist=0;
		}
		//soft.binary_corr(binary_file);
		if(interactive_flag)
                        binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, evt.mjd_ref() );
                
                evt.binary_corr(binary_file);
	}
        
        sprintf(query, "Do you want to save a light curve of the merged file?");
	flag_lc=ask_yes_no(query	, flag_lc);
	
	if(flag_lc == 'y')
	{
                sprintf(query, "Output light curve file");
                askInfo(query, filename_lc);
                
                sprintf(query, "Time bin in %s [Strongly suggested a multiple of timedel]", evt.timeunit);
                if(t_bin == -1)
                        t_bin=evt.time_del();
                ask_double(query, &t_bin);
  
        }
        
        {
            ofstream fp(conf_file);
            if(fp.is_open())
            {
                    fp << filename1 << endl;
                    fp << filename3<< endl;
                    fp << flag_binarycor<< endl;
                    fp << binary_file<< endl;
                    fp << e_min<< endl;
                    fp << e_max<< endl;
                    fp << flag_lc << endl;
                    fp << t_bin << endl;
                    fp << filename_lc << endl;
                    fp.close();
            }
            else
            {
                cerr << "Warning :: could not write open " << conf_file << endl;
            }
        }


        
        for (int i=1;i<n_files;i++)
        {
            cout << "\n\nAbout to read " << list[i] << endl;
            event_file r_evt(list[i].c_str());
            //r_evt.read_times(e_min, e_max); Need to implement energy selection
            r_evt.read_times();
            if(flag_binarycor == 'y')
                r_evt.binary_corr(binary_file);
            cout << "\n\nAbout to merge " << list[i] << endl << endl;
            evt.merge(&r_evt);
                      
        }
        
        cout << "\n\nFinished merging" << endl;
        
        evt.output();
        evt.write(filename3);
        
        if(flag_lc == 'y')
	{
            light_curve lc(evt, t_bin, e_min, e_max);
            lc.write(filename_lc);
        }
        
        return status;
}


int search_kuiper()
{


    int status=0;
    char filename1[FLEN_FILENAME];
    char conf_file[FLEN_FILENAME];
    char filename3[FLEN_FILENAME];
    char query[2048];
    char flag_binarycor='n';
    sprintf(conf_file,".search_kuiper.txt");
    sprintf(filename1, "list_evt.txt");
    sprintf(filename3, "kuiper_test");
    sprintf(query, "List of event files");
    char binary_file[FLEN_FILENAME];
    sprintf(binary_file, "%s", "orbit.dat");
    double t_ref=0;
    double f1=0;
    double f2=1e10;
    double res_sampling=1;
    
    
    double e_min=0;
    double e_max=10.0;
    
    double ff=1.2;
    char simul_flag=0;
    int simul_run;
    char flag_out_simul;
    char flag_out_char;
    
    double df=0, ddf=0, f=f1;

    {
            ifstream fp(conf_file);
            if(fp.is_open())
            {
                    fp >> filename1;
                    fp >> filename3;
                    fp >> flag_binarycor;
                    fp >> binary_file;
                    fp >> t_ref;
                    fp >> f1;
                    fp >> f2;
                    fp >> res_sampling;
                    fp >> e_min;
                    fp >> e_max;
                    fp >> ff;
                    fp >> simul_flag;
                    fp >> simul_run;
                    fp >> flag_out_simul;
                    fp >> df;
                    fp.close();
            }
            else
            {
                cerr << "Warning :: could not open " << conf_file << endl;
            }
    }

    if(flag_out_simul)
        flag_out_char='y';
    else
        flag_out_char='n';

    sprintf(query, "List of event files");
    askInfo(query, filename1);

    sprintf(query, "Minimum energy [keV]");
    ask_double(query, &e_min);

    sprintf(query, "Maximum energy [keV]");
    ask_double(query, &e_max);
    

    vector<string> list;

    read_list(list,filename1);

    int n_files=list.size();

    cout << "You have given " << n_files << " input files\n";
    if(n_files ==0)
    {
            return 1;
    }
    sprintf(query, "Root name of output files");
    askInfo(query, filename3);
    double t_start=0, t_stop=1e6, timedel=1e-1,mjdref=51544.0;
    
    event_file soft(list[0].c_str());
    soft.output();
    soft.scan_gti();
    t_start=soft.t_start();
    event_file soft_1(list[n_files-1].c_str());
    soft_1.output();
    soft_1.scan_gti();
    t_stop=soft_1.t_stop();
    timedel=soft_1.time_del();
    mjdref=soft_1.mjd_ref();


    sprintf(query, "Do you want to apply binary correction ?");
    flag_binarycor=ask_yes_no(query	, flag_binarycor);

    if(flag_binarycor == 'y')
    {
            sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
            askInfo(query, binary_file);
            char setup_file_exist=1;
            char interactive_flag=0;
            if (strcmp(binary_file, "none")==0)
            {
                    sprintf(query, "Enter the actual name of the file to store the binary parameters");
                    askInfo(query, binary_file);

                    //sprintf(binary_file,"%s_orbit.dat",active_src_name);
                    interactive_flag=1;
                    setup_file_exist=0;
            }
            binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist,mjdref);
//		soft.binary_corr(binary_file);
    }



    sprintf(query, "Start_time (Start time of events is %19.12e )", t_start);
    ask_double(query, &t_ref);

    double min_freq=1/(t_stop - t_start);
    sprintf(query, "Start frequency (minimum is %19.12e) ", min_freq);
    if (f1==0)
        f1=min_freq;
    ask_double(query, &f1);

    double max_freq=0.5/timedel;

    if(f2==1e10)
        f2=max_freq;
    sprintf(query, "Stop frequency (maximum is %19.12e) ", max_freq);
    ask_double(query, &f2);

    sprintf(query, "Resolution (over)sampling (0.1-10)");
    ask_double(query, &res_sampling);

    sprintf(query, "Frequency derivative");
    ask_double(query, &df);


/*
 * This needs to be thought and implemented, just in case
        sprintf(query, "Do you want to perform simulation of errors? (LC only single file, otherwise behaviour is undetermined !!)");

        simul_flag =ask_yes_no(query, simul_flag);
        if(simul_flag == 'y')
        {
                sprintf(query, "How many simulaiton runs?");
                ask_integer(query, &simul_run);


                sprintf(query, "Output intermmediate products?");
                flag_out_char=ask_yes_no(query, flag_out_char);

                if(flag_out_char == 'y')
                        flag_out_simul=1;
                else
                        flag_out_simul=0;


        }
*/
    ofstream fp(conf_file);
    if(fp.is_open())
    {
        fp << filename1 << endl;
        fp << filename3 << endl;
        fp << flag_binarycor << endl;
        fp << binary_file << endl;
        fp << scientific << setw(19) << setprecision(12) << t_ref << endl;
        fp << scientific << setw(19) << setprecision(12) << f1 << endl;
        fp << scientific << setw(19) << setprecision(12) << f2 << endl;
        fp << scientific << setw(19) << setprecision(12) << res_sampling << endl;
        fp << scientific << setw(19) << setprecision(12) << e_min << endl;
        fp << scientific << setw(19) << setprecision(12) << e_max<< endl;
        fp << ff <<endl;
        fp << simul_flag << endl;
        fp << simul_run << endl;
        fp << flag_out_simul << endl;
        fp << df << endl;
        fp.close();
    }

    f=f1;


    cout <<"Open output files\n";

    char out_fname[FLEN_FILENAME];
    sprintf(out_fname, "%s_periodogram_f.qdp", filename3);
    ofstream out_f(out_fname);
    sprintf(out_fname, "%s_periodogram_p.qdp", filename3);
    ofstream out_p(out_fname);
    out_f << "read serr 1 \n";
    out_f << "cpd /xw 1\n";
    out_f << "lab x \\gn [Hz]\nlab y2 V\nr x " << f1 << " " << f2 <<"\n";
    out_f << "lab y3 log\\d10\\u P\n";
    out_f << "plot vert\n";

    out_p << "read serr 1 \n";
    out_p << "cpd /xw 1\n";
    out_p << "lab x P [d]\nlab y2 V\nr x " << 1./f1/86400. << " " << 1./f2/86400. <<"\n";
    out_f << "lab y3 log\\d10\\u P\n";
    out_p << "plot vert\n";

    cout <<"Init elaboration\n";
    vector<double> freqs,periods,statistics,s_freqs,s_periods;
    
    double freq_max=0, freq_max_err=0;

    int j=0;
    //Placeholders
    event_file *run_evt=0;

    //reads first file
    run_evt = new event_file(list[0].c_str());
    //run_evt->read_times(e_min, e_max); //Need to implement energy selection
    cerr << "WARN No energy selection in event reading, implemented later in processing\n";
    run_evt->read_times();
    run_evt->scan_gti();
    run_evt->output(0,1e10,e_min,e_max);
    if(flag_binarycor == 'y')
        run_evt->binary_corr(binary_file);
//            cout << "INTERMEDIO " << endl;

    if(n_files >1)
    {
        for(int i=1;i<n_files;i++)
        {


            event_file n_run_evt(list[i].c_str());
            //n_run_evt.read_times(e_min, e_max); //Need to implement energy selection
            cerr << "WARN No energy selection in event reading, implemented in folding\n";
            n_run_evt.read_times();
            //n_run_evt.output();
            //n_run_evt.scan_gti();
            if(flag_binarycor == 'y')
                n_run_evt.binary_corr(binary_file);

            run_evt->concatenate(&n_run_evt);
            run_evt->output(0,1e10,e_min,e_max);
            //sprintf(filename1, "%03d_%s", i, filename3);
            //folded.output_lc(filename1);


        }
    }

    //Initializes folded light curve for later output
    int n_bins=run_evt->size()/100;
    if (n_bins>128)
        n_bins=128;
    if(n_bins<8)
        n_bins=8;
    cout << "allocate Folded with " << n_bins << " bins\n";
    light_curve sum(n_bins);
    sum.init_folded();
    //elaborates
    cout <<endl <<endl;
    //run->output_lc("Pippo.txt");
    double n_events_total =0;
    
    n_events_total=run_evt->output(0, 1e10, e_min, e_max);
    t_start=run_evt->t_start();
    t_stop=run_evt->t_stop();
    
    
    
    int n_trials=0;
    chrono::steady_clock::time_point begin = chrono::steady_clock::now();

    Kuiper kuiper(run_evt->_times,run_evt->_size, run_evt->GTI_START, run_evt->GTI_STOP, run_evt->n_gti);
    
    while (f<f2)
    {
        n_trials++;
        
        //  cout << "Fold freq " << f << endl;
        
        
        //cout << "Start folding\n";
        double kuiper_test = kuiper.V( t_ref, f, df, ddf);
        double kuiper_prob = kuiper.Qkp(kuiper_test, run_evt->_size);
        
        
        double s_p=1/(SQR(f)*(t_stop-t_start));
        double s_f=SQR(f)*s_p;

        out_f  << scientific << setw(19) << setprecision(12) << f << " " << s_f/2 << " " << kuiper_test <<  " " << log10(kuiper_prob) << endl;//" " << av << " " << std_dev <<  endl;
        out_p  << scientific << setw(19) << setprecision(12) << 1./f/86400. << " " << s_p/2./86400. << " " << kuiper_test<<  " " << log10(kuiper_prob) <<  endl;//" " << av <<" " << std_dev << endl;

        freqs.push_back(f);
        //periods.push_back(1/f/86400);
        s_freqs.push_back(s_f);
        //s_periods.push_back(s_p/86400);
        statistics.push_back(kuiper_test);
        //probabilities.push_back(kuiper_prob);

        f+=s_f/res_sampling;
        //if (! j++%10)
        {
            cout << "Freq " << f << " Hz\r";
            cout.flush();
        }
    }
    
    chrono::steady_clock::time_point end= chrono::steady_clock::now();
    
    
    cout << endl;

    cout << "Time needed for computation = " << chrono::duration_cast<chrono::seconds>(end - begin).count() << " s\n";

    double max_stat=-1e10, min_prob=1;
    for (int k=0;k<statistics.size();k++)
    {
        if(statistics[k] > max_stat)
        {
            max_stat=statistics[k];
            freq_max=freqs[k];
            freq_max_err=s_freqs[k];
            //min_prob=probabilities[k];
            min_prob=kuiper.Qkp(max_stat, run_evt->_size);
        }
    }
    
    //Determine significance and then sets upper limits
    
    Normaldist gaus;


    double n_effective_trials=n_trials;
    //This is taken from Paltani (2004), to be checked
    if(res_sampling>1)
    {
        n_effective_trials *= 1./(1+0.0815*res_sampling);
    }

    
    double prob=pow(1.-min_prob, n_effective_trials);
    double sig = 0;
    if(prob>=0.5 && prob < 1)
            sig=(gaus.invcdf(prob));

    if(prob==1)
            sig=10;
    //cout << z2[i_nu] << " " << sig[i_nu] << endl;

    
    
    char fname_detrend[FLEN_FILENAME];
    

    light_curve *folded=0;
    sprintf(fname_detrend, "%s_folded.qdp", filename3);
    
    folded= run_evt->fold(n_bins, t_ref, freq_max, df, ddf,
        run_evt->t_start(), run_evt->t_stop(), e_min, e_max);
    folded->output_lc(fname_detrend, 1);
    //sprintf(filename1, "%03d_%s", i, filename3);
    //folded.output_lc(filename1);

    
    sprintf(fname_detrend, "%s_res.dat", filename3);
    ofstream f_res(fname_detrend);
    cout << "**************************************\n";
    cout << "Found maximum statistics at \n";
    
    cout << "fr = " << scientific << setw(19) << setprecision(12)
            << freq_max << " +/- "<< freq_max_err << endl;
    cout << "V = " << scientific << setw(19) << setprecision(12)
            << max_stat << endl;
    cout << "Minimum probability that the fluctuation is due to chance before trials is " << min_prob <<endl;
    cout << "The probability that the fluctuation is due to chance is " 
            << 1.-prob << " corresponding to a detection of " << sig << " sigmas in Gaussian statistics\n";
    double A0=folded->average(0,10);
    double A_max=sqrt(folded->var(0,10,A0))/A0;
    if(sig<=5)
    {
        cout << "The signal is not significant at 5 sigma c.l., we determine upper limits on pulsed fraction for a sinusoidal signal\n";
        //Get the three sigma chi2
        double three_s_V=kuiper.inv_QkP(1.-gaus.cdf(3), run_evt->size());
        //We assume it sums quadratically to the maximum detected noise
        three_s_V+=max_stat;
        cout << "The Kuiper statistic V to compute the u.l. is " << three_s_V << endl;
        //Now, we get the equivalent pulsed fraction, we approximate equal error as Poissonian on the mean
        //A_max=0.5*sqrt(SQR(three_s_V) +SQR(folded->average_error()))/A0; //sqrt(2*three_s_chi*SQR((folded->average_error())/A0));
        A_max=M_PI*three_s_V; //Intrinsic variance is accounted for in the maximum noise value
        cout << "Three sigma upper limit is " << A_max << endl;      
    }
    else
    {
        cout << "rms is " << A_max << endl;
    }
    
    cout << "**************************************\n";
    f_res << scientific << setw(19) << setprecision(12)
          << (t_stop +t_start)/2-t_ref << " " << (t_stop -t_start)/2 << " "
            << freq_max << "  "<< freq_max_err <<  " "
            << 1./  freq_max << "  "<< freq_max_err/SQR(freq_max) <<  " " 
            << prob << " " << sig << " " << A_max << endl;
    f_res.close();

    
//        }//end only one file

    //Plot the 3 and 5 sigma probability levels with number of trials
    prob=gaus.cdf(3.0);
    prob = pow(prob,1.0/double(n_effective_trials));
    
    double V_level= kuiper.inv_QkP(1-prob, run_evt->size());
    A_max=M_PI*V_level;
    out_f << "win 2\n";
    out_p << "win 2\n";
    out_f << "lab 1 \"3\\gs\" pos "<< f1 << " " <<  V_level<< " line 0 0.95\n";
    out_p << "lab 1 \"3\\gs\" pos "<< f1 << " " <<  V_level<< " line 0 0.95\n";
    out_f << "! 3sigma u.l. on pulsed fraction after " << n_trials << " trials is "<< A_max << endl;
    out_p << "! 3sigma u.l. on pulsed fraction after " << n_trials << " trials is "<< A_max << endl;
    out_f << "win 3\n";
    out_p << "win 3\n";
    out_f << "lab 2 \"3\\gs\" pos "<< f1 << " " <<  log10(1-prob) << " line 0 0.95\n";
    out_p << "lab 2 \"3\\gs\" pos "<< f1 << " " <<  log10(1-prob)<< " line 0 0.95\n";
    prob=gaus.cdf(5.0);
    prob = pow(prob,1.0/double(n_effective_trials));
    V_level= kuiper.inv_QkP(1-prob, run_evt->size());
    A_max=M_PI*V_level;
    out_f << "win 2\n";
    out_p << "win 2\n";
    out_f << "lab 3 \"5\\gs\" pos "<< f1 << " " <<  V_level<< " line 0 0.95\n";
    out_p << "lab 3 \"5\\gs\" pos "<< f1 << " " <<  V_level<< " line 0 0.95\n";
    out_f << "! 5sigma u.l. on pulsed fraction after " << n_trials << " trials is "<< A_max << endl;
    out_p << "! 5sigma u.l. on pulsed fraction after " << n_trials << " trials is "<< A_max << endl;
    out_f << "win 3\n";
    out_p << "win 3\n";
    out_f << "lab 4 \"3\\gs\" pos "<< f1 << " " <<  log10(1-prob) << " line 0 0.95\n";
    out_p << "lab 4 \"3\\gs\" pos "<< f1 << " " <<  log10(1-prob)<< " line 0 0.95\n";
    out_f.close();
    out_p.close();
    
    if(folded)
        delete folded;
    cout << "No siulation implemented, yet\n";
    /*
    if(simul_flag=='y')
    {
        cout << " START " << simul_run << " SIMULATION RUNs\n";
        int n_freq=chis.size();
        VecDoub v_freq(n_freq);
        VecDoub v_chi(n_freq);
        VecDoub best_freq(simul_run);
        VecDoub best_chi(simul_run);

        for(int j=0;j<n_freq;j++)
        {
            v_freq[j]=freqs[j];
        }

        if(n_files>1 || evt_flag == 1)
        {
            cerr << "Not implemented for multiple or event files yet\n";
            return 1;

        }

        char simul_file_name[FLEN_FILENAME];
        char simul_single_file_name[FLEN_FILENAME];

        sprintf(simul_file_name, "simul_%s.qdp", filename3);

        ofstream simul_file(simul_file_name), simul_single_file;
        if(!simul_file.is_open())
        {
            cerr << "Could not open file " << simul_file_name << endl;
            return 1;

        }
        simul_file << "read\n";
        simul_file << "cpd /xw\n";
        simul_file << "lab x Simul X\n";
        simul_file << "lab y3 \\gx\\u2\\d\n";
        simul_file << "lab y2 \\gn [Hz]\n";
        simul_file << "plot vert\n";
        simul_file << "win 2\n";
        simul_file << "lab 2 col 3 line 0 100\n";
        simul_file << "lab 2 pos 0 " << scientific << setw(19) << setprecision(12)
                << freq_max << endl;
        simul_file << "lab 3 col 4 line 0 100\n";
        simul_file << "lab 3 pos 0 " << scientific << setw(19) << setprecision(12)
                << freq_max + freq_max_err<< endl;
        simul_file << "lab 4 col 4 line 0 100\n";
        simul_file << "lab 4 pos 0 " << scientific << setw(19) << setprecision(12)
                << freq_max - freq_max_err<< endl;


        if(flag_out_simul)
        {
            simul_single_file.open(simul_single_file_name);
            if(!simul_single_file.is_open())
            {
                cerr << "Could not open file " << simul_single_file_name << endl;
                return 1;

            }
            simul_single_file << "read\n";
            simul_single_file << "cpd /xw\n";
            simul_single_file << "lab y \\gx\\u2\\d\n";
            simul_single_file << "lab x \\gn [Hz]\n";

        }
        //Placeholders \reads again the LC/EVT files
        //as variables were local in the cycle, possible to improve on this
        event_file *run_evt;
        light_curve *run;

        //reads
        if(evt_flag ==1)
        {
            run_evt = new event_file(list[0].c_str());
            //run_evt->read_times(e_min, e_max); //Need to implement energy selection
            run_evt->read_times();
            run_evt->output();
            run_evt->scan_gti();
            if(flag_binarycor == 'y')
                run_evt->binary_corr(binary_file);

        }
        else
        {
            run = new light_curve(list[0].c_str());
            run->output();
            run->scan_gti();
            if(flag_binarycor == 'y')
                run->binary_corr(binary_file);
            if(detrend_flag=='y')
            {
                run->detrend(detrend_scale*2./(f1+f2), 0, NULL);
            }
        }

        cout << "Perform simul run\n";
        for (int k=0;k<simul_run;k++)
        {
            //cout << "\r";
            //cout << "\r" << (int)floor((float)k/(float)simul_run*100) << " %";
            cout << "\rRun " << k << " of "  << simul_run;
            cout.flush();
            if(flag_out_simul)
            {
                sprintf(simul_single_file_name, "simul_%04d_%s.qdp", k, filename3);

                simul_single_file.open(simul_single_file_name);

                if(!simul_single_file.is_open())
                {
                    cerr << "Could not open file " << simul_single_file_name << endl;
                    return 1;

                }
                simul_single_file << "read\n";
                simul_single_file << "cpd /xw\n";
                simul_single_file << "lab y \\gx\\u2\\d\n";
                simul_single_file << "lab x \\gn [Hz]\n";

            }


            light_curve simul_lc = run->simul_gaus();
            if(detrend_flag=='y')
            {
                simul_lc.detrend(detrend_scale*2./(f1+f2), 0, NULL);
            }

            light_curve *folded;
        //elaorates
            for(int j=0;j<freqs.size();j++)
            {
                f=freqs[j];
//                    cout << "Fold freq " << f << endl;

                if(evt_flag ==1)
                {
                    ;//Implement me
//                    light_curve folded= run_evt->fold(n_bins, t_ref, f, df, ddf,
//                         run_evt->t_start(), run_evt->t_stop(), e_min, e_max);
//                    sum+=folded;
//                                    //sprintf(filename1, "%03d_%s", i, filename3);
                //folded.output_lc(filename1);

                }
                else
                {

                    folded = (simul_lc.fold(n_bins, t_ref, f, df, ddf, weight_flag, ff));

                    //sprintf(filename1, "%05d_%s.qdp", j++, filename3);
    //                    folded.output_lc(filename1);
                }

                double av=folded->average(-1e100, 1e100);
                //double std_dev=sqrt(folded->var(-1e100, 1e100,av));
                v_chi[j]=folded->chi(av);
                //double s_p=1/(SQR(f)*n_bins*(t_stop-t_start));
                //double s_f=SQR(f)*s_p;
                if(flag_out_simul)
                {

                   simul_single_file  << scientific << setw(19) << setprecision(12) << f << " " <<
                           v_chi[j] << endl;
                }

            }//end of loop on frequencies, ind is j
            if(flag_out_simul)
            {
                simul_single_file.close();
            }
            double max_chi=-1e100;
            int ind=v_chi.max_loc_val(max_chi);
            best_chi[k]=max_chi;
            best_freq[k]=v_freq[ind];
            simul_file << k << " " <<  scientific << setw(19) << setprecision(12) << best_freq[k] << " " <<
                           best_chi[k] << endl;
            simul_file.flush();
        }//end of loop on simulation, ind is k

        cout << "\n";
        simul_file.close();



    }//end of simulation
     */
    delete run_evt;
    
    return status;
}

int lc_from_events()
{	
	int status=0;
	char filename1[FLEN_FILENAME];
	sprintf(filename1, "list_evt.txt");
	double t_ref=0;
	double bin_size=1.;
	
	char binary_file[FLEN_FILENAME];
	sprintf(binary_file, "%s", "none");
	char flag_binarycor='n';
	double e_min=0,e_max=1e10, min_frac_exp=0;;
	
	char query[2048];
	{
		ifstream conf_file(".lc_from_events.txt");
		if(conf_file.is_open())
		{
			conf_file >> filename1;
			conf_file >> t_ref;
			conf_file >> bin_size;
			conf_file >> flag_binarycor;
			conf_file >> binary_file;
			conf_file >> e_min;
			conf_file >> e_max;
			conf_file >> min_frac_exp;
			conf_file.close();
		}
	}
	sprintf(query, "List of event files");
	//readline_withspace(filename1, query);
	askInfo(query, filename1);
	
	
	vector<string> list;
	
	read_list(list,filename1);
		   
	int n_files=list.size();
	
	cout << "You have given " << n_files << " event files\n";
	if(n_files ==0)
	{
		return 1;
	}
	
	event_file ef(list[0].c_str());
	
	cout << "First file details\n";
	ef.output();
	ef.scan_gti();
	double mjdref=ef.mjd_ref();
	
	sprintf(query, "Do you want to apply binary correction?");
	flag_binarycor=ask_yes_no(query	, flag_binarycor);
	
	if(flag_binarycor == 'y')
	{
	
		sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
		askInfo(query, binary_file);
		char setup_file_exist=1;
		char interactive_flag=0;
		if (strcmp(binary_file, "none")==0)
		{
			sprintf(query, "Enter the actual name of the file to store the binary parameters");
			askInfo(query, binary_file);
			
			//sprintf(binary_file,"%s_orbit.dat",active_src_name);
			interactive_flag=1;
			setup_file_exist=0;
		}
		//soft.binary_corr(binary_file);
		if(interactive_flag)
                        binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, mjdref );
	}
	

	
	sprintf(query,"Time bin");
    ask_double(query,&bin_size);

	sprintf(query,"Minimal fractional exposure to output a time bin");
    ask_double(query,&min_frac_exp);
	

	sprintf(query,"Minimum energy of accumulation");
    ask_double(query,&e_min);
        
    sprintf(query,"Maximum energy of accumulation");
    ask_double(query,&e_max);
	if(flag_binarycor == 'y')
	{
		cout << "Perform binary correction on input file\n";
		binarycor_setup(binary_file, 0, 1, 1, mjdref );
		//std::cout << tstart_vector[i] << " " << tstop_vector[i] << endl;

	}
	
	ef.read_times();
	if(flag_binarycor == 'y')
		ef.binary_corr(binary_file);
	
	if(t_ref<=0)
		t_ref=ef._t_start+t_ref;
	
	sprintf(query,"Start time of the light curve");
        ask_double(query,&t_ref);

	if(t_ref<=0)
		t_ref=ef._t_start+t_ref;
	

	ofstream conf_file(".lc_from_events.txt");
	if(conf_file.is_open())
	{
		conf_file << filename1 << endl;
		conf_file <<  scientific << setw(19) << setprecision(12) << t_ref << endl;
		conf_file <<  scientific << setw(19) << setprecision(12) << bin_size << endl;
		conf_file << flag_binarycor << endl;
		conf_file << binary_file << endl;
		conf_file << e_min << endl;
		conf_file << e_max << endl;
		conf_file << min_frac_exp << endl;
		conf_file.close();
	}

	light_curve lc(ef, bin_size, e_min, e_max);
	char fname_out[FLEN_FILENAME];
	sprintf(fname_out, "lc_%s.qdp", list[0].c_str());
	lc.output_lc( fname_out, 0, min_frac_exp, t_ref);
	ef.close_fits();
	ef.deallocate_gti();
	ef.deallocate_events();
	lc.deallocate();
	
	lc.deallocate();
	for(int i=1;i<n_files;i++)
	{
		ef.open_fits(list[i].c_str());
		ef.read_times();
		ef.scan_gti();
		ef.output();
		if(flag_binarycor == 'y')
			ef.binary_corr(binary_file);
		new(&lc) light_curve(ef, bin_size, e_min, e_max);
		sprintf(fname_out, "lc_%s.qdp", list[i].c_str());
		lc.output_lc( fname_out, 0, min_frac_exp, t_ref);
		ef.close_fits();
		ef.deallocate_gti();
		ef.deallocate_events();
		lc.deallocate();
	}
	return 0;
}


int save_phase_evt()
{

	
	int status=0;
	char filename1[FLEN_FILENAME];
	char conf_file[FLEN_FILENAME];
	char filename3[FLEN_FILENAME];
	char query[2048];
	char flag_binarycor='n';
	sprintf(conf_file,".save_phase_evt.txt");
	sprintf(filename1, "list_evt.txt");
	sprintf(filename3, "phased_");
	
	sprintf(query, "List of event files");
	char binary_file[FLEN_FILENAME];
	sprintf(binary_file, "%s", "orbit.dat");
	double t_ref=0;
	double f=1;
	double df=0;
	double ddf=0;

	{
		ifstream fp(conf_file);
		if(fp.is_open())
		{
			fp >> filename1;
			fp >> filename3;
			fp >> flag_binarycor;
			fp >> binary_file;
			fp >> t_ref;
			fp >> f;
			fp >> df;
			fp >> ddf;
			fp.close();
		}
		else
		{
			cerr << "Warning :: could not open " << conf_file << endl;
		}
	}

	sprintf(query, "List of event files");
	askInfo(query, filename1);

	vector<string> list;

	read_list(list,filename1);

	int n_files=list.size();

	cout << "You have given " << n_files << " input files\n";
	if(n_files ==0)
	{
		return 1;
	}
	sprintf(query, "prefix to add");	
	askInfo(query, filename3);
	double t_start=0;
	double mjdref=0;
	
	event_file soft(list[0].c_str());
	soft.output();
	soft.scan_gti();
	t_start=soft.t_start();
	mjdref=soft.mjd_ref();

	sprintf(query, "Do you want to apply binary correction ?");
	flag_binarycor=ask_yes_no(query	, flag_binarycor);
	
	if(flag_binarycor == 'y')
	{
		sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
		askInfo(query, binary_file);
		char setup_file_exist=1;
		char interactive_flag=0;
		if (strcmp(binary_file, "none")==0)
		{
			sprintf(query, "Enter the actual name of the file to store the binary parameters");
			askInfo(query, binary_file);
			
			//sprintf(binary_file,"%s_orbit.dat",active_src_name);
			interactive_flag=1;
			setup_file_exist=0;
		}
//		soft.binary_corr(binary_file);
		if(interactive_flag)
                        binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, mjdref );

	}
			
	sprintf(query, "Start_time (Start time of light curve is %19.12e )", t_start);
	ask_double(query, &t_ref);
	
	sprintf(query, "frequency");
	ask_double(query, &f);
	
	sprintf(query, "frequency derivative");
	ask_double(query, &df);
	
	sprintf(query, "frequency second derivative");
	ask_double(query, &ddf);

	ofstream fp(conf_file);
	if(fp.is_open())
	{
		fp << filename1 << endl;
		fp << filename3 << endl;
		fp << flag_binarycor << endl;
		fp << binary_file << endl;
		fp << scientific << setw(19) << setprecision(12) << t_ref << endl;
		fp << scientific << setw(19) << setprecision(12) << f << endl;
		fp << scientific << setw(19) << setprecision(12) << df << endl;
		fp << scientific << setw(19) << setprecision(12) << ddf << endl;
		fp.close();
	}


	char out_filename[FLEN_FILENAME];
	for(int i=0;i<n_files;i++)
	{
		event_file run_evt(list[i].c_str());
		//run_evt.read_times(e_min, e_max);
		cerr << "Warn :: Need to implement energy selection in reading events\n";
		run_evt.read_times();
		run_evt.output();
		run_evt.scan_gti();
		if(flag_binarycor == 'y')
			run_evt.binary_corr(binary_file);
		sprintf(out_filename, "%s%s", filename3, list[i].c_str());
		run_evt.write_with_phase(out_filename, t_ref, f, df, ddf, flag_binarycor);

	}
            
	return status;
}

int make_gti_table()
{
int status=0;
	char filename1[FLEN_FILENAME];
	char conf_file[FLEN_FILENAME];
	char filename3[FLEN_FILENAME];
	char query[2048];
	char flag_binarycor='n';
	sprintf(conf_file,".make_gti_table.txt");
	sprintf(filename1, "list_evt.txt");
	sprintf(filename3, "gti_");
	
	sprintf(query, "List of event files");
	char binary_file[FLEN_FILENAME];
	sprintf(binary_file, "%s", "orbit.dat");
	double t_ref=0;
	double f=1;
	double df=0;
	double ddf=0;
	int n_bins=1;
	double start_phase =0.0;
	double end_phase =0.0;

	{
		ifstream fp(conf_file);
		if(fp.is_open())
		{
			fp >> filename1;
			fp >> filename3;
			fp >> flag_binarycor;
			fp >> binary_file;
			fp >> t_ref;
			fp >> f;
			fp >> df;
			fp >> ddf;
			fp >> start_phase;
			fp >> end_phase;
			fp >> n_bins;
			fp.close();
		}
		else
		{
			cerr << "Warning :: could not open " << conf_file << endl;
		}
	}

	sprintf(query, "List of event files");
	askInfo(query, filename1);

	vector<string> list;

	read_list(list,filename1);

	int n_files=list.size();

	cout << "You have given " << n_files << " input files\n";
	if(n_files ==0)
	{
		return 1;
	}
	sprintf(query, "prefix to add to each gti table");	
	askInfo(query, filename3);
	double t_start=0;
	double mjdref=0;
	
	event_file soft(list[0].c_str());
	soft.output();
	soft.scan_gti();
	t_start=soft.t_start();
	mjdref=soft.mjd_ref();

	sprintf(query, "Do you want to apply binary correction ?");
	flag_binarycor=ask_yes_no(query	, flag_binarycor);
	
	if(flag_binarycor == 'y')
	{
		sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
		askInfo(query, binary_file);
		char setup_file_exist=1;
		char interactive_flag=0;
		if (strcmp(binary_file, "none")==0)
		{
			sprintf(query, "Enter the actual name of the file to store the binary parameters");
			askInfo(query, binary_file);
			
			//sprintf(binary_file,"%s_orbit.dat",active_src_name);
			interactive_flag=1;
			setup_file_exist=0;
		}
//		soft.binary_corr(binary_file);
		if(interactive_flag)
                        binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, mjdref );

	}
			
	sprintf(query, "Reference Time (Start time is %19.12e )", t_start);
	ask_double(query, &t_ref);
	
	sprintf(query, "frequency");
	ask_double(query, &f);
	
	sprintf(query, "frequency derivative");
	ask_double(query, &df);
	
	sprintf(query, "frequency second derivative");
	ask_double(query, &ddf);

	sprintf(query, "Start phase");
	ask_double(query, &start_phase);

	sprintf(query, "End phase");
	ask_double(query, &end_phase);

	sprintf(query, "Number of phase bins");
	ask_integer(query, &n_bins);


	ofstream fp(conf_file);
	if(fp.is_open())
	{
		fp << filename1 << endl;
		fp << filename3 << endl;
		fp << flag_binarycor << endl;
		fp << binary_file << endl;
		fp << scientific << setw(19) << setprecision(12) << t_ref << endl;
		fp << scientific << setw(19) << setprecision(12) << f << endl;
		fp << scientific << setw(19) << setprecision(12) << df << endl;
		fp << scientific << setw(19) << setprecision(12) << ddf << endl;
		fp << scientific << setw(19) << setprecision(12) << start_phase << endl;
		fp << scientific << setw(19) << setprecision(12) << end_phase << endl;
		fp << n_bins << endl;
		fp.close();
	}


	char out_filename[FLEN_FILENAME];
	for(int i=0;i<n_files;i++)
	{
		event_file run_evt(list[i].c_str());
		//run_evt.read_times(e_min, e_max);
		run_evt.read_times();
		run_evt.output();
		run_evt.scan_gti();
		if(flag_binarycor == 'y')
			run_evt.binary_corr(binary_file);
		sprintf(out_filename, "%s%s", filename3, list[i].c_str());
		run_evt.make_gti_table(out_filename, start_phase, end_phase, flag_binarycor, t_ref, f, df, ddf, n_bins);
	}
            
	return status;
}


int main(int argc, char *argv[])
{
	
	int n_options=24;
	char *menu[] = {"elaborate_lc", "average_lags_coherence", "Find Flares", 
        "Z2 stat", "fit Z2 peak", "Power spectrum", "Simple simulation", "Hardness ratio",
        "Fold", " elaborate_lc_fold", "rebin LC",
        "Make TPHASE from event files", "Lomb Scargle", "epoch folding", "Correlation analysis", "Fit orbital model", 
        "Make En-Phase matrix from event files", "Color color diagram", "Merge events with common GTI", "Kuiper test", "LC from events", 
		"Write Phases to file", "Make GTI table",
		"Exit"};
	char Query[1024];
	sprintf(Query, "\nChoose your option");
	int option=4;
	{
		ifstream conf_file(".lc_analyzer_conf.txt");
		if(conf_file.is_open())
		{
			conf_file >> option;
			conf_file.close();
		}
	}
	
	option  = ask_menu(n_options, menu, option , Query);
	
	//cout << option << endl;
	int status=0;
	
	switch (option)
	{
			
		case 1:
			status=elaborate_lc();
			break;
			
		case 2:
			status=average_lags_coherence();
			break;
			
		case 3:
			status=find_flares();
			break;
			
		case 4:
			status=z2_stat();
			break;
			
		case 5:
			status=fit_z2_peak();
			break;	
			
		case 6:
			status=power_spectrum();
			break;				
			
		case 7:
			status=simple_simul();
			break;				

		case 8:
			status=hardness_ratio();
			break;				
			
		case 9:
			status=fold_lc();
			break;	
			
		case 10:
			status= elaborate_lc_fold();
			break;
	
		case 11:
			status= rebin_lc();
			break;
			
		case 12:
			status=make_tphase();
			break;
			
		case 13:
			status=lomb_scargle();
			break;

		case 14:
			status=search_fold();
			break;

		case 15:
			status=correlation_analysis();
			break;
			
		case 16:
			status=fit_orbital_model();
			break;
			
		case 17:
			status=make_ephase();
			break;
		case 18:
			status=color_color();
			break;
		case 19:
			merge_event_files();
			break;
		case 20:
			search_kuiper();
			break;   
		case 21:
			lc_from_events(); 
			break;
		case 22:
			save_phase_evt();
			break;
		case 23:
			make_gti_table();
			break;
		default:
			cout << "Good bye !";
			break;
	}
	
	ofstream conf_file(".lc_analyzer_conf.txt");
	if(conf_file.is_open())
	{
		conf_file << option << endl;
		conf_file.close();
	}
	return status;
}

