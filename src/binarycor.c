/*

PRO  binarycor,time,asini=asini,porb=porb,eccentricity=ecc,omega=omega_d, $
              t90=t90,t0=t0, pporb=pporb,limit=limit,maxiter=maxiter
;+
; NAME:         binarycor.pro
;
;
;
; PURPOSE:      Removes the influence of the doublestar motion for
;               circular or eliptical orbits.
;
;
;
; CATEGORY:     timing tools
;
;
;
; CALLING SEQUENCE:
;   binarycor,time,asini=asini,porb=porb,eccentricity=ecc,omega=omega_d, $
;              t90=t90,t0=t0, pporb=pporb,limit=limit,maxiter=maxiter
;
; INPUTS:
;       	time   : event time (MJD)
;
; KEYWORD PARAMETERS:
;
;	        asini  : Projected semi-major axis [lt-secs], Mandatory
;	        porb   : Orbital period at the epoch [days], Mandatory
;	        eccentricity : Eccentricity, (0<=e<1)
;	        omega: Longitude of periastron [degrees], mandatory
;
;       the epoch is given by one of the following:
;               T0     : epoch for mean longitude of 0 degrees
;                        (periastron, MJD)
;               T90    : epoch for mean longitude of 90 degrees (MJD)
;
;           T90 and T0 have to be in days and in the same time
;           system as time (e.g. JD or MJD)
;
;         Circular orbits:
;              * if time of lower conjunction Tlow is known, set
;                T0=Tlow and omega=0
;              * if time of ascending node is known, Tasc, set
;                T90=Tasc and omega=0
;              * if time of mid eclipse is known, Tecl, set
;                T0=Tecl-0.5*porb and omega=0
;
; OPTIONAL INPUTS:
;
;               pporb: rate of change of the orbital period (s/s)


;               limit: absolute precision of the correction of the
;                      computation (days, default: 1D-6)
;               maxiter: stop Newton-Raphson iteration after maxiter
;                      steps if limit is not reached (default: 20)
;
;
;
; OUTPUTS:
;               time is updated to the time in the barycenter of the
;               double star
;
; PROCEDURE:
;
;   For each time, the z-position of the emitting object is computed
;   and the time is adjusted accordingly. This is iterated until
;   convergence is reached (usually only one iteration is necessary,
;   even in high elliptic cases,
;
;   Follows equations from Hilditch's book and has also been
;   checked against fasebin/axBary. All codes give identical results,
;   (to better than 1d-7s) as checked by a Monte Carlo search using
;   1d7 different orbits.
;
;
; EXAMPLE:
;
;
;
; MODIFICATION HISTORY:
;
; $Log: binarycor.pro,v $
; Revision 1.5  2005/05/14 20:46:39  wilms
; removed special case for circular orbits since this used different
; conventions for the orbit zero point. For this version of the code,
; results for ecc=epsilon and ecc=0 will be the same. Unfortuntely this
; breaks the backwards compatibility.
;
; Revision 1.4  2005/05/14 13:43:32  wilms
; Corrected erroneous computation of mean anomaly for eccentric orbits.
;
; Revision 1.3  2005/02/23 09:46:22  wilms
; removed redundant check for t90/t0
;
; Revision 1.2  2005/02/18 20:40:33  wilms
; * added proper treatment of t90 for elliptical orbits
; * cleaned up the equations for elliptical orbits (removed excessive parentheses)
;
; Revision 1.1  2004/08/05 12:27:50  wilms
; rewrite of dblstarcor, orbital elements now given as keyword parameters,
; several speed-ups, further consistency checks.
; Verification that the equation for z is correct.
;
; Revision 1.3  2002/09/09 14:58:27  wilms
; removed debugging statement; several cosmetic changes
;
;               Version 1.0, 1997/01/09, Deepto Chakrabarty, Caltech/MIT
;               Version 1.1, 1999/11/11, Patrick Risse IAAT
;                           - Include the optional Input of pporb
;                           - abort criteria for the iteration
;
;-
*/


#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>



#include "ask.h"

#include "binarycor.h"


#define M_TWOPI     (M_PI * 2.0)
#define DAY_TO_SEC 86400.0  /* conversion factor from sec to days  */
static double MJDREF=51544.;        /*Modified Julian Date of time origin */


/*------------------------------------------------
 *
 *-----------------------------------------------*/

//double keplereq(double m,double ecc);

static int print_binarycor_par(void);
static int save_binarycor_par(char * FileName);
static int load_binarycor_par(char * FileName);
static int orbit_correction_setup(char * TimeUnit);
static int kepler_eq(double *E, double M);


/*-----------------------------------------
 *   parameters for the orbit correction
 *-----------------------------------------*/



static double asini=0;      /* Projected semi-major axis [lt-secs] */
static double porb=0;       /* Orbital period at the epoch [days]  */
static double ecc=0;        /* Orbital eccentricity (0<=e<1)       */
static double omega_deg=0;    /* Longitude of periastron [degrees]     */

static double t90=5e4;     /* epoch for mean longitude of 90 degrees (MJD) */
static double t0=5e4; 	   /* epoch for mean longitude of 0 degrees (periastron, MJD) */

static double pporb=0.;  /* rate of change of the orbital period (s/s) */


static double limit=1.E-8; /*  absolute precision of the correction computation (days) */

/*------------------------------------------
 *         working variables
 *------------------------------------------*/


static double asini_d;  /*Projected semi-major axis (days) */

static double porb_s;

static double sinw,cosw;
static double sq;

static double limit_s;
static double t0_s;


/*=======================================================*/
/*                                                       */
/*=======================================================*/

static int print_binarycor_par(void)
{


	/*---------------------------------------------------
	 *  return if no orbital parameters have been defined
	 *---------------------------------------------------*/


	if (asini<=0)
		{
		return 1;
		}





	printf("\n");
	printf("\n ================== ORBITAL PARAMETERS ===============");

	/*------------------------------------
	 *
	 *------------------------------------*/


	printf("\n  Projected semi-major axis (lt-secs): %-20.16lg",asini);

	printf("\n  Eccentricity: %-20.16lg",ecc);

	if (ecc>0)
	{
            
                printf("\n  Longitude of periastron (deg): %-20.16lg",omega_deg);   /* measured from the ascending node */

		//t90 = t0 - porb + porb * ecc /M_PI * sin(omega_deg/180.*M_PI); //Papitto et 2011

                
                //t0  = t90 + porb / 4.0 ;
                
                //t90=t0-(omega_deg-90.)/360. * porb; //Original
                
                printf("\n  Epoch for periastron (MJD): %-20.16lg",t0);             /* The time of periastron passage  */

		printf("\n  T90 (MJD): %-20.16lg",t90);             /* The time of mena anomaly equal to 90  */

	}
	else
	{
		printf("\n  Epoch for lower conjunction (MJD): %-20.16lg",t0);

	}




	printf("\n  Orbital period at the epoch (days): %-20.16lg",porb);

	printf("\n  Rate of change of the orbital period (s/s): %-20.16lg",pporb);
	printf("\n  Reference MJD %19.12e", MJDREF);
	

	printf("\n =====================================================\n");


        return 0;

}  /* end of print_binarycor_par() */



/*============================================================*/
/*    setup the variables used in the correction               */
/*============================================================*/


static int orbit_correction_setup(char * TimeUnit)
{
	
        double omega=0;




	/*---------------------------------
	 *
	 *--------------------------------*/

        if (ecc <0)
        {
            printf("Negative ecc set to zero\n");
            ecc=0;
        }
        if (ecc >1)
        {
            printf("High ecc set to one\n");
            ecc=1;
        }
        
	sq=sqrt((1.-ecc)*(1.0+ecc));


	if (ecc>0)
		omega = omega_deg * M_PI/180.0;
	else
		omega=0;


	sinw = sin(omega);
        cosw = cos(omega);


	/*--------------------------
	 *  conversion of time units
	 *-------------------------*/


	if (TimeUnit[0]=='d')
	{
		asini_d = asini/DAY_TO_SEC;  /* conversion from sec to days  */
		porb_s=porb;
		limit_s=limit;
		t0_s=t0-MJDREF;
	}
	else
	{
		porb_s=porb*DAY_TO_SEC;     /* from days to sec */
		asini_d=asini;
		limit_s=limit*DAY_TO_SEC;

		t0_s=(t0-MJDREF)*DAY_TO_SEC;
	}

//	printf("Actual t0_s = %19.12e\n", t0_s);
//	printf("MJDREF = %19.12e\n", MJDREF);
	

	return 1;
} /* end of orbit_correction_setup() */


/*============================================================*/
/*                                                            */
/*============================================================*/

int  binarycor_setup(char *FileName, int interactive, int actual_setup, int load_file_flag, double mjdref)
{
	//interactive forces the interaction
	//actual set-up forces the real set-up which assumes you have already loaded the parameters from a file

	MJDREF = mjdref;
	
	char *TimeUnit="s";


	if(load_file_flag)
	{
		if (load_binarycor_par(FileName)!=0)
		{
			return 1;
			//interactive=1;
		}
	}
		
		
	/*----------------------------------------
	* force interactive mode if cannot read
	* orbital parameters from the file
	*---------------------------------------*/


/* Only one of the t90 and t0 arguments is allowed */

	/*----------------------------------------
	 * printout the orbital parameters and
	 * force interactive mode if there is any
	 * invalid parameter
	 *---------------------------------------*/


	if (print_binarycor_par()==0)
	{
		if (interactive)
		{
			if( (ask_yes_no("\n Do you wish to change any orbital parameter?", 'n')=='n') )
			{
				if(actual_setup)
				{
					orbit_correction_setup(TimeUnit);
				}
				return 0;
			}
		}
		else
		{
			if(actual_setup)
			{
				orbit_correction_setup(TimeUnit);
			}
			return 0;
		}
	}

	/*-------------------------------------------------
     *  enter the orbital parametes with user-friendly
	 *  interface
	 *------------------------------------------------*/


	do {


		printf("\n Enter the orbital parameters \n");


    /*------------------------------------
	 *
	 *------------------------------------*/

                do
		{
		ask_double("  Projected semi-major axis (lt-secs): ",&asini);

		if (asini<=0)
			printf("\n ERROR: semi-major axis must be positive.\n");

		}
		while (asini<=0);




	/*------------------------------------
	 *           eccentricity
	 *------------------------------------*/

	do
   	  {
	  ask_double("  Eccentricity:",&ecc);

	  if (ecc<0)
		printf("\n ERROR: Eccentricity must be positive.\n");

	  if (ecc>=1)
		printf("\n Orbit correction has only been implemented for circular and elliptic orbits.\n");
 	  }

	  while ((ecc<0)||(ecc>=1));






	  /*----------------------------------------------
	   * Need omega for noncircular orbits
	   *----------------------------------------------*/

	if (ecc>0)
		ask_double("  Longitude of periastron (deg): ",&omega_deg);
	else
		omega_deg = 0;

	//	 {

		// do
		//   {

		//	if (omega_d<=0)
		//		printf("Need omega keyword for noncircular orbits");
		//	}
		//	while (omega_d<=0.);
	//	 }

	do
	 {
	  ask_double("  Orbital period (days)",&porb);


	  if (porb<=0)
		printf("\n ERROR: Orbital period must be positive.\n");



	  }
	  while (porb<=0);

	/*
         * if(flag_menu !=1)
	{
			printf("\nComputing t0 from t90\n");
			t90=t0;
			t0=t0+(omega_deg-90.)/360. * porb;
	}
	*/	
		

	ask_double("  Rate of change of the orbital period (s/s) ",&pporb);

	/*--------------------------------------------
	 *
	 *--------------------------------------------*/

	


	   /*--------------------------------------
	    *               epoch
		*--------------------------------------*/
		
		int flag_menu=1;
		char *menu[]={"T0", "T90"};		
		flag_menu=ask_menu(2, menu, flag_menu, "Reference timesystem");

		if(flag_menu == 1)
		{
			if (ecc>0)
				ask_double("  Epoch for mean longitude of periastron (MJD) ",&t0);
			else
				ask_double("  Epoch for lower conjunction (MJD) ",&t0);
                        
			t90=t0-(omega_deg-90)/360*porb; //This is not used for computation, just for output
		}
		else
		{
			if (ecc>0)
			{
				ask_double("  Epoch for T90 (MJD) ",&t90);
				t0=t90+(omega_deg-90.)/360.*porb; 
				// This is the definition of TO, T90 in Stubert et al. (2009))
				//note that this assumes pporb<<porb, which can be generally assumed
			}
			else
			{
				ask_double("  Epoch for ascending node (MJD) ",&t90); //correct below
				//This is not necessary in reality, as omega_deg is initialized to zero, but it is harmless
				t0=t90-0.25*porb; //This is the definition of ascending node in circular orbits, translated to lower conjunction
			}
		}

/*         Circular orbits:
;              * if time of lower conjunction Tlow is known, set
;                T0=Tlow and omega=0

;              * if time of ascending node is known, Tasc, set
;                T90=Tasc and omega=0
;              * if time of mid eclipse is known, Tecl, set
;                T0=Tecl-0.5*porb and omega=0

    ;; compute t0 from t90 if necessary
    ;; 

    IF (n_elements(t90) NE 0) THEN BEGIN
        t0=t90+(omega_d-90.)/360. * porb
    ENDIF

*/
	/*-----------------------------------------
	 *        orbital period parameters
	 *-----------------------------------------*/
		print_binarycor_par();
 	}
	while(ask_yes_no("\n Do you wish to change any orbital parameter?", 'n')=='y');



	/*-------------------------------------------------
	 *  setup the workig variables used by binarycor()
	 *--------------------------------------------------*/
	if(actual_setup)
		orbit_correction_setup(TimeUnit);   /* use sec time unit */

	/*---------------------------------------------------
	 *  save the orbital parameters in an ASCII file
	 *-----------------------------------------------------*/

	return save_binarycor_par(FileName);
} /* end of binarycor_setup() */

/*=============================================================*/
/*    Removes the influence of the doublestar motion for       */
/*    circular or eliptical orbits.                            */
/*                                                             */
/*	INPUTS:                                                    */
/*       	time   : event time (in sec)                       */
/*                                                             */
/*=============================================================*/


double  binarycor(double ev_time)
{

	int maxiter=20;  /*   stop Newton-Raphson iteration after maxiter steps if limit is not reached */

	double 	tper;
	double  t;

	double  m;  /* mean anomaly (radians) */

	double  z;
	double  f;
	double  df;
	double cor;
	double eanom;

	double sin_e;
	double cos_e;

	int numiter=0;

	cor=2.*limit;

	t=ev_time;
	//printf("%19.12e B\n", t);
	//printf("%19.12e %19.12e\n", cor, limit_s);
	do
	{
		tper=(t-t0_s)/porb_s;

		m=M_TWOPI*tper* (1. - 0.5*pporb* tper);   /* compute the "Mean anomaly" */

		//	double kepler_eq2(double M);

		//	eanom=kepler_eq2(m);

		//printf("%19.12e Km\n", t);
		kepler_eq(&eanom,m); /* compute the angular position of the planet on its orbit. */
		//printf("%19.12e K\n", t);

		sin_e = sin(eanom);
		cos_e = cos(eanom);

		z = asini_d*(sinw*(cos_e-ecc)+sq*cosw*sin_e);

		f = (t-ev_time) + z;

		df =(sq*cosw*cos_e - sinw*sin_e)*(M_TWOPI*asini_d/(porb_s*(1.-ecc*cos_e)));

		cor=f/(1.+df);

		t = t - cor;

		numiter++;

		//printf("\n binarycor: numiter: %d; cor:%g ",numiter,cor);
	}while ((fabs(cor)> limit_s)&& (numiter < maxiter));

	/*------------------------------------------
	 *
	 *-----------------------------------------*/

	if(numiter>=maxiter)
	{
        	printf("\n binarycor: Exceeded maxiter iterations and did not reach convergence");
	}

    ev_time=t;

	return ev_time;
} /* end of binarycor() */


/*====================================================*/
/*    save the orbital parameters in a ASCII file     */
/*====================================================*/

static int save_binarycor_par(char * FileName)
{

	FILE  *OrbitFile;

	char * format="%-20.16lg\n";

	if( (OrbitFile = fopen(FileName, "w"))==NULL)
	{

		printf("\n save_binarycor_par: cannot save orbital parameters in '%s'",FileName);
	    printf("\n                     %s.\n",strerror(errno));

		return errno;
	}

	/*-------------------------------------------
	 *          orbital parameters
	 *-------------------------------------------*/

	fprintf(OrbitFile,format, asini);    /* Projected semi-major axis (lt-secs)        */
	fprintf(OrbitFile,format, ecc);      /* Eccentricity, (0<=e<1)                     */
	fprintf(OrbitFile,format, omega_deg);  /* Longitude of periastron (degrees)          */
	fprintf(OrbitFile,format, t0);       /* epoch for mean longitude of periastron [MJD]    */
	fprintf(OrbitFile,format, porb);     /* Orbital period at the epoch (days)         */
    fprintf(OrbitFile,format, pporb);    /* rate of change of the orbital period (s/s) */

    fclose(OrbitFile);

    printf("\n Orbital parameters saved in '%s'\n", FileName);

	return 0;
} /* end of save_binarycor_par() */


/*===================================================*/
/*                                                   */
/*===================================================*/

int set_binarrycor_par(double asini_i, double ecc_i, double omega_deg_i, double t0_i, double porb_i, double pporb_i)
{
    asini=asini_i;    /* Projected semi-major axis (lt-secs)           */
	ecc=ecc_i;      /* Eccentricity, (0<=e<1)                        */
	if (ecc <0)
	{
		printf("Negative ecc\n");
		ecc=0;
	}
	if (ecc >1)
	{
		printf("High ecc\n");
		ecc=1;
	}
	omega_deg= omega_deg_i;  /* Longitude of periastron (deg)                 */
	t0=t0_i;       /* epoch for mean longitude of periastron (MJD)  */
	porb=porb_i;     /* Orbital period at the epoch (days)            */
	pporb=pporb_i;    /* rate of change of the orbital period (s/s)    */
	char *TimeUnit="s";
	orbit_correction_setup(TimeUnit);
	return 0;
}

static int load_binarycor_par(char *FileName)
{

	FILE  *OrbitFile=NULL;
	int n_par=0;

	/*---------------------------------------------
	 *
	 *---------------------------------------------*/

	if( (OrbitFile = fopen(FileName, "r"))==NULL)
		{

		printf("\n load_binarycor_par: cannot open file '%s'",FileName);
	    printf("\n                     %s.\n",strerror(errno));

		return errno;
		}

	/*-----------------------------------
	 *
	 *----------------------------------*/

	printf("\n ... reading the orbital parameters from '%s'\n", FileName);


	 /*-------------------------------------------
	  *   read the orbital parameters
	  *-------------------------------------------*/

	n_par+=fscanf(OrbitFile, "%lf", &asini);    /* Projected semi-major axis (lt-secs)           */
	n_par+=fscanf(OrbitFile, "%lf", &ecc);      /* Eccentricity, (0<=e<1)                        */
	n_par+=fscanf(OrbitFile, "%lf", &omega_deg );  /* Longitude of periastron (deg)                 */
	n_par+=fscanf(OrbitFile, "%lf", &t0);       /* epoch for mean longitude of periastron (MJD)  */
	n_par+=fscanf(OrbitFile, "%lf", &porb);     /* Orbital period at the epoch (days)            */
	n_par+=fscanf(OrbitFile, "%lf", &pporb);    /* rate of change of the orbital period (s/s)    */
//		n_par+=fscanf(OrbitFile, "%lf", &MJDREF);    /*Refrence MJD for times    */
    t90=t0-(omega_deg-90)/360*porb;
      fclose(OrbitFile);


	  /*------------------------------------------
	   *  verify the number of parameters
	   *------------------------------------------*/

	if (n_par!=6)
	{

		printf("\n load_binarycor_par: ERROR! wrong number of orbital parameters %d (must be 6).",n_par);

		asini=-1; /* mark parameters as invalid */
	}

	return 0;
} /* end of load_binarycor_par() */



/*===============================================*/
/*  Solve Kepler's equation: E = M + e sin(E)    */
/*                                               */
/*===============================================*/


static int kepler_eq(double *E, double M)
{

int count = 0;

static double sign = 1.0;

static double  E_old=M_PI/2;


static double M_old=-1000000;


	/* Do selected calculation, and quit when accuracy is bettered. */


double dE;

static double a=1;
static double b=1;


	if (fabs(M-M_old)>1.e-2)
		{



		//printf("\n restart loop: M=%g; M_old=%g; diff:%g\n",M,M_old,M-M_old);


		M_old=M;  /* store M for the next loop */


		//waitkey();

		/*-----------------------------------------
	     *  Normalize M to lie between -PI and PI
		 *-----------------------------------------*/

		sign = M > 0 ? 1.0 : -1.0;

		M = fabs(M)/(2*M_PI);

		M = (M - floor(M))*2*M_PI*sign;


		if(M > M_PI)
			M = M-2*M_PI;
		else if(M < -M_PI)
			M = M+2*M_PI;

		E_old=M_PI/2;
		}
	else
		{
		/*-----------------------------
	     * use previous values
	     *-----------------------------*/

		E_old+=(M+b)/a;

		M_old=M;  /* store M for the next loop */

		}


	/*--------------------------------
	 *             main loop
     *--------------------------------*/
	do
	  {
	  a=1. - ecc*cos(E_old);
	  b=ecc*sin(E_old) - E_old;

 // dE=(M + ecc*sin(E_old) - E_old)/(1. - ecc*cos(E_old));

	  dE=(M + b)/a;

	  E_old+=dE;
	  count++;
	  //printf("%d %19.12e %19.12e\n", count, dE, limit);
	  }
	 while (fabs(dE)>limit && count <1e1);




	//printf("\n %d dM=%g; dE=%g; dE/dM=%g; cos(E)=%g ",count,M-M_old,dE,(dE)/(M-M_old),cos(E_old));
//waitkey();


	*E = E_old;



//	printf(" M=%g; sign=%g; E=%g; ecc=%g\n",M_old,sign,*E,ecc);

//waitkey();

	return count;
}




/*=================================================*/
/*   test the binary correction                    */
/*=================================================*/


int binarycor_test(void)
{
#include <time.h>

double Tstart=905;

double Tstop=950;

double t,time_corr;

double dt=0.01;

double delay;

int j=0;

char *TimeUnit="d";

FILE * fp=NULL;


	printf("\n binarycor_test: entry ..  ");


	binarycor_setup("c:/tmp_orbit_GX.txt",1,1,1,51544.);

//binarycor_setup(">",1);


	if (ask_yes_no("\n Do you wish to use 'sec' as time unit?", 'y')=='y')
		sprintf(TimeUnit,"s");
	else
		sprintf(TimeUnit,"d");


	printf("\n Time Unit: '%s'",TimeUnit);

	orbit_correction_setup(TimeUnit);


	/*--------------------------------------------
	 *
	 *-------------------------------------------*/

		ask_double(" Enter the start time (MJD)",&Tstart);


		ask_double(" Enter the stop time (MJD)",&Tstop);


		ask_double(" Enter the time step(MJD)",&dt);




	if (TimeUnit[0]=='s')
		{
		Tstart=(Tstart-51544.)*86400;
		Tstop=(Tstop-51544.)*86400;
		dt=dt*86400;


	//	ask_double(" Enter the start time (s)",&Tstart);

	//	ask_double(" Enter the stop time (s)",&Tstop);

	//	ask_double(" Enter the stop time (s)",&dt);


		}





	/*--------------------------------------------
     *	open the output qdp file
     *-------------------------------------------*/
char * qdpfile="c:/tmp_orbit_delay.qdp";

	fp=fopen(qdpfile,"w");


	fprintf(fp,"LA X \"Time (MJD)\"\n");
	fprintf(fp,"LA Y \"Time Delay (sec)\"\n");


	/*---------------------------------------------
	 *   start of the correction loop
	 *---------------------------------------------*/

time_t start,finish;

	t=Tstart;

	start=time(NULL); /* loop start time */

	do
		{
		j++;

//		printf("\n %4d time: %g  ",j,t);

		time_corr=binarycor(t);

//		printf(" %lf  ",t);


//	if (t>925)
//		waitkey();
//		printf(" %lf  ",time_corr-time);

		delay=t-time_corr;



		if (TimeUnit[0]=='d')
			delay*=86400;

		if (TimeUnit[0]=='d')
			fprintf(fp,"%lf %lf\n",t,delay);
		else
			fprintf(fp,"%lf %lf\n",t/86400.+51544,delay);




		t+=dt;  /* time increment */

		}
		while (t<=Tstop);


	finish=time(NULL); /* loop end time */


	/*-------------------------------------------
     *
     *-------------------------------------------*/

	fclose(fp);
	printf("\n orbit correction sevd to: '%s'",qdpfile);

	printf("\n Excution time: %g seconds (%g microsec/event)\n", difftime(finish,start), difftime(finish,start)/j*1.e6);


		waitkey();
return 1;

} /* end of binarycor_test() */


int binarycor_set_mjdref(double mjdref)
{
    MJDREF = mjdref;
    return 0;
}
