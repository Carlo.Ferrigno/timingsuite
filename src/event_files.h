/*
 *  event_files.h
 *  Light_Curve_Analyzer
 *
 *  Created by Carlo Ferrigno on 10/11/09.
 *  Copyright 2009 ISDC/IAAT. All rights reserved.
 *
 */

#ifndef _EVENT_FILES_H
#define _EVENT_FILES_H

#include "fitsio.h"
#include <vector>
#include "light_curve.h"

class event_file{
	
public:
	int _size;
	fitsfile *fptr;
	
	char _is_open;
		
	double _time_del;
	char timeref[FLEN_VALUE];
	char telescop[FLEN_VALUE];
	char object[FLEN_VALUE];
	char origin[FLEN_VALUE];
	char instrume[FLEN_VALUE];
	char timeunit[FLEN_VALUE];
	char timesys[FLEN_VALUE];
	char filename[FLEN_FILENAME];
	char scwname[FLEN_VALUE];
        std::vector <int> _selection_indexes;
        
//	char og_name[FLEN_VALUE];

//	char _perform_integral_barycentric;
	double _ontime;
	
	double *_times;
	double *_energy;
		
//	double _ra_obj;
//	double _dec_obj;
	

	double _mjd_ref;

	double _t_start;
	double _t_stop;

	int time_ind;
	int energy_ind;

	double *GTI_START;
	double *GTI_STOP;
	
	int n_gti;
        
	char binary_corr_flag;

	inline double mjd_ref(void)
	{
		return _mjd_ref;
	}
	inline double t_stop(void)
	{
		return _t_stop;
	}
	
	inline double t_start(void)
	{
		return _t_start;
	}
	inline double time_del(void)
	{
		return _time_del;
	}
	inline int size(void)
	{
		return _size;
	}

	inline double *time_ptr(void)
	{
		return _times;
	}
        
//constr destr
	event_file();
	event_file(const char *fname);
	~event_file();
	void deallocate_events();
	void deallocate_gti();
	void allocate_gti(int n);
	void allocate_events(int n);
	
	void clean_values();
	void init_keywords();
	void init_values();
	
	int open_fits(const char *fname);
	int close_fits();
	int get_times(std::vector<double> &times, int flag_binarycorr, double e_min, double e_max, 
            int max_n_events, int i_start, char int_bary, double ra_obj, double dec_obj, char *og_name, 
            double max_time_separation);
	int get_gti(fitsfile *fptr, int status);
	void scan_gti();
	double output(double t_min=0, double t_max=1e10, double e_min=0, double e_max=1e10);
	int read_times();
	int get_times_tlim(std::vector<double> &times, double t1, double t2);

	class light_curve *fold(int n_bins, double tref, double f, double df, double ddf, double t1, double t2, double e_min, double e_max);
	double *compute_phases(double tref, double f, double df, double ddf);
	int init_tphase(char *fname, int n_bins, double t_ref, double period, double df, double df2, double e_min, double e_max);
	int init_ephase(char *fname, int n_bins, double t_ref, double period, double df, double df2, double t_min, double t_max);
	int update_tphase_matrix(char *fname, std::vector<double> tstart_vector, std::vector<double> tstop_vector, int n_bins, double t_ref, double period, double df, double df2, class light_curve &bl, char bck_flag, double ext_back, double ext_back_err, double e_min, double e_max);
	int update_ephase_matrix(char *fname, std::vector<double> estart_vector, std::vector<double> estop_vector, int n_bins, double t_ref, double period, double df, double df2, class light_curve &bl, char bck_flag, double ext_back, double ext_back_err, double t_min, double t_max);
	int add_key_matrix(fitsfile *fptr, double t_ref, double period, double df, double df2);
//	int isgri_barycentric_correction(double *loc_times, char *og_name, int numEvents, double RA, double DEC);
	void binary_corr(char *file_name);
	void concatenate(event_file *ee);
	void merge(event_file *ee);
	int make_gti_table(char root_fname[], double ph_start, double ph_stop, char binary_flag, double t_ref, double f, double df, double df2, int n_bins);

	int write(char *fname);
	int write_with_phase(char *fname, double tref, double f, double df, double ddf, char binary_flag);
	
	int write_timing_keywords(fitsfile *evt_on);
	int write_phase_keywords(fitsfile *evt_on, double tref, double f, double df, double ddf, char binary_flag);
	int write_gtis(fitsfile *evt_on, int ext_n_gti=0, double *ext_gti_start=NULL, double *ext_gti_stop=NULL, 
			double tref=0, double f=0, double df=0, double ddf=0, char binary_flag='n');
	double get_Rk2(int k, double omega, double t_ref, char flag_err, int flag_binarycorr, double e_min, double e_max, 
					long int n_events_per_chunk, int &stopped_event,  char int_bary, double ra_obj, double dec_obj, 
					char *og_name, double max_time_separation);
	
	int get_local_energies(double e_min, double e_max, double *l_e_min_o, double *l_e_max_o);
	};

int get_time_resolution(const char *fname, double *time_resolution, double *t_start, double *t_stop, double *mjdref, char *bary_flag, double *ra_obj, double *dec_obj, char *og_name );

double rand_uniform(void);
#endif
