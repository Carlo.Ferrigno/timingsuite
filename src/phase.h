#ifndef PHASE_H
#define PHASE_H
#include <string>
using namespace std;

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
class phase{
public:

	int    n_max;
	int    n_phases_mat;
	int    n_pulses;
	double t_ref;    //s
	double f;  //hz
	double df;
	double df2;
	double *t_start;
	double *t_stop;
	double *exposures;
	double  *pulses;
	double  *err_pulses;
	double  *corrs;
	double  *err_corrs;
	double *t_pulse;
	double *t_err_pulse;
	double exposure;
	string src_name;
	string in_fname;
	string template_fname;
	double sn_thresh;
    phase();
	 

    ~phase();
#ifndef OPTIONMM_command_line
	 int init_from_arguments(int argc, char *argv[]);
#endif
	 int read_matrix();
	 int read_template();
	 int compute_covariance();
	 int compute_covariance_2();
	 int compute_phi();
	 int total_pulse();
	 int save_matrix();
	 int find_maxima();
	 double get_phase_complete(double time, double *i_phase);
	 //double get_dt(double phi);
	 int rebin_pulses();
	 int pulse_plots();

};

/*For C++ Standard*/
double get_phi(double dt, void *params);
double get_dphi(double dt, void *params);
void   get_fdphi(double dt, void *params, double *y, double *dy);

double get_dt(double phi,double f, double df, double df2);
double get_bin(double t, void *params);
double get_solar(double t);

#endif
