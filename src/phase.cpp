/***************************************************************************
 *   Copyright (C) 2004 by Carlo Ferrigno                                  *
 *   ferrigno@pa.iasf.cnr.it                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "error.h"
#include "fitsio.h"
#include "phase.h"
#include "binarycor.h"
#include <iostream>
#include <iomanip>
#ifdef OPTIONMM_command_line
#include "command_line.hh"
#endif
#include <cstring>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_fft_complex.h>
#include <gsl/gsl_statistics_double.h>

using namespace std;

phase::phase()
{
	n_max=3; 
	n_phases_mat=0;
	n_pulses=0;
	t_start=0;
	t_stop=0;
	pulses=0;
	err_pulses=0;
	t_pulse=0;
	t_err_pulse=0;
	corrs=0;
	err_corrs=0;
	//sprintf(src_name, "SRC");
	//in_fname;
}


phase::~phase()
{

	if(t_start)
	{
		delete [] t_start;
		t_start=0;
	}

	if(t_stop)
	{
		delete [] t_stop;
		t_stop=0;
	}
	if(pulses)
	{
		delete [] pulses;
		pulses=0;
	}
	if(err_pulses)
	{
		delete [] err_pulses;
		err_pulses=0;
	}
	if(t_pulse)
	{
		delete [] t_pulse;
		t_pulse = 0;
	}
	
	if(t_err_pulse)
	{
		delete [] t_err_pulse;
		t_err_pulse = 0;
	}
	if(corrs)
	{
		delete [] corrs;
		corrs = 0;
	}

	if(err_corrs)
	{
		delete [] err_corrs;
		err_corrs = 0;
	}
	
}

#ifdef OPTIONMM_command_line
int phase::init_from_arguments(int argc, char *argv[])
{
	optionmm::basic_command_line<> cl("Computation of phases", "0.1",
												 "Copyright (c) 2006 Carlo Ferrigno",
												 "[OPTIONS] TIME_PHASE_MATRIX", argc, argv);
	optionmm::basic_option<int, true, false>  n_opt('n', "nbins", "Number of bins to search the maximum", n_max);
	optionmm::basic_option<string, true, false>  Name_opt('N', "name", "name of the source for the output file", "SRC");
	optionmm::basic_option<string, true, false>  template_opt('t', "template", "files with the template pulse", "none");
	optionmm::basic_option<double, true, false>  sn_opt('s', "SN", "signal to noise for pulse rebinning", 5.0);
	//optionmm::basic_option<bool, false, false>  try_opt('b', "", "Temptative", false);

	/*
	The optional second template parameter specifies whether or not the option needs an argument. An error will be issued if the application user didn't specify an argument for an option that has this template parameter set to true. The default value is true. If this parameter is set to false, then specifying this option to the application makes an instance of operator!(Type) which you better define unless Type is convertable to some simple type.

	The optional third template parameter specifies whether or not the option can take multiple values (arguments or set count). If this parameter is true, (the default) then specifying the same option to the application more than once causes the optionmm::basic_option object to store all those values, which can later be retrived via calls to optionmm::basic_option::value(int). If false, giving the same option several times, overwrites the previous value.*/

	cl.add(n_opt);
	cl.add(Name_opt);
	cl.add(template_opt);
	cl.add(sn_opt);
	//cl.add(try_opt);
	
	if (!cl.process()) return ERROR_CODE_INPUT;

	if (cl.help()) return CODE_SUCCESS;
	cl.version();
	if(argc==1)
	{
		cerr << cl.usage() <<endl;
		return ERROR_CODE_INPUT;
	}

// 	for (int i = 0; i < n_opt.size(); i++)
// 		std::cout << n_opt.value(i) << std::endl;
// 	for (int i = 0; i < Name_opt.size(); i++)
// 		std::cout << Name_opt.value(i) << std::endl;

	//cout << try_opt.value(0) << endl;
	n_max = n_opt.value(0);
	src_name = Name_opt.value(0);
	template_fname=template_opt.value(0);
	sn_thresh = sn_opt.value(0);
	
	in_fname = argv[1];
// 	for(int i = 0; i < argc; i++)
// 		cout << argv[i] << endl;
	
	return CODE_SUCCESS;
}
#endif

int phase::read_matrix()
{

	fitsfile *fptr;
	int status=0;
	if(fits_open_file(&fptr, in_fname.c_str(), READONLY, &status))
	{
		cerr << "Error opening '" << in_fname <<"'\n";
		return ERROR_CODE_INPUT;
	}

	long nrows=0;
	int hdutype=0;
	fits_movabs_hdu(fptr, 2, &hdutype, &status);
	fits_get_num_rows(fptr, &nrows, &status);
	if(status)
	{
		cerr <<"Error in finding nrows in '"<<in_fname<<"'\n";
		return status;
	}
	n_pulses=nrows;
	{
		char keyword[] = "NPHASES";
		fits_read_key( fptr,  TINT, keyword, &n_phases_mat, NULL, &status);
	}
	{
		char keyword[] = "EXPOSURE";
		fits_read_key( fptr,  TDOUBLE, keyword, &exposure, NULL, &status);
	}
	{
		char keyword[] = "TREF";
		fits_read_key( fptr,  TDOUBLE, keyword, &t_ref, NULL, &status);
	}
	{
		char keyword[] = "FREQ";
		fits_read_key( fptr,  TDOUBLE, keyword, &f, NULL, &status);
	}
	{
		char keyword[] = "dFREQ";
		fits_read_key( fptr,  TDOUBLE, keyword, &df, NULL, &status);
	}
	{
		char keyword[] = "ddFREQ";
		fits_read_key( fptr,  TDOUBLE, keyword, &df2, NULL, &status);
	}
	/*Allocatio of memory*/
	t_start = new double[n_pulses];
	t_stop = new double[n_pulses];
	exposures = new double[n_pulses];
	pulses = new double[n_pulses*n_phases_mat];
	err_pulses = new double[n_pulses*n_phases_mat];
	corrs = new double[n_pulses*n_phases_mat];
	err_corrs = new double[n_pulses*n_phases_mat];
	long firstrow= 1, firstelem=1;
	float fnull_val=0;
	double dnull_val=0;
	int anynull=0;
	fits_read_col(fptr, TDOUBLE, 1, firstrow, firstelem, n_pulses, &dnull_val, t_start , &anynull, &status);
	fits_read_col(fptr, TDOUBLE, 2, firstrow, firstelem, n_pulses, &dnull_val, t_stop  , &anynull, &status);
	fits_read_col(fptr, TDOUBLE, 3, firstrow, firstelem, n_pulses*n_phases_mat, &fnull_val, pulses,     &anynull, &status);
	fits_read_col(fptr, TDOUBLE, 4, firstrow, firstelem, n_pulses*n_phases_mat, &fnull_val, err_pulses ,&anynull, &status);

	for(int j=0;j<n_pulses;j++)
		exposures[j] = t_stop[j]-t_start[j];
	
	if(anynull)
	{
		cerr << "WARNING :: " << anynull << " null values found\n";
	}

	if(status)
	{
		fits_report_error(stderr, status);
		cerr <<"Error reading elements in '"<<in_fname<<"'\n";
		return status;
	}
	
	fits_close_file(fptr, &status);

	return CODE_SUCCESS;
}

int phase::save_matrix()
{

	fitsfile *fptr;
	fitsfile *ofptr;
	int status=0;
	string ofname="!Correlation_"+src_name+".fits";
	if(fits_open_file(&fptr, in_fname.c_str(), READONLY, &status))
	{
		cerr << "Error opening '" << in_fname <<"'\n";
		return ERROR_CODE_INPUT;
	}
	if(fits_create_file(&ofptr, ofname.c_str(), &status))
	{
		cerr << "Error opening '" << ofname <<"'\n";
		return ERROR_CODE_INPUT;
	}

	if(fits_copy_file(fptr, ofptr, 1, 1, 1, &status))
	{
		cerr << "Error copying extension in '" << ofname <<"'\n";
		return status;
	}
	fits_close_file(fptr, &status);
	
	long firstrow= 1, firstelem=1;
	
	fits_write_col(ofptr, TDOUBLE, 3, firstrow, firstelem, n_pulses*n_phases_mat,  corrs,      &status);
	fits_write_col(ofptr, TDOUBLE, 4, firstrow, firstelem, n_pulses*n_phases_mat,  err_corrs , &status);
	

	if(status)
	{
		fits_report_error(stderr, status);
		cerr <<"Error writing elements in '"<<ofname<<"'\n";
		return status;
	}
	
	fits_close_file(ofptr, &status);

	return CODE_SUCCESS;
}


int phase::read_template()
{
	if(strcmp(template_fname.c_str(), "none") ==0 )
	{
		cout << "No template file name to read, cannot perform Correlation analysis\n";
		return ERROR_CODE_INPUT;
	}

	ifstream ff(template_fname.c_str());
	if(!ff.is_open())
	{
		cout << "Error opening '" << template_fname << ", cannot perform Correlation analysis\n";
		return ERROR_CODE_OPEN;
	}
	//Allocates memory
	t_pulse = new double[n_phases_mat];
	t_err_pulse = new double[n_phases_mat];
	
	char line[1024];
	int i=0;
	int n=n_phases_mat;
	for(i=0;i<n;i++)
	{
		t_pulse[i]=0;
		t_err_pulse[i]=0;
	}
	i=-1;
	while(!ff.eof())
	{
		ff.getline(line,1024);
// 		cout << line <<endl;
		if(line[0]>'9' || line[0]<'0')
			continue;

		double ph, a, sa;
		sscanf(line, "%lf %*f %lf %lf", &ph, &a, &sa);
		if(ph>=0 && ph <1)
		{
			i++;
			t_pulse[i]=a;
			t_err_pulse[i]=sa;
			//cout << i << "\t" << ph << "\t" << a << "\t" << sa <<endl;
		}

		if(i>n_phases_mat)
		{
			cerr << "We read too many values in template pulse\n";
			ff.close();
			return ERROR_CODE_INPUT;
		}
	}

	ff.close();
	
	return CODE_SUCCESS;
}



int phase::compute_covariance()
{

#define REAL(z,i) ((z)[2*(i)])
#define IMAG(z,i) ((z)[2*(i)+1])

	int i;
	const int n = n_phases_mat;
	double data1[2*n],data2[2*n];
	gsl_fft_complex_wavetable * wavetable;
	gsl_fft_complex_workspace * workspace;

	wavetable = gsl_fft_complex_wavetable_alloc (n);
	workspace = gsl_fft_complex_workspace_alloc (n);
	
	
	for(int j=0;j<n_pulses;j++)
	{
		for (i = 0; i < n; i++)
		{
			REAL(data1,i) = pulses[j*n_phases_mat+i];
			IMAG(data1,i) = 0.0;
			REAL(data2,i) = t_pulse[i];
			IMAG(data2,i) = 0.0;
		}

		gsl_fft_complex_forward (data1, 1, n, wavetable, workspace);
		gsl_fft_complex_backward (data2, 1, n, wavetable, workspace);

		for(i = 0; i < n; i++)
		{
			double re=REAL(data1,i)*REAL(data2,i)-IMAG(data1,i)*IMAG(data2,i);
			double im=IMAG(data1,i)*REAL(data2,i)+REAL(data1,i)*IMAG(data2,i);
			REAL(data1,i)=re;
			IMAG(data1,i)=im;
		}
		gsl_fft_complex_inverse (data1, 1, n, wavetable, workspace);
		i=0;
		corrs[j*n_phases_mat+i] = REAL(data1,i); //sqrt(REAL(data1,i)*REAL(data1,i) + IMAG(data1,i)*IMAG(data1,i));
		for (i = 1; i < n; i++)
		{
			corrs[j*n_phases_mat+ n-i] = REAL(data1,i);//sqrt(REAL(data1,i)*REAL(data1,i) + IMAG(data1,i)*IMAG(data1,i));
		}
// 		for (i = 0; i < n; i++)
// 		{
// 			printf ("%03d % 8.3e % 8.3e % 8.3e\n", i, REAL(data1,i), IMAG(data1,i), corrs[j*n_phases_mat+i]);
// 		}
		//printf("\n\n");
		//break;
	}

	gsl_fft_complex_wavetable_free (wavetable);
	gsl_fft_complex_workspace_free (workspace);
	
	return CODE_SUCCESS;
#undef REAL
#undef IMAG
}

int phase::compute_covariance_2()
{

	int i;
	const int n = n_phases_mat;
// 	printf ("read serr 2\n");
// 	printf ("cpd /xw\n");

	//cout << t_ref << "\t" << f << "\n";
	
// 	if(t_ref ==0 || f==0)
// 	{
// 		cerr << "Cannot perform correlation analysis because frequency or reference time are zero\n";
// 		return ERROR_CODE_INPUT;
// 	}
	for(int j=0;j<n_pulses;j++)
	{
		double total=0;
		for (i = 0; i < n; i++)
		{
			//corrs[j*n_phases_mat+i] = 0;
			double tmp=0;
			double tmp_err=0;
			
			for (int k = 0; k < n; k++)
			{
				int ind = k+i;
				if(ind>=n)
					ind-=n;
				tmp += pulses[j*n_phases_mat+k] * t_pulse[ind];
				double aa = pulses[j*n_phases_mat+k] * t_err_pulse[ind];
				double bb = err_pulses[j*n_phases_mat+k] * t_pulse[ind];
				tmp_err += aa*aa+bb*bb;
			}
			//printf ("%03d % 8.3e % 8.3e % 8.3e\n", i,  corrs[j*n_phases_mat+i], tmp, corrs[j*n_phases_mat+i] -tmp);
			corrs[j*n_phases_mat+i]=tmp;
			total+=corrs[j*n_phases_mat+i];
			err_corrs[j*n_phases_mat+i]=sqrt(tmp_err);
		
		}

		for (i = 0; i < n; i++)
		{
			corrs[j*n_phases_mat+i]/=total/n;
			err_corrs[j*n_phases_mat+i]/=total/n;
// 			printf ("%03d % 8.3e % 8.3e\n", i, corrs[j*n_phases_mat+i], err_corrs[j*n_phases_mat+i]);
		}
// 		printf("\n\n");
		//break;
	}
	
	return save_matrix();
	
}

int phase::pulse_plots()
{

	ofstream ref_times("reference_times.dat");
	ref_times << setw(19)<< setprecision(12);
	for(int j=0;j<n_pulses;j++)
	{
		double *pulse = pulses+j*n_phases_mat;
		double *pulse_err = err_pulses+j*n_phases_mat;
		char fname[FLEN_FILENAME];
		sprintf(fname, "pulse_%03d.dat", j);
		ofstream norma(fname);

		double dt = t_start[j] - t_ref;
		double phase = floor(dt*(f + dt*( (df/2.) + dt* (df2/6.)  ) ) );
		double new_time = get_dt(phase, f, df, df2);

		ref_times << new_time/86400. + 51544. << endl;
// 		norma << "Reference time [MJD]: "
// 				<< setw(19)<< setprecision(12)
// 				<< new_time/86400. + 51544.
// 				<< setw(11)<< setprecision(4)
// 				<< endl;
				
		
		for(int i=0;i<n_phases_mat;i++)
		{
			float phase = ( ((float)i + 0.5)/((float)n_phases_mat)) ;
			norma << phase << "\t" << pulse[i] << "\t" << pulse_err[i] << endl;
		}
		norma.close();
	}
	ref_times.close();
	return CODE_SUCCESS;
}

int phase::compute_phi()
{

	double I_s=0, I_c=0, I_s2=0, I_c2=0, alpha_0=0, A=0, phi0=0, phi0_2=0;
	double I_s_err=0, I_c_err=0, I_s2_err=0, I_c2_err=0, alpha_0_err=0, A_err=0, phi0_err=0, phi0_2_err=0;
	string stmp = "dphi_"+src_name+".qdp";
	ofstream norma(stmp.c_str());
	stmp = "times_"+src_name+".qdp";
	ofstream time_file(stmp.c_str());

	
	norma << "read ser 1 2 3 4 5\n";
	norma << "cpd /XW\n";
	norma << "plot vertical\n";
	norma << "lab x time [s]\n";
	norma << "lab y2 \\ga\\d0\\u\n";
	norma << "lab y3 A\n";
	norma << "lab y4 \\gf\n";
	norma << "lab y5 \\gf\\d2\\u\n";

	time_file << "read ser 1 2 3\n";
	time_file << "cpd /XW\n";
	time_file << "plot vertical\n";
	time_file << "lab x time [MJD]\n";
	time_file << "lab y2 \\gDt\n";
	time_file << "lab y3 \\gn\\gf\n";


	
	for(int j=0;j<n_pulses;j++)
	{
		double *pulse = pulses+j*n_phases_mat;
		double *pulse_err = err_pulses+j*n_phases_mat;
		
		for(int i=0;i<n_phases_mat;i++)
		{
			float phase = 2.* M_PI * ( ((float)i + 0.5)/((float)n_phases_mat) ) ;
			alpha_0+=pulse[i];
			I_c += cos(phase) * pulse[i];
			I_s += sin(phase) * pulse[i];
		
			double tmp = pulse_err[i] * pulse_err[i];
			alpha_0_err+=tmp;
			I_c_err += cos(phase)*cos(phase) * tmp;
			I_s_err += sin(phase)*sin(phase) * tmp;

			I_c2 += cos(2*phase) * pulse[i];
			I_s2 += sin(2*phase) * pulse[i];
		
			I_c2_err += cos(2*phase)*cos(2*phase) * tmp;
			I_s2_err += sin(2*phase)*sin(2*phase) * tmp;
		}
	
		alpha_0/=2.0*M_PI*n_phases_mat;
		alpha_0_err = sqrt(alpha_0_err)/2./M_PI/n_phases_mat;
	
		I_c /= M_PI;
		I_s /= M_PI;
	
		I_c_err /= M_PI * M_PI;
		I_s_err /= M_PI * M_PI;

		
		I_c2 /= M_PI;
		I_s2 /= M_PI;
	
		I_c2_err /= M_PI * M_PI;
		I_s2_err /= M_PI * M_PI;
	
		if(I_c!=0)
		{
			phi0 = atan2(I_s, I_c);
			double x=I_s/I_c;
			double dtgx = 1/(1+x*x);
			phi0_err = dtgx /fabs(I_c) * sqrt( I_s_err + x*x * I_c_err);

			//secon harmonics
			phi0_2 = atan2(I_s2, I_c2);
			x=I_s2/I_c2;
			dtgx = 1/(1+x*x);
			phi0_2_err = dtgx /fabs(I_c2) * sqrt( I_s2_err + x*x * I_c2_err);
			
			A = sqrt(I_c*I_c+I_s*I_s+I_c2*I_c2+I_s2*I_s2);
			A_err =  sqrt(I_s *I_s* I_s_err + I_c * I_c * I_c_err + I_s2 *I_s2* I_s2_err + I_c2 * I_c2 * I_c2_err) / fabs(A) ;
			A/=n_phases_mat;
			A_err/=n_phases_mat;
			I_s_err = sqrt(I_s_err);
			I_c_err = sqrt(I_c_err);
			phi0 /= 2*M_PI;
		//phi0 += 0.5;
			phi0_err /= 2*M_PI;
			phi0_2 /= 2*M_PI;
		//phi0 += 0.5;
			phi0_2_err /= 2*M_PI;
			I_s2_err = sqrt(I_s2_err);
			I_c2_err = sqrt(I_c2_err);
			
		}

		//Cleaning 
// 		if(phi0_err > 0.2 || alpha_0/alpha_0_err < 3  )
// 			continue;
		
		double dt = t_start[j] - t_ref;
		double phase = (dt*(f + dt*( (df/2.) + dt* (df2/6.)  ) ) );

		phase = phi0 + floor(phase);
		double new_time = get_dt(phase, f, df, df2);
		double new_time_error = get_dt(phase+phi0_err, f, df, df2);
		new_time_error -= new_time;
		
		norma.setf(ios_base::scientific,ios_base::floatfield);
		double time = (t_start[j]+t_stop[j])/2.;
// 		if(f && t_ref)
// 		{
// 			double iph=0;
// 			get_phase_complete(time, &iph);
// 			time = get_dt(iph+phi0, f, df, df2);
// 		}
		norma << setw(19)<< setprecision(12)
				<< time << "\t" << (-t_start[j]+t_stop[j])/2. << "\t"
				<< setw(11)<< setprecision(4)
				<< alpha_0 << "\t" << alpha_0_err << "\t"
				<< A << "\t" << A_err << "\t"
				<< phi0 << "\t" << phi0_err << "\t"
				<< phi0_2 << "\t" << phi0_2_err << "\n";
		dt=new_time;
		phase = (dt*(f + dt*( (df/2.) + dt* (df2/6.)  ) ) );
// 		if(phase -floor(phase) > 0.82)
// 			phase = floor(phase)+1;
// 		else
		phase = floor(phase)+0.5;
		
		time_file<< setw(19)<< setprecision(12)
				<< (new_time+t_ref)/86400.+51544. << "\t" << new_time_error/86400. << "\t"
 				<< (new_time-get_dt(phase, f, df, df2))/86400. << "\t" << new_time_error/86400. << "\t"
				<< phi0/f/86400. << "\t" << phi0_err/f/86400.
				<< "\n";
		
	// 	cout << time << "\t" << delta_t << "\t" << alpha_0 << "\t" 
	// 		<< alpha_0_err << "\t" << I_c << "\t" << I_c_err << "\t" << I_s << "\t" << I_s_err << "\n";
		
		
	}
	norma.close();
	time_file.close();
	
	return CODE_SUCCESS;
}


int phase::total_pulse()
{

	int i;
	const int n = n_phases_mat;
	
	string stmp = "pulse_"+src_name+".qdp";
	ofstream norma(stmp.c_str());
	if(t_pulse)
		norma << "read serr 1 2 3\n";
	else
		norma << "read serr 1 2\n";
	norma << "cpd /xw\n";
	norma << "r y 0\n";

	double *tot_pulse = new double[n];
	double *err_tot_pulse = new double[n];
	double time=0;
	for(int j=0;j<n_pulses;j++)
	{
		time += (exposures[j]);
	}
	//time/=exposure;
	for (i = 0; i < n; i++)
	{
		double tmp=0;
		double tmp_err=0;
		for(int j=0;j<n_pulses;j++)
		{
			tmp += pulses[j*n_phases_mat+i] * (exposures[j]);
			tmp_err += err_pulses[j*n_phases_mat+i]*err_pulses[j*n_phases_mat+i]* (exposures[j])* (exposures[j]);
		}
		
		tot_pulse[i]=tmp/time;//*(n+1);
		err_tot_pulse[i]=sqrt(tmp_err)/time;//*(n+1);
		if(t_pulse)
			norma << ((float)i + 0.5)/n << "\t" << 0.5/n << "\t" << tot_pulse[i] << "\t" << err_tot_pulse[i] << "\t" << t_pulse[i] << "\t" << t_err_pulse[i] << endl;
		else
			norma << ((float)i + 0.5)/n << "\t" << 0.5/n << "\t" << tot_pulse[i] << "\t" << err_tot_pulse[i] << endl;
	}
	norma.close();
	
	delete [] tot_pulse;
	delete [] err_tot_pulse;
	
	return CODE_SUCCESS;
}

int phase::find_maxima()
{

	int i;
	const int n = n_phases_mat;
	const int n_max2 = n_max/2;
	if(n_max>=n/3)
	{
		
		cerr << "You chose a wrong number of bins for the smoothing : " << n_max << " , reset to " << n/3<< endl;
		n_max=n/3;
	}
	
	cout << "For the maximum search we will use " << n_max2*2+1 << " points\n";
	
	string stmp = "shifts_"+src_name+".qdp";
	ofstream norma(stmp.c_str());
	norma << "read ser 1 2 3 4\n";
	norma << "cpd /XW\n";

	double *pha = new double[n];
// 	for (i = 0; i < n; i++)
// 		pha[i] = ((double)i+0.5)/n;
	
	for(int j=0;j<n_pulses;j++)
	{
		for (i = 0; i < n; i++)
		{
			//corrs[j*n_phases_mat+i] = 0;

			//printf ("%03d % 8.3e % 8.3e % 8.3e\n", i,  corrs[j*n_phases_mat+i], tmp, corrs[j*n_phases_mat+i] -tmp);
			pha[i]=0;
			for(int k=-n_max2;k<=n_max2;k++)
			{
				int ind=i+k;
				if(ind>=n)
					ind-=n;
				else if(ind<0)
					ind+=n;
				pha[i]+= corrs[j*n_phases_mat+ind];
			}
		}
						
		//double phi0     = gsl_stats_max(corrs+j*n_phases_mat, 1, n);//gsl_stats_wmean(corrs+j*n_phases_mat, 1, pha, 1,n);
		double phi0     = gsl_stats_max_index(pha, 1, n);
// 		phi0+=0.5;
		phi0/=n;
		if(phi0>0.5)
			phi0-=1.0;
		double phi0_err = 0.5 * (float)n_max / (float)n; //gsl_stats_wvariance_m(pulses+j*n_phases_mat, 1, pha, 1,n, phi0);
		double time = (t_start[j]+t_stop[j])/2.;
		if(f && t_ref)
		{
			double iph=0;
			get_phase_complete(time, &iph);
			time = get_dt(iph+phi0, f, df, df2);
		}
		norma << setw(19)<< setprecision(12)
				<< time << "\t" << (-t_start[j]+t_stop[j])/2. << "\t"
				<< phi0 << "\t" << phi0_err << "\n";

	}
	
	delete [] pha;
	norma.close();
	return CODE_SUCCESS;
}


double phase::get_phase_complete(double time, double *i_phase)
{
	double dt = time - t_ref;
    //double tmp2=dt*dt;
    //double phase = dt*(f + dt*( (df/2.)  + dt*((df2/6.) + dt*(df3/24.) ) ) ) + 1 - d_ph;
	double phase = dt*(f + dt*( (df/2.) + dt* (df2/6.)  ) );
	return fabs(modf(phase, i_phase));
}


double get_dt(double phi, double f, double df, double df2)
{

	double dt0 = phi/f, dt=0;
        //double dt00 = dt0;
	double params[] = {f, df, df2, phi};

	const gsl_root_fdfsolver_type *T = gsl_root_fdfsolver_newton;
	gsl_root_fdfsolver * s = gsl_root_fdfsolver_alloc(T);


	gsl_function_fdf FF;
	FF.f = &get_phi;
	FF.df = &get_dphi;
	FF.fdf = &get_fdphi;
	FF.params=params;


	gsl_root_fdfsolver_set(s, &FF, dt0);
	int iter=0, status=GSL_CONTINUE;
	int max_iter=1000;
	do{
		iter++;
		status = gsl_root_fdfsolver_iterate(s);
		dt0=dt;
		dt=gsl_root_fdfsolver_root(s);
                //x_lo=gsl_root_fdfsolver_x_lower(s);
                //x_hi=gsl_root_fdfsolver_x_upper(s);
                //status = gsl_root_test_interval(x_lo, x_hi, 0 , 0.001);
		status = gsl_root_test_delta(dt, dt0, 1e-2, 1e-4);
                //printf("%d %e %e\n", iter, dt0, dt);
	} while(status == GSL_CONTINUE && iter < max_iter);


	gsl_root_fdfsolver_free(s);

        //printf("%e\t%e\t%e\t%d\n", dt00, dt, dt00-dt, iter);
	return dt;
}

int phase::rebin_pulses()
{
	
	cout << "Rebinning pulses using a S/N threshold of " << sn_thresh << endl;
	
	int new_n_pulses=0;
	double *new_pulses= new double[n_pulses*n_phases_mat];
	double *new_err_pulses= new double[n_pulses*n_phases_mat];

	double *new_t_start =new double[n_pulses];
	double *new_t_stop = new double[n_pulses];
	double *new_exposures = new double[n_pulses];
	
	for(int i=0;i<n_pulses*n_phases_mat;i++)
	{
		new_pulses[i]=0;
		new_err_pulses[i]=0;
	}

	double t_thresh=0;
	for(int j=0;j<n_pulses;j++)
		t_thresh+=t_stop[j]-t_start[j];

	t_thresh *= 3.0/n_pulses;

	cout << "Maximum separation between the scws is [3*mean(t_elapsed)] " << t_thresh << " s\n";
	
	//assumes all dcw have about the same deadtime
	
	double err=0,sum=0,new_t_elapse=0;
	int n_summed=0;
	for (int j=0;j<n_pulses;j++)
	{
		double t_elapse = -t_start[j] + t_stop[j];
		new_t_elapse += t_elapse;
		n_summed++;
		if(sum==0)
			new_t_start[new_n_pulses]=t_start[j];
		for(int i=0;i<n_phases_mat;i++)
		{
			sum+=pulses[j*n_phases_mat+i] * t_elapse;
			err+=err_pulses[j*n_phases_mat+i]*err_pulses[j*n_phases_mat+i]* t_elapse* t_elapse;
			new_pulses[new_n_pulses*n_phases_mat+i] += pulses[j*n_phases_mat+i]* t_elapse;
			new_err_pulses[new_n_pulses*n_phases_mat+i] += err_pulses[j*n_phases_mat+i]*err_pulses[j*n_phases_mat+i]* t_elapse* t_elapse;
		}
		sum/=sqrt(err);
//  		cout << "index " << j << " S/N = " << sum << endl;
		int ind=j+1;
		if(ind==n_pulses)
			ind=n_pulses-1;
		
		if(sum >= sn_thresh || (t_start[ind] - t_stop[j]) > t_thresh)
		{
			for(int i=0;i<n_phases_mat;i++)
			{
				new_err_pulses[new_n_pulses*n_phases_mat+i] = sqrt(new_err_pulses[new_n_pulses*n_phases_mat+i])/new_t_elapse;
				new_pulses[new_n_pulses*n_phases_mat+i] = new_pulses[new_n_pulses*n_phases_mat+i]/new_t_elapse;
			}
			new_t_stop[new_n_pulses] = t_stop[j];
			new_exposures[new_n_pulses] = new_t_elapse;
			
			//cout << "stop accumulation at index " << j << " for t_start = " << t_start[j] << " s\n";
			if(n_summed>1)
			{
				cout << "************* stop accumulation ******************************\n";
				cout << "new t_start - t_stop = " << new_t_start[new_n_pulses]/86400. << " - " << new_t_stop[new_n_pulses]/86400. << " [MJD-51544]\n";
				cout << "S/N = " << sum << " dT = " << t_start[ind] - t_stop[j] << endl;
				cout << "new t_elapse = " << new_t_elapse << " s\n";
				cout << "Summed " << n_summed << " pulses\n";
				cout << j << " - " << new_n_pulses <<endl;
				cout << "**************************************************************\n";
			}
			new_n_pulses++;
			err=0,sum=0;
			new_t_elapse=0;
			n_summed=0;
			continue;
		}
		else
			sum*=sqrt(err);
	}

	cout << "Before rebinning there were " << n_pulses << " pulses\n";
	cout << "After rebinning there are " << new_n_pulses << " pulses\n";
	for(int i=0;i<new_n_pulses*n_phases_mat;i++)
	{
		pulses[i]=new_pulses[i];
		err_pulses[i]=new_err_pulses[i];
	}
	
	for(int i=0;i<new_n_pulses;i++)
	{
		t_start[i]=new_t_start[i];
		t_stop[i]=new_t_stop[i];
		exposures[i] = new_exposures[i];
	}

	n_pulses=new_n_pulses;

	delete [] new_pulses;
	delete [] new_err_pulses;
	delete [] new_t_start;
	delete [] new_t_stop;
	delete [] new_exposures;
	
	return CODE_SUCCESS;
}

/* ************************************************************************/
/*                                                                        */
/* ************************************************************************/

double get_phi(double dt, void *params)
{
    double f   = ((double *)params)[0];
    double df  = ((double *)params)[1];
    double df2 = ((double *)params)[2];
    double phi = ((double *)params)[3];
//      printf("%e\t%e\t%e\n", f,df,df2);
    return dt*(f + dt*(df/2. + dt*df2/6.)) - phi;
}


double get_dphi(double dt, void *params)
{
    double f   = ((double *)params)[0];
    double df  = ((double *)params)[1];
    double df2 = ((double *)params)[2];
    return (f + dt*(df + dt*df2/2.));
}

void get_fdphi(double dt, void *params, double *y, double *dy)
{        
    double f   = ((double *)params)[0];
    double df  = ((double *)params)[1];
    double df2 = ((double *)params)[2];
    double phi = ((double *)params)[3];
    *y  = dt * (f + dt*(df/2. + dt*df2/6.))-phi;
    *dy = (f + dt*(df + dt*df2/2.));
    return;
}
 
double get_bin(double t, void *params)
{
	double *pp = (double *)params;
	return binarycor(t) - *pp;
}

double get_solar(double t_bin)
{

	double t0 = t_bin, r=t_bin;

	const gsl_root_fsolver_type *T = gsl_root_fsolver_brent;
	gsl_root_fsolver * s = gsl_root_fsolver_alloc(T);

	gsl_function F;
	F.function = &get_bin;
	F.params=(void*)(&t_bin);

	
	double del_t = binarycor(t0) - t0;
	//double del_phi = dt0/1e4;

	//printf("%f\t%f\t%f\n", t_bin, binarycor(t0), del_t);

	double fdel_t = 10.*fabs(del_t);
	//double sign = del_phi/fdel_phi;
	//t0-=del_t;
	double x_lo = t0 - fdel_t;
	double x_hi = t0 + fdel_t;
	
	//printf("%f\t%f\t%f\n", get_bin(x_lo, (void*)(&t_bin)), get_bin(x_hi, (void*)(&t_bin)), get_bin(t0, (void*)(&t_bin)));

	gsl_root_fsolver_set(s, &F, x_lo, x_hi);

	int iter=0, status=GSL_CONTINUE;
	int max_iter=100000;

	do{
		iter++;
		status = gsl_root_fsolver_iterate(s);
		r=gsl_root_fsolver_root(s);
		x_lo=gsl_root_fsolver_x_lower(s);
		x_hi=gsl_root_fsolver_x_upper(s);
		status = gsl_root_test_interval(x_lo, x_hi, 0.00001 , 1e-8);
	} while(status == GSL_CONTINUE && iter < max_iter);

// 	printf("%f\t%f\t%f\n", r, dt0, dt0-r);
	gsl_root_fsolver_free(s);

	return r;
}




