/*
 *  event_files.cpp
 *  event_file_Analyzer
 *
 *  Created by Carlo Ferrigno on 10/11/09.
 *  Copyright 2009 ISDC/IAAT. All rights reserved.
 *
 */

//#define __CINT__ 1
/*
    Hi Carlo,

I think I found part of the explanation. The culprit is the file nr3.h. 

Around line 58, it redefines the "throw" keyword. Commenting out these
redefinitions makes the compilation happy.

"throw" is fundamental keyword of the C++ language and I would absolutely avoid
to redefine this on a regular day. If one want to keep this for backward
compatibility (because maybe that keyword was not so deeply hardwired in the C++
language for some old version of the compiler), I would protect this with
special macro guard related to the gcc version. Please have a look at:

http://gcc.gnu.org/onlinedocs/cpp/Common-Predefined-Macros.html#Common-
Predefined-Macros

Cheers,

    Hubert

#include <dal3aux.h>
must be before 
#include "nr3.h"

to compile then 

*/

//#include <dal3aux.h>
#include "fitsio.h"
#include "ask.h"
#include <vector>
#include <iostream>
#include <iomanip>
#include "nr3.h"
#include "binarycor.h"
#include "event_files.h"
#include <cmath>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>
#include "phase.h"
#include <ctime>

#include "tools.h"

using namespace std;

event_file::event_file()
{
	init_values();
	init_keywords();
}

event_file::event_file(const char *fname)
{
	init_values();
	init_keywords();
	if(open_fits(fname))
	{
		cerr << "Fatal error\n";
		exit(1);
	}
}



event_file::~event_file()
{
	
	deallocate_gti();
	deallocate_events();
        init_keywords();
	init_values();
        _selection_indexes.clear();
	
}

void event_file::deallocate_events()
{
	if(_energy!=0)
	{
		delete [] _energy;
		_energy=0;
	}
	if(_times!=0)
	{
		delete [] _times;
		_times=0;
	}
	_size=0;
}

void event_file::allocate_events(int n)
{
	deallocate_events();
	_size=n;

	_times = new double[n];
	_energy = new double[n];
	
}


void event_file::allocate_gti(int n)
{
	deallocate_gti();
	n_gti=n;
	GTI_STOP=new double[n];
	GTI_START=new double[n];
}


void event_file::deallocate_gti()
{
	if(GTI_STOP!=0)
	{
		delete [] GTI_STOP;
		GTI_STOP=0;
	}
	if(GTI_START!=0)
	{
		delete [] GTI_START;
		GTI_START=0;
	}
	n_gti=0;
}


void event_file::init_keywords()
{
	sprintf(timeref, "none");
	sprintf(timeunit, "none");
	sprintf(telescop, "none");
	sprintf(instrume, "none");
	sprintf(object, "none");
	sprintf(scwname, "none");
//	sprintf(og_name, "none");
	
	
}

void event_file::init_values()
{
	_times=0;
	_is_open=0;
	_mjd_ref=0;
	_t_start=0;
	_t_stop=0;
	_ontime=0;
	_time_del=0;
	_size=0;
	GTI_START=0;
	GTI_STOP=0;
	_times=0;
	_energy=0;
        binary_corr_flag=0;
//	_perform_integral_barycentric='n';
//	_ra_obj=0;
//	_dec_obj=0;
}


int event_file::open_fits(const char *fname)
{
	sprintf(filename, "%s", fname);
	int status=0;
	int hdunum, hdutype;
	char keyword[FLEN_KEYWORD];
	char keyvalu[FLEN_VALUE];
	char keycomm[FLEN_COMMENT];
	char colname[FLEN_KEYWORD];
	
	long nrows = 0;
	
	if (fits_open_file(&fptr, fname, READONLY, &status))
	{
        fprintf(stderr, "Could not read %s\n",  fname);
        return status;
	}
	
	if ( fits_get_hdu_num(fptr, &hdunum) == 1 )
            fits_movabs_hdu(fptr, 2, &hdutype, &status);
	else
            fits_get_hdu_type(fptr, &hdutype, &status); /* Get the HDU type */
	
	if (hdutype == IMAGE_HDU)
	{
        fprintf(stderr, "Error: this program only displays tables, not images\n");
        return 1;
	}                                                                       
                                                          
	sprintf(keyword, "TELESCOP");
	if(fits_read_key( fptr,  TSTRING, keyword, telescop, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
        	
	if (strcmp(telescop, "INTEGRAL")==0) //VALID for ISGRI
	{
		cout << "Found telescop " << telescop << " moving to extension 4\n";
		if(fits_movabs_hdu(fptr, 3, &hdutype, &status))
		{
			cerr <<"INTEGRAL, cannot move to extension 4\n";
			status=0;
			if(fits_movabs_hdu(fptr, 2, &hdutype, &status))
			{
				cerr << "Error in INTEGRAL xtensions\n";
				exit(1);
			}
                        cerr <<"INTEGRAL, moved to extension 2\n";
		}


                sprintf(keyword, "EXTNAME");
                if(fits_read_key( fptr,  TSTRING, keyword, keyvalu, keycomm, &status))
                {
                    fprintf(stderr, "Could not find keyword '%s'\n", keyword);
                    status=0;
                }

                cout << "Current extension is '" <<keyvalu << "'\n";
                if(strcmp(keyvalu, "GNRL-EVTS-GTI")==0)
                {
                    cout << "Move back to events extension\n";
                    if(fits_movrel_hdu(fptr, -1, &hdutype, &status))
                    {
			cerr <<"INTEGRAL, cannot move back from extension\n";
                        exit(1);
                    }
                }

		
	}
	
	sprintf(keyword, "EXTNAME");
	if(fits_read_key( fptr,  TSTRING, keyword, keyvalu, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	cout << "Current extension is '" <<keyvalu << "'\n";
	
	if(fits_get_num_rows(fptr, &nrows, &status))
	{
		fits_report_error(stderr, status);
		fits_close_file(fptr,&status);
		return status;
	}
	
	_size=nrows;
	
	cout << "There are " << _size << " events in the file\n";
	
	char read_dx=0; 
	//check if need to read TIMEDEL, 0 no read dx, 1 read TIMEDEL, 2 read XAX_E (ignoring TIMEDEL keyword
	int dtime_ind=0;
	sprintf(colname, "XAX_E");
	if( fits_get_colnum(fptr, 0, colname, &dtime_ind, &status) == 0)
	{
		cerr << "Found col '" << colname <<"'ignore TIMEDEL\n";
		status=0;
		read_dx=2;
	}
	else
	{

            sprintf(colname, "TIMEDEL");
            if( fits_get_colnum(fptr, 0, colname, &dtime_ind, &status) == 0)
            {
		cerr << "Found col '" << colname <<"'ignore TIMEDEL\n";
		status=0;
		read_dx=2;
            }
            else
            {
		status=0;	
		sprintf(keyword, "TIMEDEL");
		if(fits_read_key( fptr,  TDOUBLE, keyword, &_time_del, keycomm, &status))
		{
			fprintf(stderr, "Could not find keyword '%s'\n", keyword);
			status=0;
			read_dx=1;
		}
            }
	}//end if on XAX_E
	
	sprintf(keyword, "MJDREF");
	if(fits_read_key( fptr,  TDOUBLE, keyword, &_mjd_ref, keycomm, &status))
	{
		fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
		status=0;
		cerr<<"Attempt to use MJDREFI and MJDREFF\n";
		sprintf(keyword, "MJDREFI");
		if(fits_read_key( fptr,  TDOUBLE, keyword, &_mjd_ref, keycomm, &status))
		{
			fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
			status=0;
		}
		else
		{
			double tmp=0;
			sprintf(keyword, "MJDREFF");
			if(fits_read_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status))
			{
				fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
				status=0;
			}
			_mjd_ref+=tmp;
		}
	}
	
	
	
	sprintf(keyword, "ONTIME");
	if(fits_read_key( fptr,  TDOUBLE, keyword, &_ontime, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	sprintf(keyword, "TIMEUNIT");
	if(fits_read_key( fptr,  TSTRING, keyword, timeunit, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	sprintf(keyword, "TIMEREF");
	if(fits_read_key( fptr,  TSTRING, keyword, timeref, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
        
	sprintf(keyword, "TIMESYS");
	if(fits_read_key( fptr,  TSTRING, keyword, timesys, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
        
        
	sprintf(keyword, "OBJECT");
	if(fits_read_key( fptr,  TSTRING, keyword, object, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	
	sprintf(keyword, "TELESCOP");
	if(fits_read_key( fptr,  TSTRING, keyword, telescop, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	sprintf(keyword, "INSTRUME");
	if(fits_read_key( fptr,  TSTRING, keyword, instrume, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	sprintf(keyword, "ORIGIN");
	if(fits_read_key( fptr,  TSTRING, keyword, origin, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
                sprintf(origin, "None");
		status=0;
	}
	
	
	sprintf(keyword, "SCWNAME");
	if(fits_read_key( fptr,  TSTRING, keyword, scwname, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s' try", keyword);
		sprintf(keyword, "SWID");
		fprintf(stderr, "'%s'\n", keyword);
		status=0;
		if(fits_read_key( fptr,  TSTRING, keyword, scwname, keycomm, &status))
		{
			fprintf(stderr, "Could not find keyword '%s'\n", keyword);
			sprintf(scwname, "none");
			status=0;
		}
	}
	
	sprintf(keyword, "TSTART");
	if(fits_read_key( fptr,  TDOUBLE, keyword, &_t_start, keycomm, &status))
	{
		fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
		status=0;
		sprintf(keyword, "TFIRST");
		fprintf(stderr, "Use keyword '%s'\n", keyword);

		if(fits_read_key( fptr,  TDOUBLE, keyword, &_t_start, keycomm, &status))
		{
			fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
			status=0;

			cerr<<"Attempt to use integer and float parts\n";
			sprintf(keyword, "TSTARTI");
			if(fits_read_key( fptr,  TDOUBLE, keyword, &_t_start, keycomm, &status))
			{
					fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
					status=0;
			}
			else
			{
					double tmp=0;
					sprintf(keyword, "TSTARTF");
					if(fits_read_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status))
					{
							fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
							status=0;
					}
					_t_start+=tmp;
			}
		}
	}
	
	sprintf(keyword, "TSTOP");
	if(fits_read_key( fptr,  TDOUBLE, keyword, &_t_stop, keycomm, &status))
	{
		fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
		status=0;
		sprintf(keyword, "TLAST");
		fprintf(stderr, "Use keyword '%s'\n", keyword);
		if(fits_read_key( fptr,  TDOUBLE, keyword, &_t_stop, keycomm, &status))
		{
			fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
			status=0;

			cerr<<"Attempt to use integer and float parts\n";
			sprintf(keyword, "TSTOPI");
			if(fits_read_key( fptr,  TDOUBLE, keyword, &_t_stop, keycomm, &status))
			{
				fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
				status=0;
			}
			else
			{
				double tmp=0;
				sprintf(keyword, "TSTOPF");
				if(fits_read_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status))
				{
						fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
						status=0;
				}
				_t_stop+=tmp;
			}
		}
	}
	
	
	//Find the column indexes
    cout << "Status is " << status << endl;
	sprintf(colname, "BARYTIME_8");

	//TODO : This logic does not WORK	
	if(strcmp(origin, "ISDC")==0 && strcmp(timeref, "LOCAL") ==0)
		sprintf(colname, "TIME"); //OSA fills barytime with zeros, why ?!
	else if(strcmp(origin, "ISDC")==0)
        sprintf(colname, "BARYTIME_1");
    else
		sprintf(colname, "BARYTIME");
	
	if(fits_get_colnum(fptr, 0, colname, &time_ind, &status))
	{
		cerr << "Warning : cannot find col '" << colname <<"', try col 'TIME'\n";
		status=0;
		sprintf(colname, "TIME");
		if(fits_get_colnum(fptr, 0, colname, &time_ind, &status))
		{
			cerr << "Error : cannot find col '" << colname <<"', abort\n";
			fits_close_file(fptr, &status);
			return status;
		}
	}
	else
	{
		sprintf(timeref,"SOLARSYSTEM");
	}
	cout << "Reading time column " << colname << "\n";

	sprintf(colname, "PHA");
	
	if( strcmp(instrume, "HXI1") ==0 ||strcmp(instrume, "HXI2") ==0 ||strcmp(instrume, "MECS") ==0 || strcmp(instrume, "XRT") ==0 ||strcmp(instrume, "EPN") ==0 
           || strcmp(instrume, "EMOS1") ==0 || strcmp(instrume, "EMOS2") ==0
           || strcmp(instrume, "XIS0") ==0 || strcmp(instrume, "XIS1") ==0
           || strcmp(instrume, "XIS2") ==0 || strcmp(instrume, "XIS3") ==0 ||  strcmp(instrume, "FPMA") ==0 ||  strcmp(instrume, "FPMB") ==0 )
                sprintf(colname, "PI") ;
	else if(strcmp(instrume, "IBIS") ==0)
		sprintf(colname, "ISGRI_ENERGY");
	else if(strcmp(instrume, "JMX1") ==0 || strcmp(instrume, "JMX2") ==0)
		sprintf(colname, "ENERGY");

	if(strcmp(instrume, "PCA") && strcmp(instrume, "HEXTE"))
	{
		cout << "Reading energy column " << colname << "\n";
		//waitkey();
		if(fits_get_colnum(fptr, 0, colname, &energy_ind, &status))
		{
			cerr << "WARNING : cannot find col '" << colname <<"' try '";
					sprintf(colname, "ENERGY");
					cout << colname << endl;
					status=0;
				if(fits_get_colnum(fptr, 0, colname, &energy_ind, &status))
					{

						cerr << "ERROR : cannot find col '" << colname <<"'\n";
					
						fits_close_file(fptr, &status);
						return status;
					}
		
		}
		cout << "End Reading energy column, index is " << energy_ind << "\n";
		//waitkey();
	}
	else
	{
		cout << "WARNING :: PCA has no Energy information\n";
		energy_ind=-1;
	}
	
	status=get_gti(fptr, status);
	
	_is_open=1;
	
	return status;
}	

int event_file::close_fits()
{
	int status=0;
	fits_close_file(fptr, &status);
	_is_open=0;
	return status;
	
}


int event_file::get_gti(fitsfile *fptr, int status)
{
	int hdutype=0;
	char keyword[FLEN_KEYWORD];
	char extname[FLEN_VALUE];
        char colname[FLEN_KEYWORD];
	sprintf(keyword,"EXTNAME");
	int num_hdus=1;
	int hdu_num=1;
	
	if(fits_get_num_hdus(fptr, &num_hdus, &status))
		fits_report_error(stderr, status );
	
	if(num_hdus < 2)
	{
		cerr << "Event files does not contain GTI or EVENTS, skipping\n";
		return 1;
	}
	
	fits_get_hdu_num(fptr, &hdu_num);
		
	fits_movrel_hdu(fptr, 1, &hdutype, &status);
	if(status)
	{
		fits_report_error(stderr, status);
		cerr << "Error: cannot move to GTI extension place\n";
		status=0;
		
		GTI_START=new double[1];
		GTI_STOP=new double[1];
		
		GTI_START[0]=_t_start;
		GTI_STOP[0]=_t_stop;
		
		
		return status;
	}
	
	fits_read_key(fptr, TSTRING, keyword, extname, NULL, &status);
	
	if(strcmp(extname, "GTI")!=0)
	{
		cerr << "WARNING :: Extension for GTI is " << extname << endl;
	}
	
	
	if (hdutype == IMAGE_HDU)
	{
		cerr<< "Error: GTI is an image\n";
		return 1;
	}
	
	long nrows;
	fits_get_num_rows(fptr, &nrows, &status);
	
	//printf("num_rows = %ld\n", nrows);
	
	GTI_START=new double[nrows];
	GTI_STOP=new double[nrows];
	
	n_gti=nrows;
	
	int anynul=0;
	double null_value = -1;
	
        int start_ind=1;
        int stop_ind=2;
        cout << "Reading GTI start column\n";
        sprintf(colname,"START");
	//waitkey();
	if(fits_get_colnum(fptr, 0, colname, &start_ind, &status))
	{
		cerr << "WARNING : cannot find col '" << colname <<"' try '";
                sprintf(colname, "T_START");
                cout << colname << endl;
                status=0;
         	if(fits_get_colnum(fptr, 0, colname, &start_ind, &status))
                {

                    cerr << "ERROR : cannot find col '" << colname <<"'\n";
				
                    fits_close_file(fptr, &status);
                    return status;
                }
	
	}
	cout << "End Reading GTI start column, index is " << start_ind << "\n";
	
	cout << "Reading GTI stop column\n";
	sprintf(colname,"STOP");
	//waitkey();
	if(fits_get_colnum(fptr, 0, colname, &stop_ind, &status))
	{
		cerr << "WARNING : cannot find col '" << colname <<"' try '";
		sprintf(colname, "T_STOP");
		cout << colname << endl;
		status=0;
		if(fits_get_colnum(fptr, 0, colname, &stop_ind, &status))
		{

			cerr << "ERROR : cannot find col '" << colname <<"'\n";
		
			fits_close_file(fptr, &status);
			return status;
		}
	
	}
	cout << "End Reading GTI stop column, index is " << stop_ind << "\n";
	if (fits_read_col(fptr, TDOUBLE, start_ind, 1, 1, nrows, &null_value, GTI_START, &anynul, &status) )
		fits_report_error(stderr, status);
	
	if (fits_read_col(fptr, TDOUBLE, stop_ind, 1, 1, nrows, &null_value, GTI_STOP, &anynul, &status) )
		fits_report_error(stderr, status);
	
	
	fits_movabs_hdu(fptr, hdu_num, &hdutype, &status);
	if(strcmp(timeunit, "d")==0)
	{
		_t_start *=86400.0;
		_t_stop *=86400.0;
		_time_del*=86400.0;
				
		for(int i=0;i<n_gti;i++)
		{
			GTI_START[i] *= 86400;
			GTI_STOP[i]  *= 86400;
		}
        
		//sprintf(timeunit, "s");
	}

	scan_gti();

	return status;
}


double event_file::output(double t1, double t2, double e_min, double e_max)
{
    cout << "Filename '" << filename << "'\n";
    cout << "Times are " << scientific << setw(19) << setprecision(12) << _t_start << "\t" << _t_stop << endl; 
    cout << "Time del is " << _time_del << " " << timeunit << endl;
    cout << "Ontime is " << _ontime << " " << timeunit << endl;
    cout << "MJDREF is " << _mjd_ref << endl;
    cout << "There are " << _size << " total events\n";
    cout << "Number of GTIs " << n_gti << endl;
    double n_events_read=0;
    
    if(_times != NULL && _energy !=NULL)
    {
        double l_e_min=e_min, l_e_max=e_max;
    
        get_local_energies(e_min, e_max, &l_e_min, &l_e_max);

        
        cout << "Local energies are " << l_e_min << " " << l_e_max <<endl;

        for(int i=0;i<_size;i++)
        {

                double tt=_times[i];
                double ee = _energy[i];
                //cout << tt << " " << ee << " "<< t1 << " " << t2 << " " << l_e_min << " " << l_e_max << endl;

                if(tt <t1 || tt > t2 || ee< l_e_min || ee>l_e_max)
                {

                    continue;
                }
                n_events_read++;

        }
        
        cout << "Read " << n_events_read << " with " << e_min << " < Energy < " << e_max <<  endl;
        cout << "Count rate is " << n_events_read /_ontime << " cts/s\n"; 
    }
    return n_events_read;
        
}

int event_file::get_local_energies(double e_min, double e_max, double *l_e_min_o, double *l_e_max_o)
{
    
    double l_e_min=e_min;
    double l_e_max=e_max;
    
    //cout << "Converting local energies for instrument '" << instrume << "'\n";
    if(strcmp(instrume, "XRT") == 0 )
    {
            l_e_min*=1e2;
            l_e_max*=1e2;
    }
    if( strcmp(instrume, "EPN") ==0 || strcmp(instrume, "EMOS1") ==0 || strcmp(instrume, "EMOS2") ==0  )
    {
            l_e_min*=1e3;
            l_e_max*=1e3;

            //cout << "EPN " << l_e_min << " " << l_e_max << endl;
    }

    if( strcmp(instrume, "XIS0") ==0 || strcmp(instrume, "XIS1") ==0 ||
	strcmp(instrume, "XIS2") ==0 || strcmp(instrume, "XIS3") ==0  )
    {
            l_e_min*=1e3/3.65;
            l_e_max*=1e3/3.65;
    }

    if(strcmp(instrume, "MECS") == 0 )
    {
        //cout << "Intrument is '" << instrume << "'" << endl;
            l_e_min*=255/11.84591;
            l_e_max*=255/11.84591;
    }

    if(strcmp(instrume, "HXI1") == 0 || strcmp(instrume, "HXI2") == 0 
            || strcmp(instrume, "HXI1HXI2") == 0|| strcmp(instrume, "HXI2HXI1") == 0)
    {
        //cout << "Intrument is '" << instrume << "'" << endl;
            l_e_min*=10;
            l_e_max*=10;
    }


    if(  strcmp(instrume, "FPMA") ==0 ||  strcmp(instrume, "FPMB") ==0 )
    {
         //cout << "Intrument is '" << instrume << "'" << endl;
         l_e_min=(l_e_min-1.6)/0.04;
         l_e_max=(l_e_max-1.6)/0.04;             
    }
    
    
    *l_e_min_o=l_e_min;
    *l_e_max_o=l_e_max;
    
    return 0;
}

int event_file::read_times()
{
	
	if(_is_open==0)
	{
		cerr <<"File is not open, cannot read times\n";
		return 1;
	}
	int status=0;

    //ALREADY determined in open_fits()
	allocate_events(_size);
	
	//Actual reading
	int firstrow  = 1;  // first row in table
	int firstelem = 1;  // first element in row  (ignored in ASCII tables) 
	
	int anynul=0;
	double nulldbl = 0;
	//numeric_limits<double>::quiet_NaN();
	
	double *tt=_times;
	double *ee=_energy;
	
	int hdutype=BINARY_TBL;
	
//	if(fits_movabs_hdu(fptr, 2, &hdutype, &status))
//	{
//		cerr << "Error reading HDU in read_evts\n";
//		return status;
//	}
	
	if(fits_read_col(fptr, TDOUBLE,    time_ind, firstrow, firstelem, _size,
					 &nulldbl, tt, &anynul, &status))
	{
		cerr << "ERROR :: error reading column at pos'"<<time_ind<<"'\n";
		fits_close_file(fptr, &status);
		return -status;
	}
        
        if(strcmp(timeunit, "d")==0)
	{
		for(int i=0;i<_size;i++)
		{
			tt[i]*=86400.0;
		}
		
	}
     
	for(int i=0;i<_size;i++)
	{

		tt[i]+=rand_uniform()*_time_del;
	}
	
	if(energy_ind >0)
	{
        //cout << "Reading energy column\n";
		if(fits_read_col(fptr, TDOUBLE,    energy_ind, firstrow, firstelem, _size, &nulldbl, ee, &anynul, &status))
		{
			cerr << "ERROR :: error reading column in '"<<energy_ind<<"'\n";
			fits_close_file(fptr, &status);
			return -status;
		}
				
	}	
	
	return status;
	
}

int event_file::get_times(vector<double> &times, int flag_binarycorr, double e_min, double e_max, int
    max_n_events, int i_start, char int_bary='n', double ra_obj=0, double dec_obj=0, char
    *og_name=NULL, double max_time_separation=1e100)
{
    
    //gets times in a vector, according to selection
    //If the times have not been read, 
	int status=0;
	if(_size < 10)
	{
		cerr << "Number of events is too low\n";
		return -1;
	}
	//Actual reading
	int firstrow  = i_start+1;  // first row in table
	int firstelem = 1;  // first element in row  (ignored in ASCII tables) 
	
	int anynul=0;
	double nulldbl = 0;
	//numeric_limits<double>::quiet_NaN();
	
        if (_times == 0)
            read_times();
        
	double *tt=new double[_size];
	double *ee=new double[_size];
	long nrows=_size;
	
	if(i_start >= nrows-10)
	{
		cerr << "get times: terminated input\n";
		return -1;
	}
	
	double l_e_min=e_min;
	double l_e_max=e_max;
	int n_to_read = MIN((int)(nrows-i_start), max_n_events);
	int i_stop=n_to_read; //this is to handle break in time step
	
	cout << "About to read " << n_to_read << " evts starting from " <<
	i_start << endl;
	if(n_to_read<=10)
	{
		n_to_read=nrows-i_start; //this is to handle max_n_events too small or zero
	}
	get_local_energies(e_min, e_max, &l_e_min, &l_e_max);

	if(fits_read_col(fptr, TDOUBLE,    time_ind, firstrow, firstelem, n_to_read,
					 &nulldbl, tt, &anynul, &status))
	{
		cerr << "ERROR :: error reading column '"<<time_ind<<"'\n";
		fits_close_file(fptr, &status);
		return -status;
	}
	if(int_bary=='y')
	{
		cout << "NOT performing barycentric correction (INTEGRAL 32 bit)\n";
//		isgri_barycentric_correction(tt, og_name, n_to_read, ra_obj, dec_obj);
	}	
        
        if(flag_binarycorr== 'y' && binary_corr_flag == 0)
        {
            cout << "Perform binary correction\n";
            for(int i=0;i<n_to_read;i++)
            {
                //cout << times[i] << "\t";
                //double ata=times[i];
                tt[i]=binarycor(tt[i]);
                //cout << times[i]-ata << "\n";
            }
        }
	

	if(strcmp(timeunit, "d")==0)
	{
		for(int i=0;i<n_to_read;i++)
		{
			tt[i]*=86400.0;
		}
                //_time_del*=86400.0; //done at keyword level
		//sprintf(timeunit, "s");
	}
			
	
	if(energy_ind >0)
	{
		if(fits_read_col(fptr, TDOUBLE,    energy_ind, firstrow, firstelem, n_to_read,
						 &nulldbl, ee, &anynul, &status))
		{
			cerr << "ERROR :: error reading column in '"<<energy_ind<<"'\n";
			fits_close_file(fptr, &status);
			return -status;
		}
		
		
		for(int i=0;i<n_to_read;i++)
		{
			if(ee[i] < l_e_max && ee[i]>= l_e_min)
			{
				//double nn=times.size();
				if (i>0)
				{
					double dt=tt[i]-tt[i-1];
					if(dt > max_time_separation)
					{
						cout << "Interrupted reading of events at #" << i << " with dt=" << dt <<" s\n";
						i_stop=i;
						break;
					}
				}
				times.push_back(tt[i]+rand_uniform()*_time_del);
			//cout << rand_uniform()*_time_del << endl;
				
			}
		}
	}
	else
	{
		for(int i=0;i<n_to_read;i++)
		{
			//double nn=times.size();
			if (i>0)
			{
				double dt=tt[i]-tt[i-1];
				if(dt > max_time_separation)
				{
					cout << "Interrupted reading of events at #" << i << " with dt=" << dt <<" s\n";
					i_stop=i;
					break;
				}
			}
			
			times.push_back(tt[i]+rand_uniform()*_time_del);
			//cout << rand_uniform()*_time_del << endl;
		}
		
	}
	
	delete [] tt;
	delete [] ee;
	
	return i_stop+firstrow-1;
	
}

double rand_uniform(void)
{
	static const double RandFact=(double)RAND_MAX+1;
	char log_name[FLEN_FILENAME];
	sprintf(log_name, "xrt_prepare_timing.txt");
	//init_log_file_auto(log_name);
	
	double r;
	
	/*--------------------------------------
	 * generate a random number uniformly
	 * distributed in range [0 1[
	 *--------------------------------------*/
	
	r=(rand()+(double)rand()/RandFact)/RandFact;   /* formula
	 to improve the random generator */
	
	return r;
}

int get_time_resolution(const char *fname, double *time_resolution, double
*t_start, double *t_stop, double *mjdref, char *bary_flag, double *ra_obj,
double *dec_obj, char *og_name)
{
	int status=0;
	fitsfile *fptr;
	
	if ( fits_open_file(&fptr, fname, READONLY, &status) ) /* open FITS file */
	{
		fits_report_error(stderr, status );           /* call printerror if error occurs */
		return status;
	}
	
	char keyword[FLEN_KEYWORD];                                             
                                                    
	char keyvalu[FLEN_VALUE];
	char telescop[FLEN_VALUE];	
	char timeunit[FLEN_VALUE];

	char keycomm[FLEN_COMMENT];                                             
                                                    
	char query[1024];
	int hdunum=1, hdutype=IMAGE_HDU;
	if ( fits_get_hdu_num(fptr, &hdunum) == 1 )
        fits_movabs_hdu(fptr, 2, &hdutype, &status);
	else
        fits_get_hdu_type(fptr, &hdutype, &status); /* Get the HDU type */
	
	if (hdutype == IMAGE_HDU)
	{
        fprintf(stderr, "Error: this program only displays tables, not images\n");
        return 1;
	} 
	
	
	sprintf(keyword, "TELESCOP");
	if(fits_read_key( fptr,  TSTRING, keyword, telescop, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	if (strcmp(telescop, "INTEGRAL")==0) //VALID for ISGRI
	{
		cout << "Found telescop " << telescop << " moving to extension 4\n";
		if(fits_movabs_hdu(fptr, 4, &hdutype, &status))
		{
			cerr <<"INTEGRAL, cannot move to extension 4\n";
			status=0;
			if(fits_movabs_hdu(fptr, 3, &hdutype, &status))
			{
				cerr << "Error in INTEGRAL xtensions\n";
				exit(1);
			}
            cerr << "Moved to extension 2\n";
		}
		

		sprintf(keyword, "EXTNAME");
		if(fits_read_key( fptr,  TSTRING, keyword, keyvalu, keycomm, &status))
		{
			fprintf(stderr, "Could not find keyword '%s'\n", keyword);
			status=0;
		}

		cout << "Current extension is '" <<keyvalu << "'\n";
		if(strcmp(keyvalu, "GNRL-EVTS-GTI")==0)
		{
			cout << "Move back to events extension\n";
			if(fits_movrel_hdu(fptr, -1, &hdutype, &status))
			{
				cerr <<"INTEGRAL, cannot move back from extension\n";
				exit(1);
			}
		}

		char query[256];
		sprintf(query, "Do you want to perform ISGRI barycentric correction?");
		*bary_flag=ask_yes_no(query, *bary_flag);
		if (*bary_flag=='y')
		{
			sprintf(query,"RA of the object in deg");
			ask_double(query, ra_obj);
			sprintf(query,"DEC of the object in deg");
			ask_double(query, dec_obj);
			sprintf(query,"OG_group_name");
			askInfo(query,og_name);
			
			cout << "Barycentric correction will be performed for a source at \n";
			cout << " Ra = " << *ra_obj << " DEC = " << *dec_obj << " for og_group " << og_name << endl;
			waitkey();
		}
	}
	
	
	
	sprintf(keyword, "EXTNAME");
	if(fits_read_key( fptr,  TSTRING, keyword, keyvalu, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	cout << "Current extension is '" <<keyvalu << "'\n";
	
	sprintf(keyword, "TIMEUNIT");
	if(fits_read_key( fptr,  TSTRING, keyword, timeunit, keycomm, &status))
	{
		fprintf(stderr, "Could not find keyword '%s'\n", keyword);
		status=0;
	}
	
	sprintf(keyword, "TIMEDEL");                                            
                                                    
	if(fits_read_key( fptr,  TDOUBLE, keyword, time_resolution, keycomm, &status))                                                     
	{                                                                       
                                                    
		fits_report_error(stderr, status);                              
                                                    
		status=0;                                                       
                                                    
		cerr << "Cannot retrieve time resolution\n";  
                if (strcmp(telescop, "INTEGRAL")==0) 
                {   
                    *time_resolution=7.06425419560185E-10*86400.0;
                    cout << "Found INTEGRAL, use time resolution of "<<
                    *time_resolution << " s\n";
                }
                else
                {

                    sprintf(query, "Enter the time resolution");
                    ask_double(query, time_resolution);
                }
	}  
	
	sprintf(keyword, "TSTART");
	if(fits_read_key( fptr,  TDOUBLE, keyword, t_start, keycomm, &status))
	{
		fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
		status=0;
		cerr<<"Attempt to use integer and float parts\n";
		sprintf(keyword, "TSTARTI");
		if(fits_read_key( fptr,  TDOUBLE, keyword, t_start, keycomm, &status))
		{
			fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
			status=0;
		}
		else
		{
			double tmp=0;
			sprintf(keyword, "TSTARTF");
			if(fits_read_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status))
			{
				fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
				status=0;
			}
			*t_start+=tmp;
			if(strcmp(timeunit, "d")==0)
			{
				(*t_start)*=86400.0;
				//sprintf(timeunit, "s");
			}
			
		}
	}
	sprintf(keyword, "TSTOP");
	if(fits_read_key( fptr,  TDOUBLE, keyword, t_stop, keycomm, &status))
	{
		fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
		status=0;
		cerr<<"Attempt to use integer and float parts\n";
		sprintf(keyword, "TSTOPI");
		if(fits_read_key( fptr,  TDOUBLE, keyword, t_stop, keycomm, &status))
		{
			fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
			status=0;
		}
		else
		{
			double tmp=0;
			sprintf(keyword, "TSTOPF");
			if(fits_read_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status))
			{
				fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
				status=0;
			}
			*t_stop+=tmp;
		}
		
		if(strcmp(timeunit, "d")==0)
		{
			(*t_stop)*=86400.0;
			//sprintf(timeunit, "s");
		}
		
	}
	
	sprintf(keyword, "MJDREF");
	double _mjd_ref=0;
	if(fits_read_key( fptr,  TDOUBLE, keyword, &_mjd_ref, keycomm, &status))
	{
		fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
		status=0;
		cerr << "Attempt to use MJDREFI and MJDREFF\n";
		sprintf(keyword, "MJDREFI");
		if(fits_read_key( fptr,  TDOUBLE, keyword, &_mjd_ref, keycomm, &status))
		{
			fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
			status=0;
		}
		else
		{
			double tmp=0;
			sprintf(keyword, "MJDREFF");
			if(fits_read_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status))
			{
				fprintf(stderr, "WARNING: could not find keyword '%s'\n", keyword);
				status=0;
			}
			_mjd_ref+=tmp;
		}
	}
	
	*mjdref = _mjd_ref;
	
	cout <<" Get time resolution MJDREF = " << *mjdref << endl;
	cout <<" Get time resolution TSTART = " << *t_start << endl;
	cout <<" Get time resolution TSTOP  = " << *t_stop << endl;

	if(fits_close_file(fptr, &status))    
	{                                            
		fits_report_error(stderr, status);                                                                                 
	}
	return status;
	
}

void event_file::scan_gti()
{

	double *gti_start=GTI_START;
	double *gti_stop=GTI_STOP;
	double min_time=1e12;
	double max_time=-1e12;
	double min_gap=1e12;
	double max_gap=-1e12;
	
	for(int i=0;i<n_gti;i++)
	{
		double tmp=gti_stop[i]-gti_start[i];
		if(tmp<min_time)
			min_time=tmp;
		if(tmp>max_time)
			max_time=tmp;
	}

	for(int i=1;i<n_gti;i++)
	{
		double tmp=-gti_stop[i-1]+gti_start[i];
		if(tmp<min_gap)
			min_gap=tmp;
		if(tmp>max_gap)
			max_gap=tmp;
	}
	
	cout <<"There are " << n_gti << " GTI intervals\n";
	cout <<"Minimum and maximum lengths are [s] " << min_time << " " << max_time << endl;
	cout <<"Minimum and maximum gaps are [s] " << min_gap << " " << max_gap << endl;
	
}

double *event_file::compute_phases(double tref, double f, double df, double ddf)
{
	double *phases = new double[_size];
	double if1=0, f1=0, tt=0;;
	for(int i=0;i<_size;i++)
	{
		tt=_times[i];
		f1 = light_curve::get_phase_complete(tt, tref, f, df, ddf, &if1);
		phases[i] = if1+f1;
	}
	return phases;
}

light_curve *event_file::fold(int n_bins, double tref, double f, double df,
double ddf, double t1, double t2, double e_min, double e_max)
{
	//if size of returned light curve is zero, this means error
        //cout << "Initialize folded LC" << endl;
	light_curve *c=new light_curve(*this, -1, 0, 1e10);
        //cout << "Deallocate folded LC" << endl;
	//c->deallocate_lc();
	double l_e_min=e_min;
	double l_e_max=e_max;
	get_local_energies(e_min,e_max, &l_e_min, &l_e_max);
	//cout << e_min << " " << e_max << " " << l_e_min << " " << l_e_max << endl;
	if(t1 >= t2 || t1 >= _t_stop || t2 <= _t_start)
	{
		cerr << "Impossible to fold curve with this time limits\n";
		cerr << scientific << setw(19) << setprecision(12) << t1 << " " << _t_start << " " << t2 << " " << _t_stop <<endl;
		waitkey();
		c->deallocate();
		return c;
	}
	
	if(t1 > _t_start || t2 < _t_stop)
	{
		if(c->crop_gti(t1,t2))
		{
			cerr << "Error in cropping GTI for folding events\n";
			waitkey();
			c->deallocate();
			return c;
		}
	}

    //cout << "About to allocate folded LC" << endl;
	c->allocate(n_bins);
	
	double *exposure=c->_frac_exp;
	double *pulse=c->_y;
	double *err_pulse=c->_dy;
	
	for(int i=0;i<n_bins;i++)
	{
		exposure[i]=0;
		pulse[i]=0;
		err_pulse[i]=0;
		c->_x[i]=((double)i+0.5)/n_bins;
		c->_dx[i]=1./(double)n_bins;
	}
	
	int flag_exp = c->compute_phase_exposure(tref, f, df, ddf); //0 if done, 1 if not
	if (flag_exp >0)
	{
		cerr << "Warning :: not computed phase exposure in event file folding\n";
		waitkey();
	}
	//else
	//	cout << "Succesfully computed phase exposure in event file\n";
	light_curve::get_phase_complete(t1, tref, f, df, ddf, &c->_phase_t_start);
    //cout << l_e_min << " " << l_e_max << endl;
	int n_selected=0;
	for(int i=0;i<_size;i++)
	{
		double tt=_times[i];
        double ee = _energy[i];
		//cout << tt << " " << ee << " "<< t1 << " " << t2 << " " << l_e_min << " " << l_e_max << endl;  
		
        if(tt < t1 || tt > t2 || ee < l_e_min || ee > l_e_max)
		{
            continue;
        }
		//cout << "S " << tt << " " << ee << " "<< t1 << " " << t2 << " " << l_e_min << " " << l_e_max << endl;  
		
		double if1=0;
		double f1 = light_curve::get_phase_complete(tt, tref, f, df, ddf, &if1);	
		//cout << f1 << " " << (int)floor(f1*n_bins) << endl;
		c->_y[(int)floor(f1*n_bins)]++;
		c->_dy[(int)floor(f1*n_bins)]++;
		n_selected++;
	}
	
	//cout << "Selected " << n_selected << " events\n";
	
	if(flag_exp==0)
	{
//		cout << "Divide phase exposure\n";
		c->divide_frac_exp_sqrt_err();
	}
	else //if (c->ontime() >0)
	{
//		cout << "Just sqrt of errors\n";
		c->sqrt_err();
		// *c /= (c->ontime()*n_bins);
	}
	return c;
}

int event_file::get_times_tlim(vector<double> &times, double t1, double t2)
{
    //cout << "Extracting events in the interval " << t1 << " " << t2 << endl;

    double mult=1;
    if(strcmp(timeunit, "d")==0)
	{
        mult=86400;		//sprintf(timeunit, "s");
	}
    //cout <<"Mult" << mult << endl;


	for(int i=0;i<_size;i++) //not efficient, but avoids complication for now
	{
        double tt=_times[i]*mult;
                //cout << t1 << " " << tt << " " << t2 << endl;
		if(tt >= t1 && tt<t2)
		{
			//cout << t1 << " " << _times[i] << " " << t2 << endl;
			times.push_back(tt);
		}
	}
	
	return 0;
}
	

int event_file::add_key_matrix(fitsfile *fptr, double t_ref, double f, double
df, double df2)
{
	
	char keyword[FLEN_KEYWORD];
	char keycomm[FLEN_COMMENT];
	int status=0;
	
	fits_update_key( fptr,  TSTRING, "OBJECT", object, "name of observed object", &status);
	sprintf(keyword, "TREF");
	double tmp = t_ref;
	sprintf(keycomm,  "[INTEGRAL seconds] ref. time for the folding");
	fits_update_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status);
	fits_report_error(stderr, status);
	
	sprintf(keyword, "FREQ");
	tmp = f;
	sprintf(keycomm,  "[Hz] spin frequency");
	fits_update_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status);
	fits_report_error(stderr, status);
	
	sprintf(keyword, "dFREQ");
	tmp = df;
	sprintf(keycomm,  "[Hz/s] spin frequency first derivative");
	fits_update_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status);
	fits_report_error(stderr, status);
	
	sprintf(keyword, "ddFREQ");
	tmp = df2;
	sprintf(keycomm,  "[Hz/s^2] spin frequency second derivative");
	fits_update_key( fptr,  TDOUBLE, keyword, &tmp, keycomm, &status);
	fits_report_error(stderr, status);
	
	return status;
	//ORIGIN            
	//IMAGETYP
	
}



int event_file::init_tphase(char *fname, int n_bins, double t_ref, double f,
double df, double df2, double e_min, double e_max)
{
	
	int status=0;
	char loc_fname[FLEN_FILENAME];
	sprintf(loc_fname, "!%s", fname);
	fitsfile *fptr_loc;
	fits_create_file(&fptr_loc, loc_fname, &status);
	if(status)
	{
		cerr <<"Error creating Time Phase matrix '" << fname <<"'\n";
		exit(1);
	}
	char extname[] = "TPHMATR";       /* extension name */
	//char spefilen[80];    /* name for the spectral file */
	char tmp[6];
	sprintf(tmp, "%dE", n_bins);
	char *ttype[] = {"T_START", "T_STOP",   "MATRIX", "Error", "Exposure"};
	char *tform[] = {"1D",      "1D",       tmp    , tmp, tmp};
	char *tunit[] = {"s",       "s",   "counts/s",  "counts/s", "s"};
	int tfields=5;
	
	
	if ( fits_create_tbl(fptr_loc, BINARY_TBL, 0, tfields, ttype, tform,
						 tunit, extname, &status) )
	{
		fits_report_error(stderr, status );
		exit(status);
	}
	char keyword[FLEN_KEYWORD];
        char keycomm[FLEN_COMMENT];
	int itmp=0;
        
	if(add_key_matrix(fptr_loc, t_ref, f, df, df2))
	{
		cerr << "Error in writing keyword in the ENERGY-PHASE matrix\n";
	}
	
	
        itmp=n_bins;
        sprintf(keyword,"NPHASES");
        //sprintf(keyvalu,  "%f", spec->exposure);
        sprintf(keycomm, "number of phase channels in the pulses");
        fits_update_key( fptr_loc,  TINT, keyword, &itmp, keycomm, &status);

        sprintf(keyword,"E_MIN");
        sprintf(keycomm, "Minimum energy of pulses");
        fits_update_key( fptr_loc,  TDOUBLE, keyword, &e_min, keycomm, &status);

        sprintf(keyword,"E_MAX");
        sprintf(keycomm, "Maximum energy of pulses");
        fits_update_key( fptr_loc,  TDOUBLE, keyword, &e_max, keycomm, &status);
        
	// 	{
	// 		sprintf(keyword,"BINCORR");
	// 		char 
	// 	//sprintf(keyvalu,  "%f", spec->exposure);
	// 		char keycomm[] = "1 if times are at systm barycenter, 0 if at solar system ";
	// 		fits_update_key( fptr_loc,  TBYTE, keyword, &binary_corr_flag, keycomm, &status);
	// 	}
	
	fits_close_file(fptr_loc, &status);
	if(status)
	{
		cerr <<"Error closing Time Phase matrix '" << fname <<"'\n";
		return status;
	}
	return status;
}


int event_file::update_tphase_matrix(char *fname, vector<double> tstart_vector, vector<double> tstop_vector, int n_bins,
double t_ref, double f, double df, double df2, class light_curve &bl, char
bck_flag, double ext_back, double ext_back_err, double e_min, double e_max)
{
	
    if (tstart_vector.size() ==1 && tstop_vector.size() ==0)
	{	
        double delta_t = tstart_vector[0];

		double t1 = GTI_START[0];
		double t2 = t1+delta_t;
		
		tstart_vector.pop_back();
		
		while (t2 <= GTI_STOP[n_gti-1]+delta_t)
		{
			tstart_vector.push_back(t1);
			tstop_vector.push_back(t2);
			t1=t2;
			t2=t1+delta_t;
		}
    }

	if (tstart_vector.size() != tstop_vector.size())
	{
		cerr << "update_tphase_matrix :: Error sizes of input vectors differ\n";
		cerr << tstart_vector.size() << " " <<  tstop_vector.size() << endl;
		waitkey();
		return 1;
	}
    
    if(n_gti==0)
    {
        cerr << "Folding matrix, no GTI are present, use start and stop\n";
        allocate_gti(1);
        cerr << "Tstart = " << _t_start << " ";
        cerr << "Tstop  = " << _t_stop << endl; 

        GTI_START[0]=_t_start;
        GTI_STOP[0]=_t_stop;
    }
	//Check loop variable
    int kkk=0;
	int local_n_gti = n_gti;
	double local_GTI_START[local_n_gti];
	double local_GTI_STOP[local_n_gti];

	for(int i=0;i<n_gti;i++)
	{
		local_GTI_START[i] = GTI_START[i];
		local_GTI_STOP[i] = GTI_STOP[i];
	}
	vector<double> trimmed_GTI_START;
	vector<double> trimmed_GTI_STOP;
	
	for(int j=0;j<tstart_vector.size();j++)
	{
		double t1=tstart_vector[j];
		double t2=tstop_vector[j];

		trimmed_GTI_START.clear();
		trimmed_GTI_STOP.clear();

		for(int i=0;i<local_n_gti;i++)
		{

			
			if ( t2 <= local_GTI_START[i])
				continue;

			if (t1 > local_GTI_STOP[i])
				continue;

			if (t1 >= local_GTI_START[i])
			{
				trimmed_GTI_START.push_back(t1);
			}
			else
			{
				trimmed_GTI_START.push_back(local_GTI_START[i]);
			}

			if (t2 <= local_GTI_STOP[i])
			{
				trimmed_GTI_STOP.push_back(t2);
			}
			else
			{
				trimmed_GTI_STOP.push_back(local_GTI_STOP[i]);
			}
		}

		//This is because the phase exposure is computed in the folding
		n_gti = trimmed_GTI_STOP.size();
		if (n_gti > local_n_gti)
		{
			cerr << "make_Ttphase: Local GTIs cannot be larger than global GTIs\n";
			waitkey();
			return 1;
		}

		for (int i =0;i<n_gti;i++)
		{
			GTI_START[i] = trimmed_GTI_START[i];
			GTI_STOP[i] = trimmed_GTI_STOP[i];
		}
		
		light_curve *l = fold(n_bins, t_ref, f, df, df2, t1, t2, e_min, e_max);		

		double back_level=0;
		if(bck_flag)
		{
				back_level=bl.total(t1,t2)/(t2-t1);
				(*l)-=back_level;
				l->sub_with_err(ext_back, ext_back_err);
		}
		
		//Debug options
		//char fname2[FLEN_FILENAME];
		//sprintf(fname2, "p_%03d.qdp", j);
		//l.output_lc(fname2);
		
		l->update_tphase(fname, back_level+ext_back);
		
		delete l;
	}//cycle on vector of TSTARTs

	//Ripristinate
	n_gti = local_n_gti;
	for(int i=0;i<n_gti;i++)
	{
		GTI_START[i] = local_GTI_START[i];
		GTI_STOP[i] = local_GTI_STOP[i];
	}
    return 0;
}


/*
int event_file::isgri_barycentric_correction(double *loc_times, char *og_name,
int numEvents, double RA, double DEC)//dal_element *InputDS, double RA, double DEC)
{
	
	time_t internal_start_time, internal_stop_time;
	std::time(&internal_start_time);
	for(int i=0;i<numEvents;i++)
	{
		
		loc_times[i]/=86400.;
		//cout << old_times[i] - time->data[i]<<endl;;
	}
	
	//double *old_times = new double[numEvents];
	//Assumes it is in days
 	
  	int status=0;
 	dal_element *InputDS;
	
	//Disregard the og_grop and looks for the indivisual scw files 
	//Opening og_group is extremely slow for huge datasets
	
	string aux(og_name);
	string::size_type t1;
	
	if( (t1 = aux.find_last_of("/") ) != string::npos)
		aux.erase(t1+1, aux.size());
	
	aux+="scw/";
	aux+=scwname;
	
	string aux_base=aux;
	
	aux+=".001/swg_ibis.fits";
	
	cout << "Opening swg_group " << aux << endl;
	status = DALobjectOpen(aux.c_str(),&InputDS,status);
	if(status != ISDC_OK)
	{
		cerr << "Error opening '" << aux.c_str() << "' trying with rev 0" <<endl;
		//cerr << status << endl;
		status=ISDC_OK;
		aux=aux_base+".000/swg_ibis.fits";
		status = DALobjectOpen(aux.c_str(),&InputDS,status);
		if(status != ISDC_OK)
		{
			cerr << "Error opening '" << aux.c_str() << "' give up"
<< endl;
			for(int i=0;i<numEvents;i++)
			{
				
				loc_times[i]*=86400.;
				//cout << old_times[i] - time->data[i]<<endl;;
			}
			return 1;
		}
   	}
	
	std::time(&internal_stop_time);
	printf("scw_ctrl :: opening swg_group accomplished in %.1f s\n",
	       difftime(internal_stop_time,internal_start_time));
	
	// 	dal_element *scw       = NULL;
	// 	cout << "Start to search for SCW in og_group\n";
	// 	status = DALobjectFindElement(InputDS, scw_name, &scw,status);
	// 	if(status)
	// 	{
	// 	    cerr << "Error opening " << scw_name << " in " << og_name <<endl;
	//             //cerr << status << endl;
	// 	   status=0;
	// 		DALobjectClose(InputDS,DAL_SAVE,status);                
                                                              
	//             return ERROR_CODE_INPUT;  
	// 	}
	// 	
	// 	std::time(&internal_stop_time);
	//         printf("scw_ctrl :: finding SCW accomplished in %.1f s\n", difftime(internal_stop_time,internal_start_time));
	
	
	cout << "\nog_group :: " << og_name << "\nRA = " << RA << " DEC = " <<
DEC << endl;
	status=DAL3AUXconvertBarycentIJD(InputDS,  //scw
									
numEvents, // SPR 3428 
									 RA,
									 DEC,
									
DAL3AUX_TDB,DAL3AUX_ANY,TCOR_ANY,
									 //BarySys      TimeSys,
									
//AUXDATA_type AuxType,
									
//TCOR_flag    Accuracy,
									
loc_times,
									
status);
	
	
	
	//Attention !!! Time is not put back in seconds, need to break in case of errors !
	
	for(int i=0;i<numEvents;i++)
	{
		
		loc_times[i]*=86400.;
		//cout << old_times[i] - time->data[i]<<endl;;
	}
	
	
	//delete [] old_times;
	
	std::time(&internal_stop_time);
	printf("scw_ctrl :: barycentric correctin accomplished in %.1f s\n",
difftime(internal_stop_time,internal_start_time));
	
	//waitkey();
	
	status = DALobjectClose(InputDS,DAL_SAVE,status);
	
  	return status;
	
}
*/

void event_file::binary_corr(char *file_name)
{


	char binary_file[FLEN_FILENAME];
	char query[256];
	sprintf(binary_file, "%s", file_name);
	char setup_file_exist=1;
	char interactive_flag=0;
	if (strcmp(binary_file, "none")==0)
	{
		sprintf(query, "Enter the name of the file to store the binary parameters");
		askInfo(query, binary_file);
		//sprintf(binary_file,"%s_orbit.dat",active_src_name);
		interactive_flag=1;
		setup_file_exist=0;
	}

	binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist,_mjd_ref);

	//double off = ( _mjd_ref - MJDREF_INTEGRAL ) * DAY2SEC;

	double corr_time =  binarycor( _t_start  );
	_t_start=corr_time;
	corr_time =   binarycor( _t_stop );
	_t_stop=corr_time;

	for(long i=0;i<_size;i++)
	{
		corr_time =  binarycor( _times[i]);
		//              printf( "%19.12e\t%19.12e\n", evt_time[i],  corr_time - evt_time[i]  );
		_times[i] = corr_time;

	}

	if(n_gti)
	{
		for(int i=0;i<n_gti;i++)
		{
			corr_time =  binarycor( GTI_START[i]  );
			GTI_START[i] = corr_time;
			corr_time =  binarycor( GTI_STOP[i]  );
			GTI_STOP[i] = corr_time;
		}
	}

	sprintf(timeref, "BINARY");

}

void event_file::concatenate(event_file *ee)
{
    //Concatenates only times energy and GTI
    //keeps all specifics of current file
    
    if(ee->_t_start <= _t_start)
    {
        cerr << "Impossible to concatenate event files which are not time ordered\n";
        return;
    }
    
    int n_size=ee->size() + _size;
    double *n_times = new double[n_size];
    double *n_energy = new double[n_size];
    int n_n_gti = n_gti + ee->n_gti;
    double *n_GTI_START = new double[n_n_gti];
    double *n_GTI_STOP = new double[n_n_gti];

    for(int i=0;i<_size;i++)
    {
        n_times[i]=_times[i];
        n_energy[i]=_energy[i];
    }
    for(int i=0;i<ee->_size;i++)
    {
        n_times[_size+i]=ee->_times[i];
        n_energy[_size+i]=ee->_energy[i];
    }

    for(int i=0;i<n_gti;i++)
    {
        n_GTI_START[i]=GTI_START[i];
        n_GTI_STOP[i]=GTI_STOP[i];
    }

    for(int i=0;i<ee->n_gti;i++)
    {
        n_GTI_START[n_gti+i]=ee->GTI_START[i];
        n_GTI_STOP[n_gti+i]=ee->GTI_STOP[i];
    }

    _ontime+=ee->_ontime;
    _t_stop=ee->_t_stop;

    delete [] _times;
    delete [] _energy;
    delete [] GTI_START;
    delete [] GTI_STOP;

    _times = new double[n_size];
    _energy = new double[n_size];

    GTI_START = new double[n_n_gti];
    GTI_STOP = new double[n_n_gti];

    _size=n_size;
    n_gti=n_n_gti;

    for(int i=0;i<_size;i++)
    {
        _times[i]=n_times[i];
        _energy[i]=n_energy[i];
    }

    for(int i=0;i<n_gti;i++)
    {
        GTI_START[i]=n_GTI_START[i];
        GTI_STOP[i]=n_GTI_STOP[i];
    }

    delete [] n_times;
    delete [] n_energy;
    delete [] n_GTI_START;
    delete [] n_GTI_STOP;
    
}

int event_file::make_gti_table(char root_fname[], double ph_start, double ph_stop, 
	char binary_flag, double t_ref, double f, double df=0, double df2=0, int n_bins=1)
{
	//cout << "Binary flag " << binary_flag << endl;
	int status = 0;
	char ofn[1024];
	double glob_ph_start=ph_start;
//	double glob_ph_stop=ph_stop;
	float step = (ph_stop - ph_start) /n_bins;
	int j;
	for(j=0;j<n_bins;j++)
	{
		char ofn[256];
		double loc_t_start = _t_start;
		double loc_t_stop = _t_stop;
		double i_phase = 0;
		double tmp = light_curve::get_phase_complete(loc_t_start, t_ref, f, df, df2, &i_phase);
		ph_start= glob_ph_start+j*step;
		ph_stop = glob_ph_start+(j+1)*step;
// 		printf("Start and stop phase %.3f - %.3f\n", ph_start, ph_stop);
		sprintf(ofn, "%s_%05.3f-%05.3f.dat", root_fname, ph_start, ph_stop);
		FILE *out=fopen(ofn, "w");
		if(out==NULL)
		{
			fprintf(stderr, "Error opening '%s'\n", ofn);
			exit(1);
		}
		double ctrl_stop=0;
		double tot_time=0;
		int i=0;
		vector<double> new_gti_start;
		vector<double> new_gti_stop;
		
		while(ctrl_stop < _t_stop)
		{	
			double phi = i_phase + (double)i + ph_start;
			loc_t_start = t_ref+get_dt(phi,f,df,df2);
			phi = i_phase + (double)i + ph_stop;
			//t_stop = tref+get_dt_1(phi,f,df,df2);
			ctrl_stop = t_ref+get_dt(phi,f,df,df2);
// 			tot_time += ctrl_stop - t_start;
			if(binary_flag == 'y')
			{
				loc_t_stop = get_solar(ctrl_stop);
				loc_t_start = get_solar(loc_t_start);
			}
			else
				loc_t_stop = ctrl_stop;
		
			fprintf(out,"%19.12e\t%19.12e\n", loc_t_start, loc_t_stop);
			tot_time += loc_t_stop - loc_t_start;
			new_gti_start.push_back(loc_t_start);
			new_gti_stop.push_back(loc_t_stop);
			
			i++;
		}

		fclose(out);
	
		double *ext_gti_start = new double[i];
		double *ext_gti_stop = new double[i];
		for (int kk=0;kk<i;kk++)
		{
			ext_gti_start[kk] = new_gti_start[kk];
			ext_gti_stop[kk]  = new_gti_stop[kk];
		}
		
		fitsfile *fptr_loc;
		sprintf(ofn, "%s_%05.3f-%05.3f.fits", root_fname, ph_start, ph_stop);
		fits_create_file(&fptr_loc, ofn, &status);
		if(status)
		{
			cerr <<"Error creating GTI file '" << ofn <<"'\n";
			return status;
		}
	
		write_gtis(fptr_loc, i, ext_gti_start, ext_gti_stop, 
			t_ref, f,df, df2, binary_flag);

		fits_close_file(fptr_loc, &status);
		if(status)
		{
			cerr <<"Error closing GTI file '" << ofn <<"'\n";
			return status;
		}

		printf("Total time is %.2lf  should be ~ %.2lf\n", tot_time, (_t_stop -_t_start)*step);

		
	}

	return status;
}


int event_file::init_ephase(char *fname, int n_bins, double t_ref, double f,
double df, double df2, double t_min, double t_max)
{
	
	int status=0;
	char loc_fname[FLEN_FILENAME];
	sprintf(loc_fname, "!%s", fname);
	fitsfile *fptr_loc;
	fits_create_file(&fptr_loc, loc_fname, &status);
	if(status)
	{
		cerr <<"Error creating Energy Phase matrix '" << fname <<"'\n";
		exit(1);
	}
	char extname[] = "EPHMATR";       /* extension name */
	//char spefilen[80];    /* name for the spectral file */
	char tmp[6];
	sprintf(tmp, "%dE", n_bins);
	char *ttype[] = {"E_MIN", "E_MAX",   "MATRIX", "Error", "Exposure"};
	char *tform[] = {"1D",      "1D",      tmp    , tmp, tmp};
	char *tunit[] = {"keV",       "keV",   "counts/s",  "counts/s", "s"};
	int tfields=5;
	
	
	if ( fits_create_tbl(fptr_loc, BINARY_TBL, 0, tfields, ttype, tform,
						 tunit, extname, &status) )
	{
		fits_report_error(stderr, status );
		exit(status);
	}
	char keyword[FLEN_KEYWORD];
        char keycomm[FLEN_COMMENT];
	int itmp=0;
        
	if(add_key_matrix(fptr_loc, t_ref, f, df, df2))
	{
		cerr << "Error in writing keyword in the ENERGY-PHASE matrix\n";
	}
	
	
        itmp=n_bins;
        sprintf(keyword,"NPHASES");
        //sprintf(keyvalu,  "%f", spec->exposure);
        sprintf(keycomm, "number of phase channels in the pulses");
        fits_update_key( fptr_loc,  TINT, keyword, &itmp, keycomm, &status);

        sprintf(keyword,"T_MIN");
        sprintf(keycomm, "Minimum time of pulses");
        fits_update_key( fptr_loc,  TDOUBLE, keyword, &t_min, keycomm, &status);

        sprintf(keyword,"T_MAX");
        sprintf(keycomm, "Maximum time of pulses");
        fits_update_key( fptr_loc,  TDOUBLE, keyword, &t_max, keycomm, &status);
        
	// 	{
	// 		sprintf(keyword,"BINCORR");
	// 		char 
	// 	//sprintf(keyvalu,  "%f", spec->exposure);
	// 		char keycomm[] = "1 if times are at systm barycenter, 0 if at solar system ";
	// 		fits_update_key( fptr_loc,  TBYTE, keyword, &binary_corr_flag, keycomm, &status);
	// 	}
	
	fits_close_file(fptr_loc, &status);
	if(status)
	{
		cerr <<"Error closing Energy Phase matrix '" << fname <<"'\n";
		return status;
	}
	return status;
}


int event_file::update_ephase_matrix(char *fname, vector<double> estart_vector, vector<double> estop_vector, int n_bins,
double t_ref, double f, double df, double df2, class light_curve &bl, char
bck_flag, double ext_back, double ext_back_err, double t_min, double t_max)
{

    
    if(n_gti==0)
    {
            cerr << "Folding matrix, no GTI are present, use start and stop\n";
            allocate_gti(1);
            cerr << "Tstart = " << _t_start << " ";
            cerr << "Tstop  = " << _t_stop << endl; 

            GTI_START[0]=_t_start;
            GTI_STOP[0]=_t_stop;
    }
		
    int kkk=0;
    double e_min=0;
    double e_max=0;
    
    for(int j=0;j<estart_vector.size();j++)
    {
        //Use time bins (less checks)
        e_min=estart_vector[j];
        e_max=estop_vector[j];

        //cout << scientific << t1  << " " << t2 << " " <<  GTI_START[i] <<  " " <<  GTI_STOP[i] << endl;

//        if( !(t_min >= GTI_START[i] && t_min <= GTI_STOP[i] ) )
//            continue;
//        if( !(t_max >= GTI_START[i] && t_max <= GTI_STOP[i] ) )
//            continue;

        if(j != kkk++)
        {
            cout << "WARN " << j << " " << kkk-1 << "\n";
            waitkey();
        }

        light_curve *l=fold(n_bins, t_ref, f, df, df2, t_min, t_max, e_min, e_max );
		//cout << t_min << " " << t_max << " " << e_min << " " << e_max << endl; 
        double back_level=0;
        if(bck_flag)
        {
                back_level=bl.total(t_min,t_max)/(t_max-t_min);
                (*l)-=back_level;
        }
        l->sub_with_err(ext_back, ext_back_err);
        char fname2[FLEN_FILENAME];
        sprintf(fname2, "p_%02d.qdp", j);
        //l.output_lc(fname2);
        l->update_ephase(fname, back_level+ext_back,e_min,e_max);
        delete l;


    }//cycle on vector
			
	return 0;
}

void event_file::merge(event_file *ee)
{
    //Merges only times energy and GTI (and))
    //Updates some keywords
    //TODO Need to work on checks for ill situations
    
    if(_t_start > ee->_t_stop || _t_stop < ee->_t_start)
    {
        cerr << "event_file::merge No overlap in files, impossible to merge, kept first file\n";
        return;
    }
    
    //The merging of GTIs is meant to find times of common exposure, hence this check
    
    //cout << "Determing the timing keywords\n";
    _mjd_ref=MAX(_mjd_ref, ee->_mjd_ref);
    _t_start=MAX(_t_start, ee->_t_start);
    _t_stop =MIN(_t_stop,  ee->_t_stop );

    //cout << "Allocate new gtis, large size\n";
    int new_n_gti=n_gti + ee->n_gti;
    double *new_gti_start = new double[new_n_gti];
    double *new_gti_stop  = new double[new_n_gti];
    int status=0;        
    
    //cout << "Merge gtis\n";
    status=mergeGTI(n_gti, GTI_START, GTI_STOP, 
                    ee->n_gti, ee->GTI_START, ee->GTI_STOP, 
                    &new_n_gti, new_gti_start, new_gti_stop, status);

    //cout << "There are " << new_n_gti << " merged GTIs\n";
    int n_size=ee->size() + _size;
    cout << "Allocate new evt vectors " << n_size << endl;
    VecDoub n_times(n_size, 0.0);
    VecDoub n_energy(n_size, 0.0);
    
    //cout << "Copy times 1\n";
    for(int i=0;i<_size;i++)
    {
        n_times[i]=_times[i];
        n_energy[i]=_energy[i];
    }
    //cout << "Copy times 2\n";
    for(int i=0;i<ee->_size;i++)
    {
        n_times[_size+i]=ee->_times[i];
        n_energy[_size+i]=ee->_energy[i];
    }

    //Sort times
    //cout << "Sorting evts\n";
    loc_piksr2(n_times, n_energy);
    
    //Filter GTIs
    //cout << "Filter evts\n";
    VecDoub f_times(n_size, 0.0);
    VecDoub f_energy(n_size, 0.0);  
    int ind_filtered=0;
    int curr_gti_ind=0;
    for (int i=0;i<n_size;i++)
    {
        if(n_times[i] >= new_gti_start[curr_gti_ind] && n_times[i] <= new_gti_stop[curr_gti_ind])
        {
            f_times[ind_filtered]=n_times[i];
            f_energy[ind_filtered]=n_energy[i];
            //cout << i << " " << ind_filtered << " " << curr_gti_ind << endl;
            ind_filtered++;
        }
        else
        {
            for (int j=curr_gti_ind;j<new_n_gti;j++)
            {
                if(n_times[i] >= new_gti_start[j] && n_times[i] <= new_gti_stop[j])
                {
                    f_times[ind_filtered]=n_times[i];
                    f_energy[ind_filtered]=n_energy[i];
                    ind_filtered++;
                    curr_gti_ind=j;
                    break;
                }
            }
        }
    }
   
    cout << "Filtered events " << ind_filtered << " good out of " << n_size << endl;
    //cout << "Free old arrays\n";
    delete [] _times;
    delete [] _energy;
    delete [] GTI_START;
    delete [] GTI_STOP;
    GTI_START = 0;
    GTI_STOP  = 0;

    //cout << "Allocate " << ind_filtered << " event arrays\n";
    _times  = new double[ind_filtered];
    _energy = new double[ind_filtered];

    //cout << "Allocate " << new_n_gti << " GTI arrays\n";
    GTI_START = new double[new_n_gti];
    GTI_STOP  = new double[new_n_gti];

    //cout << "Resize\n";
    _size=ind_filtered;
    n_gti=new_n_gti;

    //cout << "Copy output files\n";
    for(int i=0;i<_size;i++)
    {
        _times[i]=f_times[i];
        _energy[i]=f_energy[i];
    }

    _ontime=0;
    for(int i=0;i<n_gti;i++)
    {
        GTI_START[i]=new_gti_start[i];
        GTI_STOP[i]=new_gti_stop[i];
        _ontime+=GTI_STOP[i]-GTI_START[i];
    }

//    delete [] n_times;
//    delete [] n_energy;
    //cout << "Determing the timing keywords 2\n";
    _time_del =MAX(_time_del, ee->_time_del);
    
    char keyvalu[FLEN_VALUE];
    sprintf(keyvalu, "%s%s", instrume, ee->instrume);
    sprintf(instrume, "%s", keyvalu);
    
    delete [] new_gti_start;
    delete [] new_gti_stop;
    
}



int event_file::write(char *fname)
{
	
	int status=0;
	char loc_fname[FLEN_FILENAME];
	sprintf(loc_fname, "!%s", fname);
	fitsfile *fptr_loc;
	fits_create_file(&fptr_loc, loc_fname, &status);
	if(status)
	{
		cerr <<"Error creating Time file'" << fname <<"'\n";
		exit(1);
	}
	char extname[] = "EVENTS";       /* extension name */
	//char spefilen[80];    /* name for the spectral file */
	char *ttype[] = {"TIME", "Energy"};
	char *tform[] = {"1D",      "1D"};
	char *tunit[] = {timeunit,       "keV",};
	int tfields=2;
	long firstelem=1;
	
	if ( fits_create_tbl(fptr_loc, BINARY_TBL, 0, tfields, ttype, tform,
						 tunit, extname, &status) )
	{
		fits_report_error(stderr, status );
		exit(status);
	}
       	
	fits_write_col(fptr_loc, TDOUBLE, 1, 1, firstelem, _size,
                            _times, &status);

        fits_write_col(fptr_loc, TDOUBLE, 2, 1, firstelem, _size,
                            _energy, &status);

//        sprintf(keyword,"E_MIN");
//        sprintf(keycomm, "Minimum energy of pulses");
//        fits_update_key( fptr_loc,  TDOUBLE, keyword, &e_min, keycomm, &status);
//
//        sprintf(keyword,"E_MAX");
//        sprintf(keycomm, "Maximum energy of pulses");
//        fits_update_key( fptr_loc,  TDOUBLE, keyword, &e_max, keycomm, &status);
//        
        status=write_timing_keywords(fptr_loc);
        
        status=write_gtis(fptr_loc);
	
	fits_close_file(fptr_loc, &status);
	if(status)
	{
		cerr <<"Error closing file '" << fname <<"'\n";
		return status;
	}
	return status;
}


int event_file::write_with_phase(char *fname, double tref, double f, double df, double ddf, char binary_flag)
{
	
	int status=0;
	
	double *phases = compute_phases(tref, f, df, ddf);

	char loc_fname[FLEN_FILENAME];
	sprintf(loc_fname, "!%s", fname);
	fitsfile *fptr_loc;
	fits_create_file(&fptr_loc, loc_fname, &status);
	if(status)
	{
		cerr <<"Error creating Time file'" << fname <<"'\n";
		exit(1);
	}
	char extname[] = "EVENTS";       /* extension name */
	//char spefilen[80];    /* name for the spectral file */
	char *ttype[] = {"TIME", "Energy", "Phase"};
	char *tform[] = {"1D",  "1D", "1D"};
	char *tunit[] = {timeunit,  "keV", ""};
	int tfields=3;
	long firstelem=1;
	
	if ( fits_create_tbl(fptr_loc, BINARY_TBL, 0, tfields, ttype, tform,
						 tunit, extname, &status) )
	{
		fits_report_error(stderr, status );
		exit(status);
	}
       	
	fits_write_col(fptr_loc, TDOUBLE, 1, 1, firstelem, _size,
                            _times, &status);

	fits_write_col(fptr_loc, TDOUBLE, 2, 1, firstelem, _size,
						_energy, &status);

	fits_write_col(fptr_loc, TDOUBLE, 3, 1, firstelem, _size,
					phases, &status);

	status=write_timing_keywords(fptr_loc);
	status=write_phase_keywords(fptr_loc, tref, f, df, ddf, binary_flag);

        
        status=write_gtis(fptr_loc);
	
	fits_close_file(fptr_loc, &status);
	if(status)
	{
		cerr <<"Error closing file '" << fname <<"'\n";
		return status;
	}
	return status;
}


int event_file::write_gtis(fitsfile *evt_on, int ext_n_gti, double *ext_gti_start, double *ext_gti_stop, 
			double tref, double f, double df, double ddf, char binary_flag)
{
     //copies GTI
	int status=0;
	int hdutype;
	int nrows = 0;
	long firstelem=1;
	char keyword[FLEN_KEYWORD];
	char keyvalu[FLEN_VALUE];
	char keycomm[FLEN_COMMENT];
	sprintf(keyword, "GTI");
	int tfields = 2;  
	char *ttype[] = {"START", "STOP"};
	char *tform[] = {"1D",   "1D"};
	char *tunit[] = {timeunit,   timeunit};
	
	if ( fits_create_tbl(evt_on, BINARY_TBL, nrows, tfields, ttype, tform,
						tunit, keyword, &status) )
		fits_report_error(stderr, status );
	
	write_timing_keywords(evt_on);
	
	sprintf(keyword,"EXTNAME");
	sprintf(keyvalu,"GTI");
	sprintf(keycomm,  "extension standard name");
	fits_update_key( evt_on,  TSTRING, keyword, keyvalu, keycomm, &status);
	
	if (ext_n_gti == 0)
		ext_n_gti = n_gti;
	if (ext_gti_start == NULL)
		ext_gti_start = GTI_START;
	if (ext_gti_stop == NULL)
		ext_gti_stop = GTI_STOP;
	
	
	
	fits_write_col(evt_on, TDOUBLE, 1, 1, firstelem, ext_n_gti,
							ext_gti_start, &status);

	fits_write_col(evt_on, TDOUBLE, 2, 1, firstelem, ext_n_gti,
							ext_gti_stop, &status);

	if (f != 0)
		write_phase_keywords(evt_on, tref, f, df, ddf, binary_flag);
	return status;
			
}
int event_file::write_phase_keywords(fitsfile *evt_on, double tref, double f, double df, double ddf, char binary_flag)
{
	int status = 0;
    char keyword[FLEN_KEYWORD];
    char keycomm[FLEN_COMMENT];
    char keyvalu[FLEN_VALUE];

    sprintf(keyword, "TREF");
    sprintf(keycomm, "Folding reference time");
    fits_update_key(evt_on, TDOUBLE, keyword, &tref, keycomm, &status);

	sprintf(keyword, "FREQ");
    sprintf(keycomm, "Folding frequency");
    fits_update_key(evt_on, TDOUBLE, keyword, &f, keycomm, &status);

	sprintf(keyword, "DFREQ");
    sprintf(keycomm, "Folding frequency derivatve");
    fits_update_key(evt_on, TDOUBLE, keyword, &df, keycomm, &status);

	sprintf(keyword, "DDFREQ");
    sprintf(keycomm, "Folding frequency second derivatve");
    fits_update_key(evt_on, TDOUBLE, keyword, &ddf, keycomm, &status);

	sprintf(keyword, "BINARY");
	
	if (binary_flag =='y')
		sprintf(keyvalu, "YES");
	else
		sprintf(keyvalu, "NO");

    sprintf(keycomm, "if binary correction is applied");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);

	return status;

}
	
int event_file::write_timing_keywords(fitsfile *evt_on) 
{
    int status = 0;
    char keyword[FLEN_KEYWORD];
    char keycomm[FLEN_COMMENT];
    char keyvalu[FLEN_VALUE];
    
    sprintf(keyword, "TELESCOP");
    sprintf(keyvalu, "%s", telescop);
    sprintf(keycomm, "Telescope");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);

    sprintf(keyword, "INSTRUME");
    sprintf(keyvalu, "%s", instrume);
    sprintf(keycomm,  "Instrument");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);

//    sprintf(keyword, "DETNAM");
//    sprintf(keyvalu, "ISGRI");
//    //sprintf(keycomm,  "");
//    fits_update_key(evt_on, TSTRING, keyword, keyvalu, NULL, &status);

 
    sprintf(keyword, "TUNIT");
    sprintf(keyvalu, "%s", timeunit);
    sprintf(keycomm, "Time unit");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);

    sprintf(keyword, "MJDREF");
    double tmp = _mjd_ref;
    sprintf(keycomm, "Modified Julian Date of time origin");
    fits_update_key(evt_on, TDOUBLE, keyword, &tmp, keycomm, &status);

    sprintf(keyword, "TIMESYS");
    sprintf(keyvalu, "%s", timesys);
    sprintf(keycomm, "Time frame system");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);

    sprintf(keyword, "TIMEREF");
    sprintf(keyvalu, "%s", timeref);
    sprintf(keycomm,  "Reference location of photon arrival times");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);
    
    sprintf(keyword, "OBJECT");
    sprintf(keyvalu, "%s", object);
    sprintf(keycomm,  "Name of observed object");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);
 
   sprintf(keyword, "ORIGIN");
    sprintf(keyvalu, "%s", origin);
    sprintf(keycomm,  "Origin of data");
    fits_update_key(evt_on, TSTRING, keyword, keyvalu, keycomm, &status);
 
    sprintf(keyword, "TIMEDEL");
    tmp = _time_del;
    sprintf(keycomm, "Time resolution");
    fits_update_key(evt_on, TDOUBLE, keyword, &tmp, keycomm, &status);
    
    sprintf(keyword, "ONTIME");
    tmp = _ontime;
    sprintf(keycomm, "Sum of GTIs");
    fits_update_key(evt_on, TDOUBLE, keyword, &tmp, keycomm, &status);
    
    sprintf(keyword, "TSTART");
    tmp = _t_start;
    sprintf(keycomm, "Start time of first frame");
    fits_update_key(evt_on, TDOUBLE, keyword, &tmp, keycomm, &status);
    
    sprintf(keyword, "TSTOP");
    tmp = _t_stop;
    sprintf(keycomm, "Stop time of last frame");
    fits_update_key(evt_on, TDOUBLE, keyword, &tmp, keycomm, &status);
    
    return status;
    
}

double event_file::get_Rk2(int j, double omega, double t_ref, char flag_err, int flag_binarycorr, 
        double e_min, double e_max, long int n_events_per_chunk,
        int &stopped_event, char int_bary='n', double ra_obj=0, double dec_obj=0, char
        *og_name=NULL, double max_time_separation=1e100)
{
    // This is the implementation of binned extended Rayleigh statistics 
    // omega : angular frequency
    // k : harmonic order
    // t_ref : reference time for phasing
    // flag_err : if true, weight by the error
    // based on Belanger (2016)
    
    
    //
    vector <double> times;
    stopped_event=get_times( times, flag_binarycorr, e_min, e_max, n_events_per_chunk, stopped_event, int_bary, 
                            ra_obj, dec_obj, og_name, max_time_separation);
    
    
    int n_events=times.size();
    double C=0, S=0;
    double t1=times.front()-t_ref;
    double t2=times.back()-t_ref;
    double T=t2-t1;
    double tmp1=1./(omega*j*T);
    double mC=tmp1*(sin(omega*j*t2)-sin(omega*j*t1));
    double mS=-tmp1*(cos(omega*j*t2)-cos(omega*j*t1));
    double tmp_sig =  tmp1 *(sin(omega*j*t2)*cos(omega*j*t2)-sin(omega*j*t1)*cos(omega*j*t1));
    double sig_C = 0.5/n_events * (1+tmp_sig) - mC; 
    double sig_S = 0.5/n_events * (1-tmp_sig) - mS; 
    double sig_CS = 0.5 * tmp1/n_events *(sin(omega*j*t2)*sin(omega*j*t2)-sin(omega*j*t1)*sin(omega*j*t1)) - mC*mS;
    double mat_det = sig_C*sig_S - sig_CS * sig_CS;
    for(int i=0;i<=times.back();i++)
    {

            double phase=(times[i]-t_ref)*omega;
            phase=phase-floor(phase);

            C+=cos(j*phase);
            S+=sin(j*phase);



            //cout << phase  << " " << tmp1 << " " << tmp2 << " " << lz2[i_nu] << endl;
    }
    C/=n_events;
    S/=n_events;
    return ( (sig_S - sig_CS)*C*C + (sig_C - sig_CS) * S*S )/mat_det;
    //return  2*(C*C + S*S);
}
