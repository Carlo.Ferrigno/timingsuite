/* Fri Sep 22 11:23:28 gmt 2000 */



// Last Update

//

// January 2002: modified the askInfo function to avoid memory leakage when

//               input string was too long.

//





#include <sys/types.h>  /* must include TYPES.H before STAT.H */

#include <sys/stat.h>



#include "ask.h"





/*void  askInfo(char * Query,char * Answ);

int ask_float(char * Query, float * real);

int ask_integer(char * Query, int * integer);

char    ask_yes_no(char * Query, char def);

*/



#define MAXCHARS  128



static char Answer[MAXCHARS];





//#define askInfo(a,b) _askInfo(a,b,sizeof(b))

//void            _askInfo(char *Query, char *Answ,int MaxLen);



/*==============================================*/



void            waitkey(void)

{







    askInfo(" hit the enter key to continue ...","");





}





/*==============================================*/

/*                                              */

/*==============================================*/



void            _askInfo(char *Query, char *Answ,int MaxLen)

{



    char           *RetChar;

    int AnsLen;



    /*-------------------------------------------

     * printout the query and the default answer

     *-------------------------------------------*/





    printf("\n%s ==> ", Query);



    if (Answ[0] != '\0')

        printf("[%s] ", Answ);  /* default value */











    if (fgets(Answer, MAXCHARS, stdin)==NULL)

        printf("\n askInfo: ERROR!");





    if ((RetChar = strchr(Answer, '\n')) != NULL)

        *RetChar = '\0';





    /*-----------------------------------------------------

     * check wheter the lenght of the answer was too long

     *-----------------------------------------------------*/



    if ((AnsLen=strlen(Answer))==(MAXCHARS-1))

        printf("\n WARNING: Input string too long was truncated to %d chars!\n",MAXCHARS-1);



    /*-----------------------------------

     *  remove the ending newline char

    *----------------------------------*/











    if (Answer[0] == '\0')

        return;                 /* return the default */





    /*------------------------------------------

        * New january 2001: check for max lenght

        *------------------------------------------*/



    if (AnsLen>=MaxLen)

    {

        printf("\n WARNING: Input string too long was truncated to %d chars!\n",MaxLen-1);

        memcpy(Answ,Answer,MaxLen-1);

        Answ[MaxLen-1]='\0';

    }

    else

        sprintf(Answ, "%s", Answer);



    //printf("\n AnsLen=%d; MaxLen=%d ",AnsLen,MaxLen);



    return;

}







/*==============================================*/

int             ask_float(char *Query, float *real)

{

    printf("%s ==> ", Query);

    printf("[%g] ", *real);



    (void) fgets(Answer, 80, stdin);



    sscanf(Answer, "%f", real);

    return 0;

}


/*==============================================*/
/*                                              */
/*==============================================*/
int ask_n_float(int n_float, char *Query, float real[])
{
char * answ;
int i;
int n_read=0;

    printf("%s ==> ", Query);

	printf("[");

	for (i=0;i<n_float-1;i++)
        printf("%g ", real[i]);

	printf("%g] ",real[n_float-1]);

    answ= fgets(Answer,sizeof(Answer), stdin);

	if (answ!=NULL)
		{
 	   while((*answ==' ')||(*answ=='\t'))
 	         answ++;
                   
		for (i=0;i<n_float;i++)
			{
         if (sscanf(answ, "%f", &real[i])==1)
                        n_read++;
   	 	   
		
	         while((*answ!=' ')&&(*answ!='\t')&&(*answ!='\0'))
			     answ++;   
			 
              while((*answ==' ')||(*answ=='\t'))
 	         answ++; 
			  
    
              if (*answ=='\0')
					break;
			  }
		

//		printf("\n read %d values\n",n_read);
		}
	else
		printf("\n ask_float: input ERROR! ");

    return 0;
		}

/*==============================================*/

int ask_n_float_bugged(int n_float, char *Query, float *real)

{

    printf("%s ==> ", Query);

    int i,j;

    printf("[");



    for(i=0;i<n_float;i++)

        printf(" %g ", real[i]);



    printf("] ");

    (void) fgets(Answer, 80, stdin);



    j=0;

    for(i=0;i<n_float;i++)

    {

        sscanf(&Answer[j], "%f", &real[i]);



        while( Answer[j] != ' ' && Answer[j] != '\t' && j<80)

            if(Answer[++j] == '\n')

                break;

    }





    return 0;

}





/*==============================================*/



int             ask_double(char *Query, double *Dvalue)

{

    printf("\n%s ==> ", Query);

    printf("[%0.16lg] ", *Dvalue);



    (void) fgets(Answer, 80, stdin);



    sscanf(Answer, "%lf",Dvalue);



    return 0;

}



/*==============================================*/



char            ask_yes_no(char *Query, char def)

{



reask:

    printf("\n%s ==> ", Query);



    if (def != '\0')

        printf("[%c] ", def);



    (void) fgets(Answer, 80, stdin);







    if (Answer[0] == '\n')

        return def;             /* return the default value */



    if ((Answer[0] == 'y') || (Answer[0] == 'Y'))

        return 'y';



    if ((Answer[0] == 'n') || (Answer[0] == 'N'))

        return 'n';



    goto reask;



}

/*==============================================*/

/*                                              */

/*==============================================*/





int             ask_integer(char *Query, int *Value)

{

    printf("\n%s ==> ", Query);

    printf("[%i] ", *Value);



    (void) fgets(Answer, 80, stdin);



    /*-----------------------------------------------

     * use the '%d' format because '%i' will decode

     * integers with leading '0's as octals !!!

     *-----------------------------------------------*/





    sscanf(Answer, "%d",Value);

    return 0;

}

int             ask_long_integer(char *Query, long int *Value)

{

    printf("\n%s ==> ", Query);

    printf("[%ld] ", *Value);



    (void) fgets(Answer, 80, stdin);



    /*-----------------------------------------------

     * use the '%d' format because '%i' will decode

     * integers with leading '0's as octals !!!

     *-----------------------------------------------*/





    sscanf(Answer, "%ld",Value);

    return 0;

}

/*====================================================*/

/*                                                    */

/*====================================================*/





int             ask_short_integer(char *Query, short int *Value)

{

    printf("\n%s ==> ", Query);

    printf("[%hi] ", *Value);



    (void) fgets(Answer, 80, stdin);



    sscanf(Answer, "%hd",Value);

    return 0;

}

/*====================================================*/

/*                                                    */

/*====================================================*/



void _ask_Directory(char * Query,char * DirName,int MaxLen)

{

    char *Ptr;

    int flag;





    struct stat     FileInfo;







    flag=0;

    while(flag==0)

    {



        _askInfo(Query,DirName,MaxLen);



        /* remove the trailing '/' or '\' */



        Ptr=DirName+strlen(DirName)-1;	/* last character */



        if ((*Ptr=='\\')||(*Ptr=='/'))

            *Ptr--='\0';







        if (stat(DirName, &FileInfo)!=0)

            printf("\n Error! Cannot find directory '%s'",DirName);

        else if ((flag=FileInfo.st_mode&S_IFDIR)==0)

            printf("\n Error: '%s' is not a directory!",DirName);







    }



    /* terminate the directory name */



    if (strchr(DirName,'\\')!=NULL)

        *++Ptr='\\';

    else

        *++Ptr='/';



    *++Ptr='\0';

    return;

}





/*====================================================*/

/*                                                    */

/*====================================================*/





int ask_menu(int n_options,char * menu[],int def,char * Query)

{

    int j;



    printf("\n");



    for (j=0;j<n_options;j++)

        printf("\n %d) %s",j+1,menu[j]);



    ask_integer(Query,&def);





    while ((def>n_options)||(def<1))

    {

        printf("\n Invalid selection %d\n",n_options);

        ask_integer(Query,&def);

    }







    return def;

}





/*===========================================================*/

/*                                                           */

/*===========================================================*/



int ReadString(FILE * fp,char *string)



{

#define MAX_LEN 200



    char Line[MAX_LEN];

    char *Ptr;

    char c;



    c=getc(fp); /* read the newline char from previous read */







    if (c!='\n')

        ungetc(c,fp);





    if(fgets(Line,MAX_LEN,fp)==NULL)

        return 0;



    //	printf("\n Line: '%s'",Line);



    //	if (Line[0]=='\n')

    //		fgets(Line,MAX_LEN,fp);









    /*------------------------------

     *  remove the new line char

     *------------------------------*/



    Ptr=Line+strlen(Line)-1;



    if (*Ptr=='\n')

        *Ptr='\0';



    //printf("\n Line: '%s'",Line);



    if (Line[0]=='\'')

    {

        *(Ptr-1)='\0';

        strcpy(string,Line+1);

    }

    else

        strcpy(string,Line);


    //printf("\n String: '%s'",string);

    return 1;

}

