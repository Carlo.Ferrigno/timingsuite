#ifndef _error_h
#define _error_h


#define CODE_SUCCESS      0
#define ERROR_CODE_ALLOC  1
#define ERROR_CODE_INPUT  2
#define ERROR_CODE_OPEN   3
#define ERROR_CODE_RUN    4
#define ERROR_CODE_READ   5
#define ERROR_CODE_WRITE   6

#endif
/*end of _error_h #ifdef*/
