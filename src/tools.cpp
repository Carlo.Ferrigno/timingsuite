/*
 *  tools.cpp
 *  Light_Curve_Analyzer
 *
 *  Created by Carlo Ferrigno on 04/12/09.
 *  Copyright 2009 ISDC/IAAT. All rights reserved.
 *
 */

#include "tools.h"
#include "kuiper.h"
#include "gaussj.h"
#include "fitmrq.h"
#include "ask.h"
#include "event_files.h"
#include "light_curve.h"
#include "binarycor.h"
#include "readline/readline.h"
#include <algorithm>

#include "moment.h"
#include "stattests.h"
#include "erf.h"

#include "fourier.h"
#include "roots.h"

#include "spectrum.h"
#include "mins.h"
#include "custom_mins_ndim.h"


int read_list(vector<string> &list, char *fname)
{
	ifstream d_file(fname);
	if(!d_file.is_open())
	{
		cerr << "Error in opening dat file %"<< fname << "%\n";
		return 1;
	}
	
	int max_line=1024;
	char line[max_line];                                                                                                  
	while(d_file.getline(line, max_line, '\n'))
	{ 
		char scw_tmp[max_line/2];//, pif_tmp[max_line/2];                                                                    
		//sscanf(line, "%s\t%s", scw_tmp, pif_tmp);                                                                          
		if(line[0] != '\n' && line[0] != '\0')                                                                               
		{                                                                                                                    
			sscanf(line, "%s", scw_tmp);                                                                                 
			list.push_back(scw_tmp);
		}                                                                                                                    
	}
	
	d_file.close();                                                                                                              
	
	return 0;
}


void fgauss(const Doub x, VecDoub_I &a, Doub &y, VecDoub_O &dyda) {
	Int i,na=a.size();
	Doub fac,ex,arg;
	y=0.;
	for (i=0;i<na-1;i+=3) {
		arg=(x-a[i+1])/a[i+2];
		ex=exp(-SQR(arg));
		fac=a[i]*ex*2.*arg;
		y += a[i]*ex;
		dyda[i]=ex;
		dyda[i+1]=fac/a[i+2];
		dyda[i+2]=fac*arg/a[i+2];
	}
}


void fsync2(const Doub x, VecDoub_I &a, Doub &y, VecDoub_O &dyda) {
	Int i,na=a.size();
	Doub fac=0,sync=1,arg;
	y=0.;
	for (i=0;i<na-1;i+=4) {
		arg=(x-a[i+1])/a[i+2];
		if(arg==0)
		{
			y+=1;
		}
		else
		{
			//sinc and fac are initialized correctly to trap the syngularity
			sync=sin(arg)/arg;
			y += a[i]*SQR(sync)+a[i+3];
			fac=(arg*cos(arg)-sin(arg))/SQR(arg);
		}
		dyda[i]=SQR(sync);
		dyda[i+1]=a[i]*2*sync*fac*(-1./a[i+2]);
		dyda[i+2]=dyda[i+1]*arg;
		dyda[i+3]=1;
	}
}


int get_best_frequency(vector <double> frequency, vector <double> z2, double &best_freq, double &err_best_freq, double &max_value,char *filename, char flag_out, bool fit_flag)
{
	
	int n=frequency.size();
	VecDoub frequency_v(n), z2_v(n);
	
	for(int i=0;i<n;i++)
	{
		frequency_v[i]=frequency[i];
		z2_v[i]=z2[i];
	}
	
	double err_best_freq_u=0;
	double err_best_freq_d=0;
	
	get_best_frequency(frequency_v, z2_v, best_freq, err_best_freq_u, err_best_freq_d, max_value, filename, flag_out, fit_flag);
	
	err_best_freq=(err_best_freq_u+err_best_freq_d)/2;
	
	return 1;

}


int get_best_frequency( VecDoub frequency, VecDoub z2, double &best_freq, double &err_best_freq_u, double &err_best_freq_d, double &max_peak,char *filename, char flag_out, bool fit_flag)
{	
	
	//flag out = 1 output fit plot on filename
	//flag_out = 0 filename not used 
	int n_bins=frequency.size();
	cout << "Get peak: there are " << n_bins << " frequency bins\n";
	if(n_bins < 2)
	{
		cerr << "Cannot do anything\n";
		return 1;
	}
	
	int max_ind=0;
	double max_value=-10;
	
	for(int i_nu=0;i_nu<n_bins;i_nu++)
	{
		if(z2[i_nu]>max_value)
		{
			max_value=z2[i_nu];
			max_ind=i_nu;
		}
	}
	
	cout << "Maximum is at " << frequency[max_ind] << " Hz\n";
	if (fit_flag)
	{
	
		int max_ind_low=max_ind;
		int max_ind_up=max_ind;
		if(max_ind > n_bins-4 | max_ind <4)
		{
		    printf("Maximum is at the edge of window %d (n_bins = %d), fitting will not converge\n",
			    max_ind, n_bins);
		    return 1;
		}
		max_value/=3.0;
		
		for(int i=max_ind+1;i<n_bins;i++)
		{
			max_ind_up = i;
			if(z2[i] < max_value || z2[i] > 1.01 * z2[i-1])
			{
				break;
			}
		}
		
		for(int i=max_ind-1;i>0;i--)
		{
			max_ind_low = i;
			if(z2[i] < max_value|| z2[i] > 1.01 * z2[i+1])
			{
				break;
			}
		}
		max_value*=3.0;
		Doub sig=(frequency[max_ind] - frequency[max_ind_low])/2.;
		
		max_ind_low = max_ind - 1.0*(max_ind - max_ind_low);
		if(max_ind_low<0)
			max_ind_low=0;
		
		max_ind_up = max_ind + 1.0*(max_ind_up - max_ind);
		if(max_ind_up>=n_bins)
			max_ind_up=n_bins-1;
		
		int n1=max_ind_up-max_ind_low+1;
		VecDoub x(n1), y(n1), dy(n1);
		
		for(int i=0;i<n1;i++)
		{
			x[i] = frequency[max_ind_low+i];
			y[i] = z2[max_ind_low+i];
			dy[i] = sqrt(y[i]);
			if(dy[i]>0.1*y[i])
			    dy[i]=0.1*y[i];
		}
		
		int n_par=4;
		VecDoub aa(n_par);
		aa[0]=max_value;
		aa[1]=frequency[max_ind];
		aa[2]=sig;
		aa[3]=z2[max_ind_low];
		
		cout << "Initial parameters\n";
		cout << "========================================\n";
		for(int i=0;i<n_par;i++)
			cout << aa[i] << "\t";
		
		cout << endl;
		cout << "========================================\n";
		
		Fitmrq fit(x,y,dy,aa,fsync2,1e-4);
		
		try
		{
			fit.fit();
		}
		
		catch (int eee)
		{
		    cerr << "Not performing a fit exception " << eee << "\n";
		    max_peak  = aa[0];
		    best_freq = aa[1];
		    err_best_freq_u = aa[2];
		    err_best_freq_d = aa[2];
		    return 1;
		}
		
		best_freq=fit.a[1];
		max_peak=MIN(fit.a[0], fit.a[0]+fit.a[3]);
		
		double min_chi=fit.chisq;
		
		cout << "Found minimum chi2 = " << min_chi << endl;
		cout << "Final parameters\n";
		cout << "========================================\n";
		for(int i=0;i<n_par;i++)
			cout << fit.a[i] << "\t";
		
		cout << "\n========================================\n";
		
		if(flag_out)
		{
		
			ofstream ff(filename);
			
			ff << "read serr 2\n";
			ff<<"cpd /xw\n";
			ff <<"ma 1 on 2\n";
			ff <<"lw 2\n";
			ff <<"cs 1.1\n";
			ff <<"font roman\n";
			ff <<"time off\n";
			ff <<"lab title\n";
			ff <<"lab x Frequency [Hz] - " << best_freq << "\n";
			
			ff << "lab y Z\\u2\\d\n";
			
			ff << "lab file " << fit.a[0] << " " << fit.a[1] << " " << fit.a[2] <<" " << fit.a[3] << "\n";
			
			for(int i=0;i<n1;i++)
			{
				ff << scientific << setw(19) << setprecision(12) << x[i]-best_freq << "\t";
				ff << scientific << setw(19) << setprecision(12) <<y[i] ;
				ff << scientific << setw(19) << setprecision(12) << dy[i] << "\t";
				double gg=y[i];
				fsync2(x[i], fit.a, gg, aa);
				ff << scientific << setw(19) << setprecision(12) << gg << "\n";
			}
			
			ff.close();
		}
		
		cout << "Look for the 68% ( chi2 + 1.0 ) c.l.\n";
		double chi_max=min_chi + 1.0;
		fit.hold(1,best_freq);
		double step=0.1*sqrt(fit.covar[1][1]);
		double sigma_up=0;
		for(int i=0;i<100;i++)
		{
			sigma_up=best_freq+i*step;
			fit.hold(1,sigma_up);
			fit.fit();
			if(fit.chisq > chi_max)
			{
				cout << "Upper limit = " << sigma_up << endl;
				break;
			}
			//cout << i <<endl;
		}
		
		double sigma_down=0;
		for(int i=0;i<100;i++)
		{
			sigma_down=best_freq-i*step;
			fit.hold(1,sigma_down);
			fit.fit();
			if(fit.chisq > chi_max)
			{
				cout << "Lower limit = " << sigma_down << endl;
				break;
			}
			//cout << i <<endl;
		}
		
		err_best_freq_u = sigma_up;
		err_best_freq_d=sigma_down;
	}
	else
	{
		best_freq = frequency[max_ind];
                err_best_freq_u = best_freq + (frequency[max_ind+1] - best_freq)/2.;
                err_best_freq_d = best_freq - (best_freq - frequency[max_ind-1])/2.;

	}
	
	cout<< scientific << setw(19) << setprecision(12) << "Frequency [Hz] = " << best_freq << " + " << err_best_freq_u - best_freq << " - " << best_freq - err_best_freq_d << endl;
	cout<< scientific << setw(19) << setprecision(12) << "Period    [s]  = " << 1./best_freq << " + " << 1./err_best_freq_d - 1./best_freq << " - " << 1./best_freq - 1./err_best_freq_u << endl;
	cout<< scientific << setw(19) << setprecision(12) << "Period    [d]  = " << 1./best_freq / 86400 << " + " << (1./err_best_freq_d - 1./best_freq) / 86400 << " - " << (1./best_freq - 1./err_best_freq_u) / 86400 << endl;

	return 0;
}

int make_z2_stat_gb(vector<string> list, char evt_flag, double e_min, double e_max, char flag_err, char flag_binarycor,
        char *binary_file, char log_spacing, double nu_start, double nu_stop, int n_bins,
        int n_harm, int flag_out, const char *filename, double mjdref, double t_ref,
        double &best_freq, double &err_best_freq, double &max_z2, long int n_events_per_chunk,
        double min_sn , char int_bary, double ra_obj, double dec_obj, char *og_name,
        double max_time_separation, double min_significance, char do_simul, int nruns,
        double max_time_separation_simul, char flag_out_simul, double tolerance)
{
    
    VecDoub frequency(n_bins), z2(n_bins), lz2(n_bins), sig(n_bins);
    double step=(nu_stop-nu_start)/(n_bins);
    double t_start=1e10,t_stop=-1e10;
    int counter=0;
    int n_events=0;
    
    if(log_spacing == 'n')
    {
            for(int i=0;i<n_bins;i++)
            {
                    frequency[i]=nu_start+(i+0.5)*step;
            }
    }
    else
    {
            double tmp=log10(nu_start);
            step=(log10(nu_stop)-tmp)/n_bins;
            for(int i=0;i<n_bins;i++)
            {
                    frequency[i]=pow(10,tmp+(i+0.5)*step);
            }
    }


    for(int i_nu=0;i_nu<n_bins;i_nu++)
    {
            z2[i_nu]=0;
            lz2[i_nu]=0;
    }

    int n_files=list.size();
    int stopped_event=0;
    for(int k=0;k<n_files;k++)
    {
            event_file ef;
            if(evt_flag==1)
            {
                ef.open_fits(list[k].c_str());
                ef.output();
                if (ef.t_start() < t_start)
                    t_start=ef.t_start();
                if(ef.t_stop() > t_stop)
                    t_stop=ef.t_stop();
                n_events += ef.size();
                
                for(int i_nu=0;i_nu<n_bins;i_nu++)
                {
                    for (int j=0;j<n_harm;j++)
                    {
                        lz2[i_nu] += ef.get_Rk2((j+1),2*M_PI*frequency[i_nu], t_ref, flag_err, flag_binarycor, e_min,  e_max, 
                        n_events_per_chunk, stopped_event,  int_bary,  ra_obj,  dec_obj, 
                         og_name, max_time_separation);
                    }
                }
                
            }
            else
            {
                light_curve ll(list[k].c_str());
                ll.output();
                if (ll.t_start() < t_start)
                    t_start=ll.t_start();
                if(ll.t_stop() > t_stop)
                    t_stop=ll.t_stop();
                for(int i_nu=0;i_nu<n_bins;i_nu++)
                {
                    for (int j=0;j<n_harm;j++)
                    {
                        lz2[i_nu] += ll.get_Rk2((j+1),2*M_PI*frequency[i_nu], t_ref, flag_err);
                        //cout << frequency[i_nu] << " " << lz2[i_nu] << endl;
                    }
                }
                
            }

    }//end of loop on files

    
    return output_z2_stat(frequency, z2, lz2, sig,  log_spacing,  nu_start,
            nu_stop,   n_bins,  n_harm,  flag_out,
            filename,  t_ref,  best_freq,  err_best_freq,  max_z2,  counter,
            min_significance, n_events ,  t_stop,  t_start);
}

int make_z2_stat(vector<string> list, char evt_flag, double e_min, double e_max, char flag_binarycor,
        char *binary_file, char log_spacing, double nu_start, double nu_stop, int n_bins,
        int n_harm, int flag_out, const char *filename, double mjdref, double t_ref,
        double &best_freq, double &err_best_freq, double &max_z2, long int n_events_per_chunk,
        double min_sn , char int_bary, double ra_obj, double dec_obj, char *og_name,
        double max_time_separation, double min_significance, char do_simul, int nruns,
        double max_time_separation_simul, char flag_out_simul, double tolerance)
{

//	char do_simul=1;
//	int nruns=100;
//	int status=0;
    int n_files=list.size();
    char do_simul_lc='n';
    int nruns_lc=0;

    if(do_simul == 'y' && evt_flag == 2)
    {
            do_simul_lc=do_simul;
            nruns_lc =nruns;
    }

    if(flag_binarycor == 'y')
    {
            //this should be already ok
            char setup_file_exist=1;
            char interactive_flag=0;
            binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, mjdref );
    }

    vector<double> times;
    double n_events=0;
    int stopped_event=nruns_lc; //This does not affect the events, but only the lc
    int counter=0;
    vector <double> tstops;
    vector <double> tstarts;
    vector <char> stop_flag;
    if (evt_flag == 1) {
        for (int k = 0; k < n_files; k++) { //Scan files
            cout << "Scan files\n";
            event_file ef;
            ef.open_fits(list[k].c_str());
            tstops.push_back(ef.t_stop());
            tstarts.push_back(ef.t_start());
            ef.close_fits();
        }
        for (int k = 0; k < n_files-1; k++) { //Scan files
            if(tstarts[k+1] - tstops[k] >= max_time_separation)
                stop_flag.push_back(1);
            else
                stop_flag.push_back(0);
        }
        stop_flag.push_back(1);
        
        cout << "**************************************************\n";
        for (int k = 0; k < n_files; k++) {
            cout << list[k] << " " << tstarts[k] << " " << tstops[k] << " " << (int)stop_flag[k] << endl;
        }

        cout << "**************************************************\n";

    }


    for(int k=0;k<n_files;k++)
    {
            stopped_event=nruns_lc; //This does not affect the events, but only the lc
            event_file ef;
            if(evt_flag==1)
            {
                    ef.open_fits(list[k].c_str());
                    ef.output();
            }
            while(stopped_event>=0)
            {
                    if(evt_flag==1)
                    {
                            //times.clear();
                            stopped_event=ef.get_times( times, 'n', e_min, e_max, n_events_per_chunk,stopped_event,int_bary, ra_obj, dec_obj,og_name,max_time_separation);
                            cout << "Read events stooped at #" << stopped_event << endl;

                    }
                    else
                    {
                            light_curve ll(list[k].c_str());
                            if(stopped_event == nruns_lc)  //????????
                            {
                                    ll.get_times(times, min_sn);
                                    ll.output();
                            }
                            else
                            {
                                    cout << " Simulation run " << nruns_lc - stopped_event << endl;
                                    light_curve ls=simul_gaus(ll);
                                    ls.get_times(times,min_sn);
                            }
                            stopped_event--; //????????
                    }
                    //continue;

                    int n=times.size();
                    if(flag_binarycor == 'y')
                    {
                            cout << "Perform binary correction\n";
                            for(int i=0;i<n;i++)
                            {
                                    //cout << times[i] << "\t";
                                    //double ata=times[i];
                                    times[i]=binarycor(times[i]);
                                    //cout << times[i]-ata << "\n";
                            }
                    }


                    n_events+=times.size();
                    if(n >= n_events_per_chunk)// || stop_flag[k] > 0 )
                    {
                            //call the Z2 routine
                            cout << "File " << list[k].c_str() << " about to elaborate " << n << " events. Counter " << counter+1 << endl;

                            int status=actual_make_z2_stat( times, log_spacing, nu_start, nu_stop, n_bins, n_harm, flag_out, filename, t_ref, best_freq, err_best_freq,max_z2, counter, min_significance );
                            if(do_simul=='y' && evt_flag==1 && status==0)
                                    make_simul_Z2(times, log_spacing, nu_start, nu_stop, n_bins, n_harm,
                                            flag_out_simul, filename, t_ref, best_freq, err_best_freq,
                                            counter, min_significance, nruns,  max_time_separation_simul, tolerance);
                            times.clear();
                            n_events=0;
                    }

            }//end on loop on chunks
            if(evt_flag==1)
            {
                    ef.close_fits();
            }

    }//end of loop on files

    if(times.size() > 1)
    {
            //callZ2 routine
            cout << "Making the final iteration, counter " << counter+1 <<"\n";
            int status=actual_make_z2_stat( times, log_spacing, nu_start, nu_stop, n_bins, n_harm, flag_out, filename, t_ref, best_freq, err_best_freq, max_z2,counter, min_significance );
            if(do_simul=='y' && evt_flag==1 && status==0)
                    make_simul_Z2(times, log_spacing, nu_start, nu_stop, n_bins,
                            n_harm, flag_out_simul, filename, t_ref, best_freq, err_best_freq,
                            counter, min_significance, nruns, max_time_separation_simul, tolerance);
    }
    times.clear();
    return 0;
}


int make_z2_stat_slide_time_window(vector<string> list, char evt_flag, double e_min, double e_max, 
        char flag_binarycor, char *binary_file, char log_spacing, double nu_start, double nu_stop,
        int n_bins, int n_harm, int flag_out, const char *filename, double mjdref,
        double t_ref, double &best_freq, double &err_best_freq, double &max_z2, double interval,
        double step_interval, double min_sn, double min_significance)
{
	
//	int status=0;
	int n_files=list.size();
	
	if(evt_flag==0)
	{
		cerr <<"Sliding window not implemented for light curve\n";
		return 1;
	}
	
	if(flag_binarycor == 'y')
	{
		//this should be already ok
		char setup_file_exist=1;
		char interactive_flag=0;
		binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, mjdref );
	}
	
	vector<double> times;
	double n_events=0;
	int counter=0;
	
	for(int k=0;k<n_files;k++)
	{
		event_file ef;
		if(evt_flag==1)
		{
			ef.open_fits(list[k].c_str());
			ef.output();
			ef.read_times();
		}
		
		double t1=ef._t_start;
		double t2=t1+interval;
		
		if(t2 > ef._t_stop)
			t2= ef._t_stop;
		
		while(t2<=ef._t_stop)
		{
			if(evt_flag==1)
			{
				times.clear();
                               //cout << t1 << " " << t2 << endl;
				if(ef.get_times_tlim( times, t1,t2))
				{
					cerr <<"Trouble in getting event times\n";
					waitkey();
				}
			}
			else
			{
//				light_curve ll(list[k].c_str());
//				ll.get_times(times, min_sn);
//				ll.output();
//				stopped_event=-1;

				;
			}
			//continue;
			
			int n=times.size();
                        //cout << "N " << n << endl;
                        if(n >0)
                           cout << "Extracted " << n << " events\n";
                        
			if(flag_binarycor == 'y')
			{
				cout << "Perform binary correction\n";
				for(int i=0;i<n;i++)
				{
					//cout << times[i] << "\t";
					//double ata=times[i];
					times[i]=binarycor(times[i]);
					//cout << times[i]-ata << "\n";
				}
			}
			
			
			n_events+=times.size();
			if(times.size() >= 10)
			{
				//call the Z2 routine
				cout << "File " << list[k].c_str() << " about to elaborate " << n << " events. Counter " << counter+1 << endl;
				
				actual_make_z2_stat( times, log_spacing, nu_start, nu_stop, n_bins, n_harm, flag_out,
                                        filename, t_ref, best_freq, err_best_freq, max_z2, counter, min_significance );
				times.clear();
				n_events=0;
			}
			t1+=step_interval;
			t2+=step_interval;

		}//end on loop on chunks
												
		if(evt_flag==1)
		{
			ef.close_fits();
		}
		
	}//end of loop on files
	
	if(times.size())
	{
		//callZ2 routine
		cout << "Making the final iteration, counter " << counter+1 <<"\n";
		actual_make_z2_stat( times, log_spacing, nu_start, nu_stop, n_bins, n_harm, flag_out, filename, t_ref, best_freq, err_best_freq, max_z2, counter, min_significance );
	}
	times.clear();
	return 0;
}

int make_simul_Z2(vector <double> times, char log_spacing, double nu_start, double nu_stop, int n_bins, int n_harm,
        int flag_out, const char *filename, double t_ref, double &best_freq, double &err_best_freq,int &counter,
        double min_significance, int nruns, double max_time_separation, double tolerance)
{
	const Doub ACC=1.0e-11;
	char new_filename[FLEN_FILENAME];
        double reference_freq=best_freq;
	const int n_pulse_bins=16;
	int nn=times.size();
	if (nn < n_pulse_bins * 4)
	{
		cout << "Not performing simulations, too few events " << nn << endl;
		//waitkey();
		return 2;
	}
        if(n_harm > n_pulse_bins/2 )
        {
		cout << "Not performing simulations, too many harmonics " << n_harm << endl;
		//waitkey();
		return 2;
        }
	vector <double> sorted_times=times, simul_times=times, sorted_simul_times=times,
                local_tstart, local_tstop;

	// Sort the vector using predicate and std::sort
	std::sort(times.begin(), times.end());
	
	// Dump the vector to check the result
	int i=0;
	for (vector<double>::const_iterator citer = times.begin();citer != times.end(); ++citer)
	{
		sorted_times[i++]=*citer;
		//cout << (*citer) -t_start << " " << times[i-1] -t_start << endl;
	}
	
	
	
	local_tstart.push_back(sorted_times[0]);

	
	for(i=1;i<nn;i++)
	{
	//	simul_times.push_back(sorted_times[i]);
//		sorted_simul_times.push_back(sorted_times[i]);
//		
//		if(i>0)
		{
			if( (sorted_times[i] - sorted_times[i-1]) > max_time_separation)
			{
				cout << "GTI break " << i << " " << sorted_times[i-1] -t_ref << " " << sorted_times[i] -t_ref << endl;
				local_tstart.push_back(sorted_times[i]);
				local_tstop.push_back(sorted_times[i-1]);
//				waitkey();
			}
		}
		
	}
	local_tstop.push_back(times[nn-1]);
	
	int n_GTIs=local_tstop.size();

	cout << "Computed GTI simulation, found " << n_GTIs << " GTI intervals\n";
	
	double *gti_start=new double [n_GTIs];
	double *gti_stop=new double [n_GTIs];
	double t_start=sorted_times[0];
	double t_stop=sorted_times[nn-1];

	double deltat=0;
	for (int i=0;i<n_GTIs;i++)
	{
		gti_start[i]=local_tstart[i];
		gti_stop[i]=local_tstop[i];
		cout << i << " " << gti_start[i] -t_start << " " << gti_stop[i]-t_start << endl;
		deltat+=gti_stop[i]-gti_start[i];
		
	}
	
	double *gti_exposure=compute_phase_exposure(t_ref, best_freq, 0 , 0 ,n_GTIs, n_pulse_bins, gti_start,gti_stop );
		
	Ranq1 myran(time(NULL));

	//double deltat=t_stop-t_start;
	double *frequencies= new double[nruns];
	double *s_frequencies= new double[nruns];
        double *max_z2s= new double[nruns];
	for(int i=0;i<nruns;i++)
	{
		//Initialize to avoid troubles in case, things are not found
		Normaldev ndev(best_freq, err_best_freq/1.5, time(NULL));

		frequencies[i]=best_freq + ndev.dev();
		s_frequencies[i]=0;
                max_z2s[i]=0;

	}

	
	
	cout << "Computing pulse profile in snapshot\n";
	VecDoub pulse(n_pulse_bins, 0.0);
	
	for(int i=0;i<nn;i++)
	{
		double i_phase;
		double phase=get_phase_complete(times[i], t_ref, best_freq, 0, 0, &i_phase);
		int ind=(int) (phase*n_pulse_bins);
		//cout << ind << endl;
		pulse[ind]++;
	}
	
	double A0=0;
	VecDoub A1(n_harm, 0.0), phi(n_harm,0.0), I_S(n_harm,0.0), I_C(n_harm,0.0), rms(n_harm,0.0);
	
	for (int i=0;i<n_pulse_bins;i++)
	{
		//Normalize for the pulse exposure
		
		if(gti_exposure[i]>0)
			pulse[i]/=gti_exposure[i];
		else
			pulse[i]=0;

		//Fourier coefficients
		A0+=pulse[i];
		for(int j=1;j<=n_harm;j++)
		{
			double p_phase=(2.0*M_PI * (float)j* ((float)i / (float)n_pulse_bins));
			I_C[j-1]+=pulse[i]*cos(p_phase);
			I_S[j-1]+=pulse[i]*sin(p_phase);
		}
	}

	for(int j=0;j<n_harm;j++)
	{

		A1[j]=sqrt(SQR(I_C[j])+SQR(I_S[j]));
		phi[j]=atan2(I_S[j],I_C[j]);
		rms[j] = A1[j]/A0;
	}
	//
	double omega=2.*M_PI*best_freq;
	
	char pulse_name[FLEN_FILENAME];
	sprintf(pulse_name, "%s_%03d_pulse.qdp", filename, counter);
	ofstream fp(pulse_name);
	fp << "read serr 1 2" << endl;
	fp << "cpd /xw\n";
        fp << "color off 4\n";
	A0/=n_pulse_bins;
	for (int i=0;i<n_pulse_bins;i++)
	{
		double pulse_calc=A0;
		for(int j=1;j<=n_harm;j++)
		{
			double p_phase=(2.0*M_PI * (float)j * ((float)i / (float)n_pulse_bins));
                        //double p_phase_2=(2.0*M_PI * (float)(n_pulse_bins-j) * ((float)i / (float)n_pulse_bins));
                        //double p_phase_2=(2.0*M_PI * (float)(0.0-j) * ((float)i / (float)n_pulse_bins));

			pulse_calc+= A0 * rms[j-1] * (cos(p_phase - phi[j-1]) *2);// + cos(p_phase_2 + phi[j-1]));
		}
		if(gti_exposure[i]>0)
			fp << i << " 0.5 " << pulse[i] << " " << sqrt(pulse[i]/gti_exposure[i]) << " " << pulse_calc <<  " " << gti_exposure[i] << endl;
		else
			fp << i << " 0.5 " << pulse[i] << " 0.0 " << pulse_calc << endl;
//		fp << p_phase << " " << M_PI/n_pulse_bins  << " " << pulse[i] << " " << sqrt(pulse[i]) << " " << pulse_calc << endl;
	}
	
	fp.close();

        //This is the effect of discretization
        phi += 0.5/n_pulse_bins*2.0*M_PI;
	
	
	cout << "Simulating with order rms  phi  omega " <<  endl;
	for (int j=0;j<n_harm;j++)
	{
		cout << j+1 << " " << rms[j] << " " << phi[j] << " " << omega*(j+1) << endl;
	}
	cout << endl;
	
	for (int j=0;j<nruns;j++)
	{

		//sprintf(new_filename, "sim_test_evt_%03d_%s.qdp", j, filename);		
//		ofstream ff(new_filename);
//		ff << "read\ncpd /xw\nma 17 on\nplot vert\n";
            cout << "Simulation run #" << j << " of " << nruns << endl;
            for(int i=0;i<nn;i++)
            {
                    double tt=myran.doub()*deltat;
                    for (int k=1;k<n_GTIs;k++)
                    {
                            if(tt>gti_stop[k-1]-t_start)
                                    tt+=gti_start[k]-gti_stop[k-1];
                            else
                                    break;
                    }
                    tt += t_start - t_ref; //Numerics can be an issue !
                    Cumulative_Sin_Deviate cms(rms, omega, tt, phi);
                    simul_times[i]=zbrent(cms,tt-M_PI/omega,tt+M_PI/omega,ACC);
                    simul_times[i] += t_ref;
            //	ff << scientific << setw(19) << setprecision(12)  << tt-t_start << " " << simul_times[i] - t_start << " " << simul_times[i] - tt << endl;
            }
            cout << "T_REF is   " << t_ref << endl;
            cout << "T_START is " << t_start << endl;

            //ff.close();

            // Sort the vector using predicate and std::sort
            std::sort(simul_times.begin(), simul_times.end());


            int i=0;
            for (vector<double>::const_iterator citer = simul_times.begin();citer != simul_times.end(); ++citer)
            {
                    sorted_simul_times[i++]=*citer;
                    //cout << (*citer) -t_start << " " << times[i-1] -t_start << endl;
            }


            counter--;
            s_frequencies[j]=actual_make_z2_stat( sorted_simul_times, log_spacing,
                    nu_start, nu_stop, n_bins, n_harm, flag_out, new_filename, t_ref,
                    frequencies[j], s_frequencies[j], max_z2s[j], counter, min_significance );
//            if(s_frequencies[j] != 0)
//            {
//                waitkey();
//                cout << endl;
//            }
            //use s_frequencies to store the return value
            if(flag_out)
            {
                    sprintf(new_filename, "sim_evt_%03d_%s_%04d.qdp", j, filename, counter);
                    pulse.assign(n_pulse_bins, 0.0);

                    ofstream ff(new_filename);
                    ff << "read\ncpd /xw\nma 17 on\n";
                    for(int i=0;i<nn;i++)
                    {
                        ff << scientific << setw(19) << setprecision(12)  <<  sorted_simul_times[i] - t_ref << " " << sorted_times[i] - t_ref << endl;
                        double i_phase;
                        double phase = get_phase_complete(sorted_simul_times[i],
                                t_ref, best_freq, 0, 0, &i_phase);
                        int ind = (int) (phase * n_pulse_bins);
                        //cout << ind << endl;
                        pulse[ind]++;
                    }
                    ff.close();

                    sprintf(new_filename, "sim_pulse_%03d_%s_%04d.qdp", j, filename, counter);
                    ff.open(new_filename);
                    ff << "read serr 1 2\ncpd /xw\nplot vert\n";
                    for (int i = 0; i < n_pulse_bins; i++)
                    {
                    //Normalize for the pulse exposure

                        if (gti_exposure[i] > 0)
                        {
                            pulse[i] /= gti_exposure[i];
                            ff<< (float)i << " 0.5 " << pulse[i] << " " << sqrt(pulse[i]/gti_exposure[i]) << " " << gti_exposure[i] << endl;
                        }
                        else
                        {
                            pulse[i] = 0;
                            ff<< (float)i << " 0.5 " << pulse[i] << " 0 " << gti_exposure[i] << endl;
                        }



                    }
                    ff.close();
            }//if on output
		
	}//loop on runs
	
	float n_reject=0;
	
	
	sprintf(new_filename, "sim_%s_%04d.qdp", filename, counter);
	ofstream ff;
	ff.open(new_filename, ios::out | ios::trunc);
	if(!ff.is_open())
	{	
		cerr << "Cannot open " << new_filename << " return\n";
		return 1;
	}

        vector<double> dist_frequencies;

	ff<< scientific << setw(19) << setprecision(12)  << "read serr 2 3\nplot vert\nlab file\ntime off\nlab x #realization\nlab y2 Fr [Hz]\nlab y3 P [s]\ncpd /xw\nma 17 on\n";
        ff << "win 2\nLAB  2 COL 3 LIN 0 100 JUS Lef\nLAB  2 POS 0 "<< best_freq << "\""<<best_freq <<"\"\n";
	ff << "win 3\nLAB  3 COL 3 LIN 0 100 JUS Lef\nLAB  3 POS 0 "<< 1./best_freq << "\""<<1./best_freq <<"\"\n";
	for (int j=0;j<nruns;j++)
	{
		if(s_frequencies[j] != 0)// || fabs(frequencies[j]/best_freq-1)> tolerance )
                    n_reject++;
		else
		{
                    ff << scientific << setw(19) << setprecision(12)  << j << "\t" << frequencies[j] << "\t" << s_frequencies[j] << "\t";
                    ff << scientific << setw(19) << setprecision(12) << 1./frequencies[j] << "\t";
                    dist_frequencies.push_back(fabs(frequencies[j]-best_freq));
                    ff << scientific << setw(19) << setprecision(12) << fabs(frequencies[j]-best_freq) << endl;
		}
	}

        std::sort(dist_frequencies.begin(), dist_frequencies.end());




       	cout << "Not performed " << n_reject << " runs for Z^2 non convergence\n";
	if(n_reject > nruns/5)
		cout <<"WARNING :: solution might be innacurate\n";

   //     sort_nr(dist_frequencies);

        int n_threshold=(int)floor((nruns-n_reject)*0.9);

        vector<double>::const_iterator citer = dist_frequencies.begin()+n_threshold;

        double dist_threshold=*citer; //this corrextion facotr accounts for the cut o the distribution

        dist_frequencies.clear();

        cout << "Using frequencies which differ less than " << dist_threshold << " from the expected one to exclude the distribution outlyiers\n";
        cout << "Using " << n_threshold << " determinations out of " << (int)(nruns - n_reject) << endl;
        VecDoub temp_frequencies(n_threshold, best_freq);
        //Now compute the average and standard deviation on the smaller sample to exclude outlters
	Doub new_ave=0,new_var=0;	
	
	int new_j=0;
        n_reject=0; // reset n_reject
	for (int j=0;j<nruns;j++)
	{
		if(s_frequencies[j] == 0 && fabs(frequencies[j]-best_freq) <= dist_threshold  )
		{
                    temp_frequencies[new_j++]=frequencies[j];
                }
		
		if(new_j == n_threshold)
		{
			//cerr << "Error in building temporary array of simulated frequencies\n";
			//waitkey();
                        cout << endl;
			break;
		}
	}
        if(new_j < n_threshold)
        {
            cerr << "Error :: too few simulated frequencies in computing temporary average and stddev " << new_j << "\n";
            waitkey();
        }


	
	avevar(temp_frequencies, new_ave, new_var);

        //See http://helpdesk.graphpad.com/faq/viewfaq.cfm?faq=1381
        // for definition of stddev confidence interval
        
	new_var=1.34*tolerance*sqrt(new_var);
        //This is the significance in sigmas ,the 1.07 factor is to account for the reduced sample in a conservative way
        //This 1.34 is for a typical 1000 runs (26% is the reduction 8% the inaccurancy of the sample stddev)
        //formula is difficult to implement and not worth the precision of this exercise
	cout << "The standard deviation of the reduced sample is " << new_var/tolerance << endl;
        ff << "win 2\nLAB  4 COL 4 LIN 0 100 JUS Lef\nLAB  4 POS 0 "<< best_freq-new_var << "\""<<best_freq-new_var <<"\"\n";
        ff << "win 2\nLAB  5 COL 4 LIN 0 100 JUS Lef\nLAB  5 POS 0 "<< best_freq+new_var << "\""<<best_freq+new_var <<"\"\n";
	ff << "win 3\nLAB  6 COL 4 LIN 0 100 JUS Lef\nLAB  6 POS 0 "<< 1./(best_freq-new_var) << "\""<<1./(best_freq-new_var) <<"\"\n";
        ff << "win 3\nLAB  7 COL 4 LIN 0 100 JUS Lef\nLAB  7 POS 0 "<< 1./(best_freq+new_var) << "\""<<1./(best_freq+new_var) <<"\"\n";
        ff.close();
        //Close Simulation file
 //      	VecDoub i_frequencies(nruns-n_reject, 0.0);


       //Now exclude outlters and compute refined error and significance of detection


	
        n_reject=0; // reset n_reject
        double refined_stddev=0;
	double refined_mean=0;
	for (int j=0;j<nruns;j++)
	{
            if(s_frequencies[j] != 2)
            {
		if( fabs(frequencies[j]-new_ave) <= new_var  )
		{
                    dist_frequencies.push_back(frequencies[j]);
                    refined_stddev+=SQR(frequencies[j]-new_ave);
                    refined_mean+=frequencies[j];
		}
                else
                    n_reject++;
            }//use only real rejections, not non convergence !
		
	}

        int n_good=dist_frequencies.size();
        refined_stddev=sqrt(refined_stddev/(n_good-1));
        refined_mean/=n_good;
        
        double number_of_expected_outlyers=erfc(tolerance/sqrt(2))*nruns;
        //number_of_expected_outlyers+=sqrt(number_of_expected_outlyers); //add its variance
        cout << "Number of rejections " << n_reject << endl;
        cout << "Number of expected outliers " << number_of_expected_outlyers << endl;
        if(n_reject > number_of_expected_outlyers+tolerance*sqrt(number_of_expected_outlyers+n_reject))
        {
            cout << "WARNING :: innacurate detection !!!\n";
            
        }


        ////////////////

	sprintf(new_filename, "%s_simul.qdp", filename);
	if(counter == 1)
	{	
		ff.open(new_filename, ios::out | ios::trunc);
		if(!ff.is_open())
		{	
			cerr << "Cannot open " << new_filename << " return\n";
			return 1;
		}
		ff<< scientific << setw(19) << setprecision(12)  << "read terr 1 2 3\nplot vert\nlab file\ntime off\nlab x t [s -"<< t_ref << "]\nlab y2 Fr [Hz]\nlab y3 P [s]\nlab y4 %reject\ncpd /xw\nma 17 on\n";
		
	}
	else
	{
		
		ff.open(new_filename, ios::out | ios::app);
		if(!ff.is_open())
		{	
			cerr << "Cannot open " << new_filename << " return\n";
			return 1;
		}
	}

        cout << "Measured frequency "<< scientific << setw(19) << setprecision(12)  << best_freq << endl;
	cout << "Frequency simulated "<< scientific << setw(19) << setprecision(12)  << new_ave << endl;
        cout << "Refined frequency simulated "<< scientific << setw(19) << setprecision(12)  << refined_mean << endl;
	cout << "Final Err freq  simulated "<< scientific << setw(19) << setprecision(12)  << refined_stddev << endl;
	
	cout << "Difference frequency simulated "<< scientific << setw(19) << setprecision(12)  << (new_ave -best_freq)  << endl;

        cout << "Difference frequency simulated (sigmas)"<< (new_ave -best_freq)/refined_stddev << endl;
        cout << "Fraction of rejections " <<  n_reject/nruns << endl;
        cout << "Expected fraction of rejections " <<  number_of_expected_outlyers/nruns << endl;

        ff << "!Measured frequency "<< scientific << setw(19) << setprecision(12)  << best_freq << endl;
	ff << "!Final frequency simulated "<< scientific << setw(19) << setprecision(12)  << new_ave << endl;
	ff << "!Final Err freq  simulated "<< scientific << setw(19) << setprecision(12)  << new_var << endl;

	ff << "!Difference frequency simulated "<< scientific << setw(19) << setprecision(12)  << (new_ave -best_freq)  << endl;

        ff << "!Difference frequency simulated (sigmas)"<< (new_ave -best_freq)/new_var << endl;
        ff << "!Fraction of rejections " <<  n_reject/nruns << endl;
        ff << "!Expected fraction of rejections " <<  number_of_expected_outlyers/nruns << endl;

        if( fabs((new_ave -best_freq)/new_var) > 3)
        {
            cout << "WARNING :: inconsistent central value in simulation !\n";
            ff << "!WARNING :: inconsistent central value in simulation !\n";
        }
	
	//best_freq=new_ave;
	err_best_freq=new_var;
	double err_best_freq_u=best_freq+new_var;
	double err_best_freq_d=best_freq-new_var;
	
	if(n_reject/nruns<0.9)
	{
		ff<< scientific << setw(19) << setprecision(12)  << (t_stop+t_start)/2 -t_ref << "\t" <<
                        (t_stop-t_start)/2 << "\t" << (-t_stop+t_start)/2 << "\t" << best_freq << "\t" <<
                        err_best_freq_u - best_freq << "\t" << -best_freq + err_best_freq_d << "\t";
		ff<< scientific << setw(19) << setprecision(12) << 1./best_freq << "\t" <<
                        1./err_best_freq_d - 1./best_freq << "\t" << -1./best_freq + 1./err_best_freq_u <<
                        " " << n_reject/nruns << " " << number_of_expected_outlyers/nruns << endl;
	}
	ff.close();
	delete [] frequencies;
	delete [] s_frequencies;
	delete [] gti_start;
	delete [] gti_stop;
	delete [] gti_exposure;
	return 0;
}

int actual_make_z2_stat(vector <double> times, char log_spacing, double nu_start,
        double nu_stop, int n_bins, int n_harm, int flag_out,
        const char *filename, double t_ref, double &best_freq, double &err_best_freq, double &max_z2, int &counter,
        double min_significance=2.5)
{

	int n_events=times.size();
	double n_cycles=((times[n_events-1]-times[0])*nu_start);
        double nu_zero=1./(times[n_events-1]-times[0]);
        
	//cout << "There are  "<< (n_events /n_cycles) << " events per cycle\n";
        cout << "N events " << n_events << endl;
        cout << "Tstart-Tstop " << times[n_events-1] << " - " << times[0] <<endl;
	cout << "There are  "<< floor(n_cycles) << " cycles\n";
        cout << "Indipendent Fourier bins " << (nu_stop-nu_start)/nu_zero << " with " << n_bins << " searched bins\n";
        
	if(n_events / n_cycles < 0.0 || n_cycles < 2.2 || n_events < 10)
	{
		cerr << times[0] << " " << times[n_events-1] << " " << (times[n_events-1]-times[0]) << endl;
		cerr << "Warning :: Few events " << n_events << " check your analysis analysis\n";
		cerr << n_cycles<< " cycles\n";
		//waitkey();
		return 1;
	}
        counter++;
	
	cout << "Elaborate " << n_events << " events\n";
	

	VecDoub frequency(n_bins), z2(n_bins), lz2(n_bins), sig(n_bins);
	double step=(nu_stop-nu_start)/(n_bins);
	
	if(log_spacing == 'n')
	{
		for(int i=0;i<n_bins;i++)
		{
			frequency[i]=nu_start+(i+0.5)*step;
		}
	}
	else
	{
		double tmp=log10(nu_start);
		step=(log10(nu_stop)-tmp)/n_bins;
		for(int i=0;i<n_bins;i++)
		{
			frequency[i]=pow(10,tmp+(i+0.5)*step);
		}
	}
	
	
	for(int i_nu=0;i_nu<n_bins;i_nu++)
	{
		z2[i_nu]=0;
		lz2[i_nu]=0;
	}
	
	double t_start=times[0];
	double t_stop=times[n_events-1];
	
	
	for(int i_nu=0;i_nu<n_bins;i_nu++)
	{
		
		for(int j=1;j<=n_harm;j++)
		{
			double C=0, S=0;
                        /*
                         * I cannot remember what all this is, so I comment it out 11.2016
                        double t1=times[0]-t_ref;
                        double t2=times[n_events-1]-t_ref;
                        double T=t2-t1;
                        double tmp1=1./(2*M_PI*frequency[i_nu]*j*T);
                        double mC=tmp1*(sin(2*M_PI*frequency[i_nu]*j*t2)-sin(2*M_PI*frequency[i_nu]*j*t1));
                        double mS=-tmp1*(cos(2*M_PI*frequency[i_nu]*j*t2)-cos(2*M_PI*frequency[i_nu]*j*t1));
                        double tmp_sig =  tmp1 *(sin(2*M_PI*frequency[i_nu]*j*t2)*cos(2*M_PI*frequency[i_nu]*j*t2)-sin(2*M_PI*frequency[i_nu]*j*t1)*cos(2*M_PI*frequency[i_nu]*j*t1));
                        double sig_C = 0.5/n_events * (1+tmp_sig) - mC; 
                        double sig_S = 0.5/n_events * (1-tmp_sig) - mS; 
                        double sig_CS = 0.5 * tmp1/n_events *(sin(2*M_PI*frequency[i_nu]*j*t2)*sin(2*M_PI*frequency[i_nu]*j*t2)-sin(2*M_PI*frequency[i_nu]*j*t1)*sin(2*M_PI*frequency[i_nu]*j*t1)) - mC*mS;
                        double mat_det = sig_C*sig_S - sig_CS * sig_CS;
                         */
			for(int i=0;i<n_events;i++)
			{
				
				double phase=(times[i]-t_ref)*frequency[i_nu];
				phase-=floor(phase);
				
				C+=cos(2*M_PI*j*phase);
				S+=sin(2*M_PI*j*phase);
                                
                                
                                
				//cout << phase  << " " << tmp1 << " " << tmp2 << " " << lz2[i_nu] << endl;
			}
                        //C/=n_events;
                        //S/=n_events;
			//lz2[i_nu]+=( (sig_S - sig_CS)*C*C + (sig_C - sig_CS) * S*S )/mat_det;
                        lz2[i_nu] += 2.0*(C*C + S*S)/n_events;
		}
		
	}
	
	return output_z2_stat(frequency, z2, lz2, sig,  log_spacing,  nu_start,
            nu_stop,   n_bins,  n_harm,  flag_out,
            filename,  t_ref,  best_freq,  err_best_freq,  max_z2,  counter,
            min_significance, n_events ,  t_stop,  t_start);
	
	
}

int output_z2_stat(VecDoub frequency, VecDoub z2, VecDoub lz2, VecDoub sig, char log_spacing, double nu_start,
        double nu_stop, int n_bins, int n_harm, int flag_out,
        const char *filename, double t_ref, double &best_freq, double &err_best_freq, double &max_z2, int &counter,
        double min_significance, int n_events , double t_stop, double t_start)
{
    	int max_ind=0;
	double max_value=-10;
	
	for(int i_nu=0;i_nu<n_bins;i_nu++)
	{
            //lz2[i_nu]/=n;
            //cout << lz2[i_nu] << " " << lz2[i_nu]/n_events << endl;
            z2[i_nu]=lz2[i_nu];
            if(z2[i_nu]>max_value)
            {
                    max_value=z2[i_nu];
                    max_ind=i_nu;
            }
	}

        //Output
        max_z2=max_value;
        
	cout << "Maximum of Z2 statistics is at " << frequency[max_ind] << " Hz\n";
        cout << "Inspected " << n_bins << " frequencies with " << n_harm << " harmonics\n";
	char fname[FLEN_FILENAME];
	
	Chisqdist chi2(2*n_harm);
	Normaldist gaus;
	
	
	for(int i_nu=0;i_nu<n_bins;i_nu++)
	{
            //z2[i_nu]/=n_files;
            //cout << z2[i_nu] << " " << sig[i_nu] << " " << n_bins << endl;
            double prob = pow(chi2.cdf(z2[i_nu]),n_bins); //This is with the number of trials

            sig[i_nu]=0;
            //			if(prob>0 && prob<0.5)
            //				prob=1-prob;

            if(prob>=0.5 && prob<1)
                    sig[i_nu]=(gaus.invcdf(prob));

            if(prob==1)
                    sig[i_nu]=11;
            //cout << z2[i_nu] << " " << sig[i_nu] << endl;
		
	}
	
        //cout << "Flag out " << flag_out << endl;
	if(flag_out)
	{
		
            if(sig[max_ind] < min_significance || max_ind < n_bins*0.1 || max_ind > n_bins*0.9)
                sprintf(fname, "%s_%04d_low.qdp", filename, counter);
            else
                sprintf(fname, "%s_%04d.qdp", filename, counter);

            ofstream ff(fname);
            if(!ff.is_open())
            {
                    cerr << "Error in opening " << filename << endl;
                    return 1;
            }

            ff << "read\n";
            ff<<"cpd /xw\n";
            if(log_spacing =='y')
            ff <<"log x on\n";
            else
            ff <<"log x off\n";

            //ff <<"ma 1 on 2\n";
            ff <<"lw 2\n";
            ff <<"r y 0. " << 8*n_harm << "\n";
            ff <<"cs 1.1\n";
            ff <<"plot vert\n";
            ff <<"font roman\n";
            ff <<"time off\n";
            ff <<"lab title\n";
            ff <<"lab x Frequency [Hz] - " << nu_start << "\n";
            double prob=gaus.cdf(3.0);
            prob = pow(prob,1.0/double(n_bins));
            double chi_level= chi2.invcdf(prob);
            //double A_max=sqrt(n_bins*2*three_s_chi/n_events_total);
            ff <<"LAB  2 \"3\\gs\" COL 4 LIN 0 0.9\n";
            ff <<"LAB  2  POS 0 " << chi_level << "\n";	
            ff <<"LAB  3 \"5\\gs\" COL 5 LIN 0 0.9\n";
            prob=gaus.cdf(5.0);
            prob = pow(prob,1.0/double(n_bins));
            chi_level= chi2.invcdf(prob);
            ff <<"LAB  3 POS 0 " << chi_level << "\n";	

            ff << "lab y2 Z\\u2\\d\n";
            ff << "lab y3 P (%)\n";	
            ff << "lab y4 \\gs\n";

            ff << "lab file " << 2 * n_harm << " dof\n";
            for(int i_nu=0;i_nu<n_bins;i_nu++)
            {
                //z2[i_nu]/=n_files;
                double prob = pow(chi2.cdf(z2[i_nu]),n_bins);
                ff <<  scientific << setw(19) << setprecision(12) << frequency[i_nu]-nu_start << "\t" << 
                setw(19) << setprecision(12) << z2[i_nu] << "\t" << setw(19) << setprecision(12) <<
                        prob * 100.0 << "\t";
                ff << setw(19) << setprecision(12) << sig[i_nu] << endl;
            }

            ff.close();
	}
	
	cout << "Maximum significance (after trials) is " << sig[max_ind] << " for a Z2 of " << z2[max_ind] << endl;

        string fn;
 	ofstream ff;
	if(flag_out)
	{
            fn=filename;
            fn+=".dat";

            if(counter == 1)
            {
		ff.open(fn.c_str(), ios::out | ios::trunc);
		if(!ff.is_open())
		{
			cerr << "Cannot open " << fn << " return\n";
			return 1;
		}
		ff<< "Tstart\tTstop\tFrequency\tError+\tError-\tPeriod\tError+\tError-\tZ2_peak\tRate\tSigma_rate\n";
                ff.close();

            	fn=filename;
                fn+=".qdp";
		ff.open(fn.c_str(), ios::out | ios::trunc);
		if(!ff.is_open())
		{
			cerr << "Cannot open " << fn << " return\n";
			return 1;
		}
		ff<< "read terr 1 2 3 5\nplot vert\nlab file\ntime off\nlab x t [s -"<< scientific << setw(19) << setprecision(12) << t_ref <<
                        "]\nlab y2 Fr [Hz]\nlab y3 P [s]\nlab y4 Z\\u2\\d\ncpd /xw\nma 17 on\nlab y5 Rate\n";

                ff.close();
            }
        }

        if(sig[max_ind] < min_significance) // || max_ind < n_bins*0.1 || max_ind > n_bins*0.9)
	{
		cout << "Maximum significance is low " << sig[max_ind] <<endl;
		
		cout << "not computing the fine solution\n";
                fn=filename;
                fn+=".qdp";
                if(flag_out)
                {
                ff.open(fn.c_str(), ios::out | ios::app);
                if(!ff.is_open())
                {
                    cerr << "Cannot open " << fn << " return\n";
                    return 1;
                }
                ff<< scientific << setw(19) << setprecision(12)  << (t_stop+t_start)/2 -t_ref
                        << "\t" << (t_stop-t_start)/2 << "\t" << (-t_stop+t_start)/2 <<
                        "\t-1\t-1\t-1\t-1\t-1\t-1\t" <<
                        z2[max_ind] << "\t" <<
                        n_events/(t_stop-t_start) << " " << sqrt(n_events)/(t_stop-t_start)<<
                        " " << -sqrt(n_events)/(t_stop-t_start)<<endl;

                ff.close();
                }
                return 2;
	}
	fn = "fit_";
	fn+=fname;
	double err_best_freq_d=0;
	double err_best_freq_u=0;
	double peak_value=0;
        if( max_ind >= n_bins*0.1 && max_ind <= n_bins*0.9)
	{
        	get_best_frequency(frequency, z2, best_freq, err_best_freq_u, err_best_freq_d,
                peak_value, (char *)fn.c_str(), flag_out);
        }
        else
        {
            cout << "The fit is probably not converging, because at the border of interval\nWe assign the maximum value\n";
            cout << "Index is at border " << max_ind << " " << n_bins <<  endl;
            best_freq=frequency[max_ind];
            peak_value=z2[max_ind];
            if(max_ind < n_bins-1)
                err_best_freq_u=  frequency[max_ind+1]-frequency[max_ind];
            if(max_ind >0)
                err_best_freq_d=  frequency[max_ind]-frequency[max_ind-1];
            
            if(err_best_freq_u==0)
                err_best_freq_u=err_best_freq_d;
            if(err_best_freq_d==0)
                err_best_freq_d=err_best_freq_u;
        }
	fn=filename;
	fn+=".dat";

        if(flag_out)
	{
		
	ff.open(fn.c_str(), ios::out | ios::app);
	if(!ff.is_open())
	{	
            cerr << "Cannot open " << fn << " return\n";
            return 1;
	}
	
	ff<< scientific << setw(19) << setprecision(12)  << 
                t_start << "\t" << t_stop << "\t" << best_freq << "\t" <<
                err_best_freq_u - best_freq << "\t" << best_freq - err_best_freq_d << "\t";
	ff<< scientific << setw(19) << setprecision(12)  <<
                1./best_freq << "\t" << 1./err_best_freq_d - 1./best_freq << "\t" <<
                1./best_freq - 1./err_best_freq_u << "\t" << peak_value << "\t" <<
                n_events/(t_stop-t_start) << " " << sqrt(n_events)/(t_stop-t_start)<< endl;

	ff.close();
	fn=filename;
        fn+=".qdp";

	ff.open(fn.c_str(), ios::out | ios::app);
	if(!ff.is_open())
	{	
            cerr << "Cannot open " << fn << " return\n";
            return 1;
	}
	ff<< scientific << setw(19) << setprecision(12)  << (t_stop+t_start)/2 -t_ref
                << "\t" << (t_stop-t_start)/2 << "\t" << (-t_stop+t_start)/2 << "\t" <<
                best_freq << "\t" << err_best_freq_u - best_freq << "\t" <<
                -best_freq + err_best_freq_d << "\t";
	ff<< scientific << setw(19) << setprecision(12) << 1./best_freq << "\t" <<
                1./err_best_freq_d - 1./best_freq << "\t" <<
                -1./best_freq + 1./err_best_freq_u << "\t" << peak_value << "\t" <<
                n_events/(t_stop-t_start) << " " << sqrt(n_events)/(t_stop-t_start)<<
                " " << -sqrt(n_events)/(t_stop-t_start)<<endl;

	ff.close();
	}
        return 0;
}



int elaborate_lc_fold()
{
	double time_interval=10000;
	
	char filename1[FLEN_FILENAME];
	char filename2[4*FLEN_FILENAME];
	char query[2048];
	
	sprintf(filename1, "soft_lc.txt");
	sprintf(filename2, "hard_lc.txt");
	
	sprintf(query, "List of light curves 1");
	askInfo(query, filename1);
	
	sprintf(query, "List of light curves 2");
	askInfo(query, filename2);
	
	vector<string> list1;
	vector<string> list2;
	
	read_list(list1,filename1);
	read_list(list2,filename2);
	int nf=0;
	if((nf=list1.size()) != list2.size())
	{
		cerr << "The two lists should have the same number of files\n";
		return 1;
	}
	
	//investigate first lc
	
	light_curve *l1, *l2; 
	l1 = new light_curve(list1[0].c_str());
	cout << "Light curve " << list1[0].c_str() << endl;
	l1->output();

	
	double rebin_factor=2;
	sprintf(query, "Rebin factor (integer >= 1)");	
	ask_double(query, &rebin_factor);
	rebin_factor=floor(rebin_factor);
	
	cout << "Rebin factor is " << rebin_factor <<endl;
	cout << "New time bin is " << l1->time_del()*rebin_factor << " s\n";
	l1->rebin(rebin_factor);
	
	double t_ref   = l1->t_start()-1e3; //to avoid problems with barycentric/binary correction
	//double t_start = l1->t_start();
	double t_stop  = l1->t_stop();
	
	//	int size_simu=l1->size(); //used for simulations to avoid long i/o
	
	sprintf(query, "Length of time interval [s]");
	ask_double(query, &time_interval);
	//get the closest power of two length of interval
	double time_resolution=l1->time_del();
// 	int n_t = time_interval/time_resolution;
//	int n=4;
//	while(n<n_t/2)
//		n*=2;
//	
//	cout << "Original n is "<< n_t <<endl;
//	cout << "Final    n is "<< n << "= " << n*time_resolution << " s" <<endl;
	
	
	//	l1->deallocate();
	//	l1->clean_values();
	
	
	sprintf(filename1,"Prova");
	sprintf(query, "Output file");
	askInfo(query, filename1);
	
	sprintf(query, "Perform simulation?");
	char flag_simul='n';
	flag_simul=ask_yes_no(query, flag_simul);
	
	simul_obj *simul;
	char use_shot_noise='n';
	if(flag_simul == 'y')
	{
		double f1=0.2776;
		double r1=1;
		double f2=0.2776;
		double r2=1;
		double tl=0.02;
		double frac_noise=0;
		
		sprintf(query, "First frequency");
		ask_double(query, &f1);
		sprintf(query, "First rate");
		ask_double(query, &r1);
		sprintf(query, "Second frequency");
		ask_double(query, &f1);
		sprintf(query, "Second rate");
		ask_double(query, &r1);
		sprintf(query, "Time lag [s]");
		ask_double(query, &tl);
		sprintf(query, "Use Gaussian noise (0=no)");		
		ask_double(query, &frac_noise);
		
		sprintf(query,"Use shot noise from data?");
		use_shot_noise=ask_yes_no(query, use_shot_noise);
		
		simul=new simul_obj(t_ref, f1,r1,f2,r2,tl,frac_noise);
		
	}

	cout << "Folding period set-up\n";
	
	cout << "Time resolution is [s] " << time_resolution << endl;
	cout << "Nymquist frequency is [Hz] " << 0.5/time_resolution << endl;
	cout << "Minimum frequency step is [Hz] " << 1/(t_stop-t_ref) << endl;
	
	char use_z2='n';	
	sprintf(query, "Use z2 to get the period?");
	use_z2=ask_yes_no(query, use_z2);

	
	//char evt_flag=0;
	//double e_min=0.5;
	//double e_max=10;
	char flag_binarycor='n';
	char binary_file[FLEN_FILENAME];
	double mjdref=l1->_mjd_ref;
	double nu_start=0.2762; //0.05/time_resolution;
	double nu_stop=0.2773; //0.5/time_resolution;
	int n_bins=100;
	int n_harm=2;
	//delete l1;
	l1->deallocate();
	l1->clean_values();
	char log_spacing='y';
	char out_file_z2='n';
	char out_profile='n';
	char filename_z2[2*FLEN_FILENAME];
	char filename_folded[2*FLEN_FILENAME];
	
	double best_freq=0.276591966552287;//2.767154186984e-01;
	double err_best_freq=1e-7;
	
	double df=2.943e-10;
	double ddf=0;

	
	sprintf(query, "Do you want to apply binary correction?");
	flag_binarycor=ask_yes_no(query	, flag_binarycor);
	
	if(flag_binarycor == 'y')
	{
		
		sprintf(binary_file, "%s", "4U_orbit.dat");
		
		sprintf(query, "Enter the name of the file to store the binary parameters (none for interactive)");
		askInfo(query, binary_file);
		char setup_file_exist=1;
		char interactive_flag=0;
		if (strcmp(binary_file, "none")==0)
		{
			sprintf(query, "Enter the actual name of the file to store the binary parameters");
			askInfo(query, binary_file);
			
			//sprintf(binary_file,"%s_orbit.dat",active_src_name);
			interactive_flag=1;
			setup_file_exist=0;
		}
		if(interactive_flag)
			binarycor_setup(binary_file, interactive_flag, 1, setup_file_exist, mjdref );
	}
	
	if(use_z2=='y')
	{

		
		sprintf(query, "Start frequency [Hz]");
		ask_double(query, &nu_start);
		
		sprintf(query, "Stop frequency [Hz]");
		ask_double(query, &nu_stop);

		sprintf(query, "number of bins");
		ask_integer(query, &n_bins);

		sprintf(query, "How many harmonics?");
		ask_integer(query, &n_harm);	
		
		if(n_bins<10)
		{
			cerr << "Set n_bins to 10\n";
			n_bins=10;
		}
		
		sprintf(query, "Use logarithmic spacing?");
		log_spacing=ask_yes_no(query, log_spacing);
		
		sprintf(query, "Output Z2 plot?");
		out_file_z2=ask_yes_no(query, out_file_z2);
		
		if(out_file_z2=='y')
		{
			sprintf(filename_z2,"Z2.qdp");
			sprintf(query, "Output file");
			askInfo(query, filename_z2);
		}
	}
	else
	{
		sprintf(query, "frequency");
		ask_double(query, &best_freq);
		
		sprintf(query, "frequency derivative");
		ask_double(query, &df);
		
		sprintf(query, "frequency second derivative");
		ask_double(query, &ddf);
	}
	
	sprintf(query, "reference time for folding\n");
	ask_double(query, &t_ref);

	int n_bins_t=32;
	
	sprintf(query, "number of phase bins");
	ask_integer(query, &n_bins_t);
	if(n_bins_t<4)
		n_bins_t=4;
	
	int n=4;
	while(n<=n_bins_t/2)
		n*=2;
	
	cout << "Number of phase bins is the closest power of 2\n";
	cout << "N bins = " << n << endl;
	
	sprintf(query, "do you want to output the folded profile?");
	out_profile=ask_yes_no(query, out_profile);


	
	if(out_profile=='y')
	{
		sprintf(filename_folded,"%s", filename1);
		sprintf(query, "Output file");
		askInfo(query, filename_folded);
	
	}

	char flag_real='n';
	sprintf(query, "do you want to use real time correlation?");
	flag_real=ask_yes_no(query, flag_real);
	
	//Initialize storage of elements
	VecDoub gam2(n);
	VecDoub lag(n);
	VecDoub dgam2(n);
	VecDoub dlag(n);
	VecDoub a_s1(n>>1), a_s2(n>>1), a_cor(n);
	
	vector<VecDoub> all_s1, all_s2, all_cor;
	
	for(int i=0;i<n>>1;i++)
	{
		a_s1[i]=0;
		a_s2[i]=0;
		a_cor[i]=0;
		a_cor[(n>>1)+i]=0;
	}
	
	
	//Init loops
	int tot_loops=0;
	double tot_ontime=0;
	
	
	for(int k=0;k<nf;k++) //loop on files
	{
		cout << list1[k] << " Vs " << list2[k] << endl;
		
		l1 = new light_curve(list1[k].c_str());
		l2 = new light_curve(list2[k].c_str());
		if(l1->size() != l2->size())
		{
			cerr << "Light curves in the two bands have different sizes !\n";
			waitkey();
		}
		
		l1->rebin(rebin_factor);
		l2->rebin(rebin_factor);
		
		l1->scan_gti();
		
		if(flag_binarycor == 'y')
		{
			cout << "Peform first binary correction\n";
			l1->binary_corr(binary_file);
			cout << "Peform second binary correction\n";
			l2->binary_corr(binary_file);
		}
		
		light_curve sum=*l1+*l2;
		
		//cout << "PP\n";
		if(flag_simul == 'y')
		{
			cout << "SizeSB " << l1->size() << endl;
			
			for(int i=0;i<l1->size();i++)
			{
				double er=1;
				//cout << i << endl;
				if(use_shot_noise=='y' && l1->_y[i]>0)
				{
					//er=l1->_dy[i]/l1->_y[i];
					//cout << l1->_y[i] << "\t";
					l1->_y[i] = simul->get(l1->_x[i], er, 0);
					l1->_dy[i] = sqrt(fabs(l1->_y[i]));
					//l1->_dy[i] = l1->_y[i] * er;
					//cout << l1->_y[i] << "\t";
					
				}
				else if (use_shot_noise == 'n')
				{
					l1->_y[i] = simul->get(l1->_x[i], er, 0);
					l1->_dy[i] = sqrt(fabs(l1->_y[i]));
				}
					
				//cout << i << endl;			
				if(use_shot_noise=='y' && l2->_y[i]>0)
				{
					//cout << l2->_y[i] << "\t";
					//er=l2->_dy[i]/l2->_y[i];
					l2->_y[i] = simul->get(l2->_x[i], er, 1);
					l2->_dy[i] = sqrt(fabs(l2->_y[i]));
					//l2->_dy[i] = l2->_y[i] * er;
					//cout << l2->_y[i] << "\t";
				}
				else if (use_shot_noise == 'n')
				{
					l2->_y[i] = simul->get(l2->_x[i], er, 1);
					l2->_dy[i] = sqrt(fabs(l2->_y[i]));
				}
					

				//cout << endl;
			}
			
		}
		
		if(use_z2 == 'y')
		{
			cout << "Get period using Z2 test\n";
			sum.get_z2_period(flag_binarycor, binary_file, log_spacing, nu_start, nu_stop, n, n_harm, out_file_z2, filename_z2 , t_ref, best_freq, err_best_freq);
			
		}
		
		tot_ontime += l1->ontime();
		//Start loop on chunks
		double t1=l1->t_start();
		double t2=t1+time_interval;
		
		if(t2>l1->t_stop())
			t2=l1->t_stop();
		
		int cycle=0;
		light_curve sum_fold1(n);
		light_curve sum_fold2(n);
		sum_fold1.init_folded();
		sum_fold2.init_folded();
		
		while (t2<= l1->t_stop())
		{
			cycle++;
			light_curve *loc_l1 =&(l1->cut(t1, t2));
			light_curve *loc_l2 =&(l2->cut(t1, t2));
			if(loc_l1->size() < 5*n || loc_l1->size() == l1->size())
			{
				t1+=time_interval;
				t2+=time_interval;
				delete loc_l1;
				delete loc_l2;
				continue;
			}
			
			cout << "Folding the LC\n";
			light_curve *lf1= (loc_l1->fold(n, t_ref, best_freq, df, ddf));
			light_curve *lf2= (loc_l2->fold(n, t_ref, best_freq, df, ddf));
			
			//update global 
			sum_fold1 += (*lf1);
			sum_fold2 += (*lf2);			
			
			//Normalize without using weighted average
			lf1->normalize(0,'n');
			lf2->normalize(0,'n');
			
			cout << "Folded light curves\n";
			if(lf1->time_del() != lf2->time_del())
			{
				cerr << "Light curves in the two bands have different time resolution, abort !\n";
				exit(1);
			}
			
			lf1->output();
			lf2->output();
			
			if(out_profile =='y')
			{
				char temp_string[5*FLEN_FILENAME];
				sprintf(temp_string, "%s_%04d_s.qdp", filename_folded, cycle);
				lf1->output_lc(temp_string);
				sprintf(temp_string, "%s_%04d_h.qdp", filename_folded, cycle);
				lf2->output_lc(temp_string);				
			}
			
			//No padding is necessary now, since the curves are truly periodic
			VecDoub sf1(n), sf2(n), corf(n);
	/*		
			for(int i=0;i<2*n;i++)
			{
				sf1[i]=0;
				sf2[i]=0;
				corf[i]=0;
			}
	*/
	 
			for(int i=0;i<n;i++)
			{
				sf1[i]=lf1->_y[i];//*welch(i,n);
				sf2[i]=lf2->_y[i];//*welch(i,n);
	//			sf1[n+i]=lf1->_y[i];//*welch(i,n);
	//			sf2[n+i]=lf2->_y[i];//*welch(i,n);
			}
	//		for (int i=0;i<n;i++) {
	//			cout << i << " " << sf1[i] << " " << sf2[i] << endl;
	//		}	
	//		waitkey();
			
			realft(sf1,1);
			realft(sf2,1);

	//		for (int i=0;i<n;i++) {
	//			cout << i << " " << sf1[i] << " " << sf2[i] << endl;
	//		}	
	//		waitkey();
			
			//int no2=n>>1;
			//cout << n << " " << no2 << endl;
			for (int i=2;i<n;i+=2) {
				//tmp=sf1[i];
				corf[i]=(sf1[i]*sf2[i]+sf1[i+1]*sf2[i+1]);
				corf[i+1]=(sf1[i+1]*sf2[i]-sf1[i]*sf2[i+1]);
				//cout << i/2 << " " << sf1[i] << " " << sf2[i] << " " << sf1[i+1] << " " << sf2[i+1] << endl;
			}
			
			if(flag_real=='y')
			{
				//Real F_0
				//For real time, no frequency. To check
				corf[0]=sf1[0]*sf2[0];
				corf[1]=sf1[1]*sf2[1];
				realft(corf,-1);
			}
			else
			{
				corf[0]=sf1[0]*sf2[0];
				corf[1]=sf2[0]-sf1[0];
				//The real F_n/2 s discarded at the moment
			}

			//Attempt to use the whole data for averaging
			all_s1.push_back(sf1);
			all_s2.push_back(sf2);
			all_cor.push_back(corf);
			
			
			for(int i=0;i<n>>1;i++)
			{
				a_s1[i]+=sf1[2*i]*sf1[2*i] + sf1[2*i+1]*sf1[2*i+1];
				a_s2[i]+=sf2[2*i]*sf2[2*i] + sf2[2*i+1]*sf2[2*i+1];
				a_cor[2*i]+=corf[2*i];
				a_cor[2*i+1]+=corf[2*i+1];
			}

			t1+=time_interval;
			t2+=time_interval;
			delete loc_l1;
			delete loc_l2;
			lf1->deallocate();
			lf2->deallocate();
			lf1->clean_values();
			lf2->clean_values();
			tot_loops++;
			
		}	//end of loop on chunks
		//		a_cor[0]+=corf[0];
//		//a_cor[n]+=corf[n];
//		a_s1[0]+=sf1[0]*sf1[0];
//		a_s2[0]+=sf2[0]*sf2[0];
		
		l1->deallocate();
		l2->deallocate();
		l1->clean_values();
		l2->clean_values();

		if(out_profile =='y')
		{
			char temp_string[4*FLEN_FILENAME];
			sprintf(temp_string, "%s_%s_s.qdp", filename_folded, l1->filename);
			sum_fold1.output_lc(temp_string);
			sprintf(temp_string, "%s_%s_h.qdp", filename_folded, l2->filename);
			sum_fold2.output_lc(temp_string);			
		}
		

		
		//Computes gamma and lag from averaged primary elements
		double fr = best_freq;// 1./(n*time_resolution); 
			
		if(flag_real=='y')
		{
			for(int i=0;i<n;i++)
			{
				//need to compute also errors
				gam2[i]=a_cor[i]/tot_loops;
				//			lag[i]=a_cor[i]/tot_loops;
				
			}
			
		}
		else
		{
			for(int i=0;i<n>>1;i++)
			{
				//need to compute also errors
				gam2[i]=(a_cor[2*i] * a_cor[2*i] + a_cor[2*i+1]*a_cor[2*i+1])/(a_s1[i]*a_s2[i]);	
				if(isnan(gam2[i]))
				{
					cerr << i << " " << a_s1[i] << " " << a_s2[i] << endl;
					waitkey();
					
				}
				lag[i] = atan2(a_cor[2*i+1], a_cor[2*i])/M_PI/2./((i)*fr); //+0.5
			}
		}
		//makes output
		//gam2[0]=1;
		//lag[0]=0;
		sprintf(filename2, "%s_%s-%s.qdp", filename1, l1->filename, l2->filename);
		ofstream a_ff(filename2);
		
		a_ff << "read serr 1\ncpd /xw\n";
		//a_ff << "r x 1e-3 500\nr y 1e-6 1.1\n";
		//a_ff << "log x on\nlog y on\n";
		//a_ff << "error x off\n";
		a_ff << "ls 1 on 2\nls 3 on 3\n";
		a_ff << "plot vert\n";
		a_ff << "lab y2 \\gg\n";
		a_ff << "lab y3 \\gDt\n";
		a_ff << "lab y4 P1\n";
		a_ff << "lab y5 P2\n";
		a_ff <<"log y on 4 5\n";

		
		if(flag_real=='y')
		{
			for(int i=0;i<n;i++)
			{
				a_ff << scientific << setw(19) << setprecision(12)<< (1.0*i)/(1.0*n)  << setw(19) <<"\t" << 1.0/n << "\t"; 
				a_ff << setw(13) << gam2[i] << "\t" << a_s1[i] << "\t" << a_s2[i]<< endl;
				
			}
			
		}
		else
		{
			for(int i=0;i<n>>1;i++)
			{
				if(gam2[i] > 1 || gam2[i] < 0.1 || isnan(gam2[i]) || isnan(lag[i]))
					continue;
				a_ff << scientific << setw(19) << setprecision(12)<< i*fr  << setw(19) <<"\t" << fr/2 << "\t"; 			
				a_ff << setw(13) << gam2[i] << "\t" << lag[i] << "\t" << sqrt(a_s1[i]) << "\t" << sqrt(a_s2[i])<< endl;
			}
		}
		a_ff.close();
		
		
	} //end of loop on files
	
	
	
	cout << "Processed " << list1.size() << " files\n";
	cout << "Done " << tot_loops << " loops\n";
	cout << "Total ontime is " << tot_ontime << " s\n";	
	
	cout << "Starts averages\n";
	
	//computes average and stddev
	VecDoub s_s1(n>>1), s_s2(n>>1), s_cor(n);//, a_s1(n), a_s2(n), a_cor(2*n);
	//VecDoub a_gam2(n>>1), a_lag(n>>1);
	//VecDoub s_gam2(n>>1), s_lag(n>>1);
	
	//cout << "Prova\n";
	a_s1.assign(n>>1, 0);
	a_s2.assign(n>>1, 0);
	a_cor.assign(n, 0);
	
	double fr = best_freq;// 1./(n*time_resolution); 
	
	VecDoub s_gam2(n>>1);
	VecDoub s_lag(n>>1);
	
	 //average
	 for(int k=0;k<1;k++)
	 {

		 VecDoub sf1 = (all_s1[k]);
		 VecDoub sf2 = (all_s2[k]);
		 VecDoub corf = (all_cor[k]);
		 
		 for(int i=0;i<n>>1;i++)
		 {
			 a_s1[i]=sf1[2*i]*sf1[2*i] + sf1[2*i+1]*sf1[2*i+1];
			 a_s2[i]=sf2[2*i]*sf2[2*i] + sf2[2*i+1]*sf2[2*i+1];
			 a_cor[2*i]=corf[2*i];
			 a_cor[2*i+1]=corf[2*i+1];
			 s_s1[i]=0;
			 s_s2[i]=0;
			 s_cor[2*i]=0;
			 s_cor[2*i+1]=0;
		 }
		// cout << corf[2] << " " << corf[3] << endl;
	}
	
	for(int k=1;k<tot_loops;k++)
	{
		
		VecDoub sf1 = (all_s1[k]);
		VecDoub sf2 = (all_s2[k]);
		VecDoub corf = (all_cor[k]);
		
		double nn=k;
		double ra=nn/(nn+1);
		
		
		for(int i=0;i<n>>1;i++)
		{
			gam2[i]=(corf[2*i] * corf[2*i] + corf[2*i+1]*corf[2*i+1]);
			
			double x=sf1[2*i]*sf1[2*i] + sf1[2*i+1]*sf1[2*i+1];
			a_s1[i] = ra * a_s1[i] + x/nn;
			s_s1[i] = ra * s_s1[i] + SQR(x-a_s1[i])/nn;
			gam2[i]/=x;

			x = sf2[2*i]*sf2[2*i] + sf2[2*i+1]*sf2[2*i+1];
			a_s2[i] = ra * a_s2[i] + x/nn; 
			s_s2[i] = ra * s_s2[i] + SQR(x-a_s2[i])/nn;
			gam2[i]/=x;
			
			a_cor[2*i] = ra * a_cor[2*i] + corf[2*i]/nn;
			a_cor[2*i+1] = ra * a_cor[2*i+1] + corf[2*i+1]/nn;
			
			s_cor[2*i] = ra * s_cor[2*i] + SQR(corf[2*i]-a_cor[2*i])/nn;
			s_cor[2*i+1] = ra * s_cor[2*i+1] + SQR(corf[2*i+1]-a_cor[2*i+1])/nn;
			
			lag[i] = atan2(corf[2*i+1], corf[2*i])/M_PI/2./((i)*fr);
			
		}
//		cout << endl;
//		cout << corf[2] << " " << corf[3] << endl;
//		cout << a_s1[1] << " " << a_s2[1] << endl;		
//		cout << gam2[1] << " " << lag[1] << endl;
//		cout << endl;
		
	}
	
	
	cout << endl << endl;


	 for(int i=0;i<n>>1;i++)
	 {
		 s_s1[i]=sqrt(s_s1[i]);
		 s_s2[i]=sqrt(s_s2[i]);
		 s_cor[2*i]=sqrt(s_cor[2*i]);
		 s_cor[2*i+1]=sqrt(s_cor[2*i+1]);
	 }


	
	//Computes gamma and lag from averaged primary elements
	
	for(int i=0;i<n>>1;i++)
	{
		gam2[i]=(a_cor[2*i] * a_cor[2*i] + a_cor[2*i+1]*a_cor[2*i+1])/(a_s1[i]*a_s2[i]);
		//need to compute also errors
		//cout << a_cor[2*i] << " " << s_cor[2*i] << "\n" << a_cor[2*i+1] << " " << s_cor[2*i+1] << endl;
		//cout << a_s1[i]    << " " << s_s1[i]    << "\n" << a_s2[i]      << " " << s_s2[i] << endl<<endl; 
		
		double t1=SQR(2*a_cor[2*i]*s_cor[2*i]/(a_s1[i]*a_s2[i]));
		double t2=SQR(2*a_cor[2*i+1]*s_cor[2*i+1]/(a_s1[i]*a_s2[i]));
		double t3=SQR(gam2[i]*s_s1[i]/a_s1[i]);
		double t4=SQR(gam2[i]*s_s2[i]/a_s2[i]);
		
		s_gam2[i]=sqrt(t1+t2+t3+t4);
		
		if(isnan(gam2[i]))
		{
			cerr << i << " " << a_s1[i] << " " << a_s2[i] << endl;
			waitkey();
			
		}
		lag[i] = atan2(a_cor[2*i+1], a_cor[2*i])/M_PI/2./((i)*fr);
		t1 = a_cor[2*i+1] / a_cor[2*i];
		s_lag[i] = (1/(1+SQR(t1))) * fabs(t1) * sqrt( SQR(s_cor[2*1+1]/a_cor[2*i+1]) + SQR(s_cor[2*1]/a_cor[2*i]) ) /M_PI/2./((i)*fr);
		
	}
	//makes output
	//gam2[0]=1;
	//lag[0]=0;
	
	cout << "Prova_output\n";
	sprintf(filename2, "%s_error.qdp", filename1);
	ofstream a_ff(filename2);
	
	a_ff << "read serr 1 2 3 4 5\ncpd /xw\n";
	//a_ff << "r x 1e-3 500\nr y 1e-6 1.1\n";
	//a_ff << "log x on\nlog y on\n";
	//a_ff << "error x off\n";
	a_ff << "ls 1 on 2\nls 3 on 3\n";
	a_ff << "plot vert\n";
	a_ff << "lab y2 \\gg\n";
	a_ff << "lab y3 \\gDt\n";
	a_ff << "lab y4 P1\n";
	a_ff << "lab y5 P2\n";
	a_ff <<"log y on 4 5\n";
	a_ff <<"r y1 0 1\n";
	
	
	
	if(flag_real=='y')
	{
		for(int i=0;i<n;i++)
		{
			a_ff << scientific << setw(19) << setprecision(12)<< (1.0*i)/(1.0*n)  << setw(19) <<"\t" << 1.0/n << "\t"; 
			a_ff << setw(13) << gam2[i] << "\t" << s_gam2[i] << "\t" << a_s1[i] << "\t" << a_s2[i]<< endl;
			
		}
		
	}
	else
	{
		for(int i=0;i<n>>1;i++)
		{
			if(gam2[i] > 1 || gam2[i] < 0.1 || isnan(gam2[i]) || isnan(lag[i]))
				continue;
			a_ff << scientific << setw(19) << setprecision(12)<< i*fr  << setw(19) <<"\t" << fr/2 << "\t"; 			
			a_ff << setw(13) << gam2[i] << "\t" << s_gam2[i] << "\t" << lag[i] << "\t" << s_lag[i] << "\t";
			a_ff << sqrt(a_s1[i]) << "\t" << sqrt(s_s2[i]/a_s2[i]) << "\t";
			a_ff << sqrt(a_s1[i]) << "\t" << sqrt(s_s2[i]/a_s2[i]) << endl;			
		}
	}
	a_ff.close();
	
	/*
	// This was the average on contigous bins	
	
	int nbins=60;
	double fmin=log10(fr);
	double fmax=log10(fr*n);
	double step=(fmax-fmin)/(nbins+1);

	
	ofstream ff(filename1);
	ff<<"read serr 1 2 3\n";
	ff<<"cpd /xw\n";
	ff <<"log x on\nlog y off\n";
	ff <<"r y -1 1\n";
	ff <<"ma 3 on 3\nma 4 on 2\n";
	
	for(int j=0;j<nbins;j++)
	{
		double f1,f2;
		f1=pow(10.0,fmin+j*step);
		f2=pow(10.0,fmin+(j+1)*step);
		int ind1=(int)floor(f1/fr);
		int ind2=(int)(floor(f2/fr));
		double av_gam=0, s_gam=0;
		double av_lag=0, s_lag=0;
		double n_done=0;
		
		for(int i=ind1;i<ind2;i++)
		{
			av_gam+=gam2[i];
			av_lag+=lag[i];
			n_done++;
		}
		
		if(n_done)
		{
			av_gam/=n_done;
			av_lag/=n_done;
		}
		
		for(int i=ind1;i<ind2;i++)
		{
			s_gam+=(gam2[i]-av_gam)*(gam2[i]-av_gam);
			s_lag+=(lag[i]-av_lag)*(lag[i]-av_lag);
		}
		
		if(n_done > 1)
		{
			s_gam=sqrt(s_gam/(n_done*(n_done-1)));
			s_lag=sqrt(s_lag/(n_done*(n_done-1)));
		}
		
		ff<<setw(15) << (f2+f1)/2 << "\t" << (f2-f1)/2<<"\t";
		ff<<setw(13) << av_gam << "\t" << s_gam << "\t";
		ff<<setw(13) << av_lag << "\t" << s_lag << endl;
		
	}
	
	ff.close();
	 */
	return 0;
}

void readline_withspace(char *out_str, char *query)
{
	char *readline_buffer;
	
	static const char *pprr=">>";
	
	char temp_string[1024], def_query[2048];
	sprintf(def_query, "%s [%s]", query, out_str);
	
	cout << def_query << endl;
	
	readline_buffer=readline(pprr);
	
	if(readline_buffer && *readline_buffer)
	{
		sprintf(temp_string, "%s", readline_buffer);
		strip_final_spaces(out_str, temp_string);

	}
        else
        {
            fprintf(stderr, "Error in input from query %s: no line!\n", def_query);
            //waitkey();
            exit(1);
        }
	
	free(readline_buffer);
	
	
}

void readline_nospace(char *out_str, char *query)
{
	char *readline_buffer;
	
	static const char *pprr=">>";
	
	char temp_string[4*FLEN_FILENAME], def_query[2048];
	sprintf(def_query, "%s [%s]", query, out_str);
	
	cout << def_query << endl;
	
	readline_buffer=readline(pprr);
	
	if(readline_buffer[0]!=0)
	{
		sprintf(temp_string, "%s", readline_buffer);
		strip_spaces(out_str, temp_string);
	}
	
	free(readline_buffer);
	
	
}

void strip_spaces(char *out_str, char *in_str)
{
	int i=0, j=0;
	while(in_str[i] != '\0')
	{
		if(in_str[i] != ' ')
			out_str[j++]=in_str[i];
		
		i++;
	}
	out_str[j]='\0';
}

void strip_final_spaces(char *out_str, char *in_str)
{
	//int i=0, j=0;
	int n=strlen(in_str);
	strcpy(out_str, in_str);
	
	for (int i=n-1;i>=0;i--)
	{
		if(out_str[i] == ' ' || out_str[i] == '\n' || out_str[i] == '\t')
			out_str[i]='\0';
		
		if( ! (out_str[i] == ' ' || out_str[i] == '\n' || out_str[i] == '\t') || out_str[i] == '\0')
			break;
	}
}

double *compute_phase_exposure(double timeref, double f, double df, double ddf,int n_GTIs, int n_phase_bins,double *gti_start,double *gti_stop )
{
	
	int i;
	double if1,if2;
	double ff1,ff2 ;
	double GTI_exp=0;
	double n;
	double Norm;
	int a1,a2;
	cout << "Allocate phase exposure " << n_phase_bins << endl;
	double *en_ph_phase_exposure=new double[n_phase_bins];
	for(i=0;i<n_phase_bins;i++)
	     en_ph_phase_exposure[i]=0;                    
	
	//printf("\n add_pulse_exposure: entry");
	
	//for(i=0;i<n_GTIs;i++)
	//   printf("\n %2d) %lf %lf %lf",i+1,gti_start[i]-gti_start[0],
	//         gti_stop[i]-gti_start[0],
	//          gti_stop[i]-gti_start[i]);                                                      
	
	//GTI_exp=0;
	
	for(i=0;i<n_GTIs;i++)
	{
		
		// printf("\n %2d) %lf %lf %lf",i+1,gti_start[i]-gti_start[0],
		//       gti_stop[i]-gti_start[0],
		//        gti_stop[i]-gti_start[i]);                                                      
		
		
		GTI_exp+=gti_stop[i]-gti_start[i];
		
		ff1 =get_phase_complete(gti_start[i], timeref, f, df, ddf, &if1);
		ff2 =get_phase_complete(gti_stop[i], timeref, f, df, ddf, &if2);
		
		n = if2-if1;
		
		Norm = (gti_stop[i]-gti_start[i])/(if2+ff2-if1-ff1)/(double)n_phase_bins;
		
		
		a1 = (int)floor(ff1 * n_phase_bins);
		a2 = (int)floor(ff2 * n_phase_bins);
		
		//printf("\n ff1=%lf; ff2=%lf; diff=%g",ff1,ff2,ff2-ff1);
		//printf("\n a1=%d; a2=%d",a1,a2);
		
		
		if(ff1 < ff2)
		{
			
			
			//	if (1==0)
			//		if(n==0 && a1==a2)
			//		{
			//			
			//            //printf("\n a1=%d %lf",a1,(ff2-ff1)*Norm);
			//            
			//           en_ph_phase_exposure[a1] += (ff2-ff1)*n_phase_bins*Norm;
			//			continue;
			//			
			//		}
			//		else if(a1 == a2)
			//			cerr << "Warn GTI bounds are the same\n";
			
			
			
			double n1 = (n+1.);
			double alf1 = (double)(a1+1) - ff1*n_phase_bins;
			double alf2 = ff2*n_phase_bins - (double)(a2);
			
			
			n*=Norm;
			alf1*=Norm;
			alf2*=Norm;
			n1*=Norm;	
			
			
			int j;
			for(j=0;j<a1;j++)
				en_ph_phase_exposure[j] += n;
			
			en_ph_phase_exposure[a1] += n + alf1;
			
			for(j=a1+1;j<a2;j++)
				en_ph_phase_exposure[j] += n1;
			
			if (a2>a1)	
				en_ph_phase_exposure[a2] += n + alf2;
			else
				en_ph_phase_exposure[a2] += alf2-Norm;
			
			
			for(j=a2+1;j<n_phase_bins;j++)
				en_ph_phase_exposure[j] += n;
			
		}
		else // ff1> ff2 
		{
			
			
			double n1 = (n-1.);///factor;
			
			if(n1<0)
				cerr << "WARNING:: probable error in GTI folding, value less 0 encountered\n";
			
			double alf1 = ff2*n_phase_bins - (double)(a2);
			double alf2 = (double)(a1+1) - ff1*n_phase_bins;
			
			
			n*=Norm;
			alf1*=Norm;
			alf2*=Norm;
			n1*=Norm;
			
			
			int j;
			for(j=0;j<a2;j++)
				en_ph_phase_exposure[j] += n;
			
			en_ph_phase_exposure[a2] += n1 + alf1;
			
			for(j=a2+1;j<a1;j++)
				en_ph_phase_exposure[j] += n1;
			
			if (a1>a2)
				en_ph_phase_exposure[a1] += n1 + alf2;
			else
				en_ph_phase_exposure[a1] +=  alf2;
			
			
			
			for(j=a1+1;j<n_phase_bins;j++)
				en_ph_phase_exposure[j] += n;
		}
		
		
		
		
	}  /* next GTI */
	
	printf("\n"); 	
		
	double tot_pulse_exp=0;
	
	for(int k=0;k<n_phase_bins;k++)
		tot_pulse_exp+=en_ph_phase_exposure[k];
	
	cout << "\nTotal pulse exposure (sec): " << tot_pulse_exp <<endl;
	
	cout << "GTI exposure (sec): " << GTI_exp << endl;  // sum of GTI  
	//	double factor = exposure / tot ;
	cout << "Ratio is " << tot_pulse_exp / GTI_exp << endl;
	cout << "Diff (sec): " << tot_pulse_exp - GTI_exp << endl;
	
	
	return en_ph_phase_exposure;
	
}

/* This is the private data structure which contains the fixed parameters */
struct vars_struct {
    
  vector <double> *times;
  double nu_i;
  double porb_i;
  double t0_i;
  double asini_i;
  double ecc_i;
  double omega_deg_i;
  //steps
  double nu_s;
  double porb_s;
  double t0_s;
  double asini_s;
  double ecc_s;
  double omega_deg_s;
  
  double pporb_i;
  
  int n_harm;
  double t_ref;
  
  std::ofstream out; 
  
};

double wrapper_mp_z2_circ(VecDoub_I & x, void *vars)
{
    struct vars_struct *v = (struct vars_struct *) vars;
    
    //cout << "Wrapper called\n";
    
//    cout << " ** \n";
//    cout << (*v->times)[0] << endl;
//    cout << v->n_harm << endl;
//    cout << v->t_ref << endl;
//    cout << v->ecc_i << endl;
//    cout << v->omega_deg_i << endl;
//    cout << " ** \n";
    
    double retval = get_z2_single_freq(v->times, v->n_harm, v->nu_i+x[0]*v->nu_s, v->t_ref, v->asini_i+x[1]*v->asini_s, 
    v->ecc_i, v->omega_deg_i, v->t0_i+x[2]*v->t0_s, v->porb_i+x[3]*v->porb_s, 
    v->pporb_i );
    if (v->out.is_open() )
        v->out << scientific << setw(19) << setprecision(12) << retval << " " << x[0] << " " << x[1] << " " << x[2] << " " << x[3] << "\n";
    cout << scientific << setw(19) << setprecision(12)<< retval << " " << x[0] << " " << x[1] << " " << x[2] << " " << x[3] << "\n";
    //return -log( retval);
    return 1/retval;
}

double map_r_to_01(double x)
{
    double val= atan(x)/M_PI + 0.5;
    if (val <1e-7)
        val=0;
    return val;
}

double inv_map_r_to_01(double x)
{
    double xx=x;
    if(xx <= 0)
        xx=1e-7;
    if(xx>=1)
        xx=1-1e-7;
    return tan((xx - 0.5)*M_PI);
}


double wrapper_mp_z2_gen(VecDoub_I & x, void *vars)
{
    struct vars_struct *v = (struct vars_struct *) vars;
    
    //cout << "Wrapper called\n";
    
//    cout << " ** \n";
//    cout << (*v->times)[0] << endl;
//    cout << v->n_harm << endl;
//    cout << v->t_ref << endl;
//    cout << v->ecc_i << endl;
//    cout << v->omega_deg_i << endl;
//    cout << " ** \n";
    double ecc=map_r_to_01(x[4]);
//    if (ecc < 0)
//        x[4]=-v->ecc_i/v->ecc_s;
    double retval = get_z2_single_freq(v->times, v->n_harm, v->nu_i+x[0]*v->nu_s, v->t_ref, v->asini_i+x[1]*v->asini_s, 
    ecc, v->omega_deg_i + x[5] * v->omega_deg_s, v->t0_i+x[2]*v->t0_s, v->porb_i+x[3]*v->porb_s, 
    v->pporb_i );
    if (v->out.is_open() )
        v->out << scientific << setw(19) << setprecision(12) << retval << " " << x[0] << " " << x[1] << " " 
                << x[2] << " " << x[3] << " " << x[4] << " " << x[5] << "\n";
    cout << scientific << setw(19) << setprecision(12)<< retval << " " << x[0] << " " << x[1] << " " 
            << x[2] << " " << x[3] << " " << x[4] << " " << x[5] << "\n";
    //return -log( retval);
    return 1/retval;
}


double get_z2_single_freq(vector <double> *times, int n_harm, double frequency, double t_ref, 
        double asini_i, double ecc_i, double omega_deg_i, double t0_i, double porb_i, double pporb_i)
{
    //Gets the Z2 statistics for an array of times using the binary solution
    //It is used to obtain a binary solution
    //No noise reduction implemented
    int n_events=times->size();
    //cout << "about to set binary cor par\n";
    double ecc_plus=ecc_i;
    if (ecc_plus<0)
        ecc_plus=0;
    set_binarrycor_par( asini_i,  ecc_plus,  omega_deg_i,  t0_i,  porb_i, pporb_i);
    
    double lz2=0;
    for(int j=1;j<=n_harm;j++)
    {
        double tmp1=0, tmp2=0;
        for(int i=0;i<n_events;i++)
        {
            double phase=(binarycor((*times)[i])-t_ref)*frequency;
            phase=phase-floor(phase);

            tmp1+=cos(2*M_PI*j*phase);
            tmp2+=sin(2*M_PI*j*phase);
            //cout << phase  << " " << tmp1 << " " << tmp2 << " " << lz2[i_nu] << endl;
        }
        lz2+=tmp1*tmp1+tmp2*tmp2;
    }
    //cout << "Z2 = " <<  lz2 << endl;
    return lz2/n_events*2;
                
}

int get_orbital_solution(vector<string> list, char evt_flag, double e_min, double e_max, 
        double &nu_start, double &nu_start_e, 
        int n_harm, int flag_out, const char *filename, double mjdref, double t_ref,
        double &best_freq, double &err_best_freq, double &max_z2, 
        double ecc, double &asin_i, double &porb, double &t0, double &omega_deg, double &pporb, 
        double ecc_e, double &asin_i_e, double &porb_e, double &t0_e, double &omega_deg_e, double &pporb_e, 
        long int n_events_per_chunk,
        double min_sn, 
        double max_time_separation, double min_significance, char do_simul, int nruns,
        double max_time_separation_simul, char flag_out_simul, double tolerance)
{

//	char do_simul=1;
//	int nruns=100;
//	int status=0;
    int n_files=list.size();
    char do_simul_lc='n';
    int nruns_lc=0;

    if(do_simul == 'y' && evt_flag == 2)
    {
            do_simul_lc=do_simul;
            nruns_lc =nruns;
    }

    binarycor_set_mjdref(mjdref);
    vector<double> times;
    double n_events=0;
    int stopped_event=nruns_lc; //This does not affect the events, but only the lc
    int counter=0;
    vector <double> tstops;
    vector <double> tstarts;
    vector <char> stop_flag;
    if (evt_flag == 1) {
        for (int k = 0; k < n_files; k++) { //Scan files
            cout << "Scan files\n";
            event_file ef;
            ef.open_fits(list[k].c_str());
            tstops.push_back(ef.t_stop());
            tstarts.push_back(ef.t_start());
            ef.close_fits();
        }
        for (int k = 0; k < n_files-1; k++) { //Scan files
            if(tstarts[k+1] - tstops[k] >= max_time_separation)
                stop_flag.push_back(1);
            else
                stop_flag.push_back(0);
        }
        stop_flag.push_back(1);
        
        cout << "**************************************************\n";
        for (int k = 0; k < n_files; k++) {
            cout << list[k] << " " << tstarts[k] << " " << tstops[k] << " " << (int)stop_flag[k] << endl;
        }

        cout << "**************************************************\n";

     } //If on event flag


    for(int k=0;k<n_files;k++)
    {
            stopped_event=nruns_lc; //This does not affect the events, but only the lc
            event_file ef;
            if(evt_flag==1)
            {
                    ef.open_fits(list[k].c_str());
                    ef.output();
            }
            while(stopped_event>=0)
            {
                    if(evt_flag==1)
                    {
                            //times.clear();
                            stopped_event=ef.get_times( times, 'n', e_min, e_max, n_events_per_chunk,
                                    stopped_event, 'n', 0, 0, NULL,max_time_separation); //,int_bary, ra_obj, dec_obj,og_name,max_time_separation);
                            cout << "Read events stooped at #" << stopped_event << endl;
                            if(stopped_event >= ef._size)
                                stopped_event=-1;

                    }
                    else
                    {
                            light_curve ll(list[k].c_str());
                            if(stopped_event == nruns_lc)
                            {
                                    ll.get_times(times, min_sn);
                                    ll.output();
                            }
                            else
                            {
                                    cout << " Simulation run " << nruns_lc - stopped_event << endl;
                                    light_curve ls=simul_gaus(ll);
                                    ls.get_times(times,min_sn);
                            }
                            stopped_event--;
                    }
                    //continue;

                    int n=times.size();
                    //cout << "n " << n << endl;


                    n_events+=times.size();
                    if(n >= n_events_per_chunk)// || stop_flag[k] > 0 )
                    {
                            //call the Z2 routine
                            counter++;
                            cout << "File " << list[k].c_str() << " about to elaborate " << n << " events. Counter " << counter+1 << endl;
                            struct vars_struct vars;
                            //ofstream out_f(filename);
                            vars.times=&times;
                            vars.nu_i=nu_start;
                            vars.porb_i=porb;
                            vars.t0_i=t0;
                            vars.asini_i=asin_i;
                            vars.nu_s=nu_start_e;
                            vars.porb_s=porb_e;
                            vars.t0_s=t0_e;
                            vars.asini_s=asin_i_e;
                            vars.ecc_i=ecc;
                            vars.ecc_s=ecc_e;
                            vars.n_harm=n_harm;
                            vars.omega_deg_i=omega_deg;
                            vars.omega_deg_s=omega_deg_e;
                            vars.pporb_i=pporb_e;
                            vars.t_ref=t_ref;
                            char aux_fname[FLEN_FILENAME];
                            sprintf(aux_fname, "%s_%04d.dat", filename, counter);
                            vars.out.open(aux_fname);

                            VecDoub initial_par(6);
                            initial_par[0] = 0;//1e-5;
                            initial_par[1] = 0;//1e-4;
                            initial_par[2] = 0;//10;
                            initial_par[3] = 0;//1;
                            initial_par[4] = inv_map_r_to_01(ecc);//10;
                            initial_par[5] = 0;//1;

                            cout << "Initial parameters \n";
                            cout << "*********************************\n";
                            cout << "nu_start = " << scientific << setw(19) << setprecision(12) << nu_start << endl;
                            cout << "asin i = " << scientific << setw(19) << setprecision(12) << asin_i << endl;
                            cout << "t0 = " << scientific << setw(19) << setprecision(12) << t0 << endl;
                            cout << "porb = " << scientific << setw(19) << setprecision(12) << porb << endl;
                            cout << "ecc = " << scientific << setw(19) << setprecision(12) << ecc << endl;
                            cout << "omega = " << scientific << setw(19) << setprecision(12) << omega_deg << endl;
                            cout << "*********************************\n";
                            cout << "*********************************\n";

                            vars.out << "Initial parameters \n";
                            vars.out << "*********************************\n";
                            vars.out << "nu_start = " << scientific << setw(19) << setprecision(12) << nu_start << endl;
                            vars.out << "asin i = " << scientific << setw(19) << setprecision(12) << asin_i << endl;
                            vars.out << "t0 = " << scientific << setw(19) << setprecision(12) << t0 << endl;
                            vars.out << "porb = " << scientific << setw(19) << setprecision(12) << porb << endl;
                            vars.out << "ecc = " << scientific << setw(19) << setprecision(12) << ecc << endl;
                            vars.out << "omega = " << scientific << setw(19) << setprecision(12) << omega_deg << endl;
                            vars.out << "*********************************\n";
                            vars.out << "*********************************\n";

                            //Powell<Doub (VecDoub_I &, void *)> powell(wrapper_mp_z2_circ,1e-6, (void *)&vars);

                            Powell<Doub (VecDoub_I &, void *)> powell(wrapper_mp_z2_gen,1e-6, (void *)&vars);

                            VecDoub final_par = powell.minimize(initial_par);
                            max_z2=wrapper_mp_z2_circ(final_par, (void *)&vars);
                            nu_start=best_freq=vars.nu_i+final_par[0]*vars.nu_s;
                            err_best_freq=0;
                            asin_i=vars.asini_i+final_par[1]*vars.asini_s;
                            t0=vars.t0_i+final_par[2]* vars.t0_s;
                            porb=vars.porb_i+final_par[3]* vars.porb_s;

                            ecc =  map_r_to_01(final_par[4]);// vars.ecc_i + final_par[4] * vars.ecc_s;
                            omega_deg = vars.omega_deg_i + final_par[5] * vars.omega_deg_s;

                            cout << "Final parameters \n";
                            cout << "*********************************\n";
                            cout << "nu_start = " << scientific << setw(19) << setprecision(12) << nu_start << endl;
                            cout << "asin i = " << scientific << setw(19) << setprecision(12) << asin_i << endl;
                            cout << "t0 = " << scientific << setw(19) << setprecision(12) << t0  << endl;
                            cout << "porb = " << scientific << setw(19) << setprecision(12) << porb  << endl;
                            cout << "ecc = " << scientific << setw(19) << setprecision(12) << ecc << endl;
                            cout << "omega = " << scientific << setw(19) << setprecision(12) << omega_deg << endl;
                            cout << "*********************************\n";
                            cout << "*********************************\n";
                            vars.out << "Final parameters \n";
                            vars.out << "*********************************\n";
                            vars.out << "nu_start = " << scientific << setw(19) << setprecision(12) << nu_start << endl;
                            vars.out << "asin i = " << scientific << setw(19) << setprecision(12) << asin_i << endl;
                            vars.out << "t0 = " << scientific << setw(19) << setprecision(12) << t0 << endl;
                            vars.out << "porb = " << scientific << setw(19) << setprecision(12) << porb << endl;
                            vars.out << "ecc = " << scientific << setw(19) << setprecision(12) << ecc << endl;
                            vars.out << "omega = " << scientific << setw(19) << setprecision(12) << omega_deg << endl;
                            vars.out << "*********************************\n";
                            vars.out << "*********************************\n";
                            vars.out.close();

                            //aux_fname[FLEN_FILENAME];
                            sprintf(aux_fname, "binary_fit_%s_%04d.dat", filename, counter);
                            ofstream ff(aux_fname);
                            ff << scientific << setw(19) << setprecision(12) <<  asin_i << endl;
                            ff << ecc << endl;
                            ff << omega_deg << endl;
                            ff << t0 << endl;
                            ff  << porb << endl;
                            ff  << pporb << endl;
                            ff.close();
                            cout << "Written binary parameters in " << aux_fname << endl;
                            //int status=get_z2_single_freq( times, nu_start, n_bins, n_harm, flag_out, filename, t_ref, best_freq, err_best_freq,max_z2, counter, min_significance );
                            times.clear();
                            n_events=0;
                    }

            }//end on loop on chunks
            if(evt_flag==1)
            {
                    ef.close_fits();
            }

	}//end of loop on files
	
    times.clear();
    return 0;
}


int get_energy_bounds_rmf(char *rmf_name, vector <double> &e_min, vector <double> &e_max) {
    fitsfile *fptr;
    int status = 0, hdutype;
    char keyword[FLEN_KEYWORD];
    char extname[FLEN_VALUE];
    sprintf(keyword, "EXTNAME");

    int curr_hdu = 3;
    if (fits_open_file(&(fptr), rmf_name, READONLY, &status)) {

        fits_report_error(stderr, status);
        fprintf(stderr, "ERROR: cannot open rmf file\n");

        return status;
    }

    // by default moves to the 3rd extension

    fits_movabs_hdu(fptr, 3, &hdutype, &status);
    fits_read_key(fptr, TSTRING, keyword, extname, NULL, &status);
    // in case of ISDC rmf moves to the 4th
    if (strcmp(extname, "ISGR-RMF.-RSP") == 0 || strcmp(extname, "JMX1-RMF.-RSP") == 0 || strcmp(extname, "JMX2-RMF.-RSP") == 0) {
        curr_hdu = 4;
        fits_movabs_hdu(fptr, 4, &hdutype, &status);
        fits_read_key(fptr, TSTRING, keyword, extname, NULL, &status);
    }

    printf("EBOUNDS extension %s\n", extname);



    long nrows = 0;
    fits_get_num_rows(fptr, &nrows, &status);

    if (status) {
        cerr << "Error in reading the number of rows in response\n";
        return status;
    }
    cout << "There will be " << nrows << " energy channels\n";


    double *emin = new double[nrows];
    double *emax = new double[nrows];

    //cout << "nchannels is " << (int)(e_min->size) << endl;

    double nulldbl = 1e10;
    int anynul = 0;
    if (fits_read_col_dbl(fptr, 2, 1, 1, nrows, nulldbl,
            emin, &anynul, &status))
        fits_report_error(stderr, status);

    if (fits_read_col_dbl(fptr, 3, 1, 1, nrows, nulldbl,
            emax, &anynul, &status))
        fits_report_error(stderr, status);


    if (fits_close_file(fptr, &status)) {
        fits_report_error(stderr, status);
    }

    for (int i = 0; i < nrows; i++) {
        e_min.push_back(emin[i]);
        e_max.push_back(emax[i]);

    }

    delete [] emin;
    delete [] emax;

    return status;
}

/*****************************************************************************/
/*  Function :     DAL3HKmergeGTI                                            */
/*                                                                           */
/*  Description:   Merge the gti1 and the gti2 to one GTI. It is and AND     */
/*                 operation: There is a good time in mergedGti if both      */
/*                 GTIs (gti1 and gti2) are good at the given time.          */
/*                                                                           */
/*  Return code:   Error Code. If input parameter status != ISDC_OK,         */
/*                 status is returned.                                       */
/*                                                                           */
/*  Parameter arguments:                                                     */
/*                                                                           */
/*  name        I/O     valid range      description                         */
/*                                                                           */
/*  numIntervals1                                                            */
/*              input   >= 0             number of intervals defined in      */
/*                                       gtiStart1, gtiEnd1                  */
/*  gtiStart1   input   lenght ==        start times of first GTI            */
/*                      numIntervals1                                        */
/*  gtiEnd1     input   lenght ==        end times of first GTI              */
/*                      numIntervals1                                        */
/*                                                                           */
/*  numIntervals2                                                            */
/*              input   >= 0             number of intervals defined in      */
/*                                       gtiStart2, gtiEnd2                  */
/*  gtiStart2   input   lenght ==        start times of second GTI           */
/*                      numIntervals2                                        */
/*  gtiEnd2     input   lenght ==        end times of second GTI             */
/*                      numIntervals2                                        */
/*                                                                           */
/*  mumMergedIntervals                                                       */
/*              output                   number of written intervals into    */
/*                                       mergedGtiStart and mergedGtiEnd     */
/*  mergedGtiStart                                                           */
/*              output  length >=        the start times of the merged GTI   */
/*                      numIntervals1 +                                      */
/*                      numIntervals2 -1                                     */
/*  mergedGtiEnd                                                             */
/*              output  length >=        the end times of the merged GTI     */
/*                      numIntervals1 +                                      */
/*                      numIntervals2 -1                                     */
/*                                                                           */
/*  status      input   any              input error code                    */
/*                                                                           */
/*****************************************************************************/


#define ISDC_OK 0

int mergeGTI(int numIntervals1, double * gtiStart1, double * gtiEnd1, 
	                int numIntervals2, double * gtiStart2, double * gtiEnd2, 
	                int * mumMergedIntervals, 
			double * mergedGtiStart, double * mergedGtiEnd,
 	                int status)
{
	int	loop1=0;	/* interval loop counter for gti1  */
	int	loop2=0;	/* interval loop counter for gti2  */

	*mumMergedIntervals = 0;	

	if (status != ISDC_OK)	return status;


	/* do nothing if one of the input GTIs is empty                           */
	if (numIntervals1 == 0 || numIntervals2 == 0)
		return status;

	while (loop1 < numIntervals1)
		{
		/* looking for the next Interal in gti2 that has at least some time    */
		/* common with actual Interal of gti1 .                                */
		while (gtiEnd2[loop2] < gtiStart1[loop1])
			{
			loop2++;
			if (loop2 == numIntervals2)
				/* we found the end of gti2                                      */
				return status;
			}

		if (gtiStart2[loop2] > gtiEnd1[loop1] )
			/* the current interval of gti2 has no common time interval with    */
			/* gti1. Continue with the next interval of gti1                    */
			{
			loop1++;
			continue;
			}

		/* we found a new interval                                             */
		/* get the start time and the end time                                 */
		mergedGtiStart[*mumMergedIntervals] =
			(gtiStart1[loop1] > gtiStart2[loop2]) ? 
			                                 gtiStart1[loop1] : gtiStart2[loop2];	

		mergedGtiEnd[*mumMergedIntervals] =
			(gtiEnd1[loop1] < gtiEnd2[loop2]) ? gtiEnd1[loop1] : gtiEnd2[loop2];	
		(*mumMergedIntervals)++;

		/* we may have to add new merged intervals in the time range of the    */
		/* actual gti1                                                         */
		while (gtiEnd2[loop2] < gtiEnd1[loop1])
			{
			loop2++;
			if (loop2 == numIntervals2)
				/* we found the end of gti2                                      */
				return status;
			
			if (gtiStart2[loop2] <= gtiEnd1[loop1] )
				{
				mergedGtiStart[*mumMergedIntervals] = gtiStart2[loop2];
				mergedGtiEnd[*mumMergedIntervals] =
						(gtiEnd1[loop1] < gtiEnd2[loop2]) ? 
						                          gtiEnd1[loop1] : gtiEnd2[loop2];	
				(*mumMergedIntervals)++;
				}
			}
		
		loop1++;
		}


	return status;
}


void loc_piksr2(VecDoub &arr, VecDoub &brr)
{
	Int i,j,n=arr.size();
	double a;
	double b;
	for (j=1;j<n;j++) {
		a=arr[j];
		b=brr[j];
		i=j;
		while (i > 0 && arr[i-1] > a) {
			arr[i]=arr[i-1];
			brr[i]=brr[i-1];
			i--;
		}
		arr[i]=a;
		brr[i]=b;
	}
}
