
#ifndef ASK_H
#define ASK_H 1
//
// last update: January 2002: modified the askInfo function to avoid memory leakage when
//                            input string was too long.
//


#define askInfo(a,b) _askInfo(a,b,sizeof(b))
#define ask_Directory(a,b) _askDirectory(a,b,sizeof(b))

#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#include <cstdio>
#include <cstring>
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#include <stdio.h>
#include <string.h>
#endif

__BEGIN_DECLS


void            _askInfo(char *Query, char *Answ,int MaxLen);

void _ask_Directory(char * Query,char * DirName,int MaxLen);

int             ask_float(char *Query, float *real);
int             ask_n_float(int n_float, char *Query, float *real);
int             ask_integer(char *Query, int *integer);
int             ask_long_integer(char *Query, long int *integer);
char            ask_yes_no(char *Query, char def);
void			waitkey(void);
int             ask_double(char *Query, double *Dvalue);
int             ask_short_integer(char *Query, short int *Value);

int 	ask_menu(int n_options,char * menu[],int def,char * Query);

int     ReadString(FILE * fp, char *string);

__END_DECLS

#endif
