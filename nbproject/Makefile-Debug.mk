#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-MacOSX
CND_DLIB_EXT=dylib
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/NR/erf.o \
	${OBJECTDIR}/NR/fitexy.o \
	${OBJECTDIR}/NR/fourier.o \
	${OBJECTDIR}/NR/gamma.o \
	${OBJECTDIR}/NR/incgammabeta.o \
	${OBJECTDIR}/NR/moment.o \
	${OBJECTDIR}/NR/sort.o \
	${OBJECTDIR}/NR/spectrum.o \
	${OBJECTDIR}/NR/stattests.o \
	${OBJECTDIR}/src/ask.o \
	${OBJECTDIR}/src/binarycor.o \
	${OBJECTDIR}/src/event_files.o \
	${OBJECTDIR}/src/kuiper.o \
	${OBJECTDIR}/src/light_curve.o \
	${OBJECTDIR}/src/main.o \
	${OBJECTDIR}/src/phase.o \
	${OBJECTDIR}/src/tools.o


# C Compiler Flags
CFLAGS=-iquote${HEADAS}/include

# CC Compiler Flags
CCFLAGS=-iquote${HEADAS}/include -std=c++0x
CXXFLAGS=-iquote${HEADAS}/include -std=c++0x

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-Lcmpfit -L${HEADAS}/lib -L/opt/local/lib -lreadline -lgsl -lgslcblas -lcfitsio -lmpfit

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/timingsuite

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/timingsuite: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/timingsuite ${OBJECTFILES} ${LDLIBSOPTIONS} -g

${OBJECTDIR}/NR/erf.o: nbproject/Makefile-${CND_CONF}.mk NR/erf.cpp 
	${MKDIR} -p ${OBJECTDIR}/NR
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NR/erf.o NR/erf.cpp

${OBJECTDIR}/NR/fitexy.o: nbproject/Makefile-${CND_CONF}.mk NR/fitexy.cpp 
	${MKDIR} -p ${OBJECTDIR}/NR
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NR/fitexy.o NR/fitexy.cpp

${OBJECTDIR}/NR/fourier.o: nbproject/Makefile-${CND_CONF}.mk NR/fourier.cpp 
	${MKDIR} -p ${OBJECTDIR}/NR
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NR/fourier.o NR/fourier.cpp

${OBJECTDIR}/NR/gamma.o: nbproject/Makefile-${CND_CONF}.mk NR/gamma.cpp 
	${MKDIR} -p ${OBJECTDIR}/NR
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NR/gamma.o NR/gamma.cpp

${OBJECTDIR}/NR/incgammabeta.o: nbproject/Makefile-${CND_CONF}.mk NR/incgammabeta.cpp 
	${MKDIR} -p ${OBJECTDIR}/NR
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NR/incgammabeta.o NR/incgammabeta.cpp

${OBJECTDIR}/NR/moment.o: nbproject/Makefile-${CND_CONF}.mk NR/moment.cpp 
	${MKDIR} -p ${OBJECTDIR}/NR
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NR/moment.o NR/moment.cpp

${OBJECTDIR}/NR/sort.o: nbproject/Makefile-${CND_CONF}.mk NR/sort.cpp 
	${MKDIR} -p ${OBJECTDIR}/NR
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NR/sort.o NR/sort.cpp

${OBJECTDIR}/NR/spectrum.o: nbproject/Makefile-${CND_CONF}.mk NR/spectrum.cpp 
	${MKDIR} -p ${OBJECTDIR}/NR
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NR/spectrum.o NR/spectrum.cpp

${OBJECTDIR}/NR/stattests.o: nbproject/Makefile-${CND_CONF}.mk NR/stattests.cpp 
	${MKDIR} -p ${OBJECTDIR}/NR
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NR/stattests.o NR/stattests.cpp

${OBJECTDIR}/src/ask.o: nbproject/Makefile-${CND_CONF}.mk src/ask.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -INR -Isrc -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ask.o src/ask.c

${OBJECTDIR}/src/binarycor.o: nbproject/Makefile-${CND_CONF}.mk src/binarycor.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -g -INR -Isrc -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/binarycor.o src/binarycor.c

${OBJECTDIR}/src/event_files.o: nbproject/Makefile-${CND_CONF}.mk src/event_files.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/event_files.o src/event_files.cpp

${OBJECTDIR}/src/kuiper.o: nbproject/Makefile-${CND_CONF}.mk src/kuiper.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/kuiper.o src/kuiper.cpp

${OBJECTDIR}/src/light_curve.o: nbproject/Makefile-${CND_CONF}.mk src/light_curve.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/light_curve.o src/light_curve.cpp

${OBJECTDIR}/src/main.o: nbproject/Makefile-${CND_CONF}.mk src/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/main.o src/main.cpp

${OBJECTDIR}/src/phase.o: nbproject/Makefile-${CND_CONF}.mk src/phase.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/phase.o src/phase.cpp

${OBJECTDIR}/src/tools.o: nbproject/Makefile-${CND_CONF}.mk src/tools.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -INR -Isrc -Icmpfit -I/opt/local/include -I${HEADAS}/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/tools.o src/tools.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/timingsuite

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
