# Timingsuite

A C++ project with several timing routines
It needs heasoft installed and initialized to compile

A simple make should compile it on Linux

Since it was migrated from an old project on Mac, the executable will be placed in
dist/Debug/GNU-MacOSX/timingsuite

Just execute it and you will have an interative menu from the command line.

Commands can be given in batch mode with STDIN redirection
