#ifndef _SPECTRUM_H
#define _SPECTRUM_H

#include "nr3.h"
#include "fourier.h"

struct Spectreg {
	Int m,m2,nsum;
	VecDoub specsum, wksp;

	Spectreg(Int em) : m(em), m2(2*m), nsum(0), specsum(m+1,0.), wksp(m2) {
		if (m & (m-1)) throw("m must be power of 2");
	}

	template<class D>
	void adddataseg(VecDoub_I &data, D &window) {
		Int i;
		Doub w,fac,sumw = 0.;
		if (data.size() != m2) throw("wrong size data segment");
		for (i=0;i<m2;i++) {
			w = window(i,m2);
			wksp[i] = w*data[i];
			sumw += SQR(w);
		}
		fac = 2./(sumw*m2);
		realft(wksp,1);
		specsum[0] += 0.5*fac*SQR(wksp[0]);
		for (i=1;i<m;i++) specsum[i] += fac*(SQR(wksp[2*i])+SQR(wksp[2*i+1]));
		specsum[m] += 0.5*fac*SQR(wksp[1]);
		nsum++;
	}

	VecDoub spectrum() {
		VecDoub spec(m+1);
		if (nsum == 0) throw("no data yet");
		for (Int i=0;i<=m;i++) spec[i] = specsum[i]/nsum;
		return spec;
	}

	VecDoub frequencies() {
		VecDoub freq(m+1);
		for (Int i=0;i<=m;i++) freq[i] = i*0.5/m;
		return freq;
	}
};

Doub square(Int j,Int n);
Doub bartlett(Int j,Int n);
Doub welch(Int j,Int n);

struct Hann {
	Int nn;
	VecDoub win;
	Hann(Int n) : nn(n), win(n) {
		Doub twopi = 8.*atan(1.);
		for (Int i=0;i<nn;i++) win[i] = 0.5*(1.-cos(i*twopi/(nn-1.)));
	}
	Doub operator() (Int j, Int n) {
		if (n != nn) throw("incorrect n for this Hann");
		return win[j];
	}
};

struct Spectolap : Spectreg {
	Int first;
	VecDoub fullseg;

	Spectolap(Int em) : Spectreg(em), first(1), fullseg(2*em) {}

	template<class D>
	void adddataseg(VecDoub_I &data, D &window) {
		Int i;
		if (data.size() != m) throw("wrong size data segment");
		if (first) {
			for (i=0;i<m;i++) fullseg[i+m] = data [i];
			first = 0;
		} else {
			for (i=0;i<m;i++) {
				fullseg[i] = fullseg[i+m];
				fullseg[i+m] = data [i];
			}
			Spectreg::adddataseg(fullseg,window);
		}
	}

	template<class D>
	void addlongdata(VecDoub_I &data, D &window) {
		Int i, k, noff, nt=data.size(), nk=(nt-1)/m;
		Doub del = nk > 1 ? (nt-m2)/(nk-1.) : 0.;
		if (nt < m2) throw("data length too short");
		for (k=0;k<nk;k++) {
			noff = (Int)(k*del+0.5);
			for (i=0;i<m2;i++) fullseg[i] = data[noff+i];
			Spectreg::adddataseg(fullseg,window);
		}
	}
};

struct Slepian  : Spectreg {
	Int jres, kt;
	MatDoub dpss;
	Doub p,pp,d,dd;
	Slepian(Int em, Int jjres, Int kkt)
	: Spectreg(em), jres(jjres), kt(kkt), dpss(kkt,2*em) {
		if (jres < 1 || kt >= 2*jres) throw("kt too big or jres too small");
		filltable();
	}
	void filltable();
	void renorm(Int n) {
		p = ldexp(p,n); pp = ldexp(pp,n); d = ldexp(d,n); dd = ldexp(dd,n);
	}
	struct Slepwindow {
		Int k;
		MatDoub &dps;
		Slepwindow(Int kkt, MatDoub &dpss) : k(kkt), dps(dpss) {}
		Doub operator() (Int j, Int n) {return dps[k][j];}
	};

	void adddataseg(VecDoub_I &data) {
		Int k;
		if (data.size() != m2) throw("wrong size data segment");
		for (k=0;k<kt;k++) {
			Slepwindow window(k,dpss);
			Spectreg::adddataseg(data,window);
		}
	}
	
	
};

 

#endif
