#ifndef _STATTEST_H
#define _STATTEST_H 1

#include "erf.h"
#include "sort.h"
#include "nr3.h"

void ttest(VecDoub_I &data1, VecDoub_I &data2, Doub &t, Doub &prob);
void tutest(VecDoub_I &data1, VecDoub_I &data2, Doub &t, Doub &prob);
void tptest(VecDoub_I &data1, VecDoub_I &data2, Doub &t, Doub &prob);
void ftest(VecDoub_I &data1, VecDoub_I &data2, Doub &f, Doub &prob);
void chsone(VecDoub_I &bins, VecDoub_I &ebins, Doub &df,Doub &chsq, Doub &prob, const Int knstrn);
void chstwo(VecDoub_I &bins1, VecDoub_I &bins2, Doub &df,Doub &chsq, Doub &prob, const Int knstrn);
void cntab(MatInt_I &nn, Doub &chisq, Doub &df, Doub &prob, Doub &cramrv,Doub &ccc);
void pearsn(VecDoub_I &x, VecDoub_I &y, Doub &r, Doub &prob, Doub &z);
void crank(VecDoub_IO &w, Doub &s);
//void spear(VecDoub_I &data1, VecDoub_I &data2, Doub &d, Doub &zd, Doub &probd,Doub &rs, Doub &probrs); // commented out for linking problems
void kendl1(VecDoub_I &data1, VecDoub_I &data2, Doub &tau, Doub &z, Doub &prob);
void kendl2(MatDoub_I &tab, Doub &tau, Doub &z, Doub &prob);

#endif
