#ifndef _MOMENT_H
#define _MOMENT_H

#include "nr3.h"


void moment(VecDoub_I &data, Doub &ave, Doub &adev, Doub &sdev, Doub &var,
			Doub &skew, Doub &curt);
void avevar(VecDoub_I &data, Doub &ave, Doub &var);
void avevar_excl(VecDoub_I &data, Doub &ave, Doub &var, Doub level);
void wavevar(VecDoub_I &y, VecDoub_I &dy, Doub &av, Doub &var);
void wavevar_excl(VecDoub_I &y, VecDoub_I &dy, Doub &av, Doub &var, Doub level);
void test_const(VecDoub_I &y, VecDoub_I &dy, Doub &chi, Doub &prob, Doub &sigma);



#endif
