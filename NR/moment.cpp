/*
 *  moment.cpp
 *  Light_Curve_Analyzer
 *
 *  Created by Carlo Ferrigno on 10/11/09.
 *  Copyright 2009 ISDC/IAAT. All rights reserved.
 *
 */

#include "moment.h"
#include "gamma.h" //Put this first
#include "incgammabeta.h"
#include "erf.h"

void moment(VecDoub_I &data, Doub &ave, Doub &adev, Doub &sdev, Doub &var,
			Doub &skew, Doub &curt) {
	Int j,n=data.size();
	Doub ep=0.0,s,p;
	if (n <= 1) throw("n must be at least 2 in moment");
	s=0.0;
	for (j=0;j<n;j++) s += data[j];
	ave=s/n;
	adev=var=skew=curt=0.0;
	for (j=0;j<n;j++) {
		adev += abs(s=data[j]-ave);
		ep += s;
		var += (p=s*s);
		skew += (p *= s);
		curt += (p *= s);
	}
	adev /= n;
	var=(var-ep*ep/n)/(n-1);
	sdev=sqrt(var);
	if (var != 0.0) {
		skew /= (n*var*sdev);
		curt=curt/(n*var*var)-3.0;
	} else throw("No skew/kurtosis when variance = 0 (in moment)");
}

void avevar(VecDoub_I &data, Doub &ave, Doub &var) {
	Doub s,ep;
	Int j,n=data.size();
	ave=0.0;
	for (j=0;j<n;j++) ave += data[j];
	ave /= n;
	var=ep=0.0;
	for (j=0;j<n;j++) {
		s=data[j]-ave;
		ep += s;
		var += s*s;
	}
	var=(var-ep*ep/n)/(n-1);
}
void avevar_excl(VecDoub_I &data, Doub &ave, Doub &var, Doub level)
{
	Doub s,ep;
	int j;
	
	int n=data.size();
	ave=0.0;
	Doub lave=ave;
	ave=0;
	Doub lvar=level*sqrt(var);
	var=0;
	for (j=0;j<n;j++)
	{
		if( fabs(data[j]-lave) > lvar)
			continue;
		ave += data[j];
	}
	ave /= n;
	var=ep=0.0;
	for (j=0;j<n;j++) {
		if( fabs(data[j]-lave) > lvar)
			continue;
		s=data[j]-ave;
		ep += s;
		var += s*s;
	}
	var=(var-ep*ep/n)/(n-1);
}

/*
 test constant hypothesis using chi2
 */

void test_const(VecDoub_I &y, VecDoub_I &dy, Doub &chi, Doub &prob, Doub &sig)
{
	Doub av=0, var=0;
	wavevar(y, dy, av, var);
	
	int n=y.size();
	if(n<=1)
	{
		cerr << "Error o dimension in test_cons\n";
		return;
	}
	
	chi=0;
	for(int j=0;j<n;j++)
	{
		if(dy[j] == 0)
			continue;
		double tmp=1/dy[j]/dy[j];
		chi += SQR(y[j]-av)*tmp;
	}
	
	Chisqdist chi2(n-1);
	Normaldist gaus;
		
	prob = 1-chi2.cdf(chi);
	if(prob <=0 || prob>=1)
	{
		cerr << "Wrong probability " << prob << endl;
		prob=1e-100;
	//	waitkey();
	}
	sig=fabs((gaus.invcdf(prob)));

}


/*
 computes weighted average and variances
 */

void wavevar(VecDoub_I &y, VecDoub_I &dy, Doub &av, Doub &var)
{
	
	int n=y.size();
	double ww=0, V2=0;
	for(int j=0;j<n;j++)
	{
		if(dy[j] == 0)
			continue;
		double tmp=1/dy[j]/dy[j];
		av += y[j]*tmp;
		ww += tmp;
		
	}
	
	av/=ww;
	
	for(int j=0;j<n;j++)
	{
		if(dy[j] == 0)
			continue;
		double tmp=1/dy[j]/dy[j];
		var += (y[j]-av)*(y[j]-av)*tmp;
		V2 += tmp*tmp;
		
	}
	V2/=ww*ww;
	if(V2<1) //Just a check
		var/=ww*(1-V2);
	else
		var/=ww;
	
	//		cout << "Cycle " << i << endl;
	cout << "Average  = " << av <<endl;
	cout << "STD = " << sqrt(var) << endl;
	
}

/*
 computes weighted average and variances
 excluding points at more than level * sigma = sqrt(var)
 */

void wavevar_excl(VecDoub_I &y, VecDoub_I &dy, Doub &av, Doub &var, Doub level)
{
	
	int n=y.size();
	double ww=0, V2=0;
	double lav=av, lsigma=level*sqrt(var);
	av=0;
	var=0;
	for(int j=0;j<n;j++)
	{
		if(dy[j] == 0 || fabs(y[j]-lav) > lsigma)
			continue;
		double tmp=1/dy[j]/dy[j];
		av += y[j]*tmp;
		ww += tmp;
		
	}
	
	av/=ww;
	
	for(int j=0;j<n;j++)
	{
		if(dy[j] == 0  || fabs(y[j]-lav) > lsigma)
			continue;
		double tmp=1/dy[j]/dy[j];
		var += (y[j]-av)*(y[j]-av)*tmp;
		V2 += tmp*tmp;
		
	}
	V2/=ww*ww;
	if(V2<1) //Just a check
		var/=ww*(1-V2);
	else
		var/=ww;
	
	//		cout << "Cycle " << i << endl;
	cout << "Average (screened)  = " << av <<endl;
	cout << "Variance (screened) = " << var << endl;
	
}

