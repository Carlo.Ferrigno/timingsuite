#ifndef _SORT_H
#define _SORT_H 1
#include "nr3.h"


template<class T> void sort_nr(NRvector<T> &arr, Int m=-1);
template<class T, class U> void sort2(NRvector<T> &arr, NRvector<U> &brr);

template<class T> void shell(NRvector<T> &a, Int m=-1);

namespace hpsort_util {
	template<class T>	void sift_down(NRvector<T> &ra, const Int l, const Int r);

}

template<class T> void hpsort(NRvector<T> &ra);
template<class T> void piksrt(NRvector<T> &arr);
template<class T, class U> void piksr2(NRvector<T> &arr, NRvector<U> &brr);

struct Indexx {
	Int n;
	VecInt indx;

	template<class T> Indexx(const NRvector<T> &arr) {
		index(&arr[0],arr.size());
	}
	Indexx() {}

	template<class T> void sort(NRvector<T> &brr) {
		if (brr.size() != n) throw("bad size in Index sort");
		NRvector<T> tmp(brr);
		for (Int j=0;j<n;j++) brr[j] = tmp[indx[j]];
	}

	template<class T> inline const T & el(NRvector<T> &brr, Int j) const {
		return brr[indx[j]];
	}
	template<class T> inline T & el(NRvector<T> &brr, Int j) {
		return brr[indx[j]];
	}

	template<class T> void index(const T *arr, Int nn);

	inline void rank(VecInt_O &irank) {
		irank.resize(n);
		for (Int j=0;j<n;j++) irank[indx[j]] = j;
	}

};


template<class T> T select(const Int k, NRvector<T> &arr);

struct Heapselect {
	Int m,n,srtd;
	VecDoub heap;

	Heapselect(Int mm) : m(mm), n(0), srtd(0), heap(mm,1.e99) {}

	inline void add(Doub val) {
		Int j,k;
		if (n<m) {
			heap[n++] = val;
			if (n==m) sort_nr(heap);
		} else {
			if (val > heap[0]) {
				heap[0]=val;
				for (j=0;;) {
					k=(j << 1) + 1;
					if (k > m-1) break;
					if (k != (m-1) && heap[k] > heap[k+1]) k++;
					if (heap[j] <= heap[k]) break;
					SWAP(heap[k],heap[j]);
					j=k;
				}
			}
			n++;
		}
		srtd = 0;
	}

	inline Doub report(Int k) {
		Int mm = MIN(n,m);
		if (k > mm-1) throw("Heapselect k too big");
		if (k == m-1) return heap[0];
		if (! srtd) { sort_nr(heap); srtd = 1; }
		return heap[mm-1-k];
	}
};

#endif
