/* 
 * File:   custom_mins_ndim.h
 * Author: ferrigno
 *
 * Created on April 9, 2013, 5:18 PM
 */

#ifndef CUSTOM_MINS_NDIM_H
#define	CUSTOM_MINS_NDIM_H



template <class T> struct F1dim {
	const VecDoub &p;
	const VecDoub &xi;
	Int n;
	T &func;
	VecDoub xt;
        void *added;
        
	F1dim(VecDoub_I &pp, VecDoub_I &xii, T &funcc, void *added_i) :  p(pp),
		xi(xii), n(pp.size()), func(funcc), xt(n) {added=added_i;}
	Doub operator() (const Doub x)
	{
		for (Int j=0;j<n;j++)
			xt[j]=p[j]+x*xi[j];
		return func(xt,added);
	}
};

template <class T> struct Linemethod {
	VecDoub p;
	VecDoub xi;
	T &func;
	Int n;
        void *added;
	Linemethod(T &funcc, void *added_i) : func(funcc) {added=added_i;}
	Doub linmin()
	{
		Doub ax,xx,xmin;
		n=p.size();
		F1dim<T> f1dim(p,xi,func,added);
                ax=0;
                xx=1;
                //ax=-1e-3;
		//xx=1e-3;
		Brent brent;
		brent.bracket(ax,xx,f1dim);
		xmin=brent.minimize(f1dim);
		for (Int j=0;j<n;j++) {
			xi[j] *= xmin;
			p[j] += xi[j];
		}
		return brent.fmin;
	}
};

template <class T>
struct Powell : Linemethod<T> {
	Int iter;
	Doub fret;
	using Linemethod<T>::func;
	using Linemethod<T>::linmin;
	using Linemethod<T>::p;
	using Linemethod<T>::xi;
        using Linemethod<T>::added;
	const Doub ftol;
	Powell(T &func, const Doub ftoll=3.0e-8, void *added_i=NULL) : Linemethod<T>(func,added_i),
		ftol(ftoll) {}
	VecDoub minimize(VecDoub_I &pp)
	{
		Int n=pp.size();
		MatDoub ximat(n,n,0.0);
		for (Int i=0;i<n;i++) ximat[i][i]=1.0;
		return minimize(pp,added, ximat);
	}
	VecDoub minimize(VecDoub_I &pp, void *added, MatDoub_IO &ximat)
	{
		const Int ITMAX=2000;
		const Doub TINY=1.0e-25;
		Doub fptt;
		Int n=pp.size();
		p=pp;
		VecDoub pt(n),ptt(n);
		xi.resize(n);
		fret=func(p,added);
		for (Int j=0;j<n;j++) pt[j]=p[j];
		for (iter=0;;++iter) {
			Doub fp=fret;
			Int ibig=0;
			Doub del=0.0;
			for (Int i=0;i<n;i++) {
				for (Int j=0;j<n;j++) xi[j]=ximat[j][i];
				fptt=fret;
				fret=linmin();
				if (fptt-fret > del) {
					del=fptt-fret;
					ibig=i+1;
				}
			}
			if (2.0*(fp-fret) <= ftol*(abs(fp)+abs(fret))+TINY) {
				return p;
			}
			if (iter == ITMAX) throw("powell exceeding maximum iterations.");
			for (Int j=0;j<n;j++) {
				ptt[j]=2.0*p[j]-pt[j];
				xi[j]=p[j]-pt[j];
				pt[j]=p[j];
			}
			fptt=func(ptt,added);
			if (fptt < fp) {
				Doub t=2.0*(fp-2.0*fret+fptt)*SQR(fp-fret-del)-del*SQR(fp-fptt);
				if (t < 0.0) {
					fret=linmin();
					for (Int j=0;j<n;j++) {
						ximat[j][ibig-1]=ximat[j][n-1];
						ximat[j][n-1]=xi[j];
					}
				}
			}
		}
	}
};


#endif	/* CUSTOM_MINS_NDIM_H */

