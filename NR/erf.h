#ifndef _ERF_H
#define _ERF_H

#include "nr3.h"

struct Erf {
	static const Int ncof=28;
	static const Doub cof[28];	

	
	inline Doub erf(Doub x) {
		if (x >=0.) return 1.0 - erfccheb(x);
		else return erfccheb(-x) - 1.0;
	}

	inline Doub erfc(Doub x) {
		if (x >= 0.) return erfccheb(x);
		else return 2.0 - erfccheb(-x);
	}
	
	inline Doub inverf(Doub p) {return inverfc(1.-p);}
	Doub erfccheb(Doub z);
	Doub inverfc(Doub p);
		
};


Doub erfcc(const Doub x);

struct Normaldist : Erf {
	Doub mu, sig;
	Normaldist(Doub mmu = 0., Doub ssig = 1.) : mu(mmu), sig(ssig) {
		if (sig <= 0.) throw("bad sig in Normaldist");
	}
	inline Doub p(Doub x) {
		return (0.398942280401432678/sig)*exp(-0.5*SQR((x-mu)/sig));
	}
	inline Doub cdf(Doub x) {
		return 0.5*erfc(-0.707106781186547524*(x-mu)/sig);
	}
	inline Doub invcdf(Doub p) {
		if (p <= 0. || p >= 1.) throw("bad p in Normaldist");
		return -1.41421356237309505*sig*inverfc(2.*p)+mu;
	}
};
struct Lognormaldist : Erf {
	Doub mu, sig;
	Lognormaldist(Doub mmu = 0., Doub ssig = 1.) : mu(mmu), sig(ssig) {
		if (sig <= 0.) throw("bad sig in Lognormaldist");
	}
	inline Doub p(Doub x) {
		if (x < 0.) throw("bad x in Lognormaldist");
		if (x == 0.) return 0.;
		return (0.398942280401432678/(sig*x))*exp(-0.5*SQR((log(x)-mu)/sig));
	}
	inline Doub cdf(Doub x) {
		if (x < 0.) throw("bad x in Lognormaldist");
		if (x == 0.) return 0.;
		return 0.5*erfc(-0.707106781186547524*(log(x)-mu)/sig);
	}
	inline Doub invcdf(Doub p) {
		if (p <= 0. || p >= 1.) throw("bad p in Lognormaldist");
		return exp(-1.41421356237309505*sig*inverfc(2.*p)+mu);
	}
};

#endif
