#ifndef _FITEXY_H
#define _FITEXY_H 1

#include "nr3.h"
#include "mins.h"

struct Fitexy{
	Doub a, b, siga, sigb, chi2, q;
	Int ndat;
	VecDoub xx,yy,sx,sy,ww;
	Doub aa, offs;

	Fitexy(VecDoub_I &x, VecDoub_I &y, VecDoub_I &sigx, VecDoub_I &sigy);

	struct Chixy {
		VecDoub &xx,&yy,&sx,&sy,&ww;
		Doub &aa,&offs;

		Chixy(VecDoub &xxx, VecDoub &yyy, VecDoub &ssx, VecDoub &ssy,
		VecDoub &www, Doub &aaa, Doub &ooffs) : xx(xxx),yy(yyy),sx(ssx),
		sy(ssy),ww(www),aa(aaa),offs(ooffs) {}

		inline Doub operator()(const Doub bang) {
			const Doub BIG=1.0e30;
			Int j,nn=xx.size();
			Doub ans,avex=0.0,avey=0.0,sumw=0.0,b;
			b=tan(bang);
			for (j=0;j<nn;j++) {
				ww[j] = SQR(b*sx[j])+SQR(sy[j]);
				sumw += (ww[j]=(ww[j] < 1.0/BIG ? BIG : 1.0/ww[j]));
				avex += ww[j]*xx[j];
				avey += ww[j]*yy[j];
			}
			avex /= sumw;
			avey /= sumw;
			aa=avey-b*avex;
			for (ans = -offs,j=0;j<nn;j++)
				ans += ww[j]*SQR(yy[j]-aa-b*xx[j]);
			return ans;
		}
	};

};

#endif
