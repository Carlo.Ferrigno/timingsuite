#ifndef _GAMMA_H
#define _GAMMA_H 1

#include "nr3.h"

Doub gammln(const Doub xx);
Doub factrl(const Int n);
Doub factln(const Int n);
Doub bico(const Int n, const Int k);
Doub beta(const Doub z, const Doub w);

#endif
